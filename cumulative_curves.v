(* Copyright : Lucien Rakotomalala, Pierre Roux and Marc Boyer. *)

From HB Require Import structures.
From mathcomp Require Import ssreflect ssrfun ssrbool eqtype order ssrnat.
From mathcomp Require Import choice seq bigop ssralg ssrnum rat.
From mathcomp Require Import boolp classical_sets signed reals ereal.
From mathcomp.dioid Require Import dioid complete_lattice complete_dioid.
Require Import RminStruct.

(******************************************************************************)
(* Data flows for network calculus.                                           *)
(*                                                                            *)
(* We define the main object studied by network calculus.                     *)
(* A data flow cumulative function is a function from R+ to R+,               *)
(* non-decreasing, null at 0 and left continuous. It represents the total     *)
(* amount of data of a flow at some observation point up to observation time. *)
(*                                                                            *)
(* flow_cc == the type for data flows, functions in Fup that:                 *)
(*            * are equal to 0 at 0;                                          *)
(*            * are left-continuous;                                          *)
(*            * only take finite values.                                      *)
(*            flow_cc are equipped with a coercion to Fup                     *)
(*  f%:fcc == the function in R+ -> R+ equal to f : flow_cc                   *)
(* left_cont_closure f == the left continuous closure of f, i.e.              *)
(*                        t |-> sup{ f u | u < t }                            *)
(*                                                                            *)
(* flow_cc iq equipped with an addition flow_cc_plus with neutral element     *)
(* flow_cc_0 and following notations in scope flow_cc_scope (%C):             *)
(*     f + g == pointwise addition of functions f and g                       *)
(*      \sum == notation for sum with bigop                                   *)
(******************************************************************************)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope classical_set_scope.
Local Open Scope ereal_dual_scope.
Local Open Scope ring_scope.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.

Reserved Notation "x %:fcc" (at level 0, format "x %:fcc").

Section CumulativeCurves.

Context {R : realType}.

Definition left_cont_closure (f : @F R) : F :=
  fun t => Order.max 0%dE (ereal_sup [set f u | u in [set u | u < t]]).

Lemma left_cont_closure0 f : left_cont_closure f 0%:nng = 0%dE.
Proof.
apply/max_l/ub_ereal_sup => _ [x /= + _] /ltac:(exfalso).
by apply/negP; rewrite -leNgt -num_le /=.
Qed.

Lemma left_cont_closure_ge0 f t : (0 <= left_cont_closure f t)%E.
Proof. by rewrite /left_cont_closure maxEle; case: (leP _ (ereal_sup _)). Qed.

Lemma left_cont_closure_ereal_sup (f : Fplus) t :
  0 < t%:nngnum ->
  left_cont_closure f t = ereal_sup [set f u | u in [set u | u < t]].
Proof.
move=> Ht; rewrite /left_cont_closure maxEle ifT //.
apply: (@le_trans _ _ (f 0%:nng)) => //.
by apply: ereal_sup_ubound; exists 0%:nng.
Qed.

Lemma left_cont_closure_le_Fup (f : Fup) : (left_cont_closure f <= f :> F)%O.
Proof.
rewrite -[X in (_ <= X)%O]non_decr_closure_Fup.
apply/lefP => t; rewrite ge_max non_decr_closure_Fup; apply/andP; split=> //.
apply: ub_ereal_sup => _ [y /= Hy <-]; exact/ndFP'.
Qed.

Definition left_continuous_at (f : F) x :=
  forall eps : {posnum R}, exists eta : {posnum R},
  forall y, x%:nngnum - eta%:num <= y%:nngnum <= x%:nngnum ->
    (f x - eps%:num%:E < f y <= f x)%dE.

Definition left_continuous f := forall x, left_continuous_at f x.

Lemma left_continuous_atP u x (f : F) b :
  left_continuous_at f x ->
  u%:nngnum < x%:nngnum ->
  (forall y, u < y < x -> (f y <= b)%E) ->
  (f x <= b)%E.
Proof.
rewrite /left_continuous_at => lcfx u_lt_x H.
pose m (eta : {posnum R}) : {nonneg R} :=
  !!(Order.max (insubd 0%:nng (x%:num - eta%:num))%:num u%:num)%:nng.
pose y (eta : {posnum R}) := !!(((m eta)%:num + x%:num) / 2)%:nng.
have md2_lt_xd2 (eta : {posnum R}) :
  Num.max (insubd 0%:nng (x%:num - eta%:num))%:num u%:num / 2
  < x%:num / 2.
{ rewrite ltr_pdivrMr// -mulrA mulVf// mulr1.
  rewrite gt_max u_lt_x andbT.
  have [xme_ge0|xme_lt0] := leP 0 (x%:nngnum - eta%:num).
  { by rewrite insubdK// gtrDl ltrNl oppr0. }
  rewrite /insubd insubF/=; [|by apply/negbTE; rewrite -ltNge].
  exact: le_lt_trans u_lt_x. }
have y_lt_x eta : y eta < x.
{ by rewrite -num_lt/= mulrDl [X in _ < X]splitr ltrD2r. }
have Heta' eta : x%:nngnum - eta%:num <= (y eta)%:nngnum <= x%:nngnum.
{ rewrite [(y eta)%:nngnum <= _]ltW ?num_lt// andbT.
  have xme_le_m : x%:nngnum - eta%:num <= (m eta)%:nngnum.
  { rewrite le_max.
    have [xme_ge0|xme_lt0] := leP 0 (x%:nngnum - eta%:num).
    { by rewrite insubdK// lexx. }
    by rewrite /insubd insubF/=; [rewrite ltW|apply/negbTE; rewrite -ltNge]. }
  apply: (le_trans xme_le_m).
  by rewrite [(m eta)%:nngnum]splitr /= mulrDl lerD2l ltW. }
have H' eta : u < y eta < x.
{ rewrite y_lt_x andbT -num_lt/=.
  rewrite [X in X < _]splitr mulrDl.
  apply: ler_ltD.
  { by rewrite ler_pdivrMr// -mulrA mulVf// mulr1 le_max lexx orbT. }
  by rewrite ltr_pdivrMr// -mulrA mulVf// mulr1. }
case: b H => [b||] H; last first.
{ pose eps := PosNum (@ltr01 R).
  have [eta Heta] := lcfx eps.
  have /andP[{}Heta _] := Heta (y eta) (Heta' eta).
  have {}H := H (y eta) (H' eta).
  by move: (lt_le_trans Heta H); case: (_ - _)%dE. }
{ by rewrite leey. }
case Efx: (f x) => [fx||]; last first.
{ by rewrite leNye. }
{ have [eta /(_ x)] := lcfx 1%:pos.
  rewrite lexx andbT lerBlDl lerDr ge0 => /(_ erefl).
  by rewrite Efx. }
rewrite leNgt; apply/negP => b_lt_fx.
have fxmb_gt_0 : (0 < fx - b) by rewrite subr_gt0.
set eps := PosNum fxmb_gt_0.
have [eta Heta] := lcfx eps.
have /andP[{}Heta _] := Heta (y eta) (Heta' eta).
have {}H := H (y eta) (H' eta).
move: (lt_le_trans Heta H); apply/negP; rewrite -leNgt.
by rewrite /= Efx -dEFinD opprB -addrCA subrr addr0.
Qed.

Lemma left_cont_closureP (f : Fup) :
  reflect
    (f 0%:nng = 0%dE /\ left_continuous f)
    ((f <= left_cont_closure f :> F)%O && `[< forall x, f x \is a fin_num >]).
Proof.
set b := _ && _.
case Eb: b; [apply: ReflectT|apply: ReflectF].
{ move: Eb => /andP[flec /asboolP Ff] {b}.
  have f0 : f 0%:nng = 0%dE.
  { apply/le_anti; rewrite Fplus_ge0 andbT.
    move: flec => /lefP/(_ 0%:nng) Eb; apply: (le_trans Eb).
    by rewrite left_cont_closure0. }
  split=> // x eps.
  have [| Hx] := leP x%:nngnum 0.
  { rewrite le_eqVlt lt0F orbF num_eq0 => /eqP->/=.
    exists 1%:pos => y /andP[_].
    rewrite le_eqVlt lt0F orbF num_eq0 => /eqP->.
    rewrite f0 lexx andbT.
    by rewrite ltEereal /= add0r oppr_lt0. }
  have: left_cont_closure f x = f x.
  { by apply/le_anti/andP; split; apply/lefP;
      [exact/left_cont_closure_le_Fup|]. }
  rewrite left_cont_closure_ereal_sup //.
  set S := [set f u | u in _].
  move=> H.
  have eps_gt0 : 0 < eps%:num by [].
  have /(ub_ereal_sup_adherent eps_gt0) : ereal_sup S \is a fin_num by rewrite H.
  move=> -[_ [xl /= Hxl <-] Hxl'].
  have Hxmxl : 0 < x%:nngnum - xl%:nngnum; [by rewrite subr_gt0|].
  exists (PosNum Hxmxl) => y; rewrite /= subKr => /andP[Hy Hy'].
  apply/andP; split; [|exact/ndFP].
  apply: (@lt_le_trans _ _ (f xl)); [|exact/ndFP].
  move: Hxl'; apply: le_lt_trans.
  by rewrite H. }
rewrite not_andP.
move: Eb => /negbT; rewrite negb_and.
case EF: asbool.
2:{ move: EF => /existsp_asboolPn[x nFfx] _; right=> /(_ x 1%:pos)[eta /(_ x)].
  rewrite lexx andbT lerBlDl lerDr ge0 => /(_ erefl).
  by case: (f x) nFfx. }
move: EF => /asboolP EF; rewrite orbF => /negP Nflec.
have [x] : exists x, (left_cont_closure f x < f x)%E.
{ rewrite not_existsP => H; apply/Nflec/lefP => x.
  rewrite leNgt; exact/negP. }
have [| Hx] := leP x%:nngnum 0.
{ rewrite le_eqVlt lt0F orbF num_eq0 => /eqP->.
  rewrite left_cont_closure0 => f0; left=> f0'.
  by move: f0; rewrite f0' lt_irreflexive. }
rewrite left_cont_closure_ereal_sup//.
set S := [set f u | u in _] => H; right.
rewrite -existsNP; exists x.
pose fxmsS := fine (f x - ereal_sup S).
have FsS : ereal_sup S \is a fin_num.
{ case Es: ereal_sup => //.
  { by exfalso; move: H; apply/negP; rewrite -leNgt Es leey. }
  exfalso; move: Es; rewrite ereal_sup_ninfty => /(_ (f 0%:nng)) /= H'.
  by have := !!(EF 0%:nng); rewrite H'//; exists 0%:nng. }
have PfxmsS : 0 < fxmsS.
{ rewrite /fxmsS; case: (f x) (EF x) H => // fx _.
  by case: ereal_sup FsS => s //= _; rewrite lte_fin subr_gt0. }
rewrite -existsNP; exists (PosNum PfxmsS).
rewrite -forallNP => eta.
rewrite -existsNP; exists (insubd 0%:nng (x%:nngnum - eta%:num)).
rewrite not_implyP; split.
{ have [Hxmeta | Hxmeta] := leP 0 (x%:nngnum - eta%:num).
  { by rewrite insubdK// lexx /= lerBlDr lerDl. }
  rewrite /insubd insubF /=; [|by apply/negP/negP; rewrite -ltNge].
  by rewrite ltW//=. }
apply/negP; rewrite negb_and; apply/orP; left; rewrite -leNgt /=.
have -> : (f x + (- fxmsS)%:E = ereal_sup S)%dE.
{ rewrite /fxmsS; case: (f x) (EF x) => // fx _.
  by case: ereal_sup FsS => s //; rewrite -dEFinB /= subKr. }
apply: ereal_sup_ubound; eexists=> [|//] /=.
have [Hxmeta | Hxmeta] := leP 0 (x%:nngnum - eta%:num).
{ by rewrite -num_lt insubdK//= ltrBlDr ltrDl. }
by rewrite /insubd insubF /=; [|apply/negP/negP; rewrite -ltNge].
Qed.

Definition flow_cc_pred (f : Fup) :=
  `[< f 0%:nng = 0%dE /\ left_continuous f /\ forall x, f x \is a fin_num >].

Record flow_cc := {
  flow_cc_val :> Fup;
  _ : flow_cc_pred flow_cc_val
}.
Arguments Build_flow_cc : clear implicits.

Program Definition Build_flow_cc' (f : Fup)
        (Hf : (f <= left_cont_closure f :> F)%O
              && `[< forall t, f t \is a fin_num >]) := Build_flow_cc f _.
Next Obligation.
move=> f Hf; apply/asboolP; rewrite andA; split; [exact/left_cont_closureP|].
by move: Hf => /andP[_ /asboolP].
Qed.
Arguments Build_flow_cc' : clear implicits.

HB.instance Definition _ := [isSub for flow_cc_val].
HB.instance Definition _ := [Choice of flow_cc by <:].

Lemma flow_cc_le_left_cont_closure (f : flow_cc) :
  (f <= left_cont_closure f :> F)%O.
Proof.
case: f => f; rewrite /= /flow_cc_pred.
by rewrite andA asbool_and => /andP[/asboolP/left_cont_closureP/andP[]].
Qed.

Lemma left_cont_closure_flow_cc (f : flow_cc) : left_cont_closure f = f.
Proof.
by apply/le_anti; rewrite left_cont_closure_le_Fup flow_cc_le_left_cont_closure.
Qed.

Lemma flow_cc_fin (f : flow_cc) t : f t \is a fin_num.
Proof. by case: f => f /= /asboolP[_ [_]]; exact. Qed.

Lemma fin_flow_cc_prop (f : flow_cc) :
  { ff : {nonneg R} -> {nonneg R} | forall t, f t = (ff t)%:nngnum%:E }.
Proof.
apply: constructive_indefinite_description.
case: f => f /= /asboolP[_ [_ finf]].
have nngfine : forall t, 0 <= fine (f t).
{ by move=> t; case: (f t) (Fplus_ge0 f t). }
exists (fun t => NngNum (nngfine t)) => t /=.
by case: (f t) (finf t).
Qed.

Definition fin_flow_cc (f : flow_cc) : {nonneg R} -> {nonneg R} :=
  proj1_sig (fin_flow_cc_prop f).

Local Notation "f '%:fcc'" := (fin_flow_cc f).

Lemma fin_flow_ccE (f : flow_cc) : forall t : {nonneg R}, (f%:fcc t)%:nngnum%:dE = f t.
Proof. by move=> t; rewrite /fin_flow_cc; case: (fin_flow_cc_prop f). Qed.

Lemma flow_cc0 (f : flow_cc) : f 0%:nng = 0%dE.
Proof. by case: f => f /= /asboolP[]. Qed.

Lemma flow_cc_left_cont (f : flow_cc) : left_continuous f.
Proof. by case: f => f /= /asboolP[_ []]. Qed.

Lemma flow_cc_addr_closed : addr_closed flow_cc_pred.
Proof.
split.
{ rewrite inE andA; split=> [|//]; apply/left_cont_closureP/andP.
  split; [exact/lefP/left_cont_closure_ge0|exact/asboolP]. }
move=> f g /[!inE] -[f0 [lcf finf]] [g0 [lcg fing]].
rewrite andA; apply/(andPP (left_cont_closureP _) (asboolP _)).
rewrite -andbA andbb andbC; apply/andP; split.
{ by apply/asboolP => t; rewrite dfin_numD finf fing. }
apply/lefP => t.
have [|Ht] := leP t%:num 0.
{ rewrite le_eqVlt lt0F orbF num_eq0 => /eqP->/=.
  by rewrite /GRing.add/= f0 g0 add0r left_cont_closure_ge0. }
rewrite left_cont_closure_ereal_sup// leNgt.
set sup' := ereal_sup _.
set fgt' := (f + g) t.
apply/negP => H.
have [fgt Hfgt] : exists fgt, fgt%:E = fgt'.
{ rewrite /fgt'/GRing.add/=/GRing.add/=.
  by eexists; rewrite -(fineK (finf t)) -(fineK (fing t)) -dEFinD. }
have [sup Hsup] : exists sup, sup%:E = sup'.
{ rewrite /sup'.
  case E: ereal_sup; [by eexists| |]; exfalso.
  { suff: (sup' <= (f + g) t)%O.
    { rewrite /sup' E; apply/negP; rewrite -ltNge.
      rewrite /fgt'/GRing.add/=/GRing.add/=.
      by rewrite -(fineK (finf t)) -(fineK (fing t)) -dEFinD ltey. }
    apply: ub_ereal_sup => _ [u /= Hu <-].
    exact: (ndFP' (f + g) Hu). }
  move: E => /ereal_sup_ninfty /(_ ((f + g) 0%:nng)) E.
  have: ((f + g) 0%:nng = -oo%E).
  { by apply: E; exists (0%:nng). }
  by do 2![rewrite /GRing.add/=]; rewrite f0 g0 addr0. }
have Peps : 0 < (fgt - sup) / 2.
{ apply: divr_gt0 => //; rewrite subr_gt0.
  by move: H; rewrite -Hfgt -Hsup ltEereal. }
pose eps := PosNum Peps.
have [etaf Hetaf] := lcf t eps.
have [etag Hetag] := lcg t eps.
pose eta := Order.min t%:nngnum (Order.min etaf%:num etag%:num).
have Py : 0 <= t%:nngnum - eta.
{ by rewrite subr_ge0 /eta ge_min lexx. }
pose y := NngNum Py.
have Hyf : t%:nngnum - etaf%:num <= y%:nngnum <= t%:nngnum.
{ rewrite /y /= lerD2l lerBlDr lerDl.
  by rewrite lerNl opprK /eta !ge_min lexx orbT !le_min !ge0. }
have Hyg : t%:nngnum - etag%:num <= y%:nngnum <= t%:nngnum.
{ rewrite /y /= lerD2l lerBlDr lerDl.
  by rewrite lerNl opprK /eta !ge_min lexx !orbT !le_min !ge0. }
have {}Hetaf := Hetaf _ Hyf.
have {}Hetag := Hetag _ Hyg.
move: (le_refl sup'); apply/negP; rewrite -ltNge.
apply: (@lt_le_trans _ _ ((f + g) y)).
{ move: Hetaf => /andP[{}Hetaf _].
  move: Hetag => /andP[{}Hetag _].
  move: (lte_dD Hetaf Hetag); apply: le_lt_trans.
  rewrite daddeAC daddeA -[(f t + g t)%dE]/fgt'.
  rewrite -Hsup -Hfgt -EFinN -dEFinD leEereal /=.
  by rewrite -addrA -opprD -splitr subKr. }
apply: ereal_sup_ubound; exists y => //=.
by rewrite /y -num_lt/= ltrBlDr ltrDl !lt_min !gt0 Ht.
Qed.

HB.instance Definition _ := GRing.isAddClosed.Build Fup flow_cc_pred
  flow_cc_addr_closed.

HB.instance Definition _ := [SubChoice_isSubNmodule of flow_cc by <:].

Lemma flow_cc_plusE (f f' : flow_cc) x : (f + f') x = (f x + f' x)%dE.
Proof. by []. Qed.

Lemma flow_cc_sumE I (r : seq I) P (F : I -> flow_cc) t :
  (\sum_(i <- r | P i) F i) t = (\sum_(i <- r | P i) F i t)%dE.
Proof.
by apply: (big_ind2 (fun (f : flow_cc) y => f t = y)) => // f1 _ f2 _ <- <-.
Qed.

Lemma fin_flow_cc_sumE I (r : seq I) P (f : I -> flow_cc) t :
  (\sum_(i <- r | P i) ((f i)%:fcc t)%:nngnum)%:dE = (\sum_(i <- r | P i) f i) t.
Proof.
rewrite -dsumEFin flow_cc_sumE.
by apply: eq_bigr => i _; rewrite fin_flow_ccE.
Qed.

End CumulativeCurves.
Arguments Build_flow_cc {R} _ _.
Arguments Build_flow_cc' {R} _ _.

Notation "f '%:fcc'" := (fin_flow_cc f).
