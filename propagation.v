(******************************************************************************)
(* A few results about propagation.                                           *)
(*                                                                            *)
(* Propagation of local results throug a network topology in network calculus.*)
(*                                                                            *)
(******************************************************************************)

From mathcomp Require Import ssreflect ssrfun ssrbool eqtype order ssrnat.
From mathcomp Require Import choice seq fintype finfun bigop ssralg ssrnum rat.
From mathcomp Require Import boolp classical_sets signed reals ereal.
From mathcomp.dioid Require Import dioid complete_lattice complete_dioid.
Require Import RminStruct cumulative_curves arrival_curves servers services.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope ring_scope.
Local Open Scope order_scope.

Import Order.Theory GRing.Theory.

Section Propagation.

Context {R : realType}.

(* Theorem 11 p 137 *)
Theorem output_arrival_curve (S : @flow_cc R -> flow_cc -> Prop)
        (beta_m beta_M : F) (sigma : Fup) (alpha : F)
        (A D : flow_cc) :
  min_service S beta_m -> max_service S beta_M ->
  shaper_server sigma S -> max_arrival A alpha ->
  S A D ->
  max_arrival D ((((alpha : Fd) * beta_M) / beta_m)%D \min sigma).
Proof.
move=> H_beta_m H_beta_M H_sigma H_alpha HAD.
apply: max_arrival_F_min.
{ rewrite max_arrival_alt_def mulrC -F_dioidE mul_div_equiv.
  apply: (@le_trans _ _ ((((A : Fd) * beta_M) / A) / beta_m)%D).
  { apply: led_divl.
    rewrite (mulrC (A : Fd)).
    refine (le_trans _ (mul_divA _ _ _)).
    rewrite mulrC.
    exact/led_mul2l/max_arrival_deconv2. }
  rewrite -div_mul (mulrC (beta_m : Fd)).
  apply: (@le_trans _ _ (((A : Fd) * beta_M) / D)%D).
  { exact/led_divr/H_beta_m. }
  exact/led_divl/H_beta_M. }
rewrite max_arrival_alt_def.
exact: (H_sigma _ _ HAD).
Qed.

(* Corollary 4 p.138 *)
Corollary output_arrival_curve_mp_F (S : partial_server) (A D : @flow_cc R)
          (alpha beta : F) :
  S A D ->
  min_service S beta -> max_arrival A alpha ->
  max_arrival D ((alpha : Fd) / beta)%D.
Proof.
(* some preprocessing *)
rewrite !max_arrival_alt_def.
move=> SAD /(_ _ _ SAD) Hbeta Halpha.
move: SAD => /server_le HAD.
(* goal is now: D <= D * (alpha / beta)
   under hypotheses: Halpha : A <= A * alpha, Hbeta : A * beta <= D
   and HAD : D <= A *)
rewrite -F_dioidE mulrC mul_div_equiv.
(* goal: D / D <= alpha / beta *)
apply: le_trans _ (led_div (HAD : A <=^d D :> Fd) Hbeta).
(* A / (A * beta) <= alpha / beta *)
rewrite mulrC div_mul.  (* A / A / beta <= alpha / beta *)
apply: led_divl.  (* A / A <= alpha *)
by rewrite -mul_div_equiv mulrC.
Qed.

Corollary output_arrival_curve_mp_Fplus (S : partial_server) (A D : @flow_cc R)
          (alpha beta : Fplus) :
  S A D ->
  min_service S beta -> max_arrival A alpha ->
  max_arrival D ((alpha : Fplusd) / beta)%D.
Proof.
rewrite !max_arrival_alt_def => SAD /(_ _ _ SAD).
rewrite !F_dioidE -!Fplus_dioidE => Hbeta Halpha.
move: SAD => /server_le; rewrite -Fplus_dioidE => HAD.
rewrite Fplus_dioidE -!Fplus_dioidE mulrC mul_div_equiv.
refine (le_trans _ (led_div HAD Hbeta)).
rewrite mulrC div_mul.
apply: led_divl.
by rewrite -mul_div_equiv mulrC.
Qed.

Corollary output_arrival_curve_mp_Fup (S : partial_server) (A D : @flow_cc R)
          (alpha beta : Fup) :
  S A D ->
  min_service S beta -> max_arrival A alpha ->
  max_arrival D ((alpha : Fupd) / beta)%D.
Proof. rewrite Fup_divE; exact: output_arrival_curve_mp_Fplus. Qed.

End Propagation.
