# Makefile for NCCoq

COQ_PROJ := _CoqProject
COQ_MAKEFILE := Makefile.coq
COQ_MAKE := +$(MAKE) -f $(COQ_MAKEFILE)

ifneq "$(COQBIN)" ""
	COQBIN := $(COQBIN)/
else
	COQBIN := $(dir $(shell which coqc))
endif
export COQBIN

all install html gallinahtml doc deps.dot deps.png: $(COQ_MAKEFILE) Makefile
	$(COQ_MAKE) $@

%.vo: %.v
	$(COQ_MAKE) $@

$(COQ_MAKEFILE): $(COQ_PROJ)
	$(COQBIN)coq_makefile -f $< -o $@

clean:
	-$(COQ_MAKE) clean
	$(RM) depend depend.dot depend.png depend.map

distclean: clean
	$(RM) $(COQ_MAKEFILE) $(COQ_MAKEFILE).conf
	$(RM) *~ .*.aux .lia.cache


-include $(COQ_MAKEFILE).conf

.PHONY: all install html gallinahtml doc clean distclean
