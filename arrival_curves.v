(* Copyright : Lucien Rakotomalala, Pierre Roux and Marc Boyer. *)

From mathcomp Require Import ssreflect ssrfun ssrbool eqtype order ssrnat.
From mathcomp Require Import choice seq fintype finfun bigop ssralg ssrnum rat.
From mathcomp Require Import boolp classical_sets signed reals ereal.
From mathcomp.dioid Require Import dioid complete_lattice complete_dioid.
Require Import analysis_complements RminStruct cumulative_curves.
Require Import NCCoq.usual_functions.

(******************************************************************************)
(* Arrival curves in Network Calculus                                         *)
(*                                                                            *)
(* max_arrival A alpha <-> alpha is an arrival curve for the data flow A      *)
(*                                                                            *)
(* max_arrival_deconv{1,2} state that A / A                                   *)
(* is the smallest arrival curve for A                                        *)
(******************************************************************************)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope ereal_dual_scope.
Local Open Scope ring_scope.
Local Open Scope order_scope.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.

Section ArrivalCurves.

Context {R : realType}.

(** alpha is an arrival curve for A (Definition 5.1) *)
Definition max_arrival (A : @flow_cc R) (alpha : F) :=
  forall t d, (A (t%:nngnum + d%:nngnum)%:nng%R - A t <= alpha d)%dE.

(** Equivalent definition using min-plus convolution (Proposition 5.1) *)
Lemma max_arrival_alt_def (A : flow_cc) (alpha : F) :
  max_arrival A alpha <-> A <= ((A : Fd) * alpha) :> F.
Proof.
split => H.
{ apply/lefP => t.
  apply: lb_ereal_inf => _ [[u v] /= Huv <-].
  move: (H u v).
  have -> : (u%:nngnum + v%:nngnum)%:nng%R = t; [exact/val_inj|].
  by rewrite -!fin_flow_ccE lee_dsubl_addr// daddeC. }
move=> t d.
move: H => /lefP /(_ (t%:nngnum + d%:nngnum)%:nng%R).
rewrite F_dioidE => {}H.
rewrite -!fin_flow_ccE lee_dsubl_addr// !fin_flow_ccE.
apply: (le_trans H).
by apply: ereal_inf_lbound; exists (t, d) => //=; rewrite daddeC.
Qed.

(** Propositions 31 (1)) p 122 *)
Lemma max_arrival_F_min A alpha alpha' :
  max_arrival A alpha -> max_arrival A alpha' ->
  max_arrival A (alpha \min alpha').
Proof.
rewrite !max_arrival_alt_def mulrDr -!F_dioidE !le_def => /eqP Aa /eqP Aa'.
by rewrite addrAC Aa addrC Aa'.
Qed.

(** Propositions 31 (3)) p 122 *)
Lemma max_arrival_le A alpha alpha' :
  max_arrival A alpha -> alpha <= alpha' -> max_arrival A alpha'.
Proof.
rewrite !max_arrival_alt_def => H H'.
by apply: le_trans H _; rewrite -F_dioidE led_mul2l.
Qed.

(** The deconvolution of a function by itself is an arrival curve... *)
(** Propositions 31 (4 1er partie)) p 122 *)
Lemma max_arrival_deconv1 A : max_arrival A ((A : Fd) / A)%D.
Proof. by rewrite max_arrival_alt_def -F_dioidE mulrC mul_div_equiv. Qed.

(** ...and it is the least one. *)
(** Propositions 31 (4 2em partie)) p 122 *)
Lemma max_arrival_deconv2 A alpha :
  max_arrival A alpha -> ((A : Fd) / A)%D <= alpha :> F.
Proof.
by rewrite max_arrival_alt_def => H; rewrite -F_dioidE -mul_div_equiv mulrC.
Qed.

(** Propositions 31 (1)) p 122 *)
Lemma max_arrival_sum_flow_cc n (A : flow_cc^n.+1) (alpha : F^n.+1)
      (P : pred 'I_n.+1) :
  (forall i, max_arrival (A i) (alpha i)) ->
  max_arrival (\sum_(i | P i) A i) (\sum_(i | P i) alpha i).
Proof.
move=> sum_alpha t d /=.
rewrite !flow_cc_sumE sumfE.
under [X in (- X)%E]eq_bigr => i _ do rewrite -fin_flow_ccE.
rewrite dsumEFin -EFinN -sumrN -dsumEFin -big_split /=.
apply: lee_dsum => i _; rewrite EFinN fin_flow_ccE; exact: sum_alpha.
Qed.

Lemma max_arrival_delta_0 A : max_arrival A (delta 0).
Proof.
move=> t d; rewrite /delta /=.
case: (leP d%:nngnum%:E) => H; [|exact: leey].
have -> : (d = 0%:nng)%R.
{ by apply/val_inj => /=; apply/le_anti/andP; split. }
have -> : ((t%:nngnum + 0%:nng%:nngnum)%:nng)%R = t.
{ by apply/val_inj => /=; rewrite addr0. }
by rewrite -fin_flow_ccE -EFinN -dEFinB subrr.
Qed.

End ArrivalCurves.
