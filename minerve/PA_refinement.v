From mathcomp Require Import all_ssreflect.
From mathcomp Require Import ssrint ssralg ssrnum rat.
From mathcomp Require Import ereal signed.
From CoqEAL Require Import hrel param refinements binrat.
From NCCoq Require Import RminStruct.
Require Import ratdiv jump_sequences PA UPP UPP_PA.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import Order.Theory.

Import Refinements.Op.

Section classes.

Class ceil_of C := ceil_op : C -> nat.
Class lcm_of C := lcm_op : C -> C -> C.

End classes.

Parametricity extended.
Parametricity er_map.

Section generic.

Context {C : Type}.
Context `{!zero_of C, !cast_of nat C}.
Context `{!add_of C, !sub_of C, !mul_of C, !div_of C, !max_of C, !min_of C}.
Context `{!eq_of C, !lt_of C, !leq_of C}.

Definition seqjs_elem := (C * (\bar C * (C * \bar C)))%type.

Definition seqjs := seq seqjs_elem.

Definition seqjs2 :=
  seq (C * ((\bar C * (C * \bar C)) * (\bar C * (C * \bar C)))).

Definition seqjs_below (a : seqjs) (l : C) := [seq x <- a | (x.1 <= l)%C].

Definition default_seqjs : C * (\bar C * (C * \bar C)) :=
  (0, (+oo%E, (0, +oo%E)))%C.

Definition seqjs_zero : seqjs := [:: default_seqjs].

Definition seqjs_0 : seqjs := [:: (0%C, (0%C%:E, (0%C, 0%C%:E)))].

Definition seqjs_affine r s : seqjs := [:: (0%C, (s%:E, (r, s%:E)))].

Definition seqjs_cst_inf (x : C) (y c : \bar C) : seqjs :=
  [:: (x, (y, (0%C, c)))].

Definition head_seqjs := head default_seqjs.

Definition last_seqjs := last default_seqjs.

Definition seqjs_not_empty (a : seqjs) :=
  match a with [::] => false | _ => true end.

Definition seqjs_head_is_0 (a : seqjs) := ((head default_seqjs a).1 == 0)%C.

Definition seqjs_sorted (a : seqjs) := sorted (fun x y => x.1 < y.1)%C a.

Definition seqjs_er_opp (x : \bar C) :=
  match x with
  | x%:E => (0 - x)%C%:E
  | +oo%E => -oo%E
  | -oo%E => +oo%E
  end.

Definition seqjs_er_add (x x' : \bar C) :=
  match x, x' with
  | x%:E, y%:E => (x + y)%C%:E
  | +oo%E, _ | _, +oo%E => +oo%E
  | -oo%E, _ | _, -oo%E => -oo%E
  end.

Definition seqjs_er_max (x x' : \bar C) :=
  match (x, x') with
  | (+oo%E, _) | (_, +oo%E) => +oo%E
  | (-oo%E, y) | (y, -oo%E) => y
  | (x%:E, x'%:E) => (max_op x x')%:E
  end.

Definition seqjs_er_min (x x' : \bar C) :=
  match (x, x') with
  | (-oo%E, _) | (_, -oo%E) => -oo%E
  | (+oo%E, y) | (y, +oo%E) => y
  | (x%:E, x'%:E) => (min_op x x')%:E
  end.

Definition seqjs_shift_rho_sigma (rho_sigma : C * \bar C) (shift : C) :=
  (rho_sigma.1, (seqjs_er_add rho_sigma.2 (rho_sigma.1 * shift)%C%:E)).

Definition seqjs_shift_y_rho_sigma (rho_sigma : C * \bar C) (shift : C) :=
  let rho_sigma := seqjs_shift_rho_sigma rho_sigma shift in
  (rho_sigma.2, rho_sigma).

Definition seqjs_shift_elem (xyrs : seqjs_elem) (x' : C) : seqjs_elem :=
  if (x' <= xyrs.1)%C then xyrs
  else (x', seqjs_shift_y_rho_sigma xyrs.2.2 (x' - xyrs.1)%C).

Fixpoint seqjs_above_rec xyrs a l :=
  match a with
  | [::] => [:: seqjs_shift_elem xyrs l]
  | xyrs' :: a' =>
      if (xyrs'.1 <= l)%C then seqjs_above_rec xyrs' a' l
      else seqjs_shift_elem xyrs l :: a
  end.

Definition seqjs_above (a : seqjs) (l : C) :=
  match a with
  | [::] => a
  | xyrs :: a' =>
      if (l < xyrs.1)%C then a
      else seqjs_above_rec xyrs a' l
  end.

Definition seqjs_cat (a b : seqjs) : seqjs :=
  [seq x <- a | (x.1 < (head default_seqjs b).1)%C] ++ b.

Definition seqjs_delta (b : C) : seqjs :=
  if (0 <= b)%C then seqjs_cat seqjs_0 (seqjs_cst_inf b 0%C%:E +oo%E)
  else seqjs_cst_inf 0%C +oo%E +oo%E.

Definition seqjs_map T T' (f : C -> T -> T') (a : seq (C * T)) :=
  [seq (xt.1, f xt.1 xt.2) | xt <- a].

Definition seqjs_normalize (a : seqjs) (l : C) : seqjs :=
  let norm x '(y, (r, s)) :=
    let rs :=
      if (l <= x)%C then (0%C, +oo%E)
      else ((if s is EFin _ then r else 0%C), s) in
    (y, rs) in
  seqjs_map norm a.

Definition seqjs_merge (a a' : seqjs) :=
  let fix aux px prs prs' (a a' : seqjs) :=
      match a with
      | [::] =>
        (fix aux' px prs prs' (a' : seqjs) :=
           match a' with
           | [::] => [::]
           | (x', yrs') :: s' =>
             let yrs := seqjs_shift_y_rho_sigma prs (x' - px)%C in
             (x', (yrs, yrs')) :: aux' x' yrs.2 yrs'.2 s'
           end) px prs prs' a'
      | (x, yrs) :: s =>
        (fix aux' px prs prs' a' :=
           match a' with
           | [::] =>
             let yrs' := seqjs_shift_y_rho_sigma prs' (x - px)%C in
             (x, (yrs, yrs')) :: aux x yrs.2 yrs'.2 s [::]
           | (x', yrs') :: s' =>
             if (x < x')%C then
               let yrs' := seqjs_shift_y_rho_sigma prs' (x - px)%C in
               (x, (yrs, yrs')) :: aux x yrs.2 yrs'.2 s a'
             else if (x' < x)%C then
               let yrs := seqjs_shift_y_rho_sigma prs (x' - px)%C in
               (x', (yrs, yrs')) :: aux' x' yrs.2 yrs'.2 s'
             else (* x = x' *) (x, (yrs, yrs')) :: aux x yrs.2 yrs'.2 s s'
        end) px prs prs' a'
      end in
  match (a : seq _, a' : seq _) with
  | ([::], _) | (_, [::]) => [::]
  | (((x, yrs) :: a), ((_, yrs') :: a')) =>
    (x, (yrs, yrs')) :: aux x yrs.2 yrs'.2 a a'
  end.

Definition seqjs_er_eq (x x' : \bar C) :=
  match x, x' with
  | +oo%E, +oo%E => true
  | -oo%E, -oo%E => true
  | x%:E, x'%:E => (x == x')%C
  | _, _ => false
  end.

Definition seqjs_eqb a a' l :=
  let fix aux s1 s2 :=
    match s1, s2 with
    | [::], [::] => true
    | x1 :: s1', x2 :: s2' =>
      [&& (x1.1 == x2.1)%C, seqjs_er_eq x1.2.1 x2.2.1,
       (x1.2.2.1 == x2.2.2.1)%C & seqjs_er_eq x1.2.2.2 x2.2.2.2]
      && aux s1' s2'
    | _, _ => false
    end in
  let a'' := seqjs_merge a a' in
  aux (seqjs_normalize (seqjs_map (fun=> fst) a'') l)
      (seqjs_normalize (seqjs_map (fun=> snd) a'') l).

Definition seqjs_opp : seqjs -> seqjs :=
  seqjs_map (fun _ yrs =>
    (seqjs_er_opp yrs.1,
      ((0 - yrs.2.1)%C, seqjs_er_opp yrs.2.2))).

Definition seqjs_eval (a : seqjs) (x : C) : \bar C :=
  let fix aux xyrs a :=
    if (x <= xyrs.1)%C then xyrs.2.1 else
      let seg := (seqjs_shift_rho_sigma xyrs.2.2 (x - xyrs.1)%C).2 in
      match a with
      | [::] => seg
      | xyrs' :: a => if (x < xyrs'.1)%C then seg else aux xyrs' a
      end in
  if a is xyrs :: a then aux xyrs a else 0%C%:E.

Definition seqjs_max_val a l :=
  let fix aux xyrs a :=
    let x := xyrs.1 in
    let '(r, s) := xyrs.2.2 in
    match a with
    | [::] =>
        if (l <= x)%C then -oo%E else
          if (r <= 0)%C then s else seqjs_er_add s (r * (l - x))%C%:E
    | ((x', (y', _)) as xyrs') :: a =>
        let m := if (r <= 0)%C then s else seqjs_er_add s (r * (x' - x))%C%:E in
        seqjs_er_max (seqjs_er_max m y') (aux xyrs' a)
    end in
  if a is xyrs :: a then aux xyrs a else -oo%E.

Definition seqjs_min_val a l := seqjs_er_opp (seqjs_max_val (seqjs_opp a) l).

Definition seqjs_add_cst c : seqjs -> seqjs :=
  seqjs_map
    (fun _ yrs => (seqjs_er_add yrs.1 c, (yrs.2.1, seqjs_er_add yrs.2.2 c))).

Definition seqjs_y_rho_sigma_add (yrs yrs' : \bar C * (C * \bar C)) :=
  (seqjs_er_add yrs.1 yrs'.1,
    ((yrs.2.1 + yrs'.2.1)%C, seqjs_er_add yrs.2.2 yrs'.2.2)).

Definition seqjs_add (a a' : seqjs) : seqjs :=
  seqjs_map
    (fun _ '(yrs, yrs') => seqjs_y_rho_sigma_add yrs yrs')
    (seqjs_merge a a').

Definition seqjs_y_rho_sigma_min (yrs yrs' : \bar C * (C * \bar C)) (x x' : C)
  :=
  let y := seqjs_er_min yrs.1 yrs'.1 in
  match yrs.2.2, yrs'.2.2 with
  | -oo%E, _ | _, -oo%E => [:: (x, (y, (0%C, -oo%E)))]
  | +oo%E, _ => [:: (x, (y, yrs'.2))]
  | _, +oo%E => [:: (x, (y, yrs.2))]
  | EFin s, EFin s' =>
      let r := yrs.2.1 in let r' := yrs'.2.1 in
      if (r == r')%C then [:: (x, (y, (r, (min_op s s')%:E)))] else
        let x'' := (x + (s' - s) %/ (r - r'))%C in
        if (x'' <= x)%C then
          [:: (x, (y, if (r <= r')%C then yrs.2 else yrs'.2))]
        else if (x' <= x'')%C then
          [:: (x, (y, if (r <= r')%C then yrs'.2 else yrs.2))]
        else
          let shift rs := seqjs_shift_y_rho_sigma rs (x'' - x)%C in
          if (r <= r')%C then [:: (x, (y, yrs'.2)); (x'', shift yrs.2)]
          else (* r' < r *) [:: (x, (y, yrs.2)); (x'', shift yrs'.2)]
  end.

Fixpoint seqjs_min_rec x yrs yrs' a l :=
  match a with
  | [::] => seqjs_y_rho_sigma_min yrs yrs' x l
  | (x2, (yrs2, yrs2')) :: a =>
      seqjs_y_rho_sigma_min yrs yrs' x x2 ++ seqjs_min_rec x2 yrs2 yrs2' a l
  end.

Definition seqjs_min a a' l :=
  match seqjs_merge a a' with
  | [::] => [::]
  | (x, (yrs, yrs')) :: a => seqjs_min_rec x yrs yrs' a l
  end.

Definition seqjs_inf_left (a : seqjs) : seqjs :=
  if (0 < (head default_seqjs a).1)%C then default_seqjs :: a else a.

Definition seqjs_inf_left_eq (a : seqjs) : seqjs :=
  let xyrs := head default_seqjs a in
  let a := (xyrs.1, (+oo%E, xyrs.2.2)) :: behead a in
  if (0 < xyrs.1)%C then default_seqjs :: a else a.

Definition seqjs_point (x : C) (y : \bar C) : seqjs :=
  seqjs_inf_left [:: (x, (y, (0%C, +oo%E)))].

Definition seqjs_segment (x : C) (y : \bar C)
    (r : C) (s : \bar C) (l : C) (linf : bool) : seqjs :=
  if (l <= x)%C then seqjs_zero else
    let y' := if linf then +oo%E else seqjs_er_add (r * (l - x))%C%:E s in
    seqjs_inf_left [:: (x, (y, (r, s))); (l, (y', (0%C, +oo%E)))].

Definition seqjs_min_conv_point x y x' y' :=
  seqjs_point (x + x')%C (seqjs_er_add y y').

Definition seqjs_min_conv_point_affine (x : C) (y : \bar C)
    (x' : C) (r' : C) (s' : \bar C) (l' : C) (l'inf : bool) : seqjs :=
  seqjs_segment (x + x')%C +oo%E r' (seqjs_er_add s' y) (x + l')%C l'inf.

Definition seqjs_min_conv_affine r s (x l : C) linf
    r' s' (x' l' : C) l'inf : seqjs :=
  seqjs_inf_left
    (if (l <= x)%C || (l' <= x')%C then seqjs_zero else
       if (r == r')%C then
         seqjs_segment (x + x')%C +oo%E r (seqjs_er_add s s')
           (l + l')%C (linf || l'inf)
       else
         let y := if linf || l'inf then +oo%E else
           seqjs_er_add (seqjs_er_add (r * (l - x) + r' * (l' - x'))%C%:E s) s' in
         if (r < r')%C then
           [:: ((x + x')%C, (+oo%E, (r, (seqjs_er_add s s'))));
            ((x' + l)%C,
             (seqjs_er_add (seqjs_er_add (r * (l - x))%C%:E s) s',
              (r', seqjs_er_add (seqjs_er_add (r * (l - x))%C%:E s) s')));
            ((l + l')%C, (y, (0%C, +oo%E)))]
         else
           [:: ((x + x')%C, (+oo%E, (r', seqjs_er_add s s')));
            ((x + l')%C,
             (seqjs_er_add (seqjs_er_add (r' * (l' - x'))%C%:E s') s,
              (r, seqjs_er_add (seqjs_er_add (r' * (l' - x'))%C%:E s') s)));
            ((l + l')%C, (y, (0%C, +oo%E)))]).

Definition seqjs_min_conv_segment (x : C) (y : \bar C)
    (r : C) (s : \bar C) (l : C) (linf : bool)
    (x' : C) (y' : \bar C)
    (r' : C) (s' : \bar C) (l' : C) (l'inf : bool)
    (l'' : C) : seqjs :=
  if (l <= x)%C then
    if (l' <= x')%C then seqjs_below (seqjs_min_conv_point x y x' y') l'' else
      seqjs_min
        (seqjs_below (seqjs_min_conv_point x y x' y') l'')
        (seqjs_below (seqjs_min_conv_point_affine x y x' r' s' l' l'inf) l'')
        l''
  else if (l' <= x')%C then
    seqjs_min
      (seqjs_below (seqjs_min_conv_point x y x' y') l'')
      (seqjs_below (seqjs_min_conv_point_affine x' y' x r s l linf) l'')
      l''
  else
    seqjs_min
      (seqjs_min
         (seqjs_below (seqjs_min_conv_point x y x' y') l'')
         (seqjs_below (seqjs_min_conv_point_affine x y x' r' s' l' l'inf) l'')
         l'')
      (seqjs_min
         (seqjs_below (seqjs_min_conv_point_affine x' y' x r s l linf) l'')
         (seqjs_below (seqjs_min_conv_affine r s x l linf r' s' x' l' l'inf) l'')
         l'')
      l''.

Fixpoint seqjs_min_conv_segment_js_rec x y r s l linf
    x' y' r' s' (a : seqjs) (l' : C) : seqjs :=
  match a with
  | [::] => seqjs_min_conv_segment x y r s l linf x' y' r' s' l' false l'
  | (x'', (y'', (r'', s''))) :: a =>
      seqjs_min (seqjs_min_conv_segment x y r s l linf x' y' r' s' x'' true l')
        (seqjs_min_conv_segment_js_rec x y r s l linf x'' y'' r'' s'' a l')
        l'
  end.

Definition seqjs_min_conv_segment_js (x : C) (y : \bar C)
    (r : C) (s : \bar C) (l : C) (linf : bool)
    (a : seqjs) (l' : C) : seqjs :=
  match a with
  | [::] => seqjs_zero
  | (x', (y', (r', s'))) :: a =>
      seqjs_min_conv_segment_js_rec x y r s l linf x' y' r' s' a l'
  end.

Fixpoint seqjs_min_conv_rec x y r s (a b : seqjs) (l : C) : seqjs :=
  match a with
  | [::] => seqjs_min_conv_segment_js x y r s l false b l
  | (x', (y', (r', s'))) :: a =>
      seqjs_min (seqjs_min_conv_segment_js x y r s x' true b l)
        (seqjs_min_conv_rec x' y' r' s' a b l)
        l
  end.

Definition seqjs_min_conv (a b : seqjs) (l : C) : seqjs :=
  match a with
  | [::] => seqjs_zero
  | (x, (y, (r, s))) :: a => seqjs_min_conv_rec x y r s a b l
  end.

End generic.

Parametricity seqjs_below.
Parametricity default_seqjs.
Parametricity seqjs_zero.
Parametricity seqjs_0.
Parametricity seqjs_affine.
Parametricity seqjs_cst_inf.
Parametricity head_seqjs.
Parametricity last_seqjs.
Parametricity seqjs_not_empty.
Parametricity seqjs_head_is_0.
Parametricity seqjs_sorted.
Parametricity seqjs_er_opp.
Parametricity seqjs_er_add.
Parametricity seqjs_er_max.
Parametricity seqjs_er_min.
Parametricity seqjs_shift_rho_sigma.
Parametricity seqjs_shift_y_rho_sigma.
Parametricity seqjs_shift_elem.
Parametricity seqjs_above_rec.
Parametricity seqjs_above.
Parametricity seqjs_cat.
Parametricity seqjs_delta.
Parametricity seqjs_map.
Parametricity seqjs_normalize.
Parametricity seqjs_merge.
Parametricity seqjs_er_eq.
Parametricity seqjs_eqb.
Parametricity seqjs_opp.
Parametricity seqjs_eval.
Parametricity seqjs_max_val.
Parametricity seqjs_min_val.
Parametricity seqjs_add_cst.
Parametricity seqjs_y_rho_sigma_add.
Parametricity seqjs_add.
Parametricity seqjs_y_rho_sigma_min.
Parametricity seqjs_min_rec.
Parametricity seqjs_min.
Parametricity seqjs_inf_left.
Parametricity seqjs_inf_left_eq.
Parametricity seqjs_point.
Parametricity seqjs_segment.
Parametricity seqjs_min_conv_point.
Parametricity seqjs_min_conv_point_affine.
Parametricity seqjs_min_conv_affine.
Parametricity seqjs_min_conv_segment.
Parametricity seqjs_min_conv_segment_js_rec.
Parametricity seqjs_min_conv_segment_js.
Parametricity seqjs_min_conv_rec.
Parametricity seqjs_min_conv.

Section theory.

Definition rnnQ : {nonneg rat}%R -> rat -> Type := fun e1 e2 => e1%:num%R = e2.

Definition rposQ : {posnum rat}%R -> rat -> Type := fun e1 e2 => e1%:num%R = e2.

Definition Rseqjse :
  ({nonneg rat}%R * (\bar rat * (rat * \bar rat))) ->
  (rat * (\bar rat * (rat * \bar rat))) -> Type :=
  prod_R rnnQ eq.

Definition Rseqjse2 :
  ({nonneg rat}%R * ((\bar rat * (rat * \bar rat)) * (\bar rat * (rat * \bar rat)))) ->
  (rat * ((\bar rat * (rat * \bar rat)) * (\bar rat * (rat * \bar rat)))) ->
  Type :=
  prod_R rnnQ eq.

Definition Rseqjs : JS -> @seqjs rat -> Type := list_R Rseqjse.

Definition Rseqjs_pre : preJS -> @seqjs rat -> Type := list_R Rseqjse.

Definition Rseqjs' :
  seq ({nonneg rat}%R * (\bar rat * (rat * \bar rat))) -> @seqjs rat -> Type :=
  list_R Rseqjse.

Definition Rseqjs2_pre :
    preJST ((\bar rat * (rat * \bar rat)) * (\bar rat * (rat * \bar rat))) ->
    @seqjs2 rat -> Type :=
  list_R Rseqjse2.

Definition Rseqjs2' :
  seq ({nonneg rat}%R * ((\bar rat * (rat * \bar rat)) * (\bar rat * (rat * \bar rat)))) ->
  @seqjs2 rat -> Type :=
  list_R Rseqjse2.

Local Instance zero_rat : zero_of rat := 0%R.
Local Instance cast_of_nat_rat : cast_of nat rat := fun n => n%:R%R.
Local Instance add_rat : add_of rat := +%R.
Local Instance sub_rat : sub_of rat := (fun x y => x - y)%R.
Local Instance mul_rat : mul_of rat := *%R.
Local Instance div_rat : div_of rat := (fun x y => x / y)%R.
Local Instance max_rat : max_of rat := Num.max.
Local Instance min_rat : min_of rat := Num.min.
Local Instance eq_rat : eq_of rat := eqtype.eq_op.
Local Instance lt_rat : lt_of rat := Num.lt.
Local Instance le_rat : leq_of rat := Num.le.

Local Instance refines_eqxx T (x : T) : refines eq x x.
Proof. by rewrite refinesE. Qed.

Local Instance Req_Q (x : rat) : refines eq x x.
Proof. by rewrite refinesE. Qed.

Local Instance Req_Qbar (x : \bar rat) : refines eq x x.
Proof. by rewrite refinesE. Qed.

Local Instance rnnQ_nngnum (x : {nonneg rat}%R) : refines rnnQ x x%:nngnum%R.
Proof. by rewrite refinesE. Qed.

Local Instance rnnQ_add (x y : {nonneg rat}%R) :
  refines rnnQ (x%:num + y%:num)%:nng%R (x%:num%R + y%:num%R)%C.
Proof. by rewrite refinesE. Qed.

Local Instance rposQ_num (x : {posnum rat}%R) : refines rposQ x x%:num%R.
Proof. by rewrite refinesE. Qed.

Lemma RlennQ x y x' y' : rnnQ x x' -> rnnQ y y' ->
  bool_R (x%:num <= y%:num)%R (x' <= y')%C.
Proof. by move=> -> ->; rewrite /leq_op /le_rat; case: leP. Qed.

Local Instance Rnnq_eq x x' : refines rnnQ x x' -> refines eq x%:num%R x'.
Proof. by rewrite !refinesE. Qed.

Local Instance Rposq_eq x x' : refines rposQ x x' -> refines eq x%:num%R x'.
Proof. by rewrite !refinesE. Qed.

Local Instance Rlt_rat : refines (eq ==> eq ==> bool_R)%rel lt_op lt_rat.
Proof. by rewrite refinesE /lt_op => x _ <- y _ <-; case: lt_rat. Qed.

Local Instance Rleq_rat : refines (eq ==> eq ==> bool_R)%rel leq_op le_rat.
Proof. by rewrite refinesE /leq_op => x _ <- y _ <-; case: le_rat. Qed.

Local Instance Rseqjse_fst x x' yrs : refines rnnQ x x' ->
  refines Rseqjse (x, yrs) (x', yrs).
Proof. by rewrite !refinesE => ?; constructor. Qed.

Local Instance Rseqjs_Rseqjs_pre x y :
  refines Rseqjs x y -> refines Rseqjs_pre x y.
Proof. by rewrite !refinesE. Qed.

Local Instance Rseqjs_pre_Rseqjs' x y :
  refines Rseqjs_pre x y -> refines Rseqjs' x y.
Proof. by rewrite !refinesE. Qed.

Local Instance Rseqjs'_empty : refines Rseqjs' [::] [::].
Proof. rewrite refinesE; constructor. Qed.

Program Definition JS_of_seqjs (a : @seqjs rat) (Hne : seqjs_not_empty a)
        (Hh0 : seqjs_head_is_0 a) (Hs : seqjs_sorted a) :=
  Build_JS
    (Build_preJST
       [seq (@NngNum _ (nth default_seqjs a i).1 _, (nth default_seqjs a i).2)
       | i <- iota 0 (size a)] _ _) _.
Next Obligation.
move=> a Hne Hh0 Hs i; have: all (>= 0)%R (map fst a).
{ case: a {Hne} Hh0 Hs => [//|h t] => /= /eqP H0 Hp.
  apply/andP; split.
  { by rewrite H0. }
  apply: (order_path_min le_trans).
  move: Hp.
  rewrite (@eq_path _ _ (relpre fst <%R)) // -path_map.
  rewrite H0; apply: sub_path => x y; exact: ltW. }
case: (leqP (size a) i) => Hi.
{ by rewrite nth_default. }
rewrite all_map => /allP; apply; exact: mem_nth.
Qed.
Next Obligation. by case. Qed.
Next Obligation.
move=> a Hne Hh0 Hs; have: sorted (fun x y => (x.1 < y.1)%C) a by [].
have {1}-> : a = [seq (nth default_seqjs a i) | i <- iota 0 (size a)].
{ by rewrite map_nth_iota0// take_size. }
rewrite !sorted_map.
by apply: sub_sorted => i j.
Qed.
Next Obligation.
move=> a Hne Hh0 Hs; apply/eqP/val_inj.
by case: a Hne Hh0 Hs => [//|[x yrs] a] /= _ + _ => /eqP/=->.
Qed.

Local Instance Rseqjs_JS_of_seqjs (a : @seqjs rat) (Hne : seqjs_not_empty a)
      (Hh0 : seqjs_head_is_0 a) (Hs : seqjs_sorted a) :
  refines Rseqjs (@JS_of_seqjs a Hne Hh0 Hs) a.
Proof.
rewrite refinesE.
rewrite /Rseqjs /JS_of_seqjs /=.
have {5}-> : a = [seq (nth default_seqjs a i) | i <- iota 0 (size a)].
{ by rewrite map_nth_iota0// take_size. }
apply: map_R; [|by apply: iota_R; exact: nat_Rxx].
move=> i _ /nat_R_eq <-.
rewrite {3}(surjective_pairing (nth default_seqjs a i)); split=> [|//].
by apply/val_inj.
Qed.

Local Instance Rseqjs_below :
  refines
    (Rseqjs ==> rnnQ ==> Rseqjs)%rel
    JS_below seqjs_below.
Proof.
rewrite refinesE /Rseqjs /JS_below /JS_below_subdef /seqjs_below.
move=> -[[a1 _ _] _] a2 /= ra l1 l2 rl /=.
by apply: filter_R ra => _ _ [x1 x2 rx ? ? _] /=; apply: RlennQ.
Qed.

Local Instance Rseqjs_default_seqjs :
  refines Rseqjse default_JS default_seqjs.
Proof. by rewrite refinesE; split; [rewrite /rnnQ|split; constructor]. Qed.

Local Instance Rseqjs_zero :
  refines Rseqjs JS_zero seqjs_zero.
Proof. by rewrite refinesE; constructor=> //; constructor. Qed.

Local Instance Rseqjs_0 :
  refines Rseqjs JS_0 seqjs_0.
Proof. by rewrite refinesE; constructor=> //; constructor. Qed.

Local Instance Rseqjs_affine :
  refines (eq ==> eq ==> Rseqjs)%rel JS_affine seqjs_affine.
Proof.
rewrite refinesE => r _ <- s _ <-; constructor; [|by constructor].
by constructor; [|apply: f_equal2].
Qed.

Local Instance Rseqjs_cst_inf :
  refines (rnnQ ==> eq ==> eq ==> Rseqjs_pre)%rel JS_cst_inf seqjs_cst_inf.
Proof.
by rewrite refinesE => x _ <- y _ <- c _ <-; constructor; [|by constructor].
Qed.

Local Instance Rseqjs_head_seqjs :
  refines (Rseqjs' ==> Rseqjse)%rel head_JS head_seqjs.
Proof. by rewrite refinesE; apply: head_R; apply: refinesP. Qed.

Local Instance Rseqjs_last_seqjs :
  refines (Rseqjs' ==> Rseqjse)%rel last_JS last_seqjs.
Proof. by rewrite refinesE; apply: last_R; apply: refinesP. Qed.

Local Instance Rseqjs_er_opp :
  refines (@eq (\bar rat) ==> eq)%rel oppe seqjs_er_opp.
Proof.
rewrite refinesE => x _ <-; case: x => //= x.
by rewrite [X in _ = X%:E]GRing.add0r.
Qed.

Local Instance Rseqjs_er_add :
  refines (@eq (\bar rat) ==> eq ==> eq)%rel dual_adde seqjs_er_add.
Proof. by rewrite refinesE => x _ <- x' _ <-. Qed.

Local Instance Rseqjs_er_max :
  refines (eq ==> eq ==> eq)%rel Order.max seqjs_er_max.
Proof.
rewrite refinesE => x _ <- x' _ <-.
case: x => [x| |]; case: x' => [x'| |] //.
{ rewrite /seqjs_er_max /= /max_op /= /max_rat.
  rewrite /Order.max /Order.lt /= /Order.lt /=.
  by case: rat.lt_rat. }
{ by rewrite /seqjs_er_max maxEle leey. }
by rewrite /seqjs_er_max maxEle leNye.
Qed.

Local Instance Rseqjs_er_min :
  refines (eq ==> eq ==> eq)%rel Order.min seqjs_er_min.
Proof.
rewrite refinesE => x _ <- x' _ <-.
case: x => [x| |]; case: x' => [x'| |] //.
{ rewrite /seqjs_er_min /= /min_op /= /min_rat.
  rewrite /Order.min /Order.lt /= /Order.lt /=.
  by case: rat.lt_rat. }
{ by rewrite /seqjs_er_min minEle leey. }
by rewrite /seqjs_er_min minEle leNye.
Qed.

Local Instance Rseqjs_shift_rho_sigma :
  refines (eq ==> eq ==> eq)%rel shift_rho_sigma seqjs_shift_rho_sigma.
Proof. by rewrite refinesE => rs _ <- shift _ <-; case: rs => r [s| |]. Qed.

Local Instance Rseqjs_shift_y_rho_sigma :
  refines (eq ==> eq ==> eq)%rel shift_y_rho_sigma seqjs_shift_y_rho_sigma.
Proof.
rewrite refinesE => rs _ <- shift _ <-.
rewrite /shift_y_rho_sigma /seqjs_shift_y_rho_sigma.
move: Rseqjs_shift_rho_sigma; rewrite refinesE.
by move=> /(_ rs _ erefl shift _ erefl) ->.
Qed.

Local Instance Rseqjs_shift_elem :
  refines (Rseqjse ==> rnnQ ==> Rseqjse)%rel shift_JS_elem seqjs_shift_elem.
Proof.
rewrite refinesE => -[_ _] [_ _] [x _] <- yrs _ <- x' _ <-.
by rewrite /shift_JS_elem /seqjs_shift_elem/= /leq_op /le_rat; case: leP.
Qed.

Local Instance Rseqjs_above_rec :
  refines (Rseqjse ==> Rseqjs' ==> rnnQ ==> Rseqjs')%rel
    JS_above_rec_subdef seqjs_above_rec.
Proof.
rewrite refinesE => _ _ [x _ <- yrs _ <-] ? ? ra l _ <-.
rewrite /JS_above_rec_subdef /seqjs_above_rec.
elim: ra x yrs => [|_ _ [x' _ <- yrs' _ <-] a1 a2 ra IHa] x yrs.
  by constructor; apply: refinesP.
rewrite /leq_op /le_rat/=; case: leP => _; first exact: IHa.
by constructor; [|constructor]; apply: refinesP.
Qed.

Local Instance Rseqjs_above :
  refines (Rseqjs_pre ==> rnnQ ==> Rseqjs_pre)%rel JS_above seqjs_above.
Proof.
rewrite /JS_above /JS_above_subdef /seqjs_above/=.
rewrite refinesE => a1 a2 ra l _ <-.
rewrite /Rseqjs_pre/=; case: ra => [|_ _ [x _ <- yrs _ <-] {}a1 {}a2 ra].
  exact: refinesP.
by rewrite /lt_op /lt_rat; case: ltP => _; [constructor|apply: refinesP].
Qed.

Local Instance Rseqprejs_cat :
  refines (Rseqjs_pre ==> Rseqjs_pre ==> Rseqjs_pre)%rel preJS_cat seqjs_cat.
Proof.
rewrite /preJS_cat /JS_cat_subdef /seqjs_cat/=.
rewrite refinesE => a1 a2 ra b1 b2 rb; apply: cat_R => [|//].
by apply: filter_R => [_ _ [x _ <- _ _ _]/=|//]; apply: refinesP.
Qed.

Local Instance Rseqjs_cat :
  refines (Rseqjs ==> Rseqjs_pre ==> Rseqjs)%rel JS_cat seqjs_cat.
Proof.
apply: refines_abstr2 => a1 a2 /Rseqjs_Rseqjs_pre ra b1 b2 rb.
have: refines Rseqjs_pre (preJS_cat a1 b1) (seqjs_cat a2 b2).
  exact: refines_apply.
by rewrite !refinesE.
Qed.

Local Instance Rseqjs_delta :
  refines (eq ==> Rseqjs)%rel JS_delta seqjs_delta.
Proof.
rewrite /JS_delta /seqjs_delta/= refinesE /leq_op /le_rat => b _ <-.
by case: leP => b0 /=; apply: refinesP.
Qed.

Local Instance Rseqjs_normalize :
  refines (Rseqjs_pre ==> rnnQ ==> Rseqjs_pre)%rel JS_normalize seqjs_normalize.
Proof.
rewrite refinesE => a1 a2 ra l _ <-;rewrite /Rseqjs_pre/= /seqjs_normalize.
elim: ra => [|_ _ [x _ <- yrs _ <-] {}a1 {}a2 ra IH]; first by constructor.
by constructor; [|apply: IH].
Qed.

Local Instance Rseqjs_merge :
  refines (Rseqjs ==> Rseqjs ==> Rseqjs2_pre)%rel JS_merge seqjs_merge.
Proof.
rewrite /JS_merge /JS_merge_subdef /seqjs_merge.
set aux1' := fix aux' _ _ _ a' := match a' with [::] => [::] | _ => _ end.
set aux2' := fix aux' _ _ _ a' := match a' with [::] => [::] | _ => _ end.
set aux1 := fix aux _ _ _ a _ := match a with [::] => _ | _ => _ end.
set aux2 := fix aux _ _ _ a _ := match a with [::] => _ | _ => _ end.
rewrite refinesE => -[[a1 ? ?] ? a2 ra] [[a1' ? ?] ? a2' ra'].
move: ra; rewrite /Rseqjs2_pre /Rseqjs /JS_seq /preJST_seq => ra.
case: ra; [constructor|].
move=> _ _ [x _ <-] + _ <- a1'' a2'' ra'' => -[y rs].
move: ra'; rewrite /Rseqjs /JS_seq /preJST_seq => ra'.
case: ra'; [constructor|].
move=> _ _ [x1' x2' rx'] + _ <- a1''' a2''' ra''' => -[y' rs'].
constructor=> [//|].
rewrite /snd.
elim: ra'' x rs rs' a1''' a2''' ra'''.
{ move=> + + + a1''' a2''' ra'''.
  elim: ra'''; [by move=> *; constructor|].
  move=> _ _ [x'' _ <-] + _ <- a1'''' a2'''' ra'''' IH' => -[y'' rs''].
  move=> x rs rs' /=.
  by constructor; [split|exact: IH']. }
move=> _ _ [x'' _ <-] + _ <- => -[y'' rs''] a1''' a2''' ra''' IH.
move=> + + + a1'''' a2'''' ra'''' /=.
set aux1'' := fix aux'' _ _ _ a' := match a' with [::] => _ | _ => _ end.
set aux2'' := fix aux'' _ _ _ a' := match a' with [::] => _ | _ => _ end.
elim: ra''''.
{ move=> x''' rs rs'.
  constructor=> [//|].
  apply: IH; constructor. }
move=> _ _ [x''' _ <-] + _ <- => -[y''' rs'''] a1''''' a2''''' ra''''' IH'.
move=> x rs rs' /=.
rewrite /lt_op /lt_rat !num_lt.
case: ltP => Hx''x'''.
{ by constructor=> [//|]; apply: IH; constructor. }
case: ltP => Hx'''x''; last first.
{ constructor=> [//|]; exact: IH. }
by constructor.
Qed.

Local Instance Rseqjs_er_eq :
  refines (@eq (\bar rat) ==> eq ==> bool_R)%rel eqtype.eq_op seqjs_er_eq.
Proof.
rewrite refinesE => x _ <- x' _ <-; case: x => [x| |]; case: x' => [x'| |] //=.
exact: bool_Rxx.
Qed.

Local Instance Rseqjs_eqb :
  refines
    (Rseqjs ==> Rseqjs ==> rnnQ ==> bool_R)%rel
    JS_eqb seqjs_eqb.
Proof.
rewrite refinesE => a1 a2 ra a1' a2' ra' l _ <-.
rewrite /JS_eqb /seqjs_eqb; set aux := fix aux (s : seqjs) _ := _.
set m1 := JS_merge a1 a1'; set m2 := seqjs_merge a2 a2'.
have rm : Rseqjs2_pre m1 m2 by exact: refinesP.
set f1 := preJS_map (fun=> fst) m1; set f2 := seqjs_map (fun=> fst) m2.
have rf : Rseqjs_pre f1 f2 by apply: map_R rm => _ _ [x _ <- [yrs1 yrs2] _ <-].
set s1 := preJS_map (fun=> snd) m1; set s2 := seqjs_map (fun=> snd) m2.
have rs : Rseqjs_pre s1 s2 by apply: map_R rm => _ _ [x _ <- [yrs1 yrs2] _ <-].
set n11 := JS_normalize f1 _; set n12 := seqjs_normalize f2 _.
have rn1 : Rseqjs' n11 n12 by exact: refinesP.
set n21 := JS_normalize s1 _; set n22 := seqjs_normalize s2 _.
have rn2 : Rseqjs' n21 n22 by exact: refinesP.
case: n21 n22 rn2 => {}n21 p q; rewrite -[preJST_seq _]/n21 => n22 {p q}.
elim: rn1 n21 n22 => [|_ _ [x _ <- yrs _ <-] {}n11 {}n12 rn1 IH] n21 n22 rn2.
  by case: rn2; constructor.
case: rn2 => [|_ _ [x' _ <- yrs' _ <-] {}n21 {}n22 rn2]; first by constructor.
by apply: andb_R; [apply: bool_Rxx|apply: IH].
Qed.

Local Instance Rseqjs_opp :
  refines (Rseqjs_pre ==> Rseqjs_pre)%rel JS_opp seqjs_opp.
Proof.
rewrite refinesE => a1 a2 ra; rewrite /Rseqjs_pre /= /seqjs_opp.
elim: ra => [|? ? [x _ <- [y [r s]] _ <-] a1' a2' ra']; constructor=> //=.
constructor=> //; apply: f_equal2; [|apply: f_equal2];
  [|by rewrite /sub_op /sub_rat GRing.add0r|]; exact: refinesP.
Qed.

Local Instance Rseqjs_eval :
  refines (Rseqjs' ==> rnnQ ==> eq)%rel JS_eval seqjs_eval.
Proof.
rewrite refinesE => a1 a2 ra l _ <-.
case: ra => [//|_ _ [x _ <- yrs _ <-] a1' a2' ra']/=.
elim: ra' x yrs => [|_ _ [x' _ <- yrs' _ <-] a1'' a2'' ra'' IHa] x yrs /=.
  by rewrite /leq_op /le_rat; case: leP.
by rewrite /leq_op /lt_op /le_rat /lt_rat; case: ifP => _ //; case: ifP.
Qed.

Local Instance Rseqjs_max_val :
  refines (Rseqjs' ==> rnnQ ==> eq)%rel JS_max_val seqjs_max_val.
Proof.
rewrite refinesE => a1 a2 ra l _ <-.
case: ra => [//|_ _ [x _ <- yrs _ <-] {}a1 {}a2 ra]/=.
elim: ra x yrs => [//|_ _ [x' _ <- [y' rs'] _ <-] {}a1 {}a2 ra IHa] x [y [r s]].
apply: refinesP; apply: refines_apply.
by rewrite refinesE; apply: IHa.
Qed.

Local Instance Rseqjs_min_val :
  refines (Rseqjs_pre ==> rnnQ ==> eq)%rel JS_min_val seqjs_min_val.
Proof.
rewrite refinesE => a1 a2 ra l _ <-.
by rewrite /JS_min_val /seqjs_min_val; apply: refinesP.
Qed.

Local Instance Rseqjs_add_cst :
  refines (@eq (\bar rat) ==> Rseqjs ==> Rseqjs)%rel JS_add_cst seqjs_add_cst.
Proof.
rewrite refinesE => c _ <- a1 a2 ra.
rewrite /Rseqjs /= /seqjs_add_cst.
by elim: ra => [|? ? [x _ <- yrs _ <-] a1' a2' ra']; constructor.
Qed.

Local Instance Rseqjs_add :
  refines (Rseqjs ==> Rseqjs ==> Rseqjs)%rel JS_add seqjs_add.
Proof.
rewrite refinesE => a1 a2 ra a1' a2' ra'.
rewrite /JS_add /JS_add_subdef /seqjs_add.
have: Rseqjs2_pre (JS_merge a1 a1') (seqjs_merge a2 a2').
{ apply: refinesP. }
rewrite /Rseqjs2_pre /Rseqjs /JS_merge/=.
by elim=> [|_ _ [x _ <- yrs _ <-] a1'' a2'' ra2'' IH]; constructor.
Qed.

Local Instance Rseqjs_y_rho_sigma_min :
  refines (eq ==> eq ==> rnnQ ==> rnnQ ==> Rseqjs')%rel
    y_rho_sigma_min seqjs_y_rho_sigma_min.
Proof.
rewrite refinesE => yrs _ <- yrs' _ <- x _ <- x' _ <-.
rewrite /y_rho_sigma_min /seqjs_y_rho_sigma_min.
case: yrs.2.2 => [yrs22||]; last first.
- by constructor; constructor=> //; congr (_, _); apply: refinesP.
- by case: yrs'.2.2 => [yrs'22||];
    constructor; constructor=> //; congr (_, _); apply: refinesP.
case: yrs'.2.2 => [yrs'22||]; last first.
- by constructor; constructor=> //; congr (_, _); apply: refinesP.
- by constructor; constructor=> //; congr (_, _); apply: refinesP.
rewrite /eq_op /eq_rat; case: eqP => _.
  by constructor; constructor=> //; congr (_, _); apply: refinesP.
rewrite /leq_op /le_rat /div_op /div_rat /add_op /add_rat /sub_op /sub_rat.
set x'' := (_ + _ / _)%R.
case: leP => [_|x''gtx'].
  by constructor; constructor=> //; congr (_, _); apply: refinesP.
case: leP => _.
  by constructor; constructor=> //; congr (_, _); apply: refinesP.
have x''ge0 : (0 <= x'')%R by apply: le_trans (ltW x''gtx').
case: leP => _.
  constructor; constructor=> //; try constructor=> //.
    by congr (_, _); apply: refinesP.
  by rewrite /rnnQ insubdK.
constructor; constructor=> //; try constructor=> //.
  by congr (_, _); apply: refinesP.
by rewrite /rnnQ insubdK.
Qed.

Local Instance Rseqjs_min_rec :
  refines (rnnQ ==> eq ==> eq ==> Rseqjs2' ==> rnnQ ==> Rseqjs')%rel
    JS_min_rec_subdef seqjs_min_rec.
Proof.
rewrite refinesE => + _ <- + _ <- + _ <- a1 a2 ra l _ <-.
elim: ra => [|_ _ [x' _ <- [yrsl yrsr] _ <-] {}a1 {}a2 ra IHa] x yrs yrs'/=.
  exact: refinesP.
by apply: cat_R; [apply: refinesP|apply: IHa].
Qed.

Local Instance Rseqjs_min :
  refines (Rseqjs ==> Rseqjs ==> rnnQ ==> Rseqjs)%rel JS_min seqjs_min.
Proof.
rewrite refinesE => a1 a2 ra b1 b2 rb l _ <-.
have: Rseqjs2_pre (JS_merge a1 b1) (seqjs_merge a2 b2).
{ apply: refinesP. }
rewrite /Rseqjs2_pre /Rseqjs /JS_min /JS_min_subdef /seqjs_min /JS_merge/=.
case=> [|_ _ [x _ <- [y rs] _ <-] a1'' a2'' ra2''].
  by constructor.
exact: refinesP.
Qed.

Local Instance Rseqjs_inf_left :
  refines (Rseqjs_pre ==> Rseqjs)%rel JS_inf_left seqjs_inf_left.
Proof.
rewrite refinesE /Rseqjs /JS_inf_left /seqjs_inf_left => a1 a2 ra /=.
case: ra => [|_ _ [x _ <- yrs _ <-] {}a1 {}a2 ra]/=.
  by constructor.
rewrite /lt_op /lt_rat /zero_op /zero_rat; case: ltP => _.
  by constructor; [apply: refinesP|constructor].
by constructor.
Qed.

Local Instance Rseqjs_inf_left_eq :
  refines (Rseqjs_pre ==> Rseqjs)%rel JS_inf_left_eq seqjs_inf_left_eq.
Proof.
rewrite refinesE /Rseqjs /JS_inf_left_eq /seqjs_inf_left_eq => a1 a2 ra /=.
case: ra => [|_ _ [x _ <- yrs _ <-] {}a1 {}a2 ra]/=.
  by constructor; constructor.
rewrite /lt_op /lt_rat /zero_op /zero_rat; case: ltP => _.
  by constructor; [apply: refinesP|constructor].
by constructor.
Qed.

Local Instance Rseqjs_point :
  refines (rnnQ ==> @eq (\bar rat) ==> Rseqjs)%rel JS_point seqjs_point.
Proof.
rewrite refinesE /JS_point /seqjs_point => x _ <- y _ <- /=.
apply: refinesP; apply: refines_apply.
by rewrite refinesE /Rseqjs_pre/=; constructor; constructor.
Qed.

Local Instance Rseqjs_segment :
  refines
    (rnnQ ==> @eq (\bar rat) ==> @eq rat ==> @eq (\bar rat) ==> rnnQ ==> bool_R
       ==> Rseqjs)%rel
    JS_segment seqjs_segment.
Proof.
rewrite refinesE => x _ <- y _ <- r _ <- s _ <- l _ <- linf1 linf2 rlinf.
rewrite /JS_segment /seqjs_segment /leq_op /le_rat; case: leP => lx.
  exact: refinesP.
apply: refinesP; apply: refines_apply.
rewrite refinesE /Rseqjs_pre/=; constructor; constructor=> //.
  by case: rlinf.
by constructor.
Qed.

Local Instance Rseqjs_min_conv_point :
  refines
    (rnnQ ==> @eq (\bar rat) ==> rnnQ ==> @eq (\bar rat) ==> Rseqjs)%rel
    min_conv_point seqjs_min_conv_point.
Proof.
rewrite refinesE => x _ <- y _ <- x' _ <- y' _ <-.
rewrite /min_conv_point /seqjs_min_conv_point.
exact: refinesP.
Qed.

Local Instance Rseqjs_min_conv_point_affine :
  refines
    (rnnQ ==> eq ==> rnnQ ==> eq ==> eq ==> rnnQ ==> bool_R ==> Rseqjs)%rel
    min_conv_point_affine seqjs_min_conv_point_affine.
Proof.
rewrite refinesE => x _ <- y _ <- x' _ <- r' _ <- s' _ <- l' _ <- li1 li2 rli.
rewrite /min_conv_point_affine /seqjs_min_conv_point_affine.
exact: refinesP.
Qed.

Local Instance Rseqjs_min_conv_affine :
  refines
    (eq ==> eq ==> rnnQ ==> rnnQ ==> bool_R ==>
     eq ==> eq ==> rnnQ ==> rnnQ ==> bool_R ==> Rseqjs)%rel
    min_conv_affine seqjs_min_conv_affine.
Proof.
rewrite refinesE => r _ <- s _ <- x _ <- l _ <- li1 li2 rli.
move=> r' _ <- s' _ <- x' _ <- l' _ <- l'i1 l'i2 rl'i.
rewrite /min_conv_affine /seqjs_min_conv_affine.
apply: refinesP; apply: refines_apply.
rewrite refinesE /Rseqjs_pre/= /min_conv_affine_subdef.
rewrite /leq_op /le_rat; case: ifP => _.
  exact: refinesP.
rewrite /eq_op /eq_rat; case: eqP => _.
  exact: refinesP.
rewrite /lt_op /lt_rat; case: ltP => _.
  constructor; constructor=> //; constructor; constructor=> //.
  have -> : li1 || l'i1 = li2 || l'i2; last by case: orb.
  by case: rli; case: rl'i.
constructor; constructor=> //; constructor; constructor=> //.
have -> : li1 || l'i1 = li2 || l'i2; last by case: orb.
by case: rli; case: rl'i.
Qed.

Local Instance Rseqjs_min_conv_segment :
  refines
    (rnnQ ==> eq ==> eq ==> eq ==> rnnQ ==> bool_R ==>
     rnnQ ==> eq ==> eq ==> eq ==> rnnQ ==> bool_R ==> rnnQ ==> Rseqjs)%rel
    min_conv_segment seqjs_min_conv_segment.
Proof.
rewrite refinesE => x _ <- y _ <- r _ <- s ç <- l _ <- li1 li2 rli.
move=> x' _ <- y' _ <- r' _ <- s' _ <- l' _ <- l'i1 l'i2 rl'i l'' _ <-.
rewrite /min_conv_segment /seqjs_min_conv_segment.
by rewrite /leq_op /le_rat; case: leP => _; case: leP => _; apply: refinesP.
Qed.

Local Instance Rseqjs_min_conv_segment_js_rec :
  refines
    (rnnQ ==> eq ==> eq ==> eq ==> rnnQ ==> bool_R ==>
     rnnQ ==> eq ==> eq ==> eq ==> Rseqjs' ==> rnnQ ==> Rseqjs)%rel
    min_conv_segment_JS_rec_subdef seqjs_min_conv_segment_js_rec.
Proof.
rewrite refinesE => x _ <- y _ <- r _ <- s _ <- l _ <- li1 li2 rli.
move=> + _ <- + _ <- + _ <- + _ <- a1 a2 ra l' _ <-.
rewrite /min_conv_segment_JS_rec_subdef /seqjs_min_conv_segment_js_rec.
elim: ra => [|_ _ [x'' _ <- [y'' [r'' s'']] _ <-] {}a1 {}a2 ra IHa] x' y' r' s'.
  exact: refinesP.
apply: refinesP; apply: refines_apply; apply: refines_apply.
by rewrite refinesE; apply: IHa.
Qed.

Local Instance Rseqjs_min_conv_segment_js :
  refines
    (rnnQ ==> eq ==> eq ==> eq ==> rnnQ ==> bool_R ==>
     Rseqjs' ==> rnnQ ==> Rseqjs)%rel
    min_conv_segment_JS seqjs_min_conv_segment_js.
Proof.
rewrite refinesE => x _ <- y _ <- r _ <- s _ <- l _ <- li1 li2 rli.
move=> a1 a2 ra l' _ <-.
case: ra => [|[_ _ [_ _ [x' _ <- [y' [r' s']] _ <-]]] {}a1 {}a2 ra]/=.
  exact: refinesP.
exact: refinesP.
Qed.

Local Instance Rseqjs_min_conv_rec :
  refines
    (rnnQ ==> eq ==> eq ==> eq ==> Rseqjs' ==> Rseqjs' ==> rnnQ ==> Rseqjs)%rel
    JS_min_conv_rec_subdef seqjs_min_conv_rec.
Proof.
rewrite refinesE => + _ <- + _ <- + _ <- + _ <- a1 a2 ra b1 b2 rb l _ <-.
elim: ra => [|? ? [x' _ <- [y' [r' s']] _ <-] {}a1 {}a2 ra IHa]/= x y r s.
  apply: refinesP.
apply: refinesP; apply: refines_apply; apply: refines_apply.
by rewrite refinesE; apply: IHa.
Qed.

Local Instance Rseqjs_min_conv :
  refines (Rseqjs' ==> Rseqjs' ==> rnnQ ==> Rseqjs)%rel
    JS_min_conv seqjs_min_conv.
Proof.
rewrite refinesE => a1 a2 ra b1 b2 rb l _ <-.
by case: ra => [|_ _ [x _ <- [y [r s]] _ <-] {}a1 {}a2 ra]/=; apply: refinesP.
Qed.

Section param.

Context (C : Type) (rratC : rat -> C -> Type).
Context `{!spec_of C rat}.
Context `{!refines (eq ==> rratC)%rel spec spec_id}.

Definition rratCe :
  (rat * (\bar rat * (rat * \bar rat))) ->
  (C * (\bar C * (C * \bar C))) -> Type :=
  prod_R rratC (prod_R (extended_R rratC) (prod_R rratC (extended_R rratC))).

Definition seqjs_spec (s : @seqjs C) : @seqjs rat :=
  [seq (spec x.1, (er_map spec x.2.1, (spec x.2.2.1, er_map spec x.2.2.2)))
  | x <- s].

Lemma rratC_spec x : rratC (spec x) x.
Proof. by rewrite -{2}[x]/(spec_id x); apply: refinesP. Qed.

Lemma RseqjsC_spec :
  refines (eq ==> list_R rratCe)%rel seqjs_spec spec_id.
Proof.
rewrite refinesE => a _ <-.
rewrite /spec_id -{2}(map_id a).
apply: (@map_R _ _ eq); [|exact: list_Rxx].
move=> + _ <- => -[x [y [r s]]] /=; split; [|split; [|split]].
{ exact: rratC_spec. }
{ case: y => [y||]; constructor; exact: rratC_spec. }
{ exact: rratC_spec. }
case: s => [s||]; constructor; exact: rratC_spec.
Qed.

End param.
End theory.
