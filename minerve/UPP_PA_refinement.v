From mathcomp Require Import all_ssreflect.
From mathcomp Require Import ssrint ssralg ssrnum rat boolp.
From mathcomp Require Import reals ereal signed.
From CoqEAL Require Import hrel param refinements binrat.
From mathcomp.dioid Require Import complete_dioid.
From NCCoq Require Import analysis_complements RminStruct.
Require Import NCCoq.deviations.
Require Import ratdiv jump_sequences PA UPP UPP_PA PA_refinement.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.

Import Refinements.Op.

Section generic.

Context {C : Type}.
Context `{!zero_of C, !cast_of nat C}.
Context `{!add_of C, !sub_of C, !mul_of C, !div_of C, !ceil_of C}.
Context `{!max_of C, !min_of C, !lcm_of C}.
Context `{!eq_of C, !lt_of C, !leq_of C}.

Record sequpp := {
  sequpp_T : C;
  sequpp_d : C;
  sequpp_c : C;
  sequpp_js : @seqjs C
}.

Definition sequpp_T_nonneg (f : sequpp) := (0 <= sequpp_T f)%C.

Definition sequpp_d_pos (f : sequpp) := (0 < sequpp_d f)%C.

Definition sequpp_last_js_le_T_plus_d (f : sequpp) :=
  ((last_seqjs (sequpp_js f)).1 <= sequpp_T f + sequpp_d f)%C.

Definition sequpp_shift_elem (xyrs : seqjs_elem) (d c : C) (n : nat) :=
  let n := cast_op n in
  let x := (xyrs.1 + n * d)%C in
  let y := seqjs_er_add xyrs.2.1 (n * c)%C%:E in
  let s := seqjs_er_add xyrs.2.2.2 (n * c)%C%:E in
  (x, (y, (xyrs.2.2.1, s))).

Definition seqjs_shift_period (a : seqjs) (d c : C) (n : nat) : seqjs :=
  [seq sequpp_shift_elem x d c n | x <- a].

Let Td (f : sequpp) := (sequpp_T f + sequpp_d f)%C.
Let Tdd (f : sequpp) := (Td f + sequpp_d f)%C.

Definition sequpp_further_period (f : sequpp) :=
  let: suf := seqjs_above (sequpp_js f) (sequpp_T f) in
  let: y := let: l := last_seqjs suf in (seqjs_shift_elem l (Td f)).2.1 in
  let: suf' := seqjs_shift_period suf (sequpp_d f) (sequpp_c f) 1 in
  let: (_, (_, rs)) := head_seqjs suf' in
  (Td f, (y, rs)) :: behead suf'.

Definition sequpp_js_upto_subdef (f : sequpp) (l : C) :=
  let fp := sequpp_further_period f in
  let sp a n := seqjs_shift_period a (sequpp_d f) (sequpp_c f) n in
  let fix aux (n n' : nat) :=
    match n with
    | 0 => sp fp n'
    | n.+1 => seqjs_cat (sp fp n') (aux n n'.+1)
    end in
  let n := ceil_op ((l - Td f) %/ sequpp_d f)%C in
  if n isn't n'.+1 then sequpp_js f else
    seqjs_cat (sequpp_js f) (aux n' 0%N).

Definition sequpp_js_upto_subdef' (f : sequpp) (l : C) :=
  seqjs_below (sequpp_js_upto_subdef f l) l.

Definition sequpp_is_ultimately_affine f :=
  let: (x, (_, (r, _))) := last_seqjs (sequpp_js f) in
  (x <= sequpp_T f)%C && (sequpp_d f * r == sequpp_c f)%C.

Definition sequpp_js_upto f l :=
  if sequpp_is_ultimately_affine f && (sequpp_T f <= l)%C then sequpp_js f
  else sequpp_js_upto_subdef' f l.

Definition sequpp_opp f :=
  Build_sequpp
    (sequpp_T f) (sequpp_d f) (0 - sequpp_c f)%C
    (seqjs_opp (sequpp_js f)).

Definition sequpp_oopp := omap sequpp_opp.

Definition sequpp_add_cst c f :=
  Build_sequpp
    (sequpp_T f) (sequpp_d f) (sequpp_c f)
    (seqjs_add_cst c (sequpp_js f)).

Definition sequpp_add (f f' : sequpp) : sequpp :=
  let T := max_op (sequpp_T f) (sequpp_T f') in
  let d := lcm_op (sequpp_d f) (sequpp_d f') in
  let l := (T + d)%C in
  Build_sequpp
    T
    d
    (d * (sequpp_c f %/ sequpp_d f + sequpp_c f' %/ sequpp_d f'))%C
    (seqjs_add (sequpp_js_upto f l) (sequpp_js_upto f' l)).

Definition sequpp_oadd f f' :=
  match f, f' with
  | Some f, Some f' => Some (sequpp_add f f')
  | _, _ => None
  end.

Definition sequpp_min' (f f' : sequpp) : option sequpp :=
  let aux f f' T d c :=
    let l := (T + d)%C in
    let ejs := sequpp_js_upto f l in
    let ejs' := sequpp_js_upto f' l in
    let jsm := seqjs_min ejs ejs' l in
    Some (Build_sequpp T d c jsm) in
  let T := sequpp_T f in let T' := sequpp_T f' in
  let d := sequpp_d f in let d' := sequpp_d f' in
  let c := sequpp_c f in let c' := sequpp_c f' in
  let cd := (c %/ d)%C in let cd' := (c' %/ d')%C in
  if (cd == cd')%C then
    let T := max_op T T' in
    let d := lcm_op d d' in
    let c := (d * cd)%C in
    aux f f' T d c
  else
    let js := sequpp_js f in let js' := sequpp_js f' in
    let Td := (T + d)%C in let Td' := (T' + d')%C in
    let max_on js x y := seqjs_max_val (seqjs_above js x) y in
    let min_on js x y := seqjs_min_val (seqjs_above js x) y in
    if (cd < cd')%C then
      match max_on (seqjs_add js (seqjs_affine (0 - cd)%C 0%C)) T Td,
            min_on (seqjs_add js' (seqjs_affine (0 - cd')%C 0%C)) T' Td' with
      | M%:E, m%:E =>
        let T := max_op (max_op T T') (div_op (M - m) (cd' - cd))%C in
        aux f f' T d c
      | _, _ => None
      end
    else (* cd' < cd *)
      match max_on (seqjs_add js' (seqjs_affine (0 - cd')%C 0%C)) T' Td',
            min_on (seqjs_add js (seqjs_affine (0 - cd)%C 0%C)) T Td with
      | M%:E, m%:E =>
        let T := max_op (max_op T' T) (div_op (M - m) (cd - cd'))%C in
        aux f' f T d' c'
      | _, _ => None
      end.

Definition sequpp_change_d_c f d c :=
  let T := sequpp_T f in
  let a := sequpp_js f in
  let yT := seqjs_eval a T in
  let yTd := seqjs_eval a (Td f) in
  Build_sequpp
    T d c
    (seqjs_cat (seqjs_below a T) (seqjs_cst_inf T yT yTd)).

Definition sequpp_is_infinite_on_period f :=
  let js := seqjs_above (sequpp_js f) (sequpp_T f) in
  (match seqjs_min_val js (Td f) with
   | +oo%E => true | _ => false end)
  || (match seqjs_max_val js (Td f) with
      | -oo%E => true | _ => false end).

Definition sequpp_preprocess_d_c f f' :=
  if sequpp_is_infinite_on_period f then
    (sequpp_change_d_c f (sequpp_d f') (sequpp_c f'), f')
  else if sequpp_is_infinite_on_period f' then
    (f, sequpp_change_d_c f' (sequpp_d f) (sequpp_c f))
  else (f, f').

Definition sequpp_min f f' :=
  let ff' := sequpp_preprocess_d_c f f' in
  sequpp_min' ff'.1 ff'.2.

Definition sequpp_omin f f' :=
  match f, f' with
  | Some f, Some f' => sequpp_min f f'
  | _, _ => None
  end.

Definition sequpp_f1 (f : sequpp) (d1 c1 : C) : sequpp :=
  let a := sequpp_js f in
  let T := sequpp_T f in
  Build_sequpp T d1 c1
    (seqjs_cat (seqjs_below a T) (seqjs_cst_inf T (seqjs_eval a T) +oo%E)).

Definition sequpp_f2 (f : sequpp) : sequpp :=
  Build_sequpp
    (sequpp_T f) (sequpp_d f) (sequpp_c f)
    (seqjs_inf_left_eq (seqjs_above (sequpp_js f) (sequpp_T f))).

Definition sequpp_f1_f1' (f f' : sequpp) (d1 c1 : C) : sequpp :=
  let TT' := (sequpp_T f + sequpp_T f')%C in
  let l := (TT' + d1)%C in
  let ejs := sequpp_js_upto (sequpp_f1 f d1 c1) l in
  let ejs' := sequpp_js_upto (sequpp_f1 f' d1 c1) l in
  let jsmc := seqjs_min_conv ejs ejs' l in
  Build_sequpp TT' d1 c1 jsmc.

Definition sequpp_f1_f2' (f f' : sequpp) : sequpp :=
  let TT' := (sequpp_T f + sequpp_T f')%C in
  let l := (TT' + (sequpp_d f'))%C in
  let ejs := sequpp_js_upto (sequpp_f1 f (sequpp_d f') (sequpp_c f')) l in
  let ejs' := sequpp_js_upto (sequpp_f2 f') l in
  let jsmc := seqjs_min_conv ejs ejs' l in
  Build_sequpp TT' (sequpp_d f') (sequpp_c f') jsmc.

Definition sequpp_f2_f2' (f f' : sequpp) : sequpp :=
  let lcm := lcm_op (sequpp_d f) (sequpp_d f') in
  let cd := (sequpp_c f %/ sequpp_d f)%C in
  let cd' := (sequpp_c f' %/ sequpp_d f')%C in
  let TT'lcm := (sequpp_T f + sequpp_T f' + lcm)%C in
  let l := (TT'lcm + lcm)%C in
  let ejs := sequpp_js_upto (sequpp_f2 f) l in
  let ejs' := sequpp_js_upto (sequpp_f2 f') l in
  let jsmc := seqjs_min_conv ejs ejs' l in
  Build_sequpp TT'lcm lcm (lcm * min_op cd cd')%C jsmc.

Definition sequpp_min_conv' (f f' : sequpp) : option sequpp :=
  let f1f1' := sequpp_f1_f1' f f' (sequpp_d f) (sequpp_c f) in
  let f2f1' := sequpp_f1_f2' f' f in
  let f1f2' := sequpp_f1_f2' f f' in
  let f2f2' := sequpp_f2_f2' f f' in
  obind
    (fun r1 =>
       obind
         (fun r2 => sequpp_min r2 f2f2')
         (sequpp_min r1 f1f2'))
    (sequpp_min f1f1' f2f1').

Definition sequpp_min_conv (f f' : sequpp) : option sequpp :=
  let ff' := sequpp_preprocess_d_c f f' in
  sequpp_min_conv' ff'.1 ff'.2.

Definition sequpp_omin_conv f f' :=
  match f, f' with
  | Some f, Some f' => sequpp_min_conv f f'
  | _, _ => None
  end.

Definition sequpp_eqb' (f f' : sequpp) : bool :=
  (sequpp_c f %/ sequpp_d f == sequpp_c f' %/ sequpp_d f')%C &&
  let T := max_op (sequpp_T f) (sequpp_T f') in
  let d := lcm_op (sequpp_d f) (sequpp_d f') in
  let l := (T + d)%C in
  seqjs_eqb (sequpp_js_upto f l) (sequpp_js_upto f' l) l.

Definition sequpp_eqb (f f' : sequpp) : bool :=
  let ff' := sequpp_preprocess_d_c f f' in
  sequpp_eqb' ff'.1 ff'.2.

Definition sequpp_oeqb f f' :=
  match f, f' with
  | Some f, Some f' => sequpp_eqb f f'
  | _, _ => false
  end.

Definition sequpp_leb f f' :=
  match sequpp_min f f' with
  | None => false
  | Some f'' => sequpp_eqb f f''
  end.

Definition sequpp_oleb f f' :=
  match f, f' with
  | Some f, Some f' => sequpp_leb f f'
  | _, _ => false
  end.

Definition sequpp_0 d' := Build_sequpp 0%C d' 0%C seqjs_0.

Definition sequpp_delta (b d c : C) : sequpp :=
  Build_sequpp (max_op 0%C b) d c (seqjs_delta b).

Definition sequpp_non_decr_closure f :=
  omap sequpp_opp (sequpp_min_conv (sequpp_opp f) (sequpp_0 (sequpp_d f))).

Definition sequpp_onon_decr_closure := obind sequpp_non_decr_closure.

Definition sequpp_non_neg (f : sequpp) : bool :=
  sequpp_leb (sequpp_0 (sequpp_d f)) f.

Definition sequpp_non_decr (f : sequpp) : bool :=
  oapp (sequpp_leb^~ f) false (sequpp_non_decr_closure f).

Definition sequpp_hDev_bounded f f' d :=
  match d with +oo%E => true | -oo%E => false | d%:E =>
    [&& (0 <= d)%C, sequpp_non_neg f, sequpp_non_decr f &
      let deltad := sequpp_delta d (sequpp_d f) (sequpp_c f) in
      oapp (sequpp_leb ^~ f') false (sequpp_min_conv f deltad)]
  end.

Definition sequpp_hDev_obounded f f' d :=
  match f, f' with
  | Some f, Some f' => sequpp_hDev_bounded f f' d
  | _, _ => false
  end.

Definition sequpp_never_pinfty f :=
  let a := sequpp_js f in
  match seqjs_eval a 0%C, seqjs_max_val a (Td f) with
  | +oo%E, _ | _, +oo%E => false
  | _, _ => true
  end.

Definition sequpp_never_ninfty f := sequpp_never_pinfty (sequpp_opp f).

Definition sequpp_vDev_bounded f f' b :=
  [&& sequpp_never_ninfty f, sequpp_never_pinfty f' &
   sequpp_leb f (sequpp_add_cst b f')].

Definition sequpp_vDev_obounded f f' b :=
  match f, f' with
  | Some f, Some f' => sequpp_vDev_bounded f f' b
  | _, _ => false
  end.

End generic.

Parametricity sequpp.
Parametricity sequpp_T_nonneg.
Parametricity sequpp_d_pos.
Parametricity sequpp_last_js_le_T_plus_d.
Parametricity sequpp_shift_elem.
Parametricity seqjs_shift_period.
Parametricity sequpp_further_period.
Parametricity sequpp_js_upto_subdef.
Parametricity sequpp_js_upto_subdef'.
Parametricity sequpp_is_ultimately_affine.
Parametricity sequpp_js_upto.
Parametricity sequpp_opp.
Parametricity sequpp_oopp.
Parametricity sequpp_add_cst.
Parametricity sequpp_add.
Parametricity sequpp_oadd.
Parametricity sequpp_min'.
Parametricity sequpp_change_d_c.
Parametricity sequpp_is_infinite_on_period.
Parametricity sequpp_preprocess_d_c.
Parametricity sequpp_min.
Parametricity sequpp_omin.
Parametricity sequpp_f1.
Parametricity sequpp_f2.
Parametricity sequpp_f1_f1'.
Parametricity sequpp_f1_f2'.
Parametricity sequpp_f2_f2'.
Parametricity sequpp_min_conv'.
Parametricity sequpp_min_conv.
Parametricity sequpp_omin_conv.
Parametricity sequpp_eqb'.
Parametricity sequpp_eqb.
Parametricity sequpp_oeqb.
Parametricity sequpp_leb.
Parametricity sequpp_oleb.
Parametricity sequpp_0.
Parametricity sequpp_delta.
Parametricity sequpp_non_decr_closure.
Parametricity sequpp_onon_decr_closure.
Parametricity sequpp_non_neg.
Parametricity sequpp_non_decr.
Parametricity sequpp_hDev_bounded.
Parametricity sequpp_hDev_obounded.
Parametricity sequpp_never_pinfty.
Parametricity sequpp_never_ninfty.
Parametricity sequpp_vDev_bounded.
Parametricity sequpp_vDev_obounded.

Section theory.

Context {R : realType}.

Variant Rsequpp : @F_UPP_PA R -> @sequpp rat -> Type :=
  Rsequpp_spec : forall (f : F_UPP_PA) (u : @sequpp rat),
    rnnQ (F_UPP_T f) (sequpp_T u) ->
    rposQ (F_UPP_d f) (sequpp_d u) ->
    eq (F_UPP_c f) (sequpp_c u) ->
    Rseqjs (F_UPP_PA_JS f) (sequpp_js u) -> Rsequpp f u.

Local Existing Instance zero_rat.
Local Existing Instance cast_of_nat_rat.
Local Existing Instance add_rat.
Local Existing Instance sub_rat.
Local Existing Instance mul_rat.
Local Existing Instance div_rat.
Local Instance ceil_rat : ceil_of rat :=
  fun x => ssrint.absz (mathcomp_complements.ceilq x).
Local Existing Instance max_rat.
Local Existing Instance min_rat.
Local Instance lcm_rat : lcm_of rat := lcm_rat.
Local Existing Instance eq_rat.
Local Existing Instance lt_rat.
Local Existing Instance le_rat.

Local Existing Instance refines_eqxx.
Local Existing Instance Req_Q.
Local Existing Instance Req_Qbar.
Local Existing Instance rnnQ_nngnum.
Local Existing Instance rnnQ_add.
Local Existing Instance rposQ_num.
Local Existing Instance Rnnq_eq.
Local Existing Instance Rposq_eq.
Local Existing Instance Rlt_rat.
Local Existing Instance Rleq_rat.
Local Existing Instance Rseqjse_fst.
Local Existing Instance Rseqjs_Rseqjs_pre.
Local Existing Instance Rseqjs_pre_Rseqjs'.
Local Existing Instance Rseqjs'_empty.
Local Existing Instance Rseqjs_JS_of_seqjs.
Local Existing Instance Rseqjs_below.
Local Existing Instance Rseqjs_default_seqjs.
Local Existing Instance Rseqjs_zero.
Local Existing Instance Rseqjs_0.
Local Existing Instance Rseqjs_affine.
Local Existing Instance Rseqjs_cst_inf.
Local Existing Instance Rseqjs_head_seqjs.
Local Existing Instance Rseqjs_last_seqjs.
Local Existing Instance Rseqjs_er_opp.
Local Existing Instance Rseqjs_er_add.
Local Existing Instance Rseqjs_er_max.
Local Existing Instance Rseqjs_er_min.
Local Existing Instance Rseqjs_shift_rho_sigma.
Local Existing Instance Rseqjs_shift_y_rho_sigma.
Local Existing Instance Rseqjs_shift_elem.
Local Existing Instance Rseqjs_above_rec.
Local Existing Instance Rseqjs_above.
Local Existing Instance Rseqjs_cat.
Local Existing Instance Rseqjs_delta.
Local Existing Instance Rseqjs_normalize.
Local Existing Instance Rseqjs_merge.
Local Existing Instance Rseqjs_er_eq.
Local Existing Instance Rseqjs_eqb.
Local Existing Instance Rseqjs_opp.
Local Existing Instance Rseqjs_eval.
Local Existing Instance Rseqjs_max_val.
Local Existing Instance Rseqjs_min_val.
Local Existing Instance Rseqjs_add_cst.
Local Existing Instance Rseqjs_add.
Local Existing Instance Rseqjs_y_rho_sigma_min.
Local Existing Instance Rseqjs_min_rec.
Local Existing Instance Rseqjs_min.
Local Existing Instance Rseqjs_inf_left.
Local Existing Instance Rseqjs_inf_left_eq.
Local Existing Instance Rseqjs_point.
Local Existing Instance Rseqjs_segment.
Local Existing Instance Rseqjs_min_conv_point.
Local Existing Instance Rseqjs_min_conv_point_affine.
Local Existing Instance Rseqjs_min_conv_affine.
Local Existing Instance Rseqjs_min_conv_segment.
Local Existing Instance Rseqjs_min_conv_segment_js_rec.
Local Existing Instance Rseqjs_min_conv_segment_js.
Local Existing Instance Rseqjs_min_conv.

Program Definition F_UPP_PA_of_sequpp_def (a : @sequpp rat)
        (Hne : seqjs_not_empty (sequpp_js a))
        (Hh0 : seqjs_head_is_0 (sequpp_js a))
        (Hs : seqjs_sorted (sequpp_js a))
        (HT : sequpp_T_nonneg a) (Hd : sequpp_d_pos a)
        (Hl : sequpp_last_js_le_T_plus_d a) :=
  @F_UPP_PA_of_JS R
    (@JS_of_seqjs (sequpp_js a) Hne Hh0 Hs)
    (@NngNum _ (sequpp_T a) _)
    (@PosNum _ (sequpp_d a) _)
    (sequpp_c a)
    _.
Next Obligation. by []. Qed.
Next Obligation. by []. Qed.
Next Obligation.
move=> a Hne Hh0 Hs HT Hd Hl; rewrite /= -nth_last size_map size_iota.
rewrite (nth_map 0%N) /=; last first.
{ rewrite size_iota ltn_predL.
  by move: Hne; case: sequpp_js. }
rewrite nth_iota ?add0n; last first.
{ by move: Hne; case: sequpp_js. }
by rewrite nth_last.
Qed.
Fact F_UPP_PA_of_sequpp_key : unit. Proof. by []. Qed.
Definition F_UPP_PA_of_sequpp :=
  locked_with F_UPP_PA_of_sequpp_key F_UPP_PA_of_sequpp_def.
Canonical F_UPP_PA_of_sequpp_unlockable := [unlockable fun F_UPP_PA_of_sequpp].

Local Instance Rsequpp_F_UPP_PA_of_sequpp (f : @sequpp rat) Hne Hh0 Hs HT Hd Hl :
  refines Rsequpp (@F_UPP_PA_of_sequpp f Hne Hh0 Hs HT Hd Hl) f.
Proof.
rewrite refinesE unlock; constructor=> //=.
rewrite /F_UPP_PA_of_sequpp_def_obligation_1.
exact: refinesP.
Qed.

Local Instance nat_Rnn n : refines nat_R n n.
Proof. by rewrite refinesE; apply: nat_Rxx. Qed.

Local Instance Rsequpp_T f f' :
  refines Rsequpp f f' -> refines rnnQ (F_UPP_T f) (sequpp_T f').
Proof. by rewrite !refinesE => -[]. Qed.

Local Instance Rsequpp'_T f f' :
  refines Rsequpp f f' -> refines eq (F_UPP_T f)%:nngnum%R (sequpp_T f').
Proof. by rewrite !refinesE => -[]. Qed.

Local Instance Rsequpp_d f f' :
  refines Rsequpp f f' -> refines rposQ (F_UPP_d f) (sequpp_d f').
Proof. by rewrite !refinesE => -[]. Qed.

Local Instance Rsequpp'_d f f' :
  refines Rsequpp f f' -> refines eq (F_UPP_d f)%:num%R (sequpp_d f').
Proof. by rewrite !refinesE => -[]. Qed.

Local Instance Rsequpp_c f f' :
  refines Rsequpp f f' -> refines eq (F_UPP_c f) (sequpp_c f').
Proof. by rewrite !refinesE => -[]. Qed.

Local Instance Rsequpp_js :
  refines (Rsequpp ==> Rseqjs)%rel F_UPP_PA_JS sequpp_js.
Proof. by rewrite refinesE => _ _ []. Qed.

Local Instance Rsequpp_js' f f' :
  refines Rsequpp f f' -> refines Rseqjs' (F_UPP_PA_JS f) (sequpp_js f').
Proof. by rewrite !refinesE => -[]. Qed.

Local Instance Rsequpp_shift_elem :
  refines (Rseqjse ==> rposQ ==> eq ==> nat_R ==> Rseqjse)%rel
    shift_elem sequpp_shift_elem.
Proof.
rewrite refinesE => _ _ [x _ <- yrs _ <-] d _ <- c _ <- n1 n2 rn.
constructor; first by congr (_ + _%:R * _)%R; apply: nat_R_eq.
by congr (_, (_, _)); apply/refinesP/refines_apply; rewrite refinesE;
  congr (_%:R * _)%R%:E; apply: nat_R_eq.
Qed.

Local Instance Rseqjs_shift_period :
  refines (Rseqjs_pre ==> rposQ ==> eq ==> nat_R ==> Rseqjs_pre)%rel
    shift_period seqjs_shift_period.
Proof.
rewrite refinesE => a1 a2 + d _ <- c _ <- n1 n2 rn.
by apply: map_R => xyrs1 xyrs2 rxyrs; apply: refinesP.
Qed.

Local Instance Rsequpp_further_period :
  refines (Rsequpp ==> Rseqjs_pre)%rel further_period sequpp_further_period.
Proof.
rewrite /further_period /further_period_subdef /sequpp_further_period.
rewrite refinesE => _ _ [f u <- <- <- rjs].
set suf1 := JS_above _ _; set suf2 := seqjs_above _ _.
have rsuf : Rseqjs_pre suf1 suf2 by exact: refinesP.
set suf'1 := shift_period _ _ _ _; set suf'2 := seqjs_shift_period _ _ _ _.
have rsuf' : Rseqjs_pre suf'1 suf'2 by exact: refinesP.
move: rsuf' (JS_not_empty suf'1); rewrite /Rseqjs_pre/=.
case=> [//|_ _ [_ _ _ [_ rs] _ <-]] a1 a2 ra _ /=; constructor=> //.
constructor=> //; congr (_, _).
set y1 := shift_JS_elem _ _; set y2 := seqjs_shift_elem _ _.
by have [? ? ? ? ? <-] : Rseqjse y1 y2 by exact: refinesP.
Qed.

Local Instance Rsequpp_js_upto_subdef :
  refines (Rsequpp ==> rnnQ ==> Rseqjs_pre)%rel
    F_UPP_PA_JS_upto_subdef sequpp_js_upto_subdef.
Proof.
rewrite /F_UPP_PA_JS_upto_subdef /sequpp_js_upto_subdef.
rewrite refinesE => f u rfu l _ <-.
set n1 := absz _; set n2 := ceil_op _.
have [|{}n1 {}n2 rn] : nat_R n1 n2; [|exact: refinesP|].
  by rewrite /n1 /n2; case: rfu => ? ? <- <- _ _; apply: nat_Rxx.
apply: refinesP; apply: (@refines_apply _ _ Rseqjs_pre).
  exact/refines_apply/Rseqprejs_cat.
elim: rn 0%N => [|{}n1 {}n2 rn IHn] n'; apply: refines_apply.
exact/refines_apply/Rseqprejs_cat.
Qed.

Local Instance Rsequpp_js_upto_subdef' :
  refines (Rsequpp ==> rnnQ ==> Rseqjs)%rel
    F_UPP_PA_JS_upto_subdef' sequpp_js_upto_subdef'.
Proof.
rewrite /F_UPP_PA_JS_upto_subdef' /sequpp_js_upto_subdef'.
rewrite refinesE => f u rfu l _ <-.
apply: refinesP; apply: refines_apply; apply: refines_apply; rewrite refinesE.
by apply: (@refinesP _ _ Rseqjs_pre) => /=; apply: refines_apply.
Qed.

Local Instance Rsequpp_is_ultimately_affine :
  refines
    (Rsequpp ==> bool_R)%rel is_ultimately_affine sequpp_is_ultimately_affine.
Proof.
rewrite /is_ultimately_affine /sequpp_is_ultimately_affine.
rewrite refinesE => _ _ [f u <- <- <- rjs].
set l1 := last_JS _; set l2 := last_seqjs _.
have [x _ <- [y [r s]] _ <-] : Rseqjse l1 l2 by exact: refinesP.
by apply: andb_R; apply: bool_Rxx.
Qed.

Local Instance Rsequpp_js_upto :
  refines (Rsequpp ==> rnnQ ==> Rseqjs)%rel F_UPP_PA_JS_upto sequpp_js_upto.
Proof.
rewrite /F_UPP_PA_JS_upto /sequpp_js_upto refinesE => f u rfu l _ <-.
set b1 := _ && _; set b2 := _ && _.
have [] : bool_R b1 b2; [|exact: refinesP..].
by apply: andb_R; [apply: refinesP|case: rfu => ? ? <- *; apply: bool_Rxx].
Qed.

Local Instance Rsequpp_opp :
  refines (Rsequpp ==> Rsequpp)%rel F_UPP_PA_opp sequpp_opp.
Proof.
rewrite refinesE /sequpp_opp => _ _ [f u <- <- <- rfu].
constructor=> //=; [by rewrite -[LHS]GRing.add0r|].
by apply: (@refinesP _ _ Rseqjs_pre) => /=; apply: refines_apply.
Qed.

Local Instance Rsequpp_add_cst :
  refines (eq ==> Rsequpp ==> Rsequpp)%rel F_UPP_PA_add_cst sequpp_add_cst.
Proof.
rewrite refinesE /sequpp_add_cst => c _ <- _ _ [f u <- <- <- Hfu].
constructor=> //=; exact: refinesP.
Qed.

Local Instance Rsequpp_add :
  refines (Rsequpp ==> Rsequpp ==> Rsequpp)%rel F_UPP_PA_add sequpp_add.
Proof.
rewrite refinesE => a1 a2 ra a1' a2' ra'.
rewrite /F_UPP_PA_add /sequpp_add; constructor=> /=.
{ by apply: f_equal2; [case: ra|case: ra']. }
{ by rewrite unlock; apply: f_equal2; [case: ra|case: ra']. }
{ apply: f_equal2.
  { case: ra => ? ? _ <- _ _; case: ra' => ? ? _ <- _ _.
    by rewrite /= unlock. }
  apply f_equal2; apply: f_equal2.
  { by case: ra. }
  { by case: ra => ? ? _ <-. }
  { by case: ra'. }
  by case: ra' => ? ? _ <-. }
rewrite /=.
eapply refinesP.
eapply refines_apply.
{ eapply refines_apply; [exact: Rseqjs_add|].
  eapply refines_apply.
  { by eapply refines_apply; [exact: Rsequpp_js_upto|rewrite refinesE]. }
  rewrite refinesE.
  apply: f_equal2; rewrite ?unlock;
    (by apply: f_equal2; [case: ra|case: ra']). }
eapply refines_apply.
{ by eapply refines_apply; [exact: Rsequpp_js_upto|rewrite refinesE]. }
rewrite refinesE.
apply: f_equal2; rewrite ?unlock; (by apply: f_equal2; [case: ra|case: ra']).
Qed.

Local Instance Rsequpp_min' :
  refines
    (Rsequpp ==> Rsequpp ==> option_R Rsequpp)%rel
    F_UPP_PA_min' sequpp_min'.
Proof.
rewrite /F_UPP_PA_min' /sequpp_min'.
rewrite refinesE => a1 a2 ra b1 b2 rb.
move: ra (ra) => -[{}a1 {}a2 <- <- <- ra] rfa.
move: rb (rb) => -[{}b1 {}b2 <- <- <- rb] rfb.
rewrite /eq_op /eq_rat /div_op /div_rat.

pose cd : F_UPP_PA -> rat := fun a => (F_UPP_c a / (@F_UPP_d R a)%:num)%R.
pose tildeT : F_UPP_PA -> F_UPP_PA -> rat -> rat -> rat := fun a1 b1 M m =>
  (Num.max (Num.max (F_UPP_T a1)%:num (F_UPP_T b1)%:num)
     ((M - m) / (cd b1 - cd a1)))%R.
pose l := fun a1 b1 M m => (tildeT a1 b1 M m + (F_UPP_d a1)%:num)%R.
have ltc a1' a2' (ra' : Rsequpp a1' a2') b1' b2' (rb' : Rsequpp b1' b2') ltcd :
  option_R Rsequpp
    match
      erP (JS_max_val (JS_above
        (JS_add (F_UPP_PA_JS a1') (JS_affine (- cd a1') 0))
        (F_UPP_T a1')) ((F_UPP_T a1')%:num + (F_UPP_d a1')%:num)%E%:nng%R),
      erP (JS_min_val (JS_above
        (JS_add (F_UPP_PA_JS b1') (JS_affine (- cd b1') 0))
        (F_UPP_T b1')) ((F_UPP_T b1')%:num + (F_UPP_d b1')%:num)%E%:nng%R)
    with
    | @Efinite _ _ M HM, @Efinite _ _ m Hm =>
        Some (Build_F_UPP_PA
          (F_UPP_PA_min'_obligation_3 (F_UPP_PA_min'_obligation_2 ltcd HM Hm)))
    | _, _ => None
    end
    match
      seqjs_max_val (seqjs_above
        (seqjs_add (sequpp_js a2') (seqjs_affine (0 - (cd a1'))%C 0%C))
        (F_UPP_T a1')%:num%R) ((F_UPP_T a1')%:num%R + (F_UPP_d a1')%:num%R)%C,
      seqjs_min_val (seqjs_above
        (seqjs_add (sequpp_js b2') (seqjs_affine (0 - (cd b1'))%C 0%C))
        (F_UPP_T b1')%:num%R) ((F_UPP_T b1')%:num%R + (F_UPP_d b1')%:num%R)%C
    with
    | M%:E, m%:E =>
        Some (Build_sequpp (tildeT a1' b1' M m) (F_UPP_d a1')%:num%R (F_UPP_c a1')
                (seqjs_min
                   (sequpp_js_upto a2' (l a1' b1' M m))
                   (sequpp_js_upto b2' (l a1' b1' M m))
                   (l a1' b1' M m)))
    | _, _ => None
    end.
  rewrite /sub_op /sub_rat !GRing.add0r.
  set ma1 := JS_max_val _ _; set ma2 := seqjs_max_val _ _.
  have <- : ma1 = ma2 by exact: refinesP.
  set mi1 := JS_min_val _ _; set mi2 := seqjs_min_val _ _.
  have <- : mi1 = mi2 by exact: refinesP.
  case: erP => [M||] HM; rewrite HM; [|by constructor..].
  case: erP => [m||] Hm; rewrite Hm; [|by constructor..].
  have rtildeT : refines rnnQ (tilde_T_min a1' b1' M m) (tildeT a1' b1' M m).
    rewrite refinesE /rnnQ /tilde_T_min /tildeT/=.
    case: ltgtP ltcd => [|//|//] ltcd _.
    rewrite /insubd [insub (_ / _ - _)%R]insubT/= ?subr_ge0 ?ltW// => _.
    have [mM|mM] := ltP (M - m)%R 0%R; last by rewrite insubT.
    rewrite insubN/= -?ltNge// mul0r max_l// [RHS]max_l//.
    by apply: le_trans (ge0 _); rewrite pmulr_lle0 ?ltW// invr_gt0 subr_gt0.
  have rtilded : refines rposQ (tilde_d_min a1' b1') (F_UPP_d a1')%:num%R.
    by rewrite refinesE /tilde_d_min; case: ltgtP ltcd.
  have <- : tilde_c_min a1' b1' = F_UPP_c a1'.
    by rewrite /tilde_c_min; case: ltgtP ltcd.
  set l' := _%:nng%R; set l'' := _%:nng%R.
  have rl' : refines rnnQ l' (l a1' b1' M m).
    by rewrite refinesE; congr +%R; apply: refinesP.
  have rl'' : refines rnnQ l'' (l a1' b1' M m).
    by move: rl'; rewrite !refinesE => <-; apply: val_inj.
  by constructor; constructor=> /=; apply: refinesP.
case: ltgtP => ocd.
- by rewrite /lt_op /lt_rat ocd; apply: ltc.
- by rewrite /lt_op /lt_rat ifN -?leNgt ?ltW//; apply: ltc.
set tildeT' := max_op _ _; set tilded' := lcm_op _ _.
set tildec' := (tilded' * _)%C.
have rtildeT : refines rnnQ (tilde_T_min a1 b1 0 0) tildeT'.
  rewrite refinesE /tilde_T_min; case: eqP => [|//] _.
  by rewrite /rnnQ/= maxEge ifT.
have rtilded : refines rposQ (tilde_d_min a1 b1) tilded'.
  by rewrite refinesE /tilde_d_min unlock; case: eqP => [|//] _.
have <- : tilde_c_min a1 b1 = tildec'.
  by rewrite /tilde_c_min unlock; case: eqP => [|//] _.
set l' := _%:nng%R; set l'' := _%:nng%R.
have rl' : refines rnnQ l' (tildeT' + tilded')%C.
  rewrite refinesE; congr +%R => /=.
    by move: rtildeT ; rewrite refinesE => <-.
  by move: rtilded ; rewrite refinesE => <-.
have rl'' : refines rnnQ l'' (tildeT' + tilded')%C.
  by move: rl'; rewrite !refinesE => <-; apply: val_inj.
by constructor; constructor=> /=; apply: refinesP.
Qed.

Local Instance Rsequpp_is_infinite_on_period :
  refines (Rsequpp ==> bool_R)%rel
    is_infinite_on_period sequpp_is_infinite_on_period.
Proof.
rewrite refinesE => a1 a2 ra.
rewrite /is_infinite_on_period /sequpp_is_infinite_on_period.
case: ra (ra) => {}a1 {}a2 <- <- _ ra' ra.
set mi1 := JS_min_val _ _; set mi2 := seqjs_min_val _ _.
have <- : mi1 = mi2 by exact: refinesP.
set ma1 := JS_max_val _ _; set ma2 := seqjs_max_val _ _.
have <- : ma1 = ma2 by exact: refinesP.
by case: mi1 ma1 => [mi1||] [ma1||]; apply: bool_Rxx.
Qed.

Local Instance Rsequpp_change_d_c a1 a2 d1 d2 c1 c2 H :
    refines Rsequpp a1 a2 -> refines rposQ d1 d2 -> refines (@eq rat) c1 c2 ->
  refines Rsequpp
    (@F_UPP_PA_change_d_c R a1 d1 c1 H) (sequpp_change_d_c a2 d2 c2).
Proof.
rewrite !refinesE /F_UPP_PA_change_d_c /sequpp_change_d_c => ra <- <-.
constructor=> //=; first exact: refinesP.
case: ra (ra) {H} => {}a1 {}a2 <- <- _ ra' ra.
by apply: refinesP; apply: refines_apply; apply: refines_apply.
Qed.

Local Instance Rsequpp_preprocess_d_c :
  refines
    (Rsequpp ==> Rsequpp ==> prod_R Rsequpp Rsequpp)%rel
    F_UPP_PA_preprocess_d_c sequpp_preprocess_d_c.
Proof.
rewrite refinesE => a1 a2 ra b1 b2 rb.
rewrite /F_UPP_PA_preprocess_d_c /sequpp_preprocess_d_c.
set ipa1 := is_infinite_on_period a1.
set ipa2 := sequpp_is_infinite_on_period a2.
have <- : ipa1 = ipa2 by exact: refinesP.
set ipb1 := is_infinite_on_period b1.
set ipb2 := sequpp_is_infinite_on_period b2.
have <- : ipb1 = ipb2 by exact: refinesP.
case: Sumbool.sumbool_of_bool => H1; rewrite H1.
  by constructor=> //; apply: refinesP.
case: Sumbool.sumbool_of_bool => H2; rewrite H2.
  by constructor=> //; apply: refinesP.
by constructor.
Qed.

Local Instance Rsequpp_min :
  refines (Rsequpp ==> Rsequpp ==> option_R Rsequpp)%rel
    F_UPP_PA_min sequpp_min.
Proof.
apply: refines_abstr2 => a1 a2 ra a1' a2' ra'.
rewrite /F_UPP_PA_min /sequpp_min.
exact: refines_apply.
Qed.

Local Instance Rsequpp_f1 :
  refines (Rsequpp ==> rposQ ==> eq ==> Rsequpp)%rel F_UPP_PA_f1 sequpp_f1.
Proof.
rewrite refinesE => a1 a2 ra d _ <- c _ <-.
by constructor=> //=; apply: refinesP.
Qed.

Local Instance Rsequpp_f2 :
  refines (Rsequpp ==> Rsequpp)%rel F_UPP_PA_f2 sequpp_f2.
Proof.
rewrite refinesE => a1 a2 ra.
by constructor=> /=; apply: refinesP.
Qed.

Local Instance Rsequpp_f1_f1' :
  refines (Rsequpp ==> Rsequpp ==> rposQ ==> eq ==> Rsequpp)%rel
    F_UPP_PA_f1_f1' sequpp_f1_f1'.
Proof.
rewrite refinesE => a1 a2 ra b1 b2 rb d _ <- c _ <-.
constructor=> //=; first by congr +%R; apply: refinesP.
case: ra (ra) => {}a1 {}a2 <- _ _ ra' ra.
case: rb (rb) => {}b1 {}b2 <- _ _ rb' rb.
exact: refinesP.
Qed.

Local Instance Rsequpp_f1_f2' :
  refines (Rsequpp ==> Rsequpp ==> Rsequpp)%rel F_UPP_PA_f1_f2' sequpp_f1_f2'.
Proof.
rewrite refinesE => a1 a2 ra b1 b2 rb.
rewrite /F_UPP_PA_f1_f2' /sequpp_f1_f2'.
case: ra (ra) => {}a1 {}a2 <- _ _ ra' ra.
case: rb (rb) => {}b1 {}b2 <- <- <- rb' rb.
by constructor=> //=; apply: refinesP.
Qed.

Local Instance Rsequpp_f2_f2' :
  refines (Rsequpp ==> Rsequpp ==> Rsequpp)%rel F_UPP_PA_f2_f2' sequpp_f2_f2'.
Proof.
rewrite refinesE => a1 a2 ra b1 b2 rb.
rewrite /F_UPP_PA_f2_f2' /sequpp_f2_f2'.
case: ra (ra) => {}a1 {}a2 <- <- <- ra' ra.
case: rb (rb) => {}b1 {}b2 <- <- <- rb' rb.
set lc1 := (lcm_pos_rat _ _)%:num%R; set lc2 := lcm_op _ _.
have <- : lc1 = lc2 by rewrite /lc1 unlock.
by constructor=> //=; apply: refinesP.
Qed.

Local Instance Rsequpp_min_conv' :
  refines
    (Rsequpp ==> Rsequpp ==> option_R Rsequpp)%rel
    F_UPP_PA_conv' sequpp_min_conv'.
Proof.
rewrite refinesE => a1 a2 ra b1 b2 rb.
rewrite /F_UPP_PA_conv' /sequpp_min_conv'.
set r11 := F_UPP_PA_min _ (F_UPP_PA_f1_f2' _ _).
set r12 := sequpp_min _ (sequpp_f1_f2' _ _).
have [] : option_R Rsequpp r11 r12; [exact: refinesP| |by constructor].
move=> {}r11 {}r12 r1 /=.
set r21 := F_UPP_PA_min r11 _.
set r22 := sequpp_min r12 _.
have [] : option_R Rsequpp r21 r22; [exact: refinesP| |by constructor].
move=> {}r21 {}r22 r2 /=.
exact: refinesP.
Qed.

Local Instance Rsequpp_min_conv :
  refines
    (Rsequpp ==> Rsequpp ==> option_R Rsequpp)%rel
    F_UPP_PA_conv sequpp_min_conv.
Proof.
apply: refines_abstr2 => a1 a2 ra a1' a2' ra'.
rewrite /F_UPP_PA_conv /sequpp_min_conv.
exact: refines_apply.
Qed.

Local Instance Rsequpp_eqb' :
  refines (Rsequpp ==> Rsequpp ==> bool_R)%rel F_UPP_PA_eqb' sequpp_eqb'.
Proof.
rewrite refinesE => a1 a2 ra b1 b2 rb.
rewrite /F_UPP_PA_eqb' /sequpp_eqb'.
case: ra (ra) => {}a1 {}a2 <- <- <- ra' ra.
case: rb (rb) => {}b1 {}b2 <- <- <- rb' rb.
apply: andb_R => /=; first exact: bool_Rxx.
set lc1 := (lcm_pos_rat _ _)%:num%R; set lc2 := lcm_op _ _.
have <- : lc1 = lc2 by rewrite /lc1 unlock.
exact: refinesP.
Qed.

Local Instance Rsequpp_eqb :
  refines (Rsequpp ==> Rsequpp ==> bool_R)%rel F_UPP_PA_eqb sequpp_eqb.
Proof.
apply: refines_abstr2 => a1 a2 ra a1' a2' ra'.
rewrite /F_UPP_PA_eqb /sequpp_eqb.
exact: refines_apply.
Qed.

Local Instance Rsequpp_leb :
  refines (Rsequpp ==> Rsequpp ==> bool_R)%rel F_UPP_PA_leb sequpp_leb.
Proof.
rewrite refinesE => ? ? ra ? ? ra'.
rewrite /F_UPP_PA_leb /sequpp_leb.
case: (refinesP Rsequpp_min _ _ ra _ _ ra') => [? ? ? | ].
{ exact: refinesP. }
constructor.
Qed.

Local Instance Rsequpp_0 :
  refines (rposQ ==> Rsequpp)%rel F_UPP_PA_0 sequpp_0.
Proof.
rewrite refinesE => d' _ <-; constructor=> //=.
by rewrite /Rseqjs; apply: refinesP.
Qed.

Local Instance Rsequpp_delta :
  refines (eq ==> rposQ ==> eq ==> Rsequpp)%rel F_UPP_PA_delta sequpp_delta.
Proof.
rewrite refinesE => d _ <- d' _ <- c _ <-; constructor=> //.
{ rewrite /= /rnnQ /max_op /max_rat maxEle; case: leP => Hd.
  { by rewrite /= insubdK. }
  by rewrite /insubd insubF //; apply/negbTE; rewrite -ltNge. }
by rewrite /Rseqjs /=; rewrite /lt_op /lt_rat /eq_op /eq_rat; apply: refinesP.
Qed.

Local Instance Rsequpp_non_decr_closure :
  refines
    (Rsequpp ==> option_R Rsequpp)%rel
    F_UPP_PA_non_decr_closure sequpp_non_decr_closure.
Proof.
apply: refines_abstr => a1 a2 /[dup] r'a /refinesP ra.
rewrite refinesE /F_UPP_PA_non_decr_closure /sequpp_non_decr_closure.
case: (refinesP Rsequpp_min_conv _ _ (refinesP Rsequpp_opp _ _ ra)
                _ _ (refinesP Rsequpp_0 _ _ (refinesP (Rsequpp_d r'a)))) =>
  [a1' a2' ra' | ]; constructor; exact: refinesP.
Qed.

Local Instance Rsequpp_non_neg :
  refines (Rsequpp ==> bool_R)%rel F_UPP_PA_non_neg sequpp_non_neg.
Proof.
rewrite refinesE => a1 a2 ra.
by rewrite /F_UPP_PA_non_neg /sequpp_non_neg; apply: refinesP.
Qed.

Local Instance Rsequpp_non_decr :
  refines (Rsequpp ==> bool_R)%rel F_UPP_PA_non_decr sequpp_non_decr.
Proof.
rewrite refinesE => a1 a2 ra.
rewrite /F_UPP_PA_non_decr /sequpp_non_decr.
set ndc1 := F_UPP_PA_non_decr_closure a1.
set ndc2 := sequpp_non_decr_closure a2.
have [] : option_R Rsequpp ndc1 ndc2; [exact: refinesP| |by constructor].
by move=> {}ndc1 {}ndc2 rndc /=; apply: refinesP.
Qed.

Local Instance Rsequpp_hDev_bounded :
  refines
    (Rsequpp ==> Rsequpp ==> eq ==> bool_R)%rel
    F_UPP_PA_hDev_bounded sequpp_hDev_bounded.
Proof.
rewrite refinesE => a1 a2 ra b1 b2 rb d _ <-.
rewrite /F_UPP_PA_hDev_bounded /sequpp_hDev_bounded.
case: d => [d|//|//]; apply: andb_R; [|apply: andb_R; [|apply: andb_R]].
- exact: bool_Rxx.
- exact: refinesP.
- exact: refinesP.
set c1 := F_UPP_PA_conv _ _; set c2 := sequpp_min_conv _ _.
have [] : option_R Rsequpp c1 c2; [exact: refinesP| |by constructor].
by move=> {}c1 {}c2 rc /=; apply: refinesP.
Qed.

Local Instance Rsequpp_never_pinfty :
  refines (Rsequpp ==> bool_R)%rel F_UPP_PA_never_pinfty sequpp_never_pinfty.
Proof.
rewrite refinesE => a1 a2 ra.
rewrite /F_UPP_PA_never_pinfty /sequpp_never_pinfty.
case: ra (ra) => {}a1 {}a2 <- <- _ ra' ra.
set e01 := JS_eval _ _; set e02 := seqjs_eval _ _.
have <- : e01 = e02 by exact: refinesP.
set ma1 := JS_max_val _ _; set ma2 := seqjs_max_val _ _.
have <- : ma1 = ma2 by exact: refinesP.
by case: e01 => [e01|//|]; case: ma1.
Qed.

Local Instance Rsequpp_never_ninfty :
  refines (Rsequpp ==> bool_R)%rel F_UPP_PA_never_ninfty sequpp_never_ninfty.
Proof.
apply: refines_abstr => a1 a2 ra.
rewrite /F_UPP_PA_never_ninfty /sequpp_never_ninfty.
exact: refines_apply.
Qed.

Local Instance Rsequpp_vDev_bounded :
  refines
    (Rsequpp ==> Rsequpp ==> eq ==> bool_R)%rel
    F_UPP_PA_vDev_bounded sequpp_vDev_bounded.
Proof. param andb_R. Qed.

Section param.

Context (C : Type) (rratC : rat -> C -> Type).
Context `{!zero_of C, !cast_of nat C}.
Context `{!add_of C, !sub_of C, !mul_of C, !div_of C, !ceil_of C}.
Context `{!max_of C, !min_of C, !lcm_of C}.
Context `{!eq_of C, !lt_of C, !leq_of C}.
Context `{!spec_of C rat}.
Context `{!refines rratC 0%C 0%C}.
Context `{!refines (nat_R ==> rratC)%rel cast_op cast_op}.
Context `{!refines (rratC ==> rratC ==> rratC)%rel +%C +%C}.
Context `{!refines (rratC ==> rratC ==> rratC)%rel (fun x y => x - y)%C sub_op}.
Context `{!refines (rratC ==> rratC ==> rratC)%rel *%C *%C}.
Context `{!refines (rratC ==> rratC ==> rratC)%rel div_rat div_op}.
Context `{!refines (rratC ==> nat_R)%rel ceil_rat ceil_op}.
Context `{!refines (rratC ==> rratC ==> rratC)%rel max_rat max_op}.
Context `{!refines (rratC ==> rratC ==> rratC)%rel min_rat min_op}.
Context `{!refines (rratC ==> rratC ==> rratC)%rel lcm_rat lcm_op}.
Context `{!refines (rratC ==> rratC ==> bool_R)%rel eq_op eq_op}.
Context `{!refines (rratC ==> rratC ==> bool_R)%rel lt_rat lt_op}.
Context `{!refines (rratC ==> rratC ==> bool_R)%rel le_rat leq_op}.
Context `{!refines (eq ==> rratC)%rel spec spec_id}.

Definition RsequppC : F -> @sequpp C -> Type :=
  ((fun f (f' : F_UPP_PA) => f = (f' : F)) \o (Rsequpp \o sequpp_R rratC))%rel.

Variant or_None A C (rAC : A -> C -> Type) : A -> option C -> Type :=
  | or_None_Some : forall a c, rAC a c -> or_None rAC a (Some c)
  | or_None_None : forall a, or_None rAC a None.

Definition sequpp_spec (f : @sequpp C) : @sequpp rat :=
  Build_sequpp
    (spec (sequpp_T f))
    (spec (sequpp_d f))
    (spec (sequpp_c f))
    (seqjs_spec (sequpp_js f)).

Local Existing Instance RseqjsC_spec.

Local Instance RsequppC_spec :
  refines (eq ==> sequpp_R rratC)%rel sequpp_spec spec_id.
Proof.
rewrite refinesE => + _ <- => -[fT fd fc fjs]; constructor=> /=.
{ exact: (rratC_spec (rratC:=rratC)). }
{ exact: (rratC_spec (rratC:=rratC)). }
{ exact: (rratC_spec (rratC:=rratC)). }
by rewrite -{2}[fjs]/(spec_id fjs); apply: refinesP.
Qed.

Program Definition F_of_sequppC (a : @sequpp C)
        (Hne : seqjs_not_empty (sequpp_js a))
        (Hh0 : seqjs_head_is_0 (sequpp_js a))
        (Hs : seqjs_sorted (sequpp_js a))
        (HT : sequpp_T_nonneg a) (Hd : sequpp_d_pos a)
        (Hl : sequpp_last_js_le_T_plus_d a) : F :=
  @F_UPP_PA_of_sequpp (sequpp_spec a) _ _ _ _ _ _.
Next Obligation.
move=> a Hne Hh0 Hs HT Hd Hl.
rewrite /is_true -Hne; apply: refinesP; apply: refines_bool_eq.
rewrite /= -{2}[sequpp_js a]/(spec_id (sequpp_js a)).
by param seqjs_not_empty_R; rewrite refinesE => x y <-.
Qed.
Next Obligation.
move=> a Hne Hh0 Hs HT Hd Hl.
rewrite /is_true -Hh0; apply: refinesP; apply: refines_bool_eq.
rewrite /= -{2}[sequpp_js a]/(spec_id (sequpp_js a)).
by param seqjs_head_is_0_R; rewrite refinesE => x y <-.
Qed.
Next Obligation.
move=> a Hne Hh0 Hs HT Hd Hl.
rewrite /is_true -Hs; apply: refinesP; apply: refines_bool_eq.
rewrite /= -{2}[sequpp_js a]/(spec_id (sequpp_js a)).
by param seqjs_sorted_R; rewrite refinesE => x y <-.
Qed.
Next Obligation.
move=> a Hne Hh0 Hs HT Hd Hl.
rewrite /is_true -HT; apply: refinesP; apply: refines_bool_eq.
rewrite -{2}[a]/(spec_id a).
by param sequpp_T_nonneg_R; rewrite refinesE.
Qed.
Next Obligation.
move=> a Hne Hh0 Hs HT Hd Hl.
rewrite /is_true -Hd; apply: refinesP; apply: refines_bool_eq.
rewrite -{2}[a]/(spec_id a).
by param sequpp_d_pos_R; rewrite refinesE.
Qed.
Next Obligation.
move=> a Hne Hh0 Hs HT Hd Hl.
rewrite /is_true -Hl; apply: refinesP; apply: refines_bool_eq.
rewrite -{2}[a]/(spec_id a).
by param sequpp_last_js_le_T_plus_d_R; rewrite refinesE.
Qed.

Global Instance RsequppC_F_of_sequppC (f : @sequpp C) Hne Hh0 Hs HT Hd Hl :
  refines (or_None RsequppC) (@F_of_sequppC f Hne Hh0 Hs HT Hd Hl) (Some f).
Proof.
rewrite refinesE; constructor.
rewrite /F_of_sequppC.
set f' := F_UPP_PA_of_sequpp _ _ _ _ _ _.
exists f'; split=> [//|].
exists (sequpp_spec f); split.
{ exact: refinesP. }
by rewrite -{2}[f]/(spec_id f); apply: refinesP.
Qed.

Lemma F_of_sequppC_correct (f : @sequpp C) Hne Hh0 Hs HT Hd Hl :
  @F_of_sequppC f Hne Hh0 Hs HT Hd Hl =
  F_UPP_of_F_def
    (F_of_JS_def (seqjs_spec (sequpp_js f)))
    (spec (sequpp_T f)) (spec (sequpp_d f)) (spec (sequpp_c f)).
Proof.
rewrite /F_of_sequppC /F_UPP_PA_of_sequpp unlock /F_UPP_PA_of_sequpp_def.
rewrite /F_UPP_PA_of_JS /F_UPP_PA_UPP /F_UPP_of_F /F_UPP_val /= /F_of_JS.
rewrite (_ : map _ _ = seqjs_spec (sequpp_js f)) //.
rewrite -map_comp.
under eq_map => xyrs do rewrite /= -surjective_pairing.
elim: (seqjs_spec _) => [//|xyrs a IH] /=.
rewrite -[in RHS]IH; apply: f_equal2 => [//|].
by rewrite -(addn0 1) iotaDl -map_comp; apply: eq_map.
Qed.

Global Instance RsequppC_oopp :
  refines (or_None RsequppC ==> or_None RsequppC)%rel
    (fun f => \- f)%dE sequpp_oopp.
Proof.
rewrite refinesE => _ _ [+ + []|f] => [_ f2 f1 [-> rf]|]; constructor.
rewrite -F_UPP_PA_opp_correct.
exists (F_UPP_PA_opp f1); split=> [//|].
apply: refinesP.
apply: refines_apply.
param_comp sequpp_opp_R.
Qed.

Global Instance RsequppC_oadd :
  refines
    (or_None RsequppC ==> or_None RsequppC ==> or_None RsequppC)%rel
    GRing.add sequpp_oadd.
Proof.
rewrite refinesE => _ _ [+ + []|f] => [_ f2 f1 [-> rf]|];
  move=> _ _ [+ + []|f'] => [_ f2' f1' [-> rf']|]; constructor.
rewrite -F_UPP_PA_add_correct.
exists (F_UPP_PA_add f1 f1'); split=> [//|].
apply: refinesP.
apply: refines_apply.
apply: refines_apply.
param_comp sequpp_add_R.
Qed.

Global Instance RsequppC_omin :
  refines
    (or_None RsequppC ==> or_None RsequppC ==> or_None RsequppC)%rel
    Order.min_fun sequpp_omin.
Proof.
rewrite refinesE => _ _ [+ + []|f] => [_ + + [-> []]|] => [f2 f1 f3 [rf rf3]|];
  move=> _ _ [+ + []|f'] => [_ + + [-> []]|] => [f2' f1' f3' [rf' rf3']|] /=;
  [|constructor..].
have: option_R (sequpp_R rratC) (sequpp_min f3 f3') (sequpp_min f2 f2').
{ param sequpp_min_R. }
move: (@F_UPP_PA_min_correct _ f1 f1').
move: Rsequpp_min; rewrite refinesE => /(_ _ _ rf _ _ rf') [f1'' f3'' rf''|].
{ move=> /(_ f1'' erefl) <- H.
  by inversion_clear H; constructor; exists f1''; split=> [//|]; exists f3''. }
move=> _ H; inversion_clear H; constructor.
Qed.

Global Instance RsequppC_omin_conv :
  refines
    (or_None RsequppC ==> or_None RsequppC ==> or_None RsequppC)%rel
    (@GRing.mul Fd) sequpp_omin_conv.
Proof.
rewrite refinesE => _ _ [+ + []|f] => [_ + + [-> []]|] => [f2 f1 f3 [rf rf3]|];
  move=> _ _ [+ + []|f'] => [_ + + [-> []]|] => [f2' f1' f3' [rf' rf3']|] /=;
  [|constructor..].
have: option_R (sequpp_R rratC) (sequpp_min_conv f3 f3') (sequpp_min_conv f2 f2').
{ param sequpp_min_conv_R. }
move: (@F_UPP_PA_conv_correct _ f1 f1').
move: Rsequpp_min_conv; rewrite refinesE => /(_ _ _ rf _ _ rf') [f1'' f3'' rf''|].
{ move=> /(_ f1'' erefl) <- H.
  by inversion_clear H; constructor; exists f1''; split=> [//|]; exists f3''. }
move=> _ H; inversion_clear H; constructor.
Qed.

Global Instance RsequppC_oeqb :
  refines
    (or_None RsequppC ==> or_None RsequppC ==> fun T T' : Type => T' -> T)%rel
    eq sequpp_oeqb.
Proof.
rewrite refinesE => _ _ [+ + []|f] => [_ f2 f1 [-> rf]|];
  move=> _ _ [+ + []|f'] => [_ f2' f1' [-> rf']|] => //= H.
apply: F_UPP_PA_eqb_correct.
rewrite /is_true -H.
apply: refines_eq.
apply: refines_bool_eq.
apply: refines_apply.
apply: refines_apply.
param_comp sequpp_eqb_R.
Qed.

Global Instance RsequppC_oleb a1 a2 a1' a2' :
  refines (or_None RsequppC) a1 a2 -> refines (or_None RsequppC) a1' a2' ->
  refines (fun T T' : Type => T' -> T)%rel (a1 <= a1')%O (sequpp_oleb a2 a2').
Proof.
rewrite !refinesE => -[+ + []|f] => [_ f2 f1 [-> rf]|];
  move=> -[++ []|f'] => [_ f2' f1' [-> rf']|] => //= H.
apply: F_UPP_PA_leb_correct.
rewrite /is_true -H.
apply: refines_eq.
apply: refines_bool_eq.
apply: refines_apply.
apply: refines_apply.
param_comp sequpp_leb_R.
Qed.

Global Instance RsequppC_onon_decr_closure :
  refines
    (or_None RsequppC ==> or_None RsequppC)%rel
    non_decr_closure sequpp_onon_decr_closure.
Proof.
rewrite refinesE => _ _ [+ + []|f] => [_ + + [-> []]|] => [f2 f1 f3 [rf rf3]|] /=;
  [|by constructor].
have: option_R (sequpp_R rratC) (sequpp_non_decr_closure f3) (sequpp_non_decr_closure f2).
{ param sequpp_non_decr_closure_R. }
move: (@F_UPP_PA_non_decr_closure_correct _ f1).
move: (refinesP Rsequpp_non_decr_closure _ _ rf) => [f1' f3' rf'|].
{ move=> /(_ f1' erefl) <- H.
  by inversion_clear H; constructor; exists f1'; split=> [//|]; exists f3'. }
move=> _ H; inversion_clear H; constructor.
Qed.

Definition hDev_bounded_def f f' b := ((@hDev R f f')%:num%R <= (ratr b)%:E)%E.
Fact hDev_bounded_key : unit. Proof. by []. Qed.
Definition hDev_bounded := locked_with hDev_bounded_key hDev_bounded_def.
Canonical hDev_bounded_unlockable := [unlockable fun hDev_bounded].

Global Instance RsequppC_hDev_obounded a1 a2 a1' a2' (b1 : rat) b2 :
  refines (or_None RsequppC) a1 a2 -> refines (or_None RsequppC) a1' a2' ->
  refines rratC b1 b2 ->
  refines
    (fun T T' : Type => T' -> T)%rel
    (hDev_bounded a1 a1' b1)
    (sequpp_hDev_obounded a2 a2' b2%:E).
Proof.
rewrite !refinesE => -[+ + []|f] => [_ f2 f1 [-> rf]|];
  move=> -[++ []|f'] => [_ f2' f1' [-> rf']|] => // rb.
rewrite unlock /sequpp_hDev_obounded => H.
rewrite -[_%:E]/(er_map ratr b1%:E).
apply: F_UPP_PA_hDev_bounded_correct.
rewrite /is_true -H.
apply: refines_eq.
apply: refines_bool_eq.
apply: refines_apply.
{ apply: refines_apply.
  apply: refines_apply.
  param_comp sequpp_hDev_bounded_R. }
by rewrite refinesE; exists b1%:E; split=> [//|]; constructor.
Qed.

Definition vDev_bounded_def f f' b := (vDev f f' <= (ratr b)%:E :> \bar R)%E.
Fact vDev_bounded_key : unit. Proof. by []. Qed.
Definition vDev_bounded := locked_with vDev_bounded_key vDev_bounded_def.
Canonical vDev_bounded_unlockable := [unlockable fun vDev_bounded].

Global Instance RsequppC_vDev_obounded a1 a2 a1' a2' (b1 : rat) b2 :
  refines (or_None RsequppC) a1 a2 -> refines (or_None RsequppC) a1' a2' ->
  refines rratC b1 b2 ->
  refines
    (fun T T' : Type => T' -> T)%rel
    (vDev_bounded a1 a1' b1)
    (sequpp_vDev_obounded a2 a2' b2%:E).
Proof.
rewrite !refinesE => -[+ + []|f] => [_ f2 f1 [-> rf]|];
  move=> -[++ []|f'] => [_ f2' f1' [-> rf']|] => //= rb H.
rewrite unlock -[_%:E]/(er_map ratr b1%:E).
apply: F_UPP_PA_vDev_bounded_correct.
rewrite /is_true -H.
apply: refines_eq.
apply: refines_bool_eq.
apply: refines_apply.
{ apply: refines_apply.
  apply: refines_apply.
  param_comp sequpp_vDev_bounded_R. }
by rewrite refinesE; exists b1%:E; split=> [//|]; constructor.
Qed.

Global Instance RsequppC_deconv a1 a2 a1' a2' a1'' a2'' :
  refines (or_None RsequppC) a1 a2 -> refines (or_None RsequppC) a1' a2' ->
  refines (or_None RsequppC) a1'' a2'' ->
  refines
    (fun T T' : Type => T' -> T)%rel
    (((div (a1 : Fd) a1') : F) <= a1'')%O
    (sequpp_oleb a2 (sequpp_omin_conv a2'' a2')).
Proof.
move=> ra ra' ra''.
rewrite refinesE -F_dioidE -mul_div_equiv !F_dioidE => H.
have Hc := refinesP RsequppC_omin_conv _ _ (refinesP ra'') _ _(refinesP ra').
exact: (refinesP (RsequppC_oleb ra _)).
Qed.

End param.
End theory.
