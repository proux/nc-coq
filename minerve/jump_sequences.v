From mathcomp Require Import ssreflect ssrbool ssrfun fintype ssrnat bigop.
From mathcomp Require Import seq finfun path eqtype tuple choice order.
From mathcomp Require Import ssralg ssrnum ssrint rat intdiv.
From mathcomp Require Import boolp classical_sets signed reals ereal.

(******************************************************************************)
(* Definition of Jump Sequences: finite lists of coordinates to represent     *)
(* functions that are piecewise affine on a bounded segment. The elements of  *)
(* the list are of the shape (x, (y, (rho, sigma))) where y is the value of   *)
(* the function on input x and rho and sigma are respectively the slope and   *)
(* initial value of the affine function after x. See JS_of in PA.v for a      *)
(* formal semantics and soundness proofs of the functions below.              *)
(*                                                                            *)
(* * Main types                                                               *)
(*        preJST T == type of jump sequences carrying T (non empty lists      *)
(*                    strictly increasing in their x                          *)
(*           preJS := preJST JS_elem                                          *)
(*              JS == preJS whose first x is 0                                *)
(*                                                                            *)
(* * Main values and functions                                                *)
(*      default_JS == a dummy JS_elem                                         *)
(*         JS_zero == the constant JS equal to +oo                            *)
(*            JS_0 == the constant JS equal to 0                              *)
(*      JS_delta b == the JS equal to 0 upt ot b then +oo after b             *)
(*   JS_affine r s == a JS equal to affine function x |-> r * x + s           *)
(* JS_cst_inf x y c == a preJS equal to y in x then c on the right of x       *)
(*    JS_below a l == the JS a up to value l                                  *)
(*    JS_above a l == the preJS a starting at value l                         *)
(*   preJS_cat a b == concatenation of two preJS a and b                      *)
(*      JS_cat a b == concatenation of JS a and preJS b                       *)
(*   preJS_map f a == the preJS a with f applied to each element              *)
(*      JS_map f a == the JS a with f applied to each element                 *)
(* JS_normalize a l == a canonical form for JS a until l                      *)
(*   JS_eqb a a' l == equality test between a and a' until l                  *)
(*     JS_eval a x == the value of the JS a at x                              *)
(*  JS_max_val a l == the maximum value of the JS a on interval (head a, l]   *)
(*  JS_min_val a l == the minimum value of the JS a on interval (head a, l}   *)
(*        JS_opp a == the preJS (pointwise) opposite of a                     *)
(*  JS_add_cst c a == the JS equal to a + c with a : JS and c constant        *)
(*     JS_add a a' == the (pointwise) sum of two JS                           *)
(*     JS_min a a' == the (pointwise) min of two JS                           *)
(* JS_min_conv a a' == the min convolution of two JS                          *)
(*                                                                            *)
(* * Other types and notations                                                *)
(*       rho_sigma := rat * \bar^d rat                                        *)
(*                    type for slope and initial value of segments            *)
(*     y_rho_sigma := \bar^d rat * rho_sigma                                  *)
(*                    type for left value, slope and initial value of segment *)
(*      JS_elemT T := {nonneg rat} * t                                        *)
(*         JS_elem := JS_elemT y_rho_sigma                                    *)
(*                    type for JS elements                                    *)
(*         head_JS == notation for head default_JS                            *)
(*         last_JS == notation for last default_JS                            *)
(*          nth_JS == notation for nth default_JS                             *)
(*      lt_fst_num == strict order on JS_elem                                 *)
(*      le_fst_num == (non strict) order on JS_elem                           *)
(*                                                                            *)
(* * Other values and functions                                               *)
(* shift_rho_sigma == moves a rho_sigma                                       *)
(* shift_y_rho_sigma == moves a y_rho_sigma                                   *)
(*   shift_JS_elem == moves a JS_elem                                         *)
(* y_rho_sigma_add == add two y_rho_sigma                                     *)
(* y_rho_sigma_min == min of two y_rho_sigma                                  *)
(*  min_conv_point == min convolution of two points                           *)
(* min_conv_point_affine == min convolution of a point and an open segment    *)
(* min_conv_affine == min convolution of two open segments                    *)
(* min_conv_segment == min convolution of two closed-open segments            *)
(* min_conv_segment_JS == min convolution of a closed-open segment and a JS   *)
(*   JS_merge a a' == merges two JS on common x values (useful to defin       *)
(*                    JS_add or JS_min for instance                           *)
(*   JS_inf_left a == the JS equal to the preJS a with an initial +oo segment *)
(* JS_inf_left_eq a == same as JS_inf_left a but is also +oo at head a        *)
(*    JS_point x y == a JS equal to y in x and +oo elsewhere                  *)
(* JS_segment x y r s l linf == a JS equal to y in x, affine between x and l  *)
(*                    (excluded if linf is true), adn equal to +oo elsewhere  *)
(******************************************************************************)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope ereal_dual_scope.
Local Open Scope ring_scope.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.

Definition rho_sigma : Type := rat * \bar^d rat.

Definition y_rho_sigma : Type := \bar^d rat * rho_sigma.

Definition JS_elemT T : Type := {nonneg rat} * T.

Definition JS_elem := JS_elemT y_rho_sigma.
(* (x, (y, (rho, sigma))) for a segment of a PA function f, we will have
 * f x = y and for all t > x, f t = rho * (t - x) + sigma *)

Definition default_JS : JS_elem := (0%:nng, (+oo%E, (0, +oo%E))).

Notation head_JS := (head default_JS).

Notation last_JS := (last default_JS).

Notation nth_JS := (nth default_JS).

Definition lt_fst_num {T} (xt xt' : JS_elemT T) := xt.1%:num < xt'.1%:num.
Arguments lt_fst_num _ _ /.

Lemma lt_fst_num_trans {T} : transitive (@lt_fst_num T).
Proof. by move=> ? ? ?; apply: lt_trans. Qed.

#[export] Hint Resolve lt_fst_num_trans : core.

Definition le_fst_num {T} (xt xt' : JS_elemT T) := xt.1%:num <= xt'.1%:num.
Arguments le_fst_num _ _ /.

Lemma le_fst_num_trans {T} : transitive (@le_fst_num T).
Proof. by move=> ? ? ?; apply: le_trans. Qed.

#[export] Hint Resolve le_fst_num_trans : core.

Lemma le_fst_num_refl {T} : reflexive (@le_fst_num T).
Proof. by move=> x; apply: le_refl. Qed.

#[export] Hint Resolve le_fst_num_refl : core.

Lemma subrel_lt_le_fst_num {T} : subrel (@lt_fst_num T) le_fst_num.
Proof. by move=> x y; apply: ltW. Qed.

#[export] Hint Resolve subrel_lt_le_fst_num : core.

Record preJST (T : eqType) := {
  preJST_seq :> seq (JS_elemT T);
  _ : preJST_seq != [::];
  _ : sorted lt_fst_num preJST_seq;
}.
Arguments preJST T%_type.
Arguments Build_preJST {T}%_type.

Definition preJS := preJST y_rho_sigma.

Record JS := {
  JS_seq :> preJST y_rho_sigma;
  _ : (head_JS JS_seq).1%:num == 0;
}.
Arguments Build_JS : clear implicits.

Lemma JS_not_empty T (a : preJST T) : a != [::] :> seq (JS_elemT T).
Proof. by case: a => [[ | ]]. Qed.
Arguments JS_not_empty {T}.

Lemma JS_sorted T (a : preJST T) : sorted lt_fst_num a.
Proof. by case: a. Qed.

Lemma JS_sorted_le T (a : preJST T) : sorted le_fst_num a.
Proof. by case: a => a /= _; apply: sub_sorted. Qed.

Lemma JS_head (a : JS) : (head_JS a).1 = 0%:nng.
Proof. by case: a => a /= e; apply/eqP; rewrite -num_eq. Qed.

#[export] Hint Resolve JS_not_empty JS_sorted JS_sorted_le : core.

Lemma JS_head_behead (a : JS) : a = head_JS a :: behead a :> seq JS_elem.
Proof. by move: (JS_not_empty a); case: (a : seq _). Qed.

Definition JS_zero : JS :=
  Build_JS (Build_preJST [:: (0%:nng, (+oo%E, (0, +oo%E)))] erefl erefl) erefl.

Definition JS_0 : JS :=
  Build_JS (Build_preJST [:: (0%:nng, (0%:E, (0, 0%:E)))] erefl erefl) erefl.

Lemma size_JS_gt0 T (a : preJST T) : (0 < size a)%nat.
Proof. by case: a => -[]. Qed.

Program Definition JS_affine (r s : rat) : JS :=
  Build_JS (Build_preJST ([:: (0%:nng, (s%:E, (r, s%:E)))]) _ _) _.
Next Obligation. by []. Qed.
Next Obligation. by []. Qed.
Next Obligation. by []. Qed.

Definition JS_cst_inf (x : {nonneg rat}) (y c : \bar^d rat) : preJS :=
  Build_preJST [:: (x, (y, (0, c)))] erefl erefl.

Definition JS_below_subdef T (a : seq (JS_elemT T)) (l : {nonneg rat}) :
  seq (JS_elemT T) := [seq x <- a | x.1%:nngnum <= l%:num].

Program Definition JS_below (a : JS) l :=
  Build_JS (Build_preJST (JS_below_subdef a l) _ _) _.
Next Obligation. by move=> -[[[//|h t] /= _ _] /eqP->] l; rewrite ge0. Qed.
Next Obligation. by move=> a l; apply: sorted_filter. Qed.
Next Obligation.
move=> a l; rewrite /JS_below_subdef/=.
by case: a => -[[//|h t /= _ _] /eqP h0]; rewrite h0 ge0 /= h0.
Qed.

Lemma last_JS_below a l : (last_JS (JS_below a l)).1%:num <= l%:num.
Proof.
rewrite /JS_below/= /JS_below_subdef/=.
move: (!!(filter_all (fun x => x.1%:num <= l%:num) a)).
by case: (filter _ _) => [//=|x' a'] /allP/= al; apply/al/mem_last.
Qed.

Definition shift_rho_sigma (rs : rho_sigma) (shift : rat) : rho_sigma :=
  (rs.1, (rs.2 + (rs.1 * shift)%R%:E)).

Lemma shift_rho_sigma0 rs : shift_rho_sigma rs 0 = rs.
Proof. by case: rs => [r s]; rewrite /shift_rho_sigma /= mulr0 dadde0. Qed.

Definition shift_y_rho_sigma (rs : rho_sigma) (shift : rat) : y_rho_sigma :=
  let rs := shift_rho_sigma rs shift in (rs.2, rs).

Definition shift_JS_elem (xyrs : JS_elem) (x' : {nonneg rat}) : JS_elem :=
  if x'%:num <= xyrs.1%:num then xyrs
  else (x', shift_y_rho_sigma xyrs.2.2 (x'%:num - xyrs.1%:num)).

Lemma shift_JS_elem_correct xyrs x' : xyrs.1%:num <= x'%:num ->
  (shift_JS_elem xyrs x').1 = x'.
Proof.
move=> xlex'; rewrite /shift_JS_elem/=; case: ifP => // x'lex.
by apply: le_anti; rewrite -!num_le xlex'.
Qed.

Fixpoint JS_above_rec_subdef xyrs a (l : {nonneg rat}) :=
  match a with
  | [::] => [:: shift_JS_elem xyrs l]
  | xyrs' :: a' =>
      if xyrs'.1%:num <= l%:num then JS_above_rec_subdef xyrs' a' l
      else shift_JS_elem xyrs l :: a
  end.

Definition JS_above_subdef a (l : {nonneg rat}) :=
  match a with
  | [::] => a
  | xyrs :: a' =>
      if l%:num < xyrs.1%:num then a
      else JS_above_rec_subdef xyrs a' l
  end.

Program Definition JS_above (a : preJS) l : preJS :=
  Build_preJST (JS_above_subdef a l) _ _.
Next Obligation.
case=> -[//|xyrs a /= _ _] l; case: ifP => // _.
by elim: a xyrs => [//|xyrs' a IHa] xyrs /=; case: ifP.
Qed.
Next Obligation.
case=> -[//|xyrs a /= _ patha] l; case: leP => // xlel.
elim: a xyrs patha xlel => [//|xyrs' a IHa] xyrs /andP[xx' patha] xlel /=.
by case: leP => x'l; [apply: IHa|rewrite /= shift_JS_elem_correct// x'l].
Qed.

Lemma JS_above_head (a : JS) l : (head_JS (JS_above a l)).1 = l.
Proof.
rewrite /JS_above /JS_above_subdef/=.
case: a => -[[//|xyrs a] /= _ _] /eqP x0.
case: ltP => [|{x0}]; first by rewrite x0 lt0F.
elim: a xyrs => [| xyrs' a IHa] xyrs xlel /=.
  by rewrite shift_JS_elem_correct.
case: leP => x'l /=; first exact: IHa.
by rewrite shift_JS_elem_correct.
Qed.

Definition JS_cat_subdef (a b : seq JS_elem) : seq JS_elem :=
  [seq x <- a | lt_fst_num x (head_JS b)] ++ b.

Program Definition preJS_cat (a b : preJS) : preJS :=
  Build_preJST (JS_cat_subdef a b) _ _.
Next Obligation.
by move=> a [b + /= _]; rewrite -!size_eq0 -!lt0n size_cat addn_gt0 orbC => ->.
Qed.
Next Obligation.
move=> [a ane sa] [[//|b0 b] /= _ sb]; rewrite /JS_cat_subdef/=.
elim: a ane sa => [//|a0 a IHa _ /=].
rewrite (path_sortedE lt_fst_num_trans) => /andP[alla sa].
have sab : sorted lt_fst_num ([seq x <- a | x.1%:num < b0.1%:num] ++ b0 :: b).
  by case: a alla sa IHa => [//|a1 a] alla sa; apply.
have [a0ltb0|//] := ifP.
rewrite cat_cons/= (path_sortedE lt_fst_num_trans) sab andbT.
rewrite all_cat /= a0ltb0/=; apply/andP; split.
  by apply/allP => i /[!mem_filter]/andP[_]; apply: (allP alla).
by apply: sub_all (order_path_min lt_fst_num_trans sb) => x; apply: lt_trans.
Qed.

Lemma below_lt_empty a0 (a : seq JS_elem) l :
    path lt_fst_num a0 a -> l <= (a0.1)%:num ->
  [seq x <- a | (x.1)%:num < l] = [::].
Proof.
move=> pa la0; apply/eqP/negbFE/negbTE; rewrite -has_filter -all_predC.
apply/allP => x xa /=; rewrite -leNgt (le_trans la0) ?ltW//.
by move: x xa; apply/allP; apply: order_path_min pa; apply: lt_fst_num_trans.
Qed.

Lemma JS_cat_head a b :
  (head_JS (preJS_cat a b)).1%:num
  = Order.min (head_JS a).1%:num (head_JS b).1%:num.
Proof.
case: a b => [a ane sa] [[//|b0 b] /= _ sb]; rewrite /JS_cat_subdef/=.
elim: a ane sa => [//|a0 a IHa _ /= pa].
by have [//|a0geb0] := ltP; rewrite (below_lt_empty pa).
Qed.

Program Definition JS_cat (a : JS) (b : preJS) : JS :=
  Build_JS (preJS_cat a b) _.
Next Obligation. by move=> a b; rewrite JS_cat_head JS_head min_l/=. Qed.

Definition JS_delta (b : rat) : JS :=
  if leP 0 b is Order.LelNotGt bge0 then
    JS_cat JS_0 (JS_cst_inf (NngNum bge0) 0%:E +oo%E)
  else Build_JS (JS_cst_inf 0%:nng +oo%E +oo%E) erefl.

Definition preJS_map_subdef T T' (f : {nonneg rat} -> T -> T')
    (a : seq (JS_elemT T))
  := [seq (xt.1, f xt.1 xt.2) | xt <- a].

Program Definition preJS_map (T T' : eqType) (f : {nonneg rat} -> T -> T')
  (a : preJST T) : preJST T' := Build_preJST (preJS_map_subdef f a) _ _.
Next Obligation. by move=> T T' f [[]]. Qed.
Next Obligation.
by move=> T T' f [a /= _]; rewrite sorted_map; apply: sub_sorted.
Qed.

Lemma preJS_map_head (T T' : eqType) x0 x1 (f : {nonneg rat} -> T -> T') a :
  (head x0 (preJS_map f a)).1 = (head x1 a).1.
Proof. by case: a => -[//|x a /= _ _]. Qed.

Program Definition JS_map (f : {nonneg rat} -> y_rho_sigma -> y_rho_sigma)
  (a : JS) : JS := Build_JS (preJS_map f a) _.
Next Obligation.
by move=> f a; rewrite (preJS_map_head _ default_JS) JS_head.
Qed.

Lemma head_JS_map (T T' : eqType) (f : {nonneg rat} -> T -> T') a x x' :
  (head x (preJS_map f a)).1 = (head x' a).1.
Proof. by case: a => -[//|y a] /= _ _. Qed.

Definition JS_normalize (a : preJS) (l : {nonneg rat}) : preJS :=
  let norm x '(y, (r, s)) :=
    let rs :=
      if l%:num <= x%:num then (0, +oo%E)
      else ((if s is EFin _ then r else 0), s) in
    (y, rs) in
  preJS_map norm a.

Definition JS_merge_subdef (a a' : preJS) :=
  let fix aux px prs prs' (a a' : seq JS_elem) :=
      match a with
      | [::] =>
        (fix aux' px prs prs' (a' : seq JS_elem) :=
           match a' with
           | [::] => [::]
           | (x', yrs') :: s' =>
             let yrs := shift_y_rho_sigma prs (x'%:nngnum - px%:nngnum) in
             (x', (yrs, yrs')) :: aux' x' yrs.2 yrs'.2 s'
           end) px prs prs' a'
      | (x, yrs) :: s =>
        (fix aux' px prs prs' a' :=
           match a' with
           | [::] =>
             let yrs' := shift_y_rho_sigma prs' (x%:nngnum - px%:nngnum) in
             (x, (yrs, yrs')) :: aux x yrs.2 yrs'.2 s [::]
           | (x', yrs') :: s' =>
             if (x%:num < x'%:num)%O then
               let yrs' := shift_y_rho_sigma prs' (x%:nngnum - px%:nngnum) in
               (x, (yrs, yrs')) :: aux x yrs.2 yrs'.2 s a'
             else if (x'%:num < x%:num)%O then
               let yrs := shift_y_rho_sigma prs (x'%:nngnum - px%:nngnum) in
               (x', (yrs, yrs')) :: aux' x' yrs.2 yrs'.2 s'
             else (* x = x' *) (x, (yrs, yrs')) :: aux x yrs.2 yrs'.2 s s'
        end) px prs prs' a'
      end in
  match (a : seq _, a' : seq _) with
  | ([::], _) | (_, [::]) => [::]
  | (((x, yrs) :: a), ((_, yrs') :: a')) =>
    (x, (yrs, yrs')) :: aux x yrs.2 yrs'.2 a a'
  end.

Program Definition JS_merge (a a' : JS) : preJST (y_rho_sigma * y_rho_sigma) :=
  Build_preJST (JS_merge_subdef a a') _ _.
Next Obligation.
rewrite /JS_merge_subdef => a a'.
case: a' => -[a' /= + _] _.
case: a => -[a /= + _] _.
by case: a => [//|[? ?] ?] _; case: a' => [//|[? ?] ?].
Qed.
Next Obligation.
move=> a a'.
move: (JS_head a) (JS_head a') (JS_sorted a) (JS_sorted a').
move: (JS_not_empty a) (JS_not_empty a').
rewrite /JS_merge_subdef.
case: (a : seq _) => [//|[x [y rs]] {}a] /=.
case: (a' : seq _) => [//|[x' [y' rs']] {}a'] /= _ _ {1}<- -> {x'}.
elim: a x y rs y' rs' a' => [|[x1 [y1 rs1]] a IHa] x y rs y' rs' a'.
{ move=> _.
  elim: a' x y rs y' rs' => [//|[x1' [y1' rs1']] a' IHa'] x y rs y' rs' /=.
  move=> /andP [Hxx1'] Hp'; rewrite Hxx1' /=.
  exact: IHa'. }
elim: a' x y rs y' rs' => [|[x1' [y1' rs1']] a' IHa'] x y rs y' rs' /=.
{ move=> /andP [Hxx1 Hp] _; rewrite Hxx1 /=.
  exact: IHa. }
move=> /andP [Hxx1 Hp] /andP [Hxx1' Hp'].
case: ltP => Hx1x1' /=.
{ rewrite Hxx1 /=.
  apply: IHa => //=.
  by rewrite Hx1x1'. }
case: ltP => Hx1'x1 /=.
{ rewrite Hxx1' /=.
  apply: IHa' => //=.
  by rewrite Hx1'x1. }
rewrite Hxx1 /=.
apply: IHa => //.
by move: Hx1x1'; rewrite le_eqVlt ltNge Hx1'x1 orbF num_eq => /eqP<-.
Qed.

Lemma JS_merge_head (a a' : JS) :
  (head (0%:nng, ((+oo%E, (0, +oo%E)), (+oo%E, (0, +oo%E)))) (JS_merge a a')).1
  = 0%:nng.
Proof.
rewrite /JS_merge /JS_merge_subdef.
case: a' => -[a' /= _ _] +.
case: a => -[a /= _ _] +.
rewrite -[0]/(!!(0%:nng%:num : rat)) !num_eq.
by case: a => [//|[? ?] ?] /= /eqP ->; case: a' => [//|[? ?] ?].
Qed.

Definition JS_eqb (a a' : JS) (l : {nonneg rat}) : bool :=
  let a'' := JS_merge a a' in
  JS_normalize (preJS_map (fun=> fst) a'') l
  == JS_normalize (preJS_map (fun=> snd) a'') l :> seq _.

Definition JS_opp : preJS -> preJS :=
  preJS_map (fun _ yrs => (- yrs.1, ((- yrs.2.1)%R, - yrs.2.2))%E).

Lemma head_JS_opp a : (head_JS (JS_opp a)).1 = (head_JS a).1.
Proof. exact: head_JS_map. Qed.

Definition JS_eval (a : seq JS_elem) (x : {nonneg rat}) : \bar^d rat :=
  let fix aux xyrs a :=
    if x%:num <= xyrs.1%:num then xyrs.2.1 else
      let seg := (shift_rho_sigma xyrs.2.2 (x%:num - xyrs.1%:num)).2 in
      match a with
      | [::] => seg
      | xyrs' :: a => if x%:num < xyrs'.1%:num then seg else aux xyrs' a
      end in
  if a is xyrs :: a then aux xyrs a else 0%:E.

Definition JS_max_val (a : seq JS_elem) (l : {nonneg rat}) : \bar^d rat :=
  let fix aux xyrs a :=
    let x := xyrs.1 in
    let '(r, s) := xyrs.2.2 in
    match a with
    | [::] =>
        if l%:num <= x%:num then -oo%E else
          if r <= 0 then s else (s + (r * (l%:num - x%:num))%:E)%dE
    | ((x', (y', _)) as xyrs') :: a =>
      let m := if r <= 0 then s else (s + (r * (x'%:num - x%:num))%:E)%dE in
      maxe (maxe m y') (aux xyrs' a)
    end in
  if a is xyrs :: a then aux xyrs a else -oo%E.

Definition JS_min_val (a : preJS) (l : {nonneg rat}) : \bar^d rat :=
  oppe (JS_max_val (JS_opp a) l).

Definition JS_add_cst (c : \bar^d rat) : JS -> JS :=
  JS_map (fun _ yrs => (yrs.1 + c, (yrs.2.1, yrs.2.2 + c))).

Definition y_rho_sigma_add (yrs yrs' : y_rho_sigma) : y_rho_sigma :=
  (yrs.1 + yrs'.1, (yrs.2.1 + yrs'.2.1, yrs.2.2 + yrs'.2.2)).

Definition JS_add_subdef (a a' : JS) : preJS :=
  preJS_map
    (fun _ '(yrs, yrs') => y_rho_sigma_add yrs yrs')
    (JS_merge a a').

Program Definition JS_add (a a' : JS) : JS := Build_JS (JS_add_subdef a a') _.
Next Obligation.
move=> a a'; rewrite /JS_add_subdef.
move: (JS_merge_head a a'); rewrite /JS_merge/=.
by case: JS_merge_subdef => [//|[? [[? [? ?]] [? [? ?]]]] ?] /= ->.
Qed.

Definition y_rho_sigma_min (yrs yrs' : y_rho_sigma) (x x' : {nonneg rat}) :
    seq JS_elem :=
  let y := Order.min yrs.1 yrs'.1 in
  match yrs.2.2, yrs'.2.2 with
  | -oo%E, _ | _, -oo%E => [:: (x, (y, (0, -oo%E)))]
  | +oo%E, _ => [:: (x, (y, yrs'.2))]
  | _, +oo%E => [:: (x, (y, yrs.2))]
  | EFin s, EFin s' =>
      let r := yrs.2.1 in let r' := yrs'.2.1 in
      if r == r' then [:: (x, (y, (r, (Order.min s s')%:E)))] else
        let x'' := x%:num + (s' - s) / (r - r') in
        if x'' <= x%:num then
          [:: (x, (y, if r <= r' then yrs.2 else yrs'.2))]
        else if x'%:num <= x'' then
          [:: (x, (y, if r <= r' then yrs'.2 else yrs.2))]
        else
          let shift rs := shift_y_rho_sigma rs (x'' - x%:num) in
          let x'' := insubd 0%:nng x'' in
          if r <= r' then [:: (x, (y, yrs'.2)); (x'', shift yrs.2)]
          else (* r' < r *) [:: (x, (y, yrs.2)); (x'', shift yrs'.2)]
  end.

Lemma y_rho_sigma_min_not_empty yrs yrs' x x' :
  y_rho_sigma_min yrs yrs' x x' != [::].
Proof.
case: yrs yrs' => [y [r [s||]]] [y' [r' [s'||]]] //.
rewrite /y_rho_sigma_min/=; case: (@eqP _ r r') => [//|_].
by do 3![case: leP => [//|_]].
Qed.

Lemma head_y_rho_sigma_min yrs yrs' x x' :
  (head_JS (y_rho_sigma_min yrs yrs' x x')).1 = x.
Proof.
case: yrs yrs' => [y [r [s||]]] [y' [r' [s'||]]] //.
rewrite /y_rho_sigma_min/=; case: (@eqP _ r r') => _ //.
by do 3![case: leP => _ //].
Qed.

Lemma heady_y_rho_sigma_min yrs yrs' x x' :
  (head_JS (y_rho_sigma_min yrs yrs' x x')).2.1 = mine yrs.1 yrs'.1.
Proof.
case: yrs yrs' => [y [r [s||]]] [y' [r' [s'||]]] //.
rewrite /y_rho_sigma_min/=; case: (@eqP _ r r') => _ //.
by do 3![case: leP => _ //].
Qed.

Lemma y_rho_sigma_min_lt yrs yrs' x x' : x%:num < x'%:num ->
  all (fun xyrs : JS_elem => xyrs.1%:num < x'%:num)
    (y_rho_sigma_min yrs yrs' x x').
Proof.
case: yrs yrs' => [y [r [s||]]] [y' [r' [s'||]]]; [|by rewrite /= andbT//..].
move=> xltx'; rewrite /y_rho_sigma_min/=.
case: (@eqP _ r r') => _; first by rewrite /= andbT.
case: leP => _; first by rewrite /= andbT.
case: leP => x''ltx'; first by rewrite /= andbT.
by case: leP => _ /=; rewrite xltx' andbT/=; rewrite /insubd;
  case: insubP => [_ _ -> //|_ /=]; apply: le_lt_trans xltx'.
Qed.

Lemma sorted_y_rho_sigma_min yrs yrs' x x' :
  sorted lt_fst_num (y_rho_sigma_min yrs yrs' x x').
Proof.
case: yrs yrs' => [y [r [s||]]] [y' [r' [s'||]]] //.
rewrite /y_rho_sigma_min/=; case: (@eqP _ r r') => [//|] _.
case: leP => [//|] xltx''; case: leP => [//|] x''ltx'.
by case: leP => _ /=; rewrite andbT/=; rewrite /insubd;
  case: insubP => [? ? -> //|/=]; rewrite -ltNge; apply: lt_trans.
Qed.

Fixpoint JS_min_rec_subdef x yrs yrs' a l :=
  match a with
  | [::] => y_rho_sigma_min yrs yrs' x l
  | (x2, (yrs2, yrs2')) :: a =>
      y_rho_sigma_min yrs yrs' x x2 ++ JS_min_rec_subdef x2 yrs2 yrs2' a l
  end.

Definition JS_min_subdef a a' l :=
  match JS_merge a a' : seq _ with
  | [::] => [::]
  | (x, (yrs, yrs')) :: a => JS_min_rec_subdef x yrs yrs' a l
  end.

Program Definition JS_min (a a' : JS) (l : {nonneg rat}) : JS :=
  Build_JS (Build_preJST (JS_min_subdef a a' l) _ _) _.
Next Obligation.
move=> a a' l; have := JS_not_empty (JS_merge a a').
rewrite /JS_min_subdef/=; case: JS_merge_subdef => [//|[x [y y']] c] _ /=.
case: c => [|[x2 [y2 y2']] c] /=; first exact: y_rho_sigma_min_not_empty.
by have := y_rho_sigma_min_not_empty y y' x x2; case: y_rho_sigma_min.
Qed.
Next Obligation.
move=> a a' l.
have := JS_sorted (JS_merge a a').
have := JS_not_empty (JS_merge a a').
rewrite /JS_min_subdef/=; case: JS_merge_subdef => [//|[x [y y']] c] _ /= pc.
set mi := JS_min_rec_subdef x y y' c l.
suff: [&& mi != [::], (head_JS mi).1 == x & sorted lt_fst_num mi].
  by move=> /and3P[].
rewrite {}/mi.
elim: c x y y' pc => [|[x2 [y2 y2']] c IHc] x y y' pc.
  rewrite /= y_rho_sigma_min_not_empty head_y_rho_sigma_min eqxx.
  by rewrite sorted_y_rho_sigma_min.
rewrite /JS_min_rec_subdef -/JS_min_rec_subdef.
have mne := y_rho_sigma_min_not_empty y y' x x2.
have hm := head_y_rho_sigma_min y y' x x2.
apply/and3P; split.
- by move: mne; case: y_rho_sigma_min.
- by move: mne hm; case: y_rho_sigma_min => [//|? ?] _ /= ->.
have /and3P[] := IHc _ _ _ (proj2 (andP pc)).
have xltx2 : x < x2 by move: pc => /andP[].
move: mne (sorted_y_rho_sigma_min y y' x x2) (y_rho_sigma_min_lt y y' xltx2).
case: y_rho_sigma_min => [//|hs s] _ ps.
case: JS_min_rec_subdef => [//|hs' s'] als _ /eqP hs'x2 /= ps'.
rewrite -cat_rcons cat_path last_rcons ps' rcons_path andbT /=.
by apply/andP; split=> //; rewrite hs'x2; apply: (allP als); rewrite mem_last.
Qed.
Next Obligation.
move=> a a' l /=; rewrite /JS_min_subdef; have := JS_merge_head a a'.
case: JS_merge (JS_not_empty (JS_merge a a')) => -[//|[x [y y']] t] _ _ _ /= ->.
case: t => [|[x' [? ?]] ?] /=; first by rewrite head_y_rho_sigma_min.
have := !!(head_y_rho_sigma_min y y' 0%:nng x').
have := !!(y_rho_sigma_min_not_empty y y' 0%:nng x').
by case: y_rho_sigma_min => [//|? ?] _ /= ->.
Qed.

Program Definition JS_inf_left (a : preJS) : JS :=
  Build_JS
    (Build_preJST
       (if (head_JS a).1%:num > 0%R then default_JS :: a else a) _ _) _.
Next Obligation. by case=> -[//|xyrs a] /= _ _; case: ltP. Qed.
Next Obligation. by case=> -[//|xyrs a] /= _ pa; case: ltP => /=[->|]. Qed.
Next Obligation.
case=> -[//|[x yrs] a] /= _ _; case: ltP => [//|xle0] /=.
by apply/eqP/le_anti; rewrite xle0 /=.
Qed.

Program Definition JS_inf_left_eq (a : preJS) : JS :=
  let xyrs := head_JS a in
  let a := (xyrs.1, (+oo%E, xyrs.2.2)) :: behead a in
  Build_JS
    (Build_preJST (if xyrs.1%:num > 0%R then default_JS :: a else a) _ _) _.
Next Obligation. by case=> -[//|xyrs a] /= _ _; case: ltP. Qed.
Next Obligation.
by case=> -[//|xyrs a] /= _ pa; case: ltP => /=[->|]; case: a pa.
Qed.
Next Obligation.
case=> -[//|[x yrs] a] /= _ _; case: ltP => [//|xle0] /=.
by apply/eqP/le_anti; rewrite xle0 /=.
Qed.

Program Definition JS_point (x : {nonneg rat}) (y : \bar^d rat) : JS :=
  JS_inf_left (Build_preJST [:: (x, (y, (0, +oo%E)))] _ _).
Next Obligation. by []. Qed.
Next Obligation. by []. Qed.

Lemma JS_segment_subproof (x : {nonneg rat}) (y : \bar^d rat)
    (r : rat) (s : \bar^d rat) l : x%:num < l%:num ->
  sorted lt_fst_num [:: (x, (y, (r, s))); (l, (+oo, (0%R, +oo))%E)].
Proof. by move=> /= ->. Qed.

Definition JS_segment (x : {nonneg rat}) (y : \bar^d rat)
    (r : rat) (s : \bar^d rat) (l : {nonneg rat}) (linf : bool) : JS :=
  if leP l%:num x%:num isn't Order.GtlNotLe p then JS_zero else
    let y' := if linf then +oo%E else (r * (l%:num - x%:num))%:dE + s in
    JS_inf_left
      (Build_preJST [:: (x, (y, (r, s))); (l, (y', (0%R, +oo))%E)]
         erefl (JS_segment_subproof _ _ _ p)).

Definition min_conv_point (x : {nonneg rat}) (y : \bar^d rat)
    (x' : {nonneg rat}) (y' : \bar^d rat) : JS :=
  JS_point (x%:nngnum + x'%:nngnum)%:nng (y + y')%dE.

Definition min_conv_point_affine (x : {nonneg rat}) (y : \bar^d rat)
    (x' : {nonneg rat}) (r' : rat) (s' : \bar^d rat)
    (l' : {nonneg rat}) (l'inf : bool) : JS :=
  JS_segment (x%:nngnum + x'%:nngnum)%:nng +oo%E r' (s' + y)%dE
    (x%:nngnum + l'%:nngnum)%:nng l'inf.

Definition min_conv_affine_subdef r s (x l : {nonneg rat}) linf
    r' s' (x' l' : {nonneg rat}) l'inf : seq JS_elem :=
  if (l%:num <= x%:num) || (l'%:num <= x'%:num) then JS_zero : seq _ else
    if r == r' then
      JS_segment (x%:num + x'%:num)%:nng +oo%E r (s + s')%dE
        (l%:num + l'%:num)%:nng (linf || l'inf) : seq _
    else
      let y := if linf || l'inf then +oo%E else
        (r * (l%:num - x%:num) + r' * (l'%:num - x'%:num))%:dE + s + s' in
      if r < r' then
        [:: ((x%:num + x'%:num)%:nng, (+oo%E, (r, (s + s')%dE)));
         ((x'%:num + l%:num)%:nng,
           (((r * (l%:num - x%:num))%R%:E + s + s')%dE,
             (r', ((r * (l%:num - x%:num))%R%:E + s + s')%dE)));
         ((l%:num + l'%:num)%:nng, (y, (0, +oo%E)))]
      else
        [:: ((x%:num + x'%:num)%:nng, (+oo%E, (r', (s + s')%dE)));
         ((x%:num + l'%:num)%:nng,
           (((r' * (l'%:num - x'%:num))%R%:E + s' + s)%dE,
             (r, ((r' * (l'%:num - x'%:num))%R%:E + s' + s)%dE)));
         ((l%:num + l'%:num)%:nng, (y, (0, +oo%E)))].

Program Definition min_conv_affine
    (r : rat) (s : \bar^d rat) (x l : {nonneg rat}) (linf : bool)
    (r' : rat) (s' : \bar^d rat) (x' l' : {nonneg rat}) (l'inf : bool) : JS :=
  JS_inf_left
    (Build_preJST (min_conv_affine_subdef r s x l linf r' s' x' l' l'inf) _ _).
Next Obligation.
move=> r s x l linf r' s' x' l' l'inf; rewrite /min_conv_affine_subdef.
by do 2![have [|_] := ifP; first by rewrite JS_not_empty]; case: ifP.
Qed.
Next Obligation.
move=> r s x l linf r' s' x' l' l'inf; rewrite /min_conv_affine_subdef.
have [|/negbT] := ifP; first by rewrite JS_sorted.
rewrite negb_or -!ltNge => /andP[xltl x'ltl'].
have [_|_] := ifP; first by rewrite JS_sorted.
have [_|_] := ifP => /=.
- by rewrite addrC ltrD2l xltl addrC ltrD2l x'ltl'.
- by rewrite ltrD2l x'ltl' ltrD2r xltl.
Qed.

Lemma min_conv_affine_subdefC x r s l linf x' r' s' l' l'inf :
  min_conv_affine_subdef x r s l linf x' r' s' l' l'inf
  = min_conv_affine_subdef x' r' s' l' l'inf x r s l linf.
Proof.
rewrite /min_conv_affine_subdef orbC; case: ifP => [//|_].
rewrite eq_sym; case: ifP => [/eqP->|].
  by congr JS_segment; do 1?[apply: val_inj]; rewrite /= 1?orbC// addrC.
have [_ _|_ _|//] := ltgtP; do ?[congr cons]; congr pair.
- by apply/val_inj; rewrite /= addrC.
- by rewrite addrC.
- by apply/val_inj; rewrite /= addrC.
- by rewrite orbC; case: ifP => [//|_]; rewrite /= [X in X%:E]addrC addrAC.
- by apply/val_inj; rewrite /= addrC.
- by rewrite addrC.
- by apply/val_inj; rewrite /= addrC.
- by rewrite orbC; case: ifP => [//|_]; rewrite /= [X in X%:E]addrC addrAC.
Qed.

Definition min_conv_segment (x : {nonneg rat}) (y : \bar^d rat)
    (r : rat) (s : \bar^d rat) (l : {nonneg rat}) (linf : bool)
    (x' : {nonneg rat}) (y' : \bar^d rat)
    (r' : rat) (s' : \bar^d rat) (l' : {nonneg rat}) (l'inf : bool)
    (l'' : {nonneg rat}) : JS :=
  if l%:num <= x%:num then
    if l'%:num <= x'%:num then JS_below (min_conv_point x y x' y') l'' else
      JS_min
        (JS_below (min_conv_point x y x' y') l'')
        (JS_below (min_conv_point_affine x y x' r' s' l' l'inf) l'')
        l''
  else if l'%:num <= x'%:num then
    JS_min
      (JS_below (min_conv_point x y x' y') l'')
      (JS_below (min_conv_point_affine x' y' x r s l linf) l'')
      l''
  else
    JS_min
      (JS_min
         (JS_below (min_conv_point x y x' y') l'')
         (JS_below (min_conv_point_affine x y x' r' s' l' l'inf) l'')
         l'')
      (JS_min
         (JS_below (min_conv_point_affine x' y' x r s l linf) l'')
         (JS_below (min_conv_affine r s x l linf r' s' x' l' l'inf) l'')
         l'')
      l''.

Fixpoint min_conv_segment_JS_rec_subdef x y r s l linf
    x' y' r' s' (a : seq JS_elem) (l' : {nonneg rat}) : JS :=
  match a with
  | [::] => min_conv_segment x y r s l linf x' y' r' s' l' false l'
  | (x'', (y'', (r'', s''))) :: a =>
      JS_min (min_conv_segment x y r s l linf x' y' r' s' x'' true l')
        (min_conv_segment_JS_rec_subdef x y r s l linf x'' y'' r'' s'' a l')
        l'
  end.

Definition min_conv_segment_JS (x : {nonneg rat}) (y : \bar^d rat)
    (r : rat) (s : \bar^d rat) (l : {nonneg rat}) (linf : bool)
    (a : seq JS_elem) (l' : {nonneg rat}) : JS :=
  match a with
  | [::] => JS_zero
  | (x', (y', (r', s'))) :: a =>
      min_conv_segment_JS_rec_subdef x y r s l linf x' y' r' s' a l'
  end.

Fixpoint JS_min_conv_rec_subdef x y r s (a b : seq JS_elem)
    (l : {nonneg rat}) : JS :=
  match a with
  | [::] => min_conv_segment_JS x y r s l false b l
  | (x', (y', (r', s'))) :: a =>
      JS_min (min_conv_segment_JS x y r s x' true b l)
        (JS_min_conv_rec_subdef x' y' r' s' a b l)
        l
  end.

Definition JS_min_conv (a b : seq JS_elem) (l : {nonneg rat}) : JS :=
  match a with
  | [::] => JS_zero
  | (x, (y, (r, s))) :: a => JS_min_conv_rec_subdef x y r s a b l
  end.
