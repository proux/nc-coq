From mathcomp Require Import all_ssreflect.
Require Import tactic.

Local Open Scope ereal_scope.
Local Open Scope ring_scope.
Local Open Scope order_scope.
Local Open Scope dioid_scope.

Section Test.

Variable R : realType.

Notation F := (@F R).

Section Test1.

Definition all_finite : _ -> @seqjs bigQ :=
  map (fun xyrs => (xyrs.1, (xyrs.2.1%:E, (xyrs.2.2.1, xyrs.2.2.2%:E)))).

(* here, Coq would complain if the definition is ill formed
   (sequence not sorted or not starting at 0 for instance) *)
Let f : F := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 2;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    all_finite
      [:: (0, (0, (2, 0)))] |}%bigQ.

(* Uncomment the following lines to ease examination of definitions below. *)

(* From mathcomp Require Import ssrint ssralg ssrnum rat signed. *)
(* From CoqEAL Require Import hrel param refinements. *)
(* From NCCoq Require Import RminStruct. *)
(* Import Refinements.Op. *)

(* Looking at file tactic.v, one can see that F_of_sequpp is an abbreviation
   calling F_of_sequppC. To examinate its semantics, one can: *)

(* Check F_of_sequppC_correct. *)

(* seqjs_spec and spec are basically translations from bigQ (C here) to Q
   (mathcomp's rationals rat). Then one can examinate PA.F_of_JS_def
   and UPP.F_UPP_of_F_def: *)

(* Print PA.F_of_JS_def. *)
(* Print UPP.F_UPP_of_F_def. *)

Let f' : F := F_of_sequpp {|
  sequpp_T := 4;
  sequpp_d := 4;
  sequpp_c := 3;
  sequpp_js :=
    all_finite
      [:: (0, (2, (0, 2)));
      (2, (2, (0, 3)));
      (4, (3, (0, 5)));
      (6, (5, (0, 6)))] |}%bigQ.

Let g : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 1;
  sequpp_c := 1 / 11;
  sequpp_js :=
    all_finite
      [:: (0, (0, (1 / 3, 0)));
      (3, (1, (1 / 11, 1)))] |}%bigQ.

Let h : F := F_of_sequpp {|
  sequpp_T := 4;
  sequpp_d := 4;
  sequpp_c := 37 / 11;
  sequpp_js :=
    all_finite
      [:: (0, (0, (7 / 3, 0)));
      (1, ((7 / 3), (1 / 3, 7 / 3)));
      (2, ((8 / 3), (1 / 3, 11 / 3)));
      (3, (4, (1 / 11, 4)));
      (4, ((45 / 11), (1 / 11, 67 / 11)));
      (6, ((69 / 11), (1 / 11, 80 / 11)))] |}%bigQ.

Goal ((f \min f') : F) + g = h.
Proof. nccoq. Qed.
(* Goals handled by the automatic nccoq tactic are:
     goal ::= expr = expr | exp <= expr | (expr : Fd) / expr <= expr :> F
            | hDev_bounded expr expr cst | vDev_bounded expr expr cst
     expr ::= (expr : F) + expr | ((expr \min expr) : F) | (expr : Fd) * expr
            | - expr | expr - expr | non_decr_closure expr | fupp
   where fupp are obtained through F_of_sequpp as above. *)

(* F_min and F_plus can be inspected: *)

(* Locate "_ \min _". *)
(* Print Order.min_fun. *)
(* Locate "_ + _". *)

(* Test incorrect inputs *)

Fail Let fT : F := F_of_sequpp {|
  sequpp_T := -1;
  sequpp_d := 4;
  sequpp_c := 3;
  sequpp_js :=
    all_finite
      [:: (0, (2, (0, 2)));
      (2, (2, (0, 3)));
      (4, (3, (0, 5)));
      (6, (5, (0, 6)))] |}%bigQ.

Fail Let fd : F := F_of_sequpp {|
  sequpp_T := 4;
  sequpp_d := 0;
  sequpp_c := 3;
  sequpp_js :=
    all_finite
      [:: (0, (2, (0, 2)));
      (2, (2, (0, 3)));
      (4, (3, (0, 5)));
      (6, (5, (0, 6)))] |}%bigQ.

Fail Let flast : F := F_of_sequpp {|
  sequpp_T := 4;
  sequpp_d := 2;
  sequpp_c := 3;
  sequpp_js :=
    all_finite
      [:: (0, (2, (0, 2)));
      (2, (2, (0, 3)));
      (4, (3, (0, 5)));
      (7, (5, (0, 6)))] |}%bigQ.

Fail Let fempty : F := F_of_sequpp {|
  sequpp_T := 4;
  sequpp_d := 4;
  sequpp_c := 3;
  sequpp_js := [::] |}%bigQ.

Fail Let f0 : F := F_of_sequpp {|
  sequpp_T := 4;
  sequpp_d := 4;
  sequpp_c := 3;
  sequpp_js :=
    all_finite
      [:: (1, (2, (0, 2)));
      (2, (2, (0, 3)));
      (4, (3, (0, 5)));
      (6, (5, (0, 6)))] |}%bigQ.

Fail Let fsorted : F := F_of_sequpp {|
  sequpp_T := 4;
  sequpp_d := 4;
  sequpp_c := 3;
  sequpp_js :=
    all_finite
      [:: (0, (2, (0, 2)));
      (2, (2, (0, 3)));
      (2, (3, (0, 5)));
      (6, (5, (0, 6)))] |}%bigQ.

End Test1.

Section Test2.

Let f : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 1;
  sequpp_c := 1 / 11;
  sequpp_js :=
    [:: (0, (EFin 1, (0, EFin 1)));
    (3, (+oo, (0, +oo)))] |}%bigQ.

Let g : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 2;
  sequpp_c := 0;
  sequpp_js :=
    all_finite
      [:: (0, (2, (0, 2)))] |}%bigQ.

Let h : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js :=
    all_finite
      [:: (0, (1, (0, 1)));
      (3, (2, (0, 2)))] |}%bigQ.

Goal f \min g = h.
Proof. nccoq. Qed.

Let f' : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 12;
  sequpp_c := 42;
  sequpp_js :=
    [:: (0, (EFin 1, (0, EFin 1)));
    (3, (+oo, (0, +oo)))] |}%bigQ.

Goal f = f'.
Proof. nccoq. Qed.

End Test2.

Section Test3.

Let f : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 1;
  sequpp_c := 2;
  sequpp_js :=
    all_finite
      [:: (0, (0, (2, 0)));
      (1, (2, (0, 2)));
      (3, (2, (2, 2)))] |}%bigQ.

Let g : F := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 1;
  sequpp_js := all_finite [:: (0, (0, (1, 0)))] |}%bigQ.

Let h : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 1;
  sequpp_c := 1;
  sequpp_js :=
    all_finite
      [:: (0, (0, (1, 0)));
      (2, (2, (0, 2)));
      (3, (2, (1, 2)))] |}%bigQ.

Goal (f : Fd) * g = h.
Proof. nccoq. Qed.

End Test3.

Section Test4.

Let f : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 1;
  sequpp_c := 2;
  sequpp_js := all_finite [:: (0, (1, (0, 1)))] |}%bigQ.

Let g : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 1;
  sequpp_c := 2;
  sequpp_js := all_finite [:: (0, (2, (1, 2)))] |}%bigQ.

Goal f <= g.
Proof. nccoq. Qed.

End Test4.

Section Test5.

Let f : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 1;
  sequpp_c := 2;
  sequpp_js := all_finite [:: (0, (1, (1, 1)))] |}%bigQ.

Let g : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 1;
  sequpp_c := 2;
  sequpp_js :=
    all_finite
      [:: (0, (0, (0, 0)));
      (1, (0, (3, 0)))] |}%bigQ.

Goal vDev_bounded f g 2.
Proof. nccoq. Qed.

End Test5.

Section Test6.

Let f : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 1;
  sequpp_c := 2;
  sequpp_js := all_finite [:: (0, (1, (2, 1)))] |}%bigQ.

Let g : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 1;
  sequpp_c := -2;
  sequpp_js := all_finite [:: (0, (-1, (-2, -1)))] |}%bigQ.

(* TODO *)
(* Goal f = (\- g)%E. *)
(* Proof. nccoq. Qed. *)

End Test6.

Section Test7.

Let f : F := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js :=
    all_finite
      [:: (0, (0, (2, 0)));
      (1, (2, (0, 2)))] |}%bigQ.

Let g : F := F_of_sequpp {|
  sequpp_T := 4;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js :=
    all_finite
      [:: (0, (0, (0, 0)));
      (2, (0, (1, 0)));
      (4, (2, (0, 2)))] |}%bigQ.

Goal hDev_bounded f g 3.
Proof. nccoq. Qed.

End Test7.

Section Test8.

Let f : F := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js :=
    all_finite
      [:: (0, (0, (2, 0)));
      (1, (2, (0, 2)))] |}%bigQ.

Goal non_decr_closure f = f.
Proof. nccoq. Qed.

End Test8.

Section Test9.

Let f : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 1;
  sequpp_c := 2;
  sequpp_js :=
    all_finite
      [:: (0, (0, (2, 0)));
      (1, (2, (0, 2)));
      (3, (2, (2, 2)))] |}%bigQ.

Let g : F := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 1;
  sequpp_js := all_finite [:: (0, (0, (1, 0)))] |}%bigQ.

Let h : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 1;
  sequpp_c := 1;
  sequpp_js :=
    all_finite
      [:: (0, (0, (1, 0)));
      (2, (2, (0, 2)));
      (3, (2, (1, 2)))] |}%bigQ.

Goal (h : Fd) / g <= f :> F.
Proof. nccoq. Qed.

End Test9.

Section NFM_test.

Definition map_sequpp (T : BigQ.t_) d c l := {|
  sequpp_T := T;
  sequpp_d := d;
  sequpp_c := c;
  sequpp_js := all_finite l
|}.

Let f : F := F_of_sequpp (map_sequpp 4 4 3 [:: (0, (0, (2, 0))); (1, (2, (0, 2)));
                                       (2, ( 2,  (0, 3)));   (4, ( 3,  (0, 5)));
                                       (6, ( 5,  (0, 6)))])%bigQ.

Let g : F := F_of_sequpp (map_sequpp 4 4 (4/11) [:: (0, (0, (1 / 3, 0)));
                                            (3, (1, (1 / 11, 1))) ])%bigQ.

Let h : F := F_of_sequpp (map_sequpp 4 4 (37 / 11) [:: (0, ( 0, (7 / 3, 0)));
                                               (1, (7 / 3, (1 / 3, 7 / 3)));
                                               (2, (8 / 3, (1 / 3, 11 / 3)));
                                               (3, (4, (1 / 11, 4)));
                                               (4, (45 / 11, (1 / 11, 67 / 11)));
                                               (6, (69 / 11, (1 / 11, 80 / 11)))
    ])%bigQ.

Goal f + g = h.
Proof. nccoq. Qed.

End NFM_test.

End Test.
