Require Import ZArith QArith.
From Bignums Require Export BigQ.
From mathcomp Require Import all_ssreflect.
From mathcomp Require Export ssrbool seq order ssralg boolp.
From mathcomp Require Import ssrint ssrnum rat.
From CoqEAL Require Import hrel param.
From CoqEAL Require Export refinements binrat.
From mathcomp Require Export ereal reals.
From mathcomp.dioid Require Export complete_dioid.
From NCCoq Require Export analysis_complements RminStruct deviations.
Require Export jump_sequences PA_refinement UPP_PA_refinement.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import Order.Theory GRing.Theory Num.Theory.

Import Refinements.Op.

Open Scope ereal_dual_scope.
Open Scope order_scope.

Global Instance ceil_bigQ : ceil_of bigQ :=
  fun x =>
    match BigQ.red x with
    | BigQ.Qz z => Z.abs_nat (BigZ.to_Z z)
    | BigQ.Qq (BigZ.Pos n) d =>
      Z.abs_nat (BigZ.to_Z ((BigZ.Pos n + BigZ.Pos d - 1) / BigZ.Pos d))
    | BigQ.Qq (BigZ.Neg n) d =>
      Z.abs_nat (BigZ.to_Z (BigZ.Pos n / BigZ.Pos d))
    end.
Global Instance lcm_bigQ : lcm_of bigQ :=
  fun x y =>
    let '(xn, xd) :=
      match BigQ.red x with
      | BigQ.Qz z => (z, 1%bigN)
      | BigQ.Qq n d => (n, d)
      end in
    let '(yn, yd) :=
      match BigQ.red y with
      | BigQ.Qz z => (z, 1%bigN)
      | BigQ.Qq n d => (n, d)
      end in
    let xn := BigZ.to_N xn in
    let yn := BigZ.to_N yn in
    let d := BigN.lcm xd yd in
    (BigZ.Pos (BigN.lcm (xn * (d / xd)) (yn * (d / yd))) # d)%bigQ.

Global Instance refine_ratBigQ_ceil :
  refines (r_ratBigQ ==> nat_R)%rel ceil_rat ceil_op.
Proof.
rewrite refinesE => x1 x2 /r_ratBigQ_red rx.
suff: ceil_rat x1 = ceil_bigQ x2; [by move=> ->; exact: nat_Rxx|].
rewrite /ceil_rat /mathcomp_complements.ceilq /ceil_bigQ.
case: (ratP x1) rx => n1 d1 _.
case: (BigQ.red x2) => [n|n d] [-> /[dup] d1_d ->].
{ by rewrite !intdiv.divz1 addrK opprK Zabs_natE; case: ifP. }
case: n => n.
{ rewrite ifT.
  2:{ rewrite /BigZ.to_Z -[0%R]/(Z2int 0) Z2int_le; exact: BigN.spec_pos. }
  rewrite Zabs_natE BigZ.spec_div Z2int_div.
  { by rewrite BigZ.spec_sub BigZ.spec_add !Z2int_add Z2int_opp. }
  { rewrite BigZ.spec_sub BigZ.spec_add /Z.sub -Z.add_assoc -Z2int_le Z2int_add.
    apply: addr_ge0.
    { rewrite -[0%R]/(Z2int 0) Z2int_le; exact: BigN.spec_pos. }
    rewrite Z2int_add -d1_d Z2int_opp.
    by rewrite -[Z2int _]/(1%R : int) -addn1 PoszD addrK le0z_nat. }
  exact: BigN.spec_pos. }
case: (BigN.eq_0_gt_0_cases n) => n0.
{ rewrite Zabs_natE BigZ.spec_div /BigZ.to_Z n0.
  rewrite -[BigN.to_Z BigN.zero]/Z0 -[Z2int (- 0)]/(0%R : int) lexx add0r.
  rewrite -d1_d -addn1 PoszD addrK -PoszD addn1 /= mul1n; exact: divn_small. }
rewrite ifF.
2:{ rewrite /BigZ.to_Z Z2int_opp; apply/negP/negP; rewrite -ltNge oppr_lt0.
  by rewrite -[0%R]/(Z2int 0) Z2int_lt. }
rewrite Zabs_natE BigZ.spec_div Z2int_div; [|exact: BigN.spec_pos..].
by rewrite /BigZ.to_Z Z2int_opp opprK abszN.
Qed.

(* TODO PR bignums *)
Lemma BigN_spec_lcm a b :
  BigN.to_Z (BigN.lcm a b) = Z.lcm (BigN.to_Z a) (BigN.to_Z b).
Proof.
rewrite /BigN.lcm BigN.spec_mul BigN.spec_div BigN.spec_gcd.
rewrite /Z.lcm Z.abs_eq //.
apply: Z.mul_nonneg_nonneg.
{ exact: BigN.spec_pos. }
move: (Z.gcd_nonneg (BigN.to_Z a) (BigN.to_Z b)) => /Zle_lt_or_eq [H|<-].
{ by apply: Z.div_pos; [exact: BigN.spec_pos|]. }
by rewrite Zdiv_0_r.
Qed.

Lemma BigZ_spec_lcm a b :
  BigZ.to_Z (BigZ.lcm a b) = Z.lcm (BigZ.to_Z a) (BigZ.to_Z b).
Proof.
by rewrite /BigZ.lcm BigZ.spec_abs BigZ.spec_mul BigZ.spec_div BigZ.spec_gcd.
Qed.

Global Instance refine_ratBigQ_lcm :
  refines (r_ratBigQ ==> r_ratBigQ ==> r_ratBigQ)%rel lcm_rat lcm_op.
Proof.
rewrite refinesE => x1 x2 /r_ratBigQ_red rx y1 y2 /r_ratBigQ_red ry.
rewrite /lcm_rat /ratdiv.lcm_rat fracqE.
case: (ratP x1) rx => nx1 dx1 _ rx {x1}.
case: (ratP y1) ry => ny1 dy1 _ ry {y1}.
rewrite /intdiv.lcmz !abszM /= !mul1n /lcm_op /lcm_bigQ.
set m := match BigQ.red x2 with BigQ.Qz _ => _ |  _ => _ end.
set m' := match BigQ.red y2 with BigQ.Qz _ => _ |  _ => _ end.
have: nx1 = Z2int (BigZ.to_Z m.1) /\ (dx1.+1 : int) = Z2int (BigN.to_Z m.2).
{ by rewrite {}/m; case: (BigQ.red x2) rx. }
move: m => [xn xd] /= [-> dx1_1] {nx1 x2 rx}.
have: ny1 = Z2int (BigZ.to_Z m'.1) /\ (dy1.+1 : int) = Z2int (BigN.to_Z m'.2).
{ by rewrite {}/m'; case: (BigQ.red y2) ry. }
move: m' => [yn yd] /= [-> dy1_1] {ny1 y2 ry}.
rewrite /r_ratBigQ /fun_hrel unlock /BigQ.to_Q ifF.
2:{ apply/BigN.eqb_spec => /(f_equal Z2int).
  rewrite BigN_spec_lcm Z2int_lcm; [|exact: BigN.spec_pos..].
  by apply/eqP/lt0n_neq0; rewrite lcmn_gt0 -dx1_1 -dy1_1. }
rewrite Z2int_Qred.
rewrite [in LHS]/BigZ.to_Z BigN_spec_lcm Z2int_lcm; [|exact: BigN.spec_pos..].
rewrite 2!BigN.spec_mul !BigN.spec_div BigN_spec_lcm !Z2int_mul.
do 2![rewrite Z2int_div; [|exact: Z.lcm_nonneg|exact: BigN.spec_pos]].
rewrite Z2int_lcm; [|exact: BigN.spec_pos..].
rewrite Z2Pos.id.
2:{ rewrite -Z2int_lt Z2int_lcm; [|exact: BigN.spec_pos..].
  by rewrite -[Z2int 0]/(Posz 0) ltz_nat lcmn_gt0 -dx1_1 -dy1_1. }
rewrite Z2int_lcm; [|exact: BigN.spec_pos..].
rewrite -dx1_1 -dy1_1 => {xd yd dx1_1 dy1_1}.
rewrite !abszM !mul1n !absz_nat.
congr GRing.mul; congr intmul; congr lcmn; congr muln;
  rewrite spec_to_Z Z2int_mul abszM;
  by case: (BigZ.to_Z _) => //= ?; rewrite mul1n.
Qed.

Notation F_of_sequpp s :=
  (F_of_sequppC
     (erefl (seqjs_not_empty (sequpp_js s)))
     (erefl (@seqjs_head_is_0 _ _ eq_bigQ (sequpp_js s)))
     (erefl (seqjs_sorted (sequpp_js s)))
     (erefl (sequpp_T_nonneg s))
     (erefl (sequpp_d_pos s))
     (erefl (sequpp_last_js_le_T_plus_d s))).

Notation hDev_bounded f f' b := (hDev_bounded f f' (bigQ2rat b)).

Notation vDev_bounded f f' b := (vDev_bounded f f' (bigQ2rat b)).

Ltac nccoq := abstract (apply: refines_goal; vm_cast_no_check (erefl true)).
