Require Export String.
From mathcomp Require Import all_ssreflect.
From mathcomp Require Import ssrint ssralg ssrnum rat.
From mathcomp Require Import reals ereal signed.
From NCCoq Require Import RminStruct deviations.
Require Export tactic.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope dioid_scope.
Local Open Scope string_scope.

Section Program.

Context {R : realType}.

Definition env := string -> option (@F R).

Definition get (x : string) (e : env) : option F := e x.

Definition set x v e : env :=
  fun y => if String.string_dec x y then Some v else e y.

Definition empty_env : env := fun=> None.

(* Section TestEnv. *)

(* Variables f g : F. *)

(* Let ex1 : env := set "x" f (set "y" g empty_env). *)

(* Eval compute in get "x" ex1. *)
(* Eval compute in get "y" ex1. *)
(* Eval compute in get "z" ex1. *)

(* End TestEnv. *)

Variant binop := BPlus | BMin | BConv | BSub.

Variant unop := UMinus | UNonDecrClosure.

Inductive expr :=
  | Cst of @F R
  | Var of string
  | BinOp of binop & expr & expr
  | UnOp of unop & expr.

Variant statement :=
  | Assign of string & expr
  | Exists of string
  | AssertEq of expr & expr
  | AssertLe of expr & expr
  | AssertVDevBounded of expr & expr & rat
  | AssertHDevBounded of expr & expr & rat
  | AssertDeConvLe of expr & expr & expr.

Definition program := seq statement.

Fixpoint sem_expr (ex : expr) (e : env) :=
  match ex with
  | Cst f => Some f
  | Var x => get x e
  | BinOp op e1 e2 =>
    match sem_expr e1 e, sem_expr e2 e with
    | None, _ | _, None => None
    | Some v1, Some v2 =>
      match op with
      | BPlus => Some (v1 + v2)%R
      | BMin => Some (v1 \min v2)
      | BConv => Some ((v1 : Fd) * v2)%R
      | BSub => Some (v1 \- v2)
      end
    end
  | UnOp op e1 =>
    match sem_expr e1 e with
    | None => None
    | Some v1 =>
      match op with
      | UMinus => Some (\- v1)
      | UNonDecrClosure => Some (non_decr_closure v1)
      end
    end
  end.

Fixpoint sem_program_in (p : program) (e : env) :=
  match p with
  | [::] => True
  | s :: p' =>
    match s with
    | Assign v e1 =>
      match sem_expr e1 e with
      | None => False
      | Some v1 => sem_program_in p' (set v v1 e)
      end
    | Exists v => exists v1, sem_program_in p' (set v v1 e)
    | AssertEq e1 e2 =>
      match sem_expr e1 e, sem_expr e2 e with
      | None, _ | _, None => False
      | Some v1, Some v2 => v1 = v2 /\ sem_program_in p' e
      end
    | AssertLe e1 e2 =>
      match sem_expr e1 e, sem_expr e2 e with
      | None, _ | _, None => False
      | Some v1, Some v2 => (v1 <= v2)%O /\ sem_program_in p' e
      end
    | AssertVDevBounded e1 e2 b =>
      match sem_expr e1 e, sem_expr e2 e with
      | None, _ | _, None => False
      | Some v1, Some v2 =>
        UPP_PA_refinement.vDev_bounded v1 v2 b /\ sem_program_in p' e
      end
    | AssertHDevBounded e1 e2 b =>
      match sem_expr e1 e, sem_expr e2 e with
      | None, _ | _, None => False
      | Some v1, Some v2 =>
        UPP_PA_refinement.hDev_bounded v1 v2 b /\ sem_program_in p' e
      end
    | AssertDeConvLe e1 e2 e3 =>
      match sem_expr e1 e, sem_expr e2 e, sem_expr e3 e with
      | None, _, _ | _, None, _ | _, _, None => False
      | Some v1, Some v2, Some v3 =>
        (((v1 : Fd) / v2) <= v3 :> F)%O /\ sem_program_in p' e
      end
    end
  end.

Definition sem_program (p : program) := sem_program_in p empty_env.

Fixpoint fill_ex (l : list F) (p : program) :=
  match l, p with
  | [::], _ | _, [::] => p
  | f :: l', s :: p' =>
    match s with
    | Exists v => Assign v (Cst f) :: fill_ex l' p'
    | _ => s :: fill_ex l p'
    end
  end.

Lemma fill_ex_correct l p : sem_program (fill_ex l p) -> sem_program p.
Proof.
rewrite /sem_program.
elim: p l empty_env => [//|s p +] [//|f l] e => IH.
case: s => /= [v e1|v sf|e1 e2|e1 e2|e1 e2 r|e1 e2 r|e1 e2 e3].
{ case: sem_expr => [v1|//]; exact: IH. }
{ exists f; exact/IH/sf. }
{ case: sem_expr => [v1|//]; case: sem_expr => [_ [<- sp]|//].
  split=> [//|]; exact/IH/sp. }
{ case: sem_expr => [v1|//]; case: sem_expr => [v2 [vle sp]|//].
  split=> [//|]; exact/IH/sp. }
{ case: sem_expr => [v1|//]; case: sem_expr => [v2 [vle sp]|//].
  split=> [//|]; exact/IH/sp. }
{ case: sem_expr => [v1|//]; case: sem_expr => [v2 [vle sp]|//].
  split=> [//|]; exact/IH/sp. }
case: sem_expr => [v1|//]; case: sem_expr => [v2|//]; case: sem_expr => [v3|//].
move=> [vle sp]; split=> [//|]; exact/IH/sp.
Qed.

End Program.
Arguments fill_ex_correct {R}.
