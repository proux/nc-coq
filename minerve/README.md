# MIN-plus ExpRession VErification

Verifying computation of min-plus operations on functions (actually
the subset of Ultimately Pseudo Periodic Picewise Affine functions
(UPP-PA)).

See `test.v` for commented examples.

Dependencies
------------

Coq (tested with version 8.20.1)
MathComp (tested with version 2.3.0)
MathComp Analysis (tested with version 1.8.0)
Hierarchy Builder (tested with version 1.8.0)
MathComp Dioid (tested with master branch)
CoqEAL (tested with version 2.0.3)
NCCoq (tested with master)

Most dependencies can be installed using OPAM (version >= 2) by
just typing

```
% opam repo add coq-released https://coq.inria.fr/opam/released
% opam repo add coq-extra-dev https://coq.inria.fr/opam/extra-dev
% opam update
% opam install coq.8.20.1 coq-mathcomp-algebra.2.3.0 coq-mathcomp-analysis.1.8.0 coq-coqeal.2.0.3
```

and it only remains to install NC-Coq (see ../README.md)

Compilation
-----------

When above dependencies are installed, just type

```
% make
```

to compile everything and

```
% make install
```

to install if needed.
