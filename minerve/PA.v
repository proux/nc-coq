From mathcomp Require Import ssreflect ssrbool ssrfun ssrnat seq path ssrnum.
From mathcomp Require Import fintype finfun bigop ssralg rat eqtype order.
From mathcomp Require Import interval.
From mathcomp Require Import boolp classical_sets signed reals ereal.
From NCCoq Require Import analysis_complements RminStruct.
Require Import NCCoq.usual_functions.
From minerve Require Import jump_sequences.

(******************************************************************************)
(* Soundness proofs of the functions defined in jump_sequences.v.             *)
(*                                                                            *)
(* * Main definitions                                                         *)
(*     JS_of f a l == f is equal to the JS a until l                          *)
(*       F_of_JS a == the function encoded by the JS a                        *)
(*                                                                            *)
(* * Other definitions                                                        *)
(*  eq_point f x y == f(x) = y                                                *)
(* affine_on f rs x y == f(t) = rs.1 (t - x) + rs.2 for any t in (x, y)       *)
(*                    or (x, y]                                               *)
(*    affine_on_oo == same for (x, y)                                         *)
(*    affine_on_oc == same for (x, y]                                         *)
(* eq_segment f xyrs y := eq_point f xyrs.1 xyrs.2.1                          *)
(*                    && affine_on f xyrs.2.2 xyrs.1 y                        *)
(*   eq_segment_co == same with affine_on_oo                                  *)
(*   eq_segment_cc == same with affine_on_oc                                  *)
(*   f_point f x y == f(x) = y and +oo elsewhere                              *)
(* f_affine f rs x l == f is affine (c.f. affine_on) on x l and +oo elsewhere *)
(* f_segment f y rs x l linf == same as above for eq_segment                  *)
(* cutting_below f l linf == f(t) for t < l (or t <= l if linf) and +oo then  *)
(******************************************************************************)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope classical_set_scope.
Local Open Scope ereal_dual_scope.
Local Open Scope ring_scope.
Local Open Scope order_scope.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.

Section PA.

Context {R : realType}.

Local Notation F := (@F R).

Definition eq_point (f : F) (x : {nonneg rat}) (y : \bar rat) :=
  f (ratr x%:num)%:nng == er_map ratr y.

Lemma eq_point_eq (f f' : F) (x : {nonneg rat}) y :
  f (ratr x%:num)%:nng = f' (ratr x%:num)%:nng ->
  eq_point f x y = eq_point f' x y.
Proof. by rewrite/eq_point => ->. Qed.

Definition affine_on (f : F) (rs : rat * \bar rat) (x : rat) (y : itv_bound R) :=
  `[< forall t : {nonneg R}, ratr x < t%:num -> (BRight t%:num <= y)%O ->
        f t = (ratr rs.1 * (t%:num - ratr x))%:E + er_map ratr rs.2 >].

Definition affine_on_oo f rs (x y : {nonneg rat}) :=
  affine_on f rs x%:num (BLeft (ratr y%:num)).

Definition affine_on_oc f rs (x y : {nonneg rat}) :=
  affine_on f rs x%:num (BRight (ratr y%:num)).

Lemma affine_on_eq f f' rs x (y : itv_bound R) :
  (forall t : {nonneg R}, ratr x < t%:num -> (BRight t%:num <= y)%O ->
     f t = f' t) ->
  affine_on f rs x y = affine_on f' rs x y.
Proof.
move=> ff'xy; apply: asbool_equiv_eq; split=> fxy t tx ty.
- by rewrite -ff'xy// fxy.
- by rewrite ff'xy// fxy.
Qed.

Lemma affine_on_oo_eq f f' rs (x y : {nonneg rat}) :
  (forall t : {nonneg R}, ratr x%:num < t%:num < ratr y%:num -> f t = f' t) ->
  affine_on_oo f rs x y = affine_on_oo f' rs x y.
Proof.
by move=> ff'; apply: affine_on_eq => t tx ty; apply/ff'/andP; split.
Qed.

Lemma affine_on_oc_eq f f' rs (x y : {nonneg rat}) :
  (forall t : {nonneg R}, ratr x%:num < t%:num <= ratr y%:num -> f t = f' t) ->
  affine_on_oc f rs x y = affine_on_oc f' rs x y.
Proof.
by move=> ff'; apply: affine_on_eq => t tx ty; apply/ff'/andP; split.
Qed.

Lemma affine_on_oc_oo f rs x y :
  affine_on_oc f rs x y -> affine_on_oo f rs x y.
Proof.
move/asboolP => aff; apply/asboolP => t xltt tlty.
by rewrite aff//; apply: le_trans tlty _; rewrite lteBSide/=.
Qed.

Lemma affine_on_oo_oc f rs x y y' : y'%:num < y%:num ->
  affine_on_oo f rs x y -> affine_on_oc f rs x y'.
Proof.
move=> y'lty /asboolP aff; apply/asboolP => t xltt tlty.
by rewrite aff//; apply: le_trans tlty _; rewrite lteBSide/= ltr_rat.
Qed.

Lemma affine_onW f (rs : rat * \bar rat) (x x' : rat) (y y' : itv_bound R) :
  x <= x' -> (y' <= y)%O ->
  affine_on f rs x y ->
  affine_on f (shift_rho_sigma rs (x' - x)) x' y'.
Proof.
move=> xlex' y'ley /asboolP aff; apply/asboolP => t x'ltt tlty'.
have xltt : ratr x < t%:num by apply: le_lt_trans x'ltt; rewrite ler_rat.
have tley : BRight t%:num <= y by apply: le_trans y'ley.
rewrite (aff _ xltt tley)/= er_map_ratr_add addrA addrAC -dEFinD !rmorphM/=.
by rewrite -mulrDr rmorphD/= addrACA addrA addrK rmorphN/=.
Qed.

Lemma affine_onWl f (rs : rat * \bar rat) (x x' : rat) (y : itv_bound R) :
  x <= x' ->
  affine_on f rs x y ->
  affine_on f (shift_rho_sigma rs (x' - x)) x' y.
Proof. by move=> xlex'; apply: affine_onW. Qed.

Lemma affine_onWr f (rs : rat * \bar rat) (x : rat) (y y' : itv_bound R) :
  (y' <= y)%O ->
  affine_on f rs x y ->
  affine_on f rs x y'.
Proof.
move=> y'ley /asboolP aff; apply/asboolP => t x'ltt tlty'.
by apply: aff => //; apply: le_trans y'ley.
Qed.

Lemma affine_on_ooW f (rs : rat * \bar rat) (x y x' y' : {nonneg rat}) :
  x%:num <= x'%:num -> y'%:num <= y%:num ->
  affine_on_oo f rs x y ->
  affine_on_oo f (shift_rho_sigma rs (x'%:nngnum - x%:nngnum)) x' y'.
Proof.
by move=> xlex' y'ley; apply: affine_onW => //; rewrite lteBSide/= ler_rat.
Qed.

Lemma affine_on_ooWl f (rs : rat * \bar rat) (x y x' : {nonneg rat}) :
  x%:num <= x'%:num ->
  affine_on_oo f rs x y ->
  affine_on_oo f (shift_rho_sigma rs (x'%:nngnum - x%:nngnum)) x' y.
Proof. by move=> xlex' aff; apply: affine_on_ooW. Qed.

Lemma affine_on_ooWr f (rs : rat * \bar rat) (x y y' : {nonneg rat}) :
  y'%:num <= y%:num ->
  affine_on_oo f rs x y ->
  affine_on_oo f rs x y'.
Proof. by move=> y'ley; apply: affine_onWr; rewrite lteBSide/= ler_rat. Qed.

Lemma affine_on_ocW f (rs : rat * \bar rat) (x y x' y' : {nonneg rat}) :
  x%:num <= x'%:num -> y'%:num <= y%:num ->
  affine_on_oc f rs x y ->
  affine_on_oc f (shift_rho_sigma rs (x'%:nngnum - x%:nngnum)) x' y'.
Proof.
by move=> xlex' y'ley; apply: affine_onW => //; rewrite lteBSide/= ler_rat.
Qed.

Lemma affine_on_ocWl f (rs : rat * \bar rat) (x y x' : {nonneg rat}) :
  x%:num <= x'%:num ->
  affine_on_oc f rs x y ->
  affine_on_oc f (shift_rho_sigma rs (x'%:nngnum - x%:nngnum)) x' y.
Proof. by move=> xlex' aff; apply: affine_on_ocW. Qed.

Lemma affine_on_ocWr f (rs : rat * \bar rat) (x y y' : {nonneg rat}) :
  y'%:num <= y%:num ->
  affine_on_oc f rs x y ->
  affine_on_oc f rs x y'.
Proof. by move=> y'ley; apply: affine_onWr; rewrite lteBSide/= ler_rat. Qed.

Lemma eq_point_in f rs (x : rat) (x' : itv_bound R) (x'' : {nonneg rat}) :
  x < x''%:num -> (BRight (ratr x''%:num) <= x')%O ->
  affine_on f rs x x' ->
  eq_point f x'' (shift_rho_sigma rs (x''%:num - x)).2.
Proof.
move=> xx'' x''x' /asboolP /(_ (ratr x''%:num)%:nng) /=.
rewrite !ltr_rat xx'' x''x' /eq_point => /(_ erefl erefl)->.
by case: rs => r [s|//|//]; rewrite /= rmorphD/= rmorphM/= rmorphB dEFinD addrC.
Qed.

Lemma eq_point_in_oo f rs (x x' x'' : {nonneg rat}) :
  x%:num < x''%:num < x'%:num ->
  affine_on_oo f rs x x' ->
  eq_point f x'' (shift_rho_sigma rs (x''%:num - x%:num)).2.
Proof.
by move=> /andP[xx'' x''x']; apply: eq_point_in; rewrite // lteBSide/= ltr_rat.
Qed.

Lemma eq_point_in_oc f rs (x x' x'' : {nonneg rat}) :
  x%:num < x''%:num <= x'%:num ->
  affine_on_oc f rs x x' ->
  eq_point f x'' (shift_rho_sigma rs (x''%:num - x%:num)).2.
Proof.
by move=> /andP[xx'' x''x']; apply: eq_point_in; rewrite // lteBSide/= ler_rat.
Qed.

Definition eq_segment (f : F) (xyrs : JS_elem) (y : itv_bound R) :=
  eq_point f xyrs.1 xyrs.2.1 && affine_on f xyrs.2.2 xyrs.1%:num y.

Definition eq_segment_co (f : F) (xyrs : JS_elem) (y : {nonneg rat}) :=
  eq_segment f xyrs (BLeft (ratr y%:num)).

Definition eq_segment_cc (f : F) (xyrs : JS_elem) (y : {nonneg rat}) :=
  eq_segment f xyrs (BRight (ratr y%:num)).

Lemma eq_segment_eq f f' xyrs (y : itv_bound R) :
  (BRight (ratr xyrs.1%:num) <= y)%O ->
  (forall t : {nonneg R},
     ratr xyrs.1%:num <= t%:num -> (BRight t%:num <= y)%O ->
     f t = f' t) ->
  eq_segment f xyrs y = eq_segment f' xyrs y.
Proof.
move=> xley ff'xy.
rewrite /eq_segment (@eq_point_eq _ f') ?(@affine_on_eq _ f')//.
- by move=> t /ltW xlet tley; apply: ff'xy.
- exact: ff'xy.
Qed.

Lemma eq_segment_co_eq f f' xyrs (y : {nonneg rat}) :
  xyrs.1%:num < y%:num ->
  (forall t : {nonneg R}, ratr xyrs.1%:num <= t%:num < ratr y%:num ->
     f t = f' t) ->
  eq_segment_co f xyrs y = eq_segment_co f' xyrs y.
Proof.
move=> xley ff'xy; apply: eq_segment_eq; first by rewrite lteBSide/= ltr_rat.
by move=> t xlet tley; apply: ff'xy; rewrite xlet; move: tley; rewrite lteBSide.
Qed.

Lemma eq_segment_cc_eq f f' xyrs (y : {nonneg rat}) :
  xyrs.1%:num <= y%:num ->
  (forall t : {nonneg R}, ratr xyrs.1%:num <= t%:num <= ratr y%:num ->
     f t = f' t) ->
  eq_segment_cc f xyrs y = eq_segment_cc f' xyrs y.
Proof.
move=> xley ff'xy; apply: eq_segment_eq; first by rewrite lteBSide/= ler_rat.
by move=> t xlet tley; apply: ff'xy; rewrite xlet; move: tley; rewrite lteBSide.
Qed.

Lemma eq_segment_cc_co f xyrs y :
  eq_segment_cc f xyrs y -> eq_segment_co f xyrs y.
Proof. by move=> /andP[ep /affine_on_oc_oo aff]; apply/andP; split. Qed.

Lemma eq_segment_co_cc f xyrs y y' : y'%:num < y%:num ->
  eq_segment_co f xyrs y -> eq_segment_cc f xyrs y'.
Proof.
move=> y'lty /andP[ep aff]; apply/andP; split=> //.
by apply: affine_on_oo_oc aff.
Qed.

Lemma eq_segment_co_complete f x yrs x' : x%:num < x'%:num ->
  eq_segment_co f (x, yrs) x' ->
  eq_segment_cc
    (fun t => if t%:num < ratr x'%:num then f t
              else er_map ratr (shift_rho_sigma yrs.2 (x'%:num - x%:num)).2)
    (x, yrs) x'.
Proof.
move=> xltx' /andP[ep /asboolP aff]; apply/andP; split.
  by rewrite /eq_point/= (eqbRL (ltr_rat _ _ _) xltx').
apply/asboolP => t xltt; rewrite lteBSide/= le_eqVlt => /orP[].
- by move=> /eqP->; rewrite ltxx /= er_map_ratr_add addrC /= rmorphM/= rmorphB.
- by move=> /[dup] + ->; apply: aff.
Qed.

Lemma eq_segment_inW f (x : {nonneg rat}) rs
    (x' : {nonneg rat}) (y y' : itv_bound R) :
  x%:num < x'%:num -> (BRight (ratr x'%:num) <= y)%O -> (y' <= y)%O ->
  affine_on f rs x%:num y ->
  eq_segment f (x', (shift_y_rho_sigma rs (x'%:num - x%:num))) y'.
Proof.
move=> xltx' x'lty y'ley aff; apply/andP; split; first exact: eq_point_in aff.
by apply: affine_onW aff => //; apply: ltW.
Qed.

Lemma eq_segment_inWl f (x : {nonneg rat}) rs
    (x' : {nonneg rat}) (y : itv_bound R) :
  x%:num < x'%:num -> (BRight (ratr x'%:num) <= y)%O ->
  affine_on f rs x%:num y ->
  eq_segment f (x', (shift_y_rho_sigma rs (x'%:num - x%:num))) y.
Proof. by move=> xltx' x'ley; apply: eq_segment_inW. Qed.

Lemma eq_segmentW f (xyrs : JS_elem) (x' : {nonneg rat}) (y y' : itv_bound R) :
  xyrs.1%:num <= x'%:num -> (BRight (ratr x'%:num) <= y)%O -> (y' <= y)%O ->
  eq_segment f xyrs y ->
  eq_segment f (shift_JS_elem xyrs x') y'.
Proof.
move=> xlex' x'lty y'ley /andP[eqp aff].
rewrite /eq_segment shift_JS_elem_correct//= /shift_JS_elem.
case: leP => x'x.
  have -> : x' = xyrs.1 by apply/val_inj/le_anti; rewrite /= x'x.
  by rewrite eqp/=; apply: affine_onWr aff.
apply/andP; split; last exact: affine_onW xlex' _ aff.
exact: eq_point_in aff.
Qed.

Lemma eq_segmentWl f (xyrs : JS_elem) (x' : {nonneg rat}) (y : itv_bound R) :
  xyrs.1%:num <= x'%:num -> (BRight (ratr x'%:num) <= y)%O ->
  eq_segment f xyrs y ->
  eq_segment f (shift_JS_elem xyrs x') y.
Proof. by move=> xx' x'y; apply: eq_segmentW. Qed.

Lemma eq_segmentWr f (xyrs : JS_elem) (y y' : itv_bound R) :
  (y' <= y)%O ->
  eq_segment f xyrs y ->
  eq_segment f xyrs y'.
Proof.
by move=> y'ley /andP[eqp aff]; apply/andP; split=> //; apply: affine_onWr aff.
Qed.

Lemma eq_segment_ooW f (x : {nonneg rat}) rs (x' y y' : {nonneg rat}) :
  x%:num < x'%:num < y%:num -> y'%:num <= y%:num ->
  affine_on_oo f rs x y ->
  eq_segment_co f (x', (shift_y_rho_sigma rs (x'%:num - x%:num))) y'.
Proof.
move=> /andP[xltx' x'lty] y'ley.
by apply: eq_segment_inW; rewrite //= lteBSide/= ?ltr_rat// ler_rat.
Qed.

Lemma eq_segment_ooWl f (x : {nonneg rat}) rs (x' y : {nonneg rat}) :
  x%:num < x'%:num < y%:num ->
  affine_on_oo f rs x y ->
  eq_segment_co f (x', (shift_y_rho_sigma rs (x'%:num - x%:num))) y.
Proof. by move=> ?; apply: eq_segment_ooW. Qed.

Lemma eq_segment_coW f (xyrs : JS_elem) (y x' y' : {nonneg rat}) :
  xyrs.1%:num <= x'%:num < y%:num -> y'%:num <= y%:num ->
  eq_segment_co f xyrs y ->
  eq_segment_co f (shift_JS_elem xyrs x') y'.
Proof.
move=> /andP[xlex' x'lty] y'ley.
by apply: eq_segmentW; rewrite // lteBSide/= ?ltr_rat// ler_rat.
Qed.

Lemma eq_segment_coWl f (xyrs : JS_elem) (y x' : {nonneg rat}) :
  xyrs.1%:num <= x'%:num < y%:num ->
  eq_segment_co f xyrs y ->
  eq_segment_co f (shift_JS_elem xyrs x') y.
Proof. by move=> xx'y; apply: eq_segment_coW. Qed.

Lemma eq_segment_coWr f (xyrs : JS_elem) (y x' y' : {nonneg rat}) :
  y'%:num <= y%:num ->
  eq_segment_co f xyrs y ->
  eq_segment_co f xyrs y'.
Proof. by move=> y'ley; apply: eq_segmentWr; rewrite lteBSide/= ler_rat. Qed.

Lemma eq_segment_ocW f (x : {nonneg rat}) rs (x' y y' : {nonneg rat}) :
  x%:num < x'%:num <= y%:num -> y'%:num <= y%:num ->
  affine_on_oc f rs x y ->
  eq_segment_cc f (x', (shift_y_rho_sigma rs (x'%:num - x%:num))) y'.
Proof.
move=> /andP[xltx' x'ley] y'ley.
by apply: eq_segment_inW; rewrite //= lteBSide/= ler_rat.
Qed.

Lemma eq_segment_ocWl f (x : {nonneg rat}) rs (x' y : {nonneg rat}) :
  x%:num < x'%:num <= y%:num ->
  affine_on_oc f rs x y ->
  eq_segment_cc f (x', (shift_y_rho_sigma rs (x'%:num - x%:num))) y.
Proof. by move=> ?; apply: eq_segment_ocW. Qed.

Lemma eq_segment_ccW f (xyrs : JS_elem) (y x' y' : {nonneg rat}) :
  xyrs.1%:num <= x'%:num <= y%:num -> y'%:num <= y%:num ->
  eq_segment_cc f xyrs y ->
  eq_segment_cc f (shift_JS_elem xyrs x') y'.
Proof.
move=> /andP[xlex' x'lty] y'ley.
by apply: eq_segmentW; rewrite // lteBSide/= ler_rat.
Qed.

Lemma eq_segment_ccWl f (xyrs : JS_elem) (y x' : {nonneg rat}) :
  xyrs.1%:num <= x'%:num <= y%:num ->
  eq_segment_cc f xyrs y ->
  eq_segment_cc f (shift_JS_elem xyrs x') y.
Proof. by move=> xx'y; apply: eq_segment_ccW. Qed.

Lemma eq_segment_ccWr f (xyrs : JS_elem) (y x' y' : {nonneg rat}) :
  y'%:num <= y%:num ->
  eq_segment_cc f xyrs y ->
  eq_segment_cc f xyrs y'.
Proof. by move=> y'ley; apply: eq_segmentWr; rewrite lteBSide/= ler_rat. Qed.

Lemma affine_on_empty f rs x y : y <= BRight (ratr x) -> affine_on f rs x y.
Proof.
move=> ylex; apply/asboolP => t xltt tley; move: (le_trans tley ylex).
by rewrite lteBSide/= => /(lt_le_trans xltt); rewrite ltxx.
Qed.

Lemma affine_on_oo_empty f rs x y : y%:num <= x%:num -> affine_on_oo f rs x y.
Proof. by move=> ylex; apply: affine_on_empty; rewrite lteBSide/= ler_rat. Qed.

Lemma affine_on_oc_empty f rs x y : y%:num <= x%:num -> affine_on_oc f rs x y.
Proof. by move=> ylex; apply: affine_on_empty; rewrite lteBSide/= ler_rat. Qed.

Lemma eq_point_segment f xyrs l : l <= BRight (ratr xyrs.1%:num) ->
  eq_point f xyrs.1 xyrs.2.1 -> eq_segment f xyrs l.
Proof. by rewrite /eq_segment; move=> llex -> /=; apply: affine_on_empty. Qed.

Lemma eq_point_segment_co f xyrs l : l%:num <= xyrs.1%:num ->
  eq_point f xyrs.1 xyrs.2.1 -> eq_segment_co f xyrs l.
Proof. by move=> llex; apply: eq_point_segment; rewrite lteBSide/= ler_rat. Qed.

Lemma eq_point_segment_cc f xyrs l : l%:num <= xyrs.1%:num ->
  eq_point f xyrs.1 xyrs.2.1 -> eq_segment_cc f xyrs l.
Proof. by move=> llex; apply: eq_point_segment; rewrite lteBSide/= ler_rat. Qed.

Fixpoint JS_of_rec (f : F) (xyrs : JS_elem) (a : seq JS_elem) l :=
  match a with
  | [::] => eq_segment_cc f xyrs l
  | xyrs' :: a => eq_segment_co f xyrs xyrs'.1 && JS_of_rec f xyrs' a l
  end.

Definition JS_of (f : F) (a : seq JS_elem) (l : {nonneg rat}) :=
  all (fun xyrs : JS_elem => xyrs.1%:num <= l%:num) a
  && match a with
     | [::] => true
     | xyrs' :: a' => JS_of_rec f xyrs' a' l
     end.

Lemma JS_of_rec_eq xyrs a f f' (l : {nonneg rat}) :
  all (fun xyrs => xyrs.1%:num <= l%:num) (xyrs :: a) ->
  path lt_fst_num xyrs a ->
  (forall t, ratr xyrs.1%:num <= t%:num <= ratr l%:num -> f t = f' t) ->
  JS_of_rec f xyrs a l = JS_of_rec f' xyrs a l.
Proof.
elim: a xyrs => [|xyrs' a IHa] xyrs /=.
  by rewrite andbT => ? _; exact: eq_segment_cc_eq.
move=> /and3P[xyrsl xyrs'l alla] /andP[xyrsxyrs' patha] ff'; congr andb.
  apply: eq_segment_co_eq => [//|t /andP[xyt txy']]; apply: ff'.
  by rewrite xyt (le_trans (ltW txy'))// ler_rat.
apply: IHa => [|//|t /andP[xyt tl]]; first by rewrite /= xyrs'l alla.
by apply: ff'; rewrite (le_trans _ xyt) ?tl// ler_rat ltW.
Qed.

Lemma JS_of_eq a f f' (l : {nonneg rat}) :
  sorted lt_fst_num a ->
  (forall t, ratr (head_JS a).1%:num <= t%:num <= ratr l%:num -> f t = f' t) ->
  JS_of f a l = JS_of f' a l.
Proof.
move=> sorta ff'; apply: andb_id2l => alla.
by case: a alla sorta ff' => [//|xyrs a] alla sorta /= ff'; apply: JS_of_rec_eq.
Qed.

Lemma JS_of_recP f xyrs a l :
  reflect
    ((forall i, (i < size a)%nat ->
        eq_segment_co f (nth_JS (xyrs :: a) i) (nth_JS a i).1)
     /\ (eq_segment_cc f (last_JS (xyrs :: a)) l))
    (JS_of_rec f xyrs a l).
Proof.
apply/(iffP idP).
- elim: a xyrs => [//|xyrs' a IHa] xyrs /= /andP[seq0 JSrec].
  have [{}IHa IHa'] := IHa _ JSrec.
  by split=> [[_|i]|]; [exact: seq0|rewrite ltnS; exact: IHa|exact: IHa'].
- elim: a xyrs => [? [] //|xyrs' a IHa] xyrs /= [seqi seql].
  rewrite (seqi 0%N erefl)/=; apply: IHa; split=> [i|]; last exact: seql.
  exact: (seqi i.+1).
Qed.

Lemma JS_ofP f a l :
  reflect
    (all (fun xyrs => xyrs.1%:num <= l%:num) a
     /\ (forall i, (i.+1 < size a)%nat ->
           eq_segment_co f (nth_JS a i) (nth_JS a i.+1).1)
     /\ (a != [::] -> eq_segment_cc f (last_JS a) l))
    (JS_of f a l).
Proof.
apply/(andPP idP)/(iffP idP).
- case: a => [//|xyrs a] /= /JS_of_recP[seqi seql].
  by split=> [i|_]; [exact: seqi|exact: seql].
- case: a => [//|xyrs a] /= [seqi /(_ erefl) seql].
  by apply/JS_of_recP; split=> [i|]; [exact: seqi|exact: seql].
Qed.

Lemma JS_of_rec_eq_point_l f xyrs a l : JS_of_rec f xyrs a l ->
  eq_point f xyrs.1 xyrs.2.1.
Proof. by case: a => [|? ? /andP[]] /andP[]. Qed.

Lemma JS_of_rec_consr f xyrs xyrs' a l :
  JS_of_rec f xyrs (xyrs' :: a) l -> JS_of_rec f xyrs' a l.
Proof. by move=> /andP[_]. Qed.

Lemma JS_of_rec_consl f xyrs xyrs' a l : xyrs'.1%:num <= l%:num ->
  JS_of_rec f xyrs (xyrs' :: a) l -> JS_of_rec f xyrs [:: xyrs'] xyrs'.1.
Proof.
move=> x'lel /JS_of_recP[seq seql]; apply/JS_of_recP; split=> [i /=|].
  by rewrite ltnS leqn0 => /eqP-> /=; apply: (seq 0%N erefl).
case: a seq seql => [|xyrs'' a seq] _ /=; first exact: eq_segment_ccWr.
apply/andP; split; first exact: (proj1 (andP (seq 1%N erefl))).
apply/asboolP => t x'ltt tlex'.
by move: tlex'; rewrite lteBSide/= => /(lt_le_trans x'ltt); rewrite ltxx.
Qed.

Lemma JS_of_rec_cons f xyrs xyrs' a l : xyrs'.1%:num <= l%:num ->
  JS_of_rec f xyrs (xyrs' :: a) l =
  (JS_of_rec f xyrs [::xyrs'] xyrs'.1 && JS_of_rec f xyrs' a l).
Proof.
move=> x'lel; apply/idP/idP; last by move=> /andP[/andP[/=->]].
move=> JSa; apply/andP; split; first exact: JS_of_rec_consl JSa.
exact: JS_of_rec_consr JSa.
Qed.

Lemma JS_of_consr f xyrs a l : JS_of f (xyrs :: a) l -> JS_of f a l.
Proof.
case: a => [//|xyrs' a] /andP[al /JS_of_rec_consr JSa].
apply/andP; split=> //; apply/allP => xyrs'' xyrs''in; apply: (allP al).
by rewrite in_cons xyrs''in orbT.
Qed.

Lemma JS_of_consl f xyrs xyrs' a l : xyrs.1%:num <= xyrs'.1%:num ->
  JS_of f [:: xyrs, xyrs' & a] l -> JS_of f [:: xyrs; xyrs'] xyrs'.1.
Proof.
move=> xlex' /andP[/allP ala JSa]; apply/andP; split.
  by apply/allP => xyrs'' /[!inE]/= /orP[]/eqP->.
by apply: JS_of_rec_consl JSa; apply: ala; rewrite !inE eqxx orbT.
Qed.

Lemma JS_of_cons f xyrs xyrs' a l : xyrs.1%:num <= xyrs'.1%:num <= l%:num ->
  JS_of f [:: xyrs, xyrs' & a] l =
  (JS_of f [:: xyrs; xyrs'] xyrs'.1 && JS_of f (xyrs' :: a) l).
Proof.
move=> /andP[xlex' x'lel]; rewrite /JS_of andbACA -JS_of_rec_cons//; congr andb.
by rewrite /=; congr andb; rewrite xlex' lexx/=; apply: le_trans x'lel.
Qed.

Lemma JS_of_rec_catr f xyrs a xyrs' a' l :
  JS_of_rec f xyrs (a ++ xyrs' :: a') l -> JS_of_rec f xyrs' a' l.
Proof. by elim: a xyrs => [|xyrs'' a IHa] xyrs /andP[_]//; apply: IHa. Qed.

Lemma JS_of_rec_catl f xyrs a xyrs' a' l : xyrs'.1%:num <= l%:num ->
  JS_of_rec f xyrs (a ++ xyrs' :: a') l ->
  JS_of_rec f xyrs (rcons a xyrs') xyrs'.1.
Proof.
move=> x'lel.
elim: a xyrs => [|xyrs'' a IHa] xyrs; first exact: JS_of_rec_consl.
by move=> /andP[/=->]; apply: IHa.
Qed.

Lemma JS_of_rec_cat f xyrs a xyrs' a' l : xyrs'.1%:num <= l%:num ->
  JS_of_rec f xyrs (a ++ xyrs' :: a') l =
  (JS_of_rec f xyrs (rcons a xyrs') xyrs'.1 && JS_of_rec f xyrs' a' l).
Proof.
move=> x'lel; elim: a xyrs => [|xyrs'' a IHa] xyrs; first exact: JS_of_rec_cons.
by rewrite /= IHa andbA.
Qed.

Lemma JS_of_catr f a a' l : JS_of f (a ++ a') l -> JS_of f a' l.
Proof.
move=> /andP[ala jsa]; apply/andP; split=> [|{ala}].
  by move: ala; rewrite all_cat => /andP[].
by case: a jsa => [//|xyrs a]; case: a' => [//|xyrs' a']; apply: JS_of_rec_catr.
Qed.

Lemma JS_of_catl f a xyrs a' l :
  all (fun xyrs' : JS_elem => xyrs'.1%:num <= xyrs.1%:num) a ->
  JS_of f (a ++ xyrs :: a') l -> JS_of f (rcons a xyrs) xyrs.1.
Proof.
move=> ala /andP[ala' JSa]; apply/andP; split.
  by rewrite all_rcons lexx.
case: a ala ala' JSa => [|xyrs' a] /= ala /andP[xlel /allP ala']; last first.
  by apply/JS_of_rec_catl/ala'; rewrite mem_cat in_cons eqxx orbT.
case: a' ala' => [|xyrs'' a'] ala' /=; first exact: eq_segment_ccWr.
by move=> /andP[/andP[+ _] _]; apply: eq_point_segment_cc.
Qed.

Lemma JS_of_cat f a xyrs a' l :
  all (fun xyrs' : JS_elem => xyrs'.1%:num <= xyrs.1%:num) a ->
  JS_of f (a ++ xyrs :: a') l =
  (JS_of f (rcons a xyrs) xyrs.1 && JS_of f (xyrs :: a') l).
Proof.
move=> ala; rewrite /JS_of andbACA all_cat /=.
have [xlel|_] := leP; last by rewrite !andbF.
rewrite all_rcons lexx /=; congr andb; [congr andb|].
  apply: eq_in_all => x xa.
  by rewrite (allP ala _ xa) (le_trans _ xlel)// (allP ala _ xa).
case: a ala => [_|xyrs' a /andP[x'lex ala]]; last first.
  by rewrite -JS_of_rec_cat//= x'lex (le_trans x'lex xlel) all_rcons lexx.
apply/esym/andb_idl; case: a' => [|xyrs'' a']; first exact: eq_segment_ccWr.
by move=> /andP[/andP[+ _] _]; apply: eq_point_segment_cc.
Qed.

Lemma last_JS_of a f l :
  a != [::] -> JS_of f a l -> (last_JS a).1%:num <= l%:num.
Proof.
case: a => [//|xyrs a] _ /andP[/allP + _]; apply.
by rewrite (last_nth default_JS)/= mem_nth.
Qed.

Fixpoint F_of_JS_def (a : seq (rat * (\bar rat * (rat * \bar rat))))
    (t : {nonneg R}) :=
  match a with
  | [::] => +oo%E
  | (x, (y, (r, s))) :: a =>
    if t%:num <= ratr x then er_map ratr y else
      let seg := (ratr r * (t%:num - ratr x))%:E + er_map ratr s in
      match a with
      | [::] => seg
      | (x', _) :: _ =>
        if t%:num < ratr x' then seg else F_of_JS_def a t
      end
  end.

Definition F_of_JS (a : seq JS_elem) :=
  F_of_JS_def [seq (xyrs.1%:num, xyrs.2) | xyrs <- a].

Lemma F_of_JS_correct (a : preJS) (l : {nonneg rat}) :
  (last_JS a).1%:num <= l%:num -> JS_of (F_of_JS a) a l.
Proof.
case: a => /= -[//|xyrs a] _ sorta lastl.
have alllel : all (fun xyrs => (xyrs.1)%:num <= l%:num) (xyrs :: a).
  apply/(all_nthP default_JS) => i isz.
  apply: le_trans lastl; rewrite -nth_last.
  apply: (sorted_leq_nth le_fst_num_trans) => //.
  - exact: sub_sorted sorta.
  - by rewrite inE.
rewrite /JS_of alllel andTb /F_of_JS.
elim: a xyrs sorta lastl alllel => [|xyrs' a IHa] xyrs sorta lastl alllel.
  case: xyrs sorta lastl alllel => x [y [r s]] /= _ _ xlel.
  rewrite /JS_of_rec/=; apply/andP; split; first by rewrite /eq_point/= lexx.
  by apply/asboolP => t /= tgt0 _; rewrite leNgt tgt0.
rewrite /JS_of_rec -/JS_of_rec; apply/andP; split.
  case: xyrs sorta lastl alllel => x [y [r s]] /andP[].
  case: xyrs' => x' [y' [r' s']] /= xltx' patha lastl /and3P[xlel x'lel alllel].
  apply/andP; split; first by rewrite /eq_point/= lexx.
  by apply/asboolP => t /= xltt; rewrite lteBSide/= leNgt xltt/= => ->.
have /andP[_ {}alllel] := alllel.
rewrite (@JS_of_rec_eq _ _ _ (F_of_JS (xyrs' :: a)))//.
- apply: IHa; [by move: sorta => /andP[]|exact: lastl|exact: alllel].
- by move: sorta => /andP[].
case: xyrs sorta lastl alllel => x [y [r s]] /andP[].
case: xyrs' => x' [y' [r' s']] /= xltx' patha lastl xlel.
move=> t /andP[x'let tlel].
have xltt : ratr x%:num < t%:num by apply: lt_le_trans x'let; rewrite ltr_rat.
by rewrite leNgt xltt /= ltNge x'let.
Qed.

Lemma JS_affine_correct (r s : rat) (l : {posnum rat}) :
  JS_of (fun t => (ratr r * t%:nngnum + ratr s)%:E) (JS_affine r s) l%:num%:nng.
Proof.
apply/and3P; split; rewrite /= ?andbT//.
  by rewrite /eq_point/= raddf0 mulr0 add0r.
by apply/asboolP => t tgt0 tlel /=; rewrite raddf0 subr0 dEFinD.
Qed.

Lemma JS_cst_inf_correct f x y cst (l : {nonneg rat}) : x%:num <= l%:num ->
    eq_point f x y ->
    (forall t, ratr x%:num < t%:num <= ratr l%:num -> f t = er_map ratr cst) ->
  JS_of f (JS_cst_inf x y cst) l.
Proof.
move=> xl fx fcst; apply/andP; split; rewrite /= ?xl//; apply/andP; split=> //.
by apply/asboolP => t /[!bnd_simp] xt tl; rewrite rmorph0 mul0r add0r fcst ?xt.
Qed.

Lemma JS_0_correct l : JS_of \0 JS_0 l.
Proof.
apply/andP; split; rewrite /= ?andbT//; apply/andP; split.
  by rewrite /eq_point/= rmorph0.
by apply/asboolP => t _ _ /=; rewrite rmorph0 mul0r addr0.
Qed.

Lemma JS_below_correct f (a : JS) (l' l : {nonneg rat}) :
  l'%:num <= l%:nngnum -> JS_of f a l -> JS_of f (JS_below a l') l'.
Proof.
move=> l'lel; rewrite /JS_below/= /JS_below_subdef/=.
move=> /andP[ala JSa]; apply/andP; split.
  by apply/allP => x; rewrite mem_filter /= => /andP[].
case: a ala JSa => /= -[/= [//|xyrs a] _ sorta] /= xyrs0 /andP[_ ala] JSa.
have xlel' : xyrs.1%:num <= l'%:num by rewrite (eqP xyrs0) ge0.
rewrite xlel' => {xyrs0 ala}.
elim: a xyrs xlel' sorta JSa => [|xyrs' a IHa] xyrs xlel' sorta JSa /=.
  exact: eq_segment_ccWr JSa.
case: leP => x'l' /=.
  by move: JSa => /andP[-> JSa] /=; apply: IHa => //; apply: path_sorted sorta.
have -> : [seq x <- a | x.1%:num <= l'%:num] = [::].
  apply/eqP/negbNE; rewrite -has_filter -all_predC; apply/allP => x xa /=.
  rewrite -ltNge; apply: lt_trans x'l' _; move: sorta => /= /andP[_].
  by rewrite path_sortedE// => /andP[/allP + _]; apply.
by move: JSa => /andP[+ _]; apply: eq_segment_co_cc.
Qed.

Lemma JS_above_correct (a : JS) f l l' : l%:num <= l'%:num ->
  JS_of f a l' -> JS_of f (JS_above a l) l'.
Proof.
move=> llel'; rewrite /JS_above/= /JS_above_subdef/= /JS_above_rec_subdef/=.
move=> /andP[ala JSa]; apply/andP; split.
  case: a ala JSa => -[[//|xyrs a] _ /= _] _ /andP[xl' ala] _.
  case: ltP => xl; rewrite /= ?xl'// => {xl'}.
  elim: a xyrs xl ala => [xyrs xl /=|xyrs' a IHa xyrsl xl /andP[x'l' ala]].
    by rewrite shift_JS_elem_correct// llel'.
  case: leP => lx' /=; first exact: IHa.
  by rewrite shift_JS_elem_correct// llel' x'l'.
case: a ala JSa => -[[//|xyrs a] _ /= _] _ /andP[xl' ala] JSa.
case: ltP => xl // {xl' ala}.
elim: a xyrs JSa xl => [xyrs JSa |xyrs' a IHa xyrs /andP[seg JSa]] xl /=.
  by apply: eq_segment_ccWl JSa; rewrite xl.
case: leP => x'l; first exact: IHa.
by apply/andP; split=> //; apply: eq_segment_coWl seg; rewrite xl.
Qed.

Lemma JS_cat_correct (f : F) (a b : preJS) (l l' : {nonneg rat}) :
    l%:num = (head_JS b).1%:num -> JS_of f a l -> JS_of f b l' ->
  JS_of f (preJS_cat a b) l'.
Proof.
case: a b => [[//|a0 a] /= _ sa] [[//|b0 b] /= _ sb] + + /andP[allb JSb].
move=> /eqP; rewrite num_eq => /eqP-> {l}; rewrite /JS_cat_subdef.
move=> /andP[alla JSa]; apply/andP; split=> [|/=].
  rewrite all_cat allb andbT; apply/allP => x /[!mem_filter]/=/andP[xb0 _].
  exact: le_trans (ltW xb0) (andP allb).1.
have [a0b0/=|a0b0] := ltP; last by rewrite (below_lt_empty sa).
elim: a a0 a0b0 sa alla JSa => [|a1 a IHa] a0 a0b0 /= pa /andP[_ alla] JSa.
  by rewrite eq_segment_cc_co.
have [a1b0/=|a1b0] := ltP; last first.
  by rewrite (below_lt_empty (andP pa).2)//= (eq_segment_coWr _ _ (andP JSa).1).
by rewrite (andP JSa).1/= IHa// ?(andP JSa).2 ?(andP pa).2.
Qed.
Arguments JS_cat_correct [f a b] l l'.

Lemma JS_delta_correct b l : b <= l%:num ->
  JS_of (delta (ratr b)%:E) (JS_delta b) l.
Proof.
move=> bl; rewrite /delta/JS_delta/=; case: leP => [bge0|blt0].
- apply: (JS_cat_correct (NngNum bge0)) => //.
  + rewrite -(@JS_of_eq _ \0) ?JS_0_correct// => t /andP[_ /=].
    by rewrite lee_fin => ->.
  + rewrite JS_cst_inf_correct// /eq_point/= ?lexx ?rmorph0// => t /andP[tb _].
    by rewrite lee_fin leNgt tb.
- rewrite JS_cst_inf_correct//= => [|t /andP[tgt0 _]].
    by rewrite /eq_point/= lee_fin ler_rat leNgt ?blt0.
  by rewrite ifN// -ltNge lte_fin (lt_trans _ tgt0)// ltr_rat.
Qed.

Lemma preJS_eval_correct f (a : preJS) x l : JS_of f a l ->
    (head_JS a).1%:num <= x%:num <= l%:num ->
  eq_point f x (JS_eval a x).
Proof.
case: a => [[//|xyrs a] /= _ _]; set aux := fix aux _ (a : seq JS_elem) := _.
move/andP => [_ +] /andP[+ xl].
elim: a xyrs => [|xyrs' a IHa] xyrs /= JSa xyrsx.
  case: ltgtP xyrsx => [xyrsx|//|xyrsx] _.
    by apply: eq_point_in (andP JSa).2 => //; rewrite bnd_simp ler_rat.
  by (have -> : x = xyrs.1 by exact: val_inj); apply: (andP JSa).1.
have seg := (andP JSa).1; have {}JSa := (andP JSa).2.
case: (ltgtP _ xyrs.1%:num) xyrsx => [//||] xyrsx _; last first.
  by (have -> : x = xyrs.1 by exact: val_inj); apply: (andP seg).1.
have [xxyrs'|] := ltP; last exact: IHa JSa.
by apply: eq_point_in (andP seg).2 => //; rewrite bnd_simp ltr_rat.
Qed.

Lemma JS_eval_correct f (a : JS) x l : JS_of f a l -> x%:num <= l%:num ->
  eq_point f x (JS_eval a x).
Proof.
by move=> JSa xl; rewrite (preJS_eval_correct JSa)// xl andbT JS_head/=.
Qed.

Lemma JS_map_correct f f' g (a : seq JS_elem) l :
  (forall x yrs, eq_point f x yrs.1 -> eq_point f' x (g x yrs).1) ->
  (forall x yrs y,
      affine_on f yrs.2 x%:num y -> affine_on f' (g x yrs).2 x%:num y) ->
  JS_of f a l -> JS_of f' (preJS_map_subdef g a) l.
Proof.
move=> ep aff /andP[ala JSa]; apply/andP; split.
  apply/(all_nthP default_JS) => i; rewrite size_map => isz.
  apply: le_trans (all_nthP default_JS ala i isz).
  by rewrite (nth_map default_JS).
case: a ala JSa => [//|xyrs a] _ /=.
elim: a xyrs => /= [|xyrs' a IHa] xyrs => [|/andP[+ JSa]] => /andP[p seg].
  by apply/andP; split; [apply: ep p | apply: aff seg].
apply/andP; split; last exact: IHa.
by apply/andP; split; [apply: ep p | apply: aff seg].
Qed.

Lemma JS_normalize_correct (a : preJS) f l :
  JS_of f a l -> JS_of f (JS_normalize a l) l.
Proof.
case: a => a /= _ _.
case: a => [//|[x [y [r s]]] a /andP[/=/andP[xlel ala] JSa]].
apply/andP; split=> [{JSa}|].
  apply/andP; split; first by case: ifP; rewrite xlel.
  elim: a ala => [//|[x' [y' [r' s']]] a IHa] /= /andP[-> ala] /=.
  exact: IHa.
elim: a x y r s xlel ala JSa => [|[x' [y' [r' s']]] a IHa] x y r s xlel /=.
  move=> _; case: ltgtP xlel => // xl _.
  - move=> /andP[/=ep /asboolP aff]; apply/andP; split=> //=.
    by apply/asboolP => t xltt tlel /=; rewrite {}aff//=; case: s.
  - move=> /andP[/=ep _]; apply/andP; split=> //=.
    by apply/asboolP => t xltt /(lt_le_trans xltt); rewrite xl ltxx.
move=> /andP[x'lel ala] /andP[seg JSa]; apply/andP; split; last exact: IHa.
apply/andP; split; first by move: seg => /andP[].
case: ltgtP xlel seg => [xltl|//|xl] _ /andP[/=ep /asboolP aff].
- by apply/asboolP => t /= xltt tltx'; rewrite {}aff//=; case: s.
- by apply/asboolP => t xltt /(lt_trans xltt); rewrite ltr_rat xl ltNge x'lel.
Qed.

Definition JS_of2 (f f' : F) (a : preJST _) l :=
  JS_of f (preJS_map (fun=> fst) a) l
  && JS_of f' (preJS_map (fun=> snd) a) l.

Lemma JS_merge_correct (a a' : JS) f f' l :
  JS_of f a l -> JS_of f' a' l -> JS_of2 f f' (JS_merge a a') l.
Proof.
rewrite /JS_merge /JS_merge_subdef/JS_of2.
case: a' (JS_head a') => -[a' /= + +] _.
case: a (JS_head a) => -[[//|[px [py prs]] a] /= _ +] _ Hpx.
case: a' => [//|[px' [py' prs']] a'] /= + _ + Hpx'.
set aux' := fix aux' _ _ _ a' := match a' with [::] => [::] | _ => _ end.
set aux := fix aux _ _ _ a _ := match a with [::] => _ | _ => _ end.
rewrite Hpx' -Hpx => {Hpx Hpx' px'}.
move=> Hp Hp' /andP[/andP[/= Hpxl +] JSa] /andP[/andP[_ +] JSa'].
rewrite -/(all (_ : pred JS_elem) a) => ala.
rewrite -/(all (_ : pred JS_elem) a') => ala'.
rewrite /JS_of2 /JS_of andbACA.
rewrite /all/= Hpxl/= -!/(all (_ : pred JS_elem) _).
rewrite 2!all_map/=; set im := preim _ _; set im' := preim _ _.
rewrite -(@eq_all _ im im')// andbb => {im'}.
move: Hp Hp' Hpxl ala ala' JSa JSa'.
elim: a a' px py prs py' prs' => [|[x [y rs]] a IHa] a'.
{ move=> + + + + + _ + + _.
  elim: a' => [|[x' [y' rs']] a' IHa'] px py prs py' prs'.
  { by move=> _ _ _ -> ->. }
  move=> /= /andP[Hpxx' Hp] Hpxl /andP[x'lel ala'] Hfpxrs.
  move=> /andP[Hf'pxrs' HJSf'].
  have -> : eq_segment_co f (px, (py, prs)) x'.
    by apply: eq_segment_cc_co; apply: eq_segment_ccWr Hfpxrs.
  rewrite x'lel Hf'pxrs'/=; apply: IHa' => //.
  by apply: eq_segment_ocWl (proj2 (andP Hfpxrs)); rewrite /= Hpxx'. }
elim: a' IHa => [|[x' [y' rs']] a' IHa'] IHa px py prs py' prs'.
{ move=> /= /andP[Hpxx Hp] _ Hpxl /andP[Hxl ala] _ /andP[Hfpxrs] HJSf Hf'pxrs'.
  have -> : eq_segment_co f' (px, (py', prs')) x.
    by apply: eq_segment_cc_co; apply: eq_segment_ccWr Hf'pxrs'.
  rewrite Hxl Hfpxrs/=; apply: IHa => //.
  by apply: eq_segment_ocWl (proj2 (andP Hf'pxrs')); rewrite Hpxx. }
move=> /= /andP[Hpxx Hp] /andP [Hpxx' Hp'] Hpxl.
move=> /andP[xlel ala] /andP[x'lel ala'] /andP[Hfpxrs HJSf] /andP[Hf'pxrs' HJSf'].
set aux'' := fix aux' _ _ _ a' := match a' with [::] => _ | _ => _ end.
case: ltP => Hxx' /=.
{ have -> : eq_segment_co f' (px, (py', prs')) x.
    by apply: eq_segment_coWr Hf'pxrs'; rewrite // ltW.
  rewrite xlel Hfpxrs/=; apply: IHa; rewrite //= ?Hxx' ?x'lel//.
  apply/andP; split=> //.
  by apply: eq_segment_ooWl (proj2 (andP Hf'pxrs')); rewrite // Hpxx. }
case: ltP => Hx'x /=.
{ have -> : eq_segment_co f (px, (py, prs)) x' by exact: eq_segment_coWr Hfpxrs.
  rewrite x'lel Hf'pxrs'/=; apply: IHa'; rewrite //= ?Hx'x ?xlel//.
  apply/andP; split=> //.
  by apply: eq_segment_ooWl (proj2 (andP Hfpxrs)); rewrite /= Hpxx'. }
move: Hxx'; rewrite le_eqVlt ltNge Hx'x orbC /= num_eq => /eqP {}Hx'x.
have -> : eq_segment_co f' (px, (py', prs')) x.
  by apply: eq_segment_coWr Hf'pxrs'; rewrite //= Hx'x.
by rewrite xlel Hfpxrs /=; apply: IHa; rewrite //= -Hx'x.
Qed.

Lemma preJS_eq (a : preJS) f f' l : JS_of f a l -> JS_of f' a l ->
  forall t, ratr (head_JS a).1%:num <= t%:num <= ratr l%:num -> f t = f' t.
Proof.
case: a => -[//|/=[x yrs] a _ pa].
move=> /andP[/=/andP[xlel ala]] JSa /andP[_ JSa'] + /andP[].
elim: a x yrs xlel pa ala JSa JSa' => [|[x' yrs'] a IHa] x yrs xlel /=.
  move=> _ _ /andP[/=ep /asboolP aff] /andP[/=ep' /asboolP aff'] t.
  case: ltgtP => [xltt|//|xt] _ tlel; first by rewrite aff// aff'.
  have -> : t = (ratr x%:num)%:nng by apply/val_inj; rewrite /= xt.
  by rewrite (eqP ep) (eqP ep').
move=> /andP[xltx' pa] /andP[x'lel ala] /andP[/andP[ep /asboolP aff] JSa].
move=> /andP[/andP[ep' /asboolP aff'] JSa'] t xlet tlel.
have [tltx'|x'let] := ltP t%:num (ratr x'%:num).
  case: ltgtP xlet => [xltt|//|xt] _; first by rewrite aff// aff'.
  have -> : t = (ratr x%:num)%:nng by apply/val_inj; rewrite /= xt.
  by rewrite (eqP ep) (eqP ep').
exact: IHa JSa' (t) x'let tlel.
Qed.

Lemma JS_eq (a : JS) f f' l : JS_of f a l -> JS_of f' a l ->
  forall t : {nonneg R}, t%:num <= ratr l%:num -> f t = f' t.
Proof.
case: a => a /= /eqP x0 JSa JSa'.
suff {x0} : forall t, ratr (head_JS a).1%:num <= t%:num <= ratr l%:num ->
                 f t = f' t.
  by move=> + t tlel; apply; rewrite x0 raddf0 ge0.
exact: preJS_eq JSa'.
Qed.

Lemma JS_eqb_correct (a a' : JS) f f' l : JS_of f a l -> JS_of f' a' l ->
  JS_eqb a a' l -> forall t : {nonneg R}, t%:num <= ratr l%:num -> f t = f' t.
Proof.
move=> JSa JSa' /eqP aa'.
have /andP[/JS_normalize_correct+ /JS_normalize_correct] :=
  JS_merge_correct JSa JSa'.
rewrite -{}aa'; set a'' := JS_normalize _ _ => {}JSa {}JSa' t tlel.
have: ratr (head_JS a'').1%:num <= t%:num <= ratr l%:num.
  rewrite tlel andbT (preJS_map_head _ default_JS).
  rewrite (preJS_map_head _ (0%:nng, (default_JS.2, default_JS.2))).
  by rewrite JS_merge_head raddf0 ge0.
exact: preJS_eq JSa' (t).
Qed.

Lemma JS_opp_correct (a : preJS) f l : JS_of f a l -> JS_of (\- f)%E (JS_opp a) l.
Proof.
apply: JS_map_correct => x yrs => [|y].
- by rewrite /eq_point/= => /eqP->; rewrite er_map_ratr_opp.
- move=> /asboolP aff; apply/asboolP => t xltt tley /=.
  rewrite er_map_ratr_opp rmorphN/= mulNr EFinN addrC.
  by rewrite -doppeD 1?adde_defC// addrC -(aff t xltt tley).
Qed.

Lemma JS_max_val_correct (a : preJS) f l : JS_of f a l ->
  forall t : {nonneg R}, ratr (head_JS a).1%:num < t%:num <= ratr l%:num ->
    (f t <= er_map ratr (JS_max_val a l))%dE.
Proof.
case: a => -[//|xyrs a] /= _ pa /andP[/=/andP[_ ala] JSa] t /andP[xt tl].
set aux := fix aux _ (a : seq JS_elem) := _.
elim: a xyrs pa ala JSa xt => [|xyrs' a IHa] [x [y [r s]]] /= pa ala JSa xt.
  have [llex|lgtx] := leP l%:num x%:num.
    suff: t%:num < t%:num by rewrite ltxx.
    by rewrite (le_lt_trans tl)// (le_lt_trans _ xt)// ler_rat.
  have /andP[_ /asboolP->//=] := JSa; rewrite addrC.
  have [rle0|rgt0] := leP r.
    by rewrite gee_dDl// lee_fin mulr_le0_ge0 ?lerq0// subr_ge0 ltW.
  rewrite er_map_ratr_add/= rmorphM/= lee_dD2l// lee_fin ler_pM2l ?ltr0q//.
  by rewrite rmorphB/= lerD2r.
case: xyrs' pa ala JSa => x' [y' rs'] /= /andP[xx' pa] /andP[x'l ala] JSa.
have /andP[/andP[_ /=/asboolP aff] {}JSa] := JSa.
rewrite !er_map_ratr_max !le_max.
have [x't|x't|x't] := ltgtP (ratr x'%:num) t%:num.
- by rewrite IHa ?orbT.
- rewrite aff//= addrC; apply/orP; left; apply/orP; left.
  have [rle0|rgt0] := leP r.
    by rewrite gee_dDl// lee_fin mulr_le0_ge0 ?lerq0// subr_ge0 ltW.
  rewrite er_map_ratr_add/= rmorphM/= lee_dD2l// lee_fin ler_pM2l ?ltr0q//.
  by rewrite rmorphB/= lerD2r ltW.
- apply/orP; left; apply/orP; right.
  have -> : t = (ratr x'%:num)%:nng by apply: val_inj.
  by have := JS_of_rec_eq_point_l JSa => /eqP->.
Qed.

Lemma JS_min_val_correct (a : preJS) f l : JS_of f a l ->
  forall t : {nonneg R},ratr (head_JS a).1%:num < t%:num <= ratr l%:num ->
    (f t >= er_map ratr (JS_min_val a l))%dE.
Proof.
move=> /JS_opp_correct /JS_max_val_correct m t itvt.
by rewrite -leeN2 er_map_ratr_opp oppeK; apply: m; rewrite head_JS_opp.
Qed.

Lemma JS_add_cst_correct c (a : JS) f l : JS_of f a l ->
  JS_of (f + (fun=> er_map ratr c)) (JS_add_cst c a) l.
Proof.
apply: JS_map_correct => x yrs => [|y].
- by rewrite /eq_point/GRing.add/= er_map_ratr_add => /eqP->.
- move=> /asboolP aff; apply/asboolP => t xltt tley /=.
  by rewrite [LHS]/GRing.add/= er_map_ratr_add addrA -(aff t xltt tley).
Qed.

Lemma y_rho_sigma_add_eq_segment f f' x yrs yrs' l :
  eq_segment f (x, yrs) l -> eq_segment f' (x, yrs') l ->
  eq_segment (f + f') (x, y_rho_sigma_add yrs yrs') l.
Proof.
rewrite /GRing.add/= => /andP[/eqP fx /asboolP fs] /andP[/eqP f'x /asboolP f's].
apply/andP; split; first by rewrite /eq_point/= fx f'x er_map_ratr_add.
apply/asboolP => t xltt tlel; rewrite /= fs// f's// addrACA -er_map_ratr_add.
by rewrite -dEFinD -mulrDl -rmorphD/=.
Qed.

Lemma JS_add_correct (a a' : JS) f f' l :
  JS_of f a l -> JS_of f' a' l -> JS_of (f + f') (JS_add a a') l.
Proof.
move=> JSa JSa'.
have := JS_merge_correct JSa JSa'.
rewrite /JS_add /JS_add_subdef/= => /andP[/andP[/=al {}JSa] /andP[_ {}JSa']].
case: JS_merge_subdef al JSa JSa' => [//|[x [yrs yrs']] c] /= => {a a'}.
elim: c x yrs yrs' => [|[x2 [yrs2 yrs2']] c IHc] x yrs yrs' /=.
  move=> /andP[xlel _] seg seg'; apply/andP; split; rewrite /= ?xlel//.
  exact: y_rho_sigma_add_eq_segment.
move=> /andP[xlel /andP[x2lel al]] /andP[fxx2 JSc] /andP[f'xx2 JSc'].
rewrite /JS_of/= xlel/=.
have -> /= : eq_segment_co (f + f')%E (x, y_rho_sigma_add yrs yrs') x2.
  exact: y_rho_sigma_add_eq_segment.
by apply: IHc; rewrite // x2lel.
Qed.

Lemma y_rho_sigma_min_eq_segment_cc f f' (yrs yrs' : y_rho_sigma)
    (x x' : {nonneg rat}) : x%:num <= x'%:num ->
  eq_segment_cc f (x, yrs) x' -> eq_segment_cc f' (x, yrs') x' ->
  JS_of (f \min f') (y_rho_sigma_min yrs yrs' x x') x'.
Proof.
move=> xlex' /andP[fx frs] /andP[f'x f'rs']; apply/andP; split.
  rewrite /y_rho_sigma_min/= => {fx frs f'x f'rs'}.
  case: yrs yrs' => [yx [r s]] [yx' [r' s']] /=.
  case: s s' => [s||] [s'||] /=; rewrite ?xlex'//.
  case: eqP => _ /=; first by rewrite xlex'.
  case: leP => xltx'' /=; first by rewrite xlex'.
  case: leP => x''ltx' /=; first by rewrite xlex'.
  by case: leP => _ /=;
    rewrite xlex' insubdK ?ltW//; apply: le_trans (ltW xltx'').
case: yrs yrs' fx frs f'x f'rs' => [yx [r s]] [yx' [r' s']] /= fx + f'x.
have px : eq_point (f \min f') x (mine yx yx').
  by rewrite /eq_point/= (eqP fx) (eqP f'x) er_map_ratr_min.
have myl z : affine_on_oc f (r, -oo%E) x x' -> affine_on_oc f' (r', z) x x' ->
    eq_segment_cc (f \min f') (x, (mine yx yx', (0, -oo%E))) x'.
  move=> /asboolP fy /asboolP f'y; apply/andP; split=> //.
  by apply/asboolP => t /= xltt tltx'; rewrite fy// f'y//= daddeNy// minNye.
have myr z : affine_on_oc f (r, z) x x' -> affine_on_oc f' (r', -oo%E) x x' ->
    eq_segment_cc (f \min f') (x, (mine yx yx', (0, -oo%E))) x'.
  move=> /asboolP fy /asboolP f'y; apply/andP; split=> //.
  by apply/asboolP => t /= xltt tltx'; rewrite fy// f'y//= daddeNy// mineNy.
have pyl z : affine_on_oc f (r, +oo%E) x x' -> affine_on_oc f' (r', z) x x' ->
    eq_segment_cc (f \min f') (x, (mine yx yx', (r', z))) x'.
  move=> /asboolP fy /asboolP f'y; apply/andP; split=> //.
  by apply/asboolP => t /= xltt tltx'; rewrite fy// f'y//= daddey minye.
have pyr z : affine_on_oc f (r, z) x x' -> affine_on_oc f' (r', +oo%E) x x' ->
    eq_segment_cc (f \min f') (x, (mine yx yx', (r, z))) x'.
  move=> /asboolP fy /asboolP f'y; apply/andP; split=> //.
  by apply/asboolP => t /= xltt tltx'; rewrite fy// f'y//= daddey miney.
case: s s' => [s||] [s'||] /=;
  rewrite ?sx'x' ?andbT; do ?[exact: myl|exact: myr|exact: pyl|exact: pyr].
rewrite /y_rho_sigma_min/= => /asboolP frs /asboolP f'rs'.
have [reqr' | rner']/= := eqP.
  apply/andP; split=> //; apply/asboolP => t /= xltt tltx'.
  by rewrite frs// f'rs'//= -reqr' -dadde_minr -EFin_min minr_rat.
have lex'' x_ r_ r'_ s_ s'_ : r_ < r'_ -> forall t : R,
    ratr (x_ + (s'_ - s_) / (r_ - r'_)) <= t -> ratr x_ < t ->
    (ratr r_ * (t - ratr x_) + ratr s_)%:dE
    <= (ratr r'_ * (t - ratr x_) + ratr s'_)%:dE.
  move=> rltr' t x''let xltt.
  rewrite lee_fin -lerBrDr -addrA -lerBlDl -mulrBl.
  rewrite mulrC -ler_ndivrMr ?subr_lt0 ?ltr_rat// lerBrDl.
  by rewrite -!rmorphB/= -fmorph_div/= -rmorphD/=.
have gex'' x_ x'_ r_ r'_ s_ s'_ : x'_ <= x_ + (s'_ - s_) / (r_ - r'_) ->
    r'_ < r_ -> forall t : {nonneg R}, t%:num <= ratr x'_ ->
    (ratr r_ * (t%:num - ratr x_) + ratr s_)%:dE
    <= (ratr r'_ * (t%:num - ratr x_) + ratr s'_)%:dE.
  move=> x'lex'' r'ltr t tlex'.
  rewrite lee_fin -lerBrDr -addrA -lerBlDl -mulrBl.
  rewrite -ler_pdivlMl ?subr_gt0 ?ltr_rat// mulrC lerBlDl.
  rewrite -!rmorphB/= -fmorph_div/= -rmorphD/=.
  by apply: le_trans tlex' _; rewrite ler_rat.
set x'' := x%:num + _ / _.
have [x''lex | xltx'']/= := leP.
  apply/andP; split=> //.
  have [rltr' | r'ltr] := leP.
    have {}rltr' : r < r'.
      by move: rltr' rner'; rewrite le_eqVlt => /orP[/eqP->|].
    apply/asboolP => t /= xltt tltx'; rewrite frs// f'rs'//= min_l//.
    by apply: lex'' => //; apply: le_trans (ltW xltt); rewrite ler_rat.
  apply/asboolP => t /= xltt tltx'; rewrite frs// f'rs'//= min_r//.
  by apply: lex''; rewrite // -divrNN !opprB (le_trans _ (ltW xltt))// ler_rat.
have [x'lex'' | x''ltx']/= := leP.
  apply/andP; split=> //.
  have [rltr' | r'ltr] := leP.
    have {}rltr' : r < r'.
      by move: rltr' rner'; rewrite le_eqVlt => /orP[/eqP->|].
    apply/asboolP => t /= xltt tltx'; rewrite frs// f'rs'//= min_r//.
    by apply: gex'' tltx'; rewrite // -divrNN !opprB.
  apply/asboolP => t /= xltt tltx'; rewrite frs// f'rs'//= min_l//.
  exact: gex'' tltx'.
have px'' : (insubd 0%:nng x'')%:num = x''.
  by rewrite insubdK//; apply: le_trans (ltW xltx'').
have [rltr' | r'ltr]/= := leP.
  have {}rltr' : r < r'.
    by move: rltr' rner'; rewrite le_eqVlt => /orP[/eqP->|].
  apply/andP; split; apply/andP; split=> //.
  - apply/asboolP => t /= xltt /[!px'']tltx''.
    have tltx' := lt_trans tltx'' (eqbRL (ltr_rat _ _ _) x''ltx').
    rewrite frs ?ltW// f'rs' ?ltW// min_r//=.
    by apply: gex'' (ltW tltx''); rewrite //= -divrNN !opprB.
  - rewrite /eq_point/= frs ?f'rs' ?ltW// ?lteBSide/= ?ltr_rat ?px''// min_l//.
      by rewrite [x'']lock rmorphD/= rmorphM/= rmorphB/= addrC dEFinD.
    by apply: lex''; rewrite // ltr_rat.
  - apply/asboolP => t /= /[!px'']x''ltt tltx'.
    have xltt : ratr x%:num < t%:num by apply: lt_trans x''ltt; rewrite ltr_rat.
    rewrite frs// f'rs'// min_l//=.
      rewrite -!dEFinD [x'']lock !rmorphD/= !rmorphM/= rmorphB/= !mulrBr.
      rewrite [ratr s + _]addrCA addrACA addrA addrK [ratr s + _]addrC.
      by rewrite addrA -mulrBr.
    by apply: lex''; rewrite // ltW.
apply/andP; split; apply/andP; split=> //.
- apply/asboolP => t /= xltt /[!px'']tltx''.
  have tltx' := lt_trans tltx'' (eqbRL (ltr_rat _ _ _) x''ltx').
  by rewrite frs ?ltW// f'rs' ?ltW// min_l//=; apply: gex'' (ltW tltx'').
- rewrite /eq_point/= frs ?f'rs' ?ltW// ?lteBSide/= ?ltr_rat ?px''// min_r//.
    by rewrite [x'']lock rmorphD/= rmorphM/= rmorphB/= addrC dEFinD.
  by apply: lex''; rewrite // ?ltr_rat// -divrNN !opprB.
- apply/asboolP => t /= /[!px'']x''ltt tltx'.
  have xltt : ratr x%:num < t%:num by apply: lt_trans x''ltt; rewrite ltr_rat.
  rewrite frs// f'rs'// min_r//=.
    rewrite -!dEFinD [x'']lock !rmorphD/= !rmorphM/= rmorphB/= !mulrBr.
    rewrite [ratr s' + _]addrCA addrACA addrA addrK [ratr s' + _]addrC.
    by rewrite addrA -mulrBr.
  by apply: lex''; rewrite //= -divrNN !opprB ltW.
Qed.

Lemma y_rho_sigma_min_eq_segment_co f f' (yrs yrs' : y_rho_sigma)
    (x x' : {nonneg rat}) (y y' : \bar^d rat) (rs : rho_sigma) :
  x%:num < x'%:num ->
  eq_segment_co f (x, yrs) x' -> eq_segment_co f' (x, yrs') x' ->
  eq_point f x' y -> eq_point f' x' y' ->
  JS_of (f \min f')
    (rcons (y_rho_sigma_min yrs yrs' x x') (x', (Order.min y y', rs))) x'.
Proof.
move=> xltx' fyrs fyrs' fy fy'.
have f2yrs := eq_segment_co_complete xltx' fyrs.
have f2yrs' := eq_segment_co_complete xltx' fyrs'.
have := y_rho_sigma_min_eq_segment_cc (ltW xltx') f2yrs f2yrs'.
set f2 := fun t => _.
set f2' := fun t => _.
have := y_rho_sigma_min_lt yrs yrs' xltx'.
have := sorted_y_rho_sigma_min yrs yrs' x x'.
case: y_rho_sigma_min => [_ _ _|[x'' yrs''] a].
  apply/andP; split; [|apply: eq_point_segment_cc]; rewrite /= ?lexx//.
  by rewrite /eq_point/= (eqP fy) (eqP fy') er_map_ratr_min.
move=> /= pa /andP[x''ltx' ala] /andP[/= ala' JSa].
apply/andP; split; first by rewrite /= all_rcons andbA andbAC ala' lexx.
move=> {ala'}; elim: a x'' yrs'' pa x''ltx' ala JSa => [|[x''' yrs'''] a IHa] /=.
  move=> x'' yrs'' _ x''ltx' _ seg; apply/and3P; split.
  - rewrite /is_true -(eq_segment_cc_co seg); apply: eq_segment_co_eq => //.
    by rewrite /f2 /f2'; move=> t /andP[_ /= ->].
  - by rewrite /eq_point/= (eqP fy) (eqP fy') er_map_ratr_min.
  - exact: affine_on_empty.
move=> x'' yrs'' /andP[x''ltx''' pa] x''ltx' /andP[x'''ltx' ala] /andP[seg JS].
apply/andP; split; last exact: IHa.
rewrite /is_true -seg; apply: eq_segment_co_eq => // t /andP[_ tltx'''].
by rewrite /f2 /f2' /= (lt_trans tltx''' (eqbRL (ltr_rat _ _ _) x'''ltx')).
Qed.

Lemma JS_min_correct (a a' : JS) f f' l :
  JS_of f a l -> JS_of f' a' l -> JS_of (f \min f') (JS_min a a' l) l.
Proof.
move=> JSa JSa'.
have := JS_sorted (JS_merge a a').
have := JS_merge_correct JSa JSa'.
rewrite /JS_min /JS_min_subdef/= => /andP[/andP[/=al {}JSa] /andP[_ {}JSa']].
case: JS_merge_subdef al JSa JSa' => [//|[x [[y rs] [y' rs']]] c] /= => {a a'}.
move=> al JS JS' pc.
suff: [&& (JS_min_rec_subdef x (y, rs) (y', rs') c l != [::])
    , ((head_JS (JS_min_rec_subdef x (y, rs) (y', rs') c l)).1 == x)
    , ((head_JS (JS_min_rec_subdef x (y, rs) (y', rs') c l)).2.1 == mine y y')
    & JS_of (f \min f') (JS_min_rec_subdef x (y, rs) (y', rs') c l) l].
  by move=> /and4P[].
move: x y rs y' rs' al JS JS' pc.
elim: c => [|[x2 [[y2 rs2] [y2' rs2']]] c IHc] x y rs y' rs' /=.
  move=> /andP[xlel _] ccxl cc'xl _; apply/and4P; split.
  - exact: y_rho_sigma_min_not_empty.
  - by rewrite head_y_rho_sigma_min.
  - by rewrite heady_y_rho_sigma_min.
  - exact: y_rho_sigma_min_eq_segment_cc.
move=> /andP[xlel alc] /andP[fxx2 JS] /andP[f'xx2 JS'] /andP[xltx2 pc].
have mne := y_rho_sigma_min_not_empty (y, rs) (y', rs') x x2.
have mh := head_y_rho_sigma_min (y, rs) (y', rs') x x2.
apply/and4P; split.
- by case: y_rho_sigma_min mne.
- by case: y_rho_sigma_min mne mh => [//|? ?] _ ->.
- have := heady_y_rho_sigma_min (y, rs) (y', rs') x x2.
  have := y_rho_sigma_min_not_empty (y, rs) (y', rs') x x2.
  by case: y_rho_sigma_min => [//|? ?] _ /= ->.
have fx2 : eq_point f x2 y2 by exact: JS_of_rec_eq_point_l JS.
have f'x2 : eq_point f' x2 y2' by exact: JS_of_rec_eq_point_l JS'.
have := IHc _ _ _ _ _ alc JS JS' pc.
have := y_rho_sigma_min_eq_segment_co _ xltx2 fxx2 f'xx2 fx2 f'x2.
case: JS_min_rec_subdef => [//|[_ [_ rs3]] c'] yrsmin /and4P[_ /= /eqP-> /eqP->].
rewrite JS_of_cat; first by move=> ->; rewrite andbT yrsmin.
by apply: sub_all (y_rho_sigma_min_lt _ _ xltx2) => z /ltW.
Qed.

Definition f_point (f : F) (x : {nonneg rat}) y :=
  eq_point f x y
  /\ forall t, t%:num != ratr x%:num -> f t = +oo%E.

Definition f_affine (f : F) rs (x l : {nonneg rat}) linf :=
  [/\ x%:num < l%:num
    , (forall t, t%:num <= ratr x%:num -> f t = +oo%E)
    , affine_on f rs x%:num (BSide linf (ratr l%:num))
    & (forall t, ratr l%:num < t%:num ?<= if linf -> f t = +oo%E)].

Definition f_segment (f : F) y rs (x l : {nonneg rat}) linf :=
  [/\ x%:num < l%:num ?<= if ~~ linf
    , (forall t, t%:num < ratr x%:num -> f t = +oo%E)
    , eq_segment f (x, (y, rs)) (BSide linf (ratr l%:num))
    & (forall t, ratr l%:num < t%:num ?<= if linf -> f t = +oo%E)].

Lemma f_segment_point_affine f y rs x l linf : x%:num < l%:num ->
  f_segment f y rs x l linf ->
  {gh | [/\ f = gh.1 \min gh.2, f_point gh.1 x y & f_affine gh.2 rs x l linf]}.
Proof.
move=> xltl [_ fltx /andP[/eqP ep /asboolP aff] fgtl].
pose g t := if t%:num <= ratr x%:num then f t else +oo%E.
pose h t := if t%:num <= ratr x%:num then +oo%E else f t.
exists (g, h); split=> /=.
- by apply/funext => t; rewrite /g /h /=; case: ltP => _; rewrite ?minye ?miney.
- split=> [|t]; first by rewrite /eq_point /g/= lexx ep.
  rewrite /g/= neq_lt => /orP[tltx|xltt]; first by rewrite (ltW tltx) fltx.
  by rewrite leNgt xltt.
split=> [//|t tlex||t lltt]; first by rewrite /h tlex.
  by apply/asboolP => t xltt tltl; rewrite /h leNgt xltt/= aff.
by rewrite /h ifN ?fgtl// -ltNge (lt_le_trans _ (lteifW lltt))// ltr_rat.
Qed.

Lemma f_segment_point f y rs x l linf : l%:num <= x%:num ->
  f_segment f y rs x l linf -> f_point f x y.
Proof.
move=> llex []; case: linf => /=; first by rewrite ltNge llex.
case: ltgtP llex => [//|//|/eqP +] _ _; rewrite num_eq => /eqP->.
move=> fltx /andP[/eqP/=fx _] fgtx; split=> [|t]; first by rewrite /eq_point fx.
by rewrite neq_lt => /orP[tltx|xltt]; [apply: fltx|apply: fgtx].
Qed.

Definition cutting_below (f : F) (l : {nonneg rat}) linf : F :=
  fun t => if t%:num < ratr l%:num ?<= if ~~ linf then f t else +oo%E.

Lemma cutting_below_comp f l l' :
  cutting_below (cutting_below f l false) l' false
  = cutting_below f (Order.min l%:num l'%:num)%:nng false.
Proof.
apply/funext => t; rewrite /cutting_below /= minr_rat le_min.
by case: leP => _; [case: ltP|rewrite andbF].
Qed.

Lemma cutting_below_conv f f' l :
  cutting_below ((f : Fd) * f') l false
  = cutting_below
      ((cutting_below f l false : Fd) * cutting_below f' l false) l false.
Proof.
apply: funext => t; rewrite /cutting_below /=.
case: leP => [tlel|//].
rewrite F_dioid_mulE /F_min_conv; apply: f_equal; rewrite predeqP => x; split.
  move=> -[[u v] /= uvt <-]; exists (u, v) => //=.
  by rewrite ifT ?ifT // (le_trans _ tlel)// -uvt ?lerDr// lerDl.
move=> -[[u v] /= uvt <-]; exists (u, v) => //=.
by rewrite ifT ?ifT // (le_trans _ tlel)// -uvt ?lerDr// lerDl.
Qed.

Lemma JS_of_cutting_below f a l :
  JS_of (cutting_below f l false) a l = JS_of f a l.
Proof.
case: a => [// | [x yrs] a] /=; rewrite /JS_of/cutting_below.
have [/=/andP[xlel]|//] := boolP (all _ _).
elim: a x yrs xlel => /=[|[x' yrs'] a IHa] x yrs xlel => [_|/andP[x'lel ala]].
  by apply: eq_segment_cc_eq => // t /=/andP[xlet tlel]; rewrite tlel.
set seg' := eq_segment_co _ _ _.
set seg := eq_segment_co _ _ _.
have -> : seg' = seg; congr andb; last exact: IHa.
- by apply: eq_point_eq => /=; rewrite ler_rat xlel.
- apply: affine_on_eq => t /= xltt tltx'.
  by rewrite (le_trans (ltW tltx'))// ler_rat.
Qed.

Lemma JS_of_first_segment x y r s x' y' r' s' (a : seq JS_elem) f l :
  x'%:num <= l%:num ->
  x%:num < x'%:num -> path lt_fst_num (x', (y', (r', s'))) a ->
  all (fun xyrs : JS_elem => (xyrs.1)%:num <= l%:num) a ->
  (forall t, t%:num < ratr x%:num -> f t = +oo%E) ->
  eq_segment_co f (x, (y, (r, s))) x' ->
  JS_of_rec f (x', (y', (r', s'))) a l ->
  (forall t, ratr l%:num < t%:num -> f t = +oo%E) ->
  exists f' f'' : F,
    [/\ f = f' \min f''
      , f_segment f' y (r, s) x x' true
      , (forall t, t%:num < ratr x'%:num -> f'' t = +oo%E)
      , JS_of_rec f'' (x', (y', (r', s'))) a l
      & (forall t, ratr l%:num < t%:num -> f'' t = +oo%E)].
Proof.
move=> x'lel xltx' pa ala fltx /andP[ep /asboolP aff] JSa fgtl.
exists (cutting_below f x' true); rewrite /cutting_below/=.
exists (fun t => if ratr x'%:num <= t%:num then f t else +oo%E).
split.
- by apply/funext => t /=; have [tltx'|tgex'] := ltP; rewrite ?miney// minye.
- split=> //; [|apply/andP; split|].
  + by move=> t tltx; rewrite (lt_trans tltx) ?ltr_rat// fltx.
  + by rewrite /eq_point/= ltr_rat xltx' (eqP ep).
  + by apply/asboolP => t /= xltt tltx'; rewrite ifT// aff.
- by move=> t /= x'let; rewrite ltNge x'let.
- by move=> t tltx'; rewrite leNgt tltx'.
- rewrite /is_true -JSa; apply: JS_of_rec_eq => //=; first by apply/andP; split.
  by move=> t /andP[x'let tlel]; rewrite x'let.
- by move=> t lltt; rewrite (le_trans _ (ltW lltt)) ?ler_rat// fgtl.
Qed.

Lemma JS_inf_left_correct f (a : preJS) l :
  (forall t : {nonneg R}, t%:num < ratr (head_JS a).1%:num -> f t = +oo%E) ->
  JS_of f a l -> JS_of f (JS_inf_left a) l.
Proof.
case: a => -[//|[x yrs] a] _ /= pa f0x.
have [//|xgt0] := leP.
move=> /andP[/=/andP[xlel ala] JSa].
apply/and3P; split=> //=; first by rewrite ge0 xlel ala.
apply/andP; split; first by rewrite /eq_point f0x//= ltr_rat.
by apply/asboolP => t /= _ tltx; rewrite f0x.
Qed.

Lemma JS_inf_left_eq_correct f f' (a : preJS) l :
    (forall t : {nonneg R}, ratr (head_JS a).1%:num < t%:num -> f t = f' t) ->
    (forall t : {nonneg R}, t%:num <= ratr (head_JS a).1%:num -> f' t = +oo%E) ->
  JS_of f a l -> JS_of f' (JS_inf_left_eq a) l.
Proof.
case: a => -[//|[x [y rs]] a] _ /= pa ff' f'oo /andP[/=/andP[xl ala] JSa].
apply/andP; split; first by case: ifP => _ /=; rewrite ?ge0 xl/=.
have JSooa : JS_of_rec f' (x, (+oo%E, rs)) a l.
  case: a pa ala JSa => [_ _|xyrs' a /=/andP[xxyrs' pa] ala]/=.
    case/andP => _ aff; apply/andP; split; rewrite /eq_point ?f'oo//.
    by rewrite -(@affine_on_eq f)//= => + + _.
  case/andP => /andP[_ aff]; rewrite -(@JS_of_rec_eq _ _ f')//; last first.
    by move=> t /andP[xyrs't _]; rewrite ff'// (lt_le_trans _ xyrs't)// ltr_rat.
  move=> ->/[!andbT]; apply/andP; split; rewrite /eq_point ?f'oo//.
  by rewrite -(@affine_on_eq f)//= => + + _.
case: leP => _ //; apply/andP; split=> [|//]; apply/andP; split.
  by rewrite /eq_point/= f'oo//= ler_rat.
by apply/asboolP => t _ /[!bnd_simp] tx; rewrite daddey// f'oo// ltW.
Qed.

Lemma min_conv_point_correct (x x' l l' : {nonneg rat}) y y' f f' :
  x%:num <= l%:num -> x'%:num <= l'%:num -> f_point f x y -> f_point f' x' y' ->
  JS_of ((f : Fd) * f') (min_conv_point x y x' y') (l%:num + l'%:num)%:nng.
Proof.
move=> xlel x'lel' [/eqP fx fnx] [/eqP f'x' f'nx'].
have pxx' : eq_point ((f : Fd) * f') (x%:num + x'%:num)%:nng (y + y')%dE.
  apply/eqP/le_anti/andP; split.
    apply: ereal_inf_lbound; exists ((ratr x%:nngnum)%:nng, (ratr x'%:nngnum)%:nng).
      by rewrite /= rmorphD.
    by rewrite fx f'x' er_map_ratr_add.
  apply: lb_ereal_inf=> [_ [[u v] /= uvxx' <-]].
  case: (@eqP _ u%:nngnum (ratr x%:nngnum)) => [|/eqP] ux /=.
    move: uvxx'; rewrite ux rmorphD /= => /addrI vx'.
    have -> : u = (ratr x%:nngnum)%:nng by exact: val_inj.
    have -> : v = (ratr x'%:nngnum)%:nng by exact: val_inj.
    by rewrite fx f'x' er_map_ratr_add.
  by rewrite fnx // leey.
have npxx' t : t%:num != ratr (x%:num + x'%:num) -> ((f : Fd) * f') t = +oo%E.
  move=> tnexx'; apply/le_anti; rewrite leey /=.
  apply: lb_ereal_inf => [_ [[u v] /= uvt <-]].
  case: (@eqP _ u%:num (ratr x%:num)) => [|/eqP] ux; last by rewrite fnx.
  move: uvt; rewrite ux.
  case: (@eqP _ v%:num (ratr x'%:num)) => [-> xx't|/eqP vx' xvt].
    by exfalso; move: tnexx' => /eqP; apply; rewrite rmorphD.
  by rewrite f'nx' // daddeC.
apply: JS_inf_left_correct => /= [t tltxx'|].
  by rewrite npxx'// neq_lt tltxx'.
apply/and3P; split=> /=; [by rewrite lerD|exact pxx'|].
by apply/asboolP => t /= xx't _; rewrite npxx'// neq_lt xx't orbT.
Qed.

Lemma min_conv_point_affine_correct (x : {nonneg rat}) y x' r' s'
    (l' : {nonneg rat}) l'inf f f' (l'' l''' : {nonneg rat}) :
  x%:num <= l''%:num -> l'%:num <= l'''%:num ->
  f_point f x y -> f_affine f' (r', s') x' l' l'inf ->
  JS_of ((f : Fd) * f') (min_conv_point_affine x y x' r' s' l' l'inf)
    (l''%:num + l'''%:num)%:nng.
Proof.
move=> xlel'' l'lel''' [/eqP fx fnx] [x'ltl' f'lex' /asboolP f'x'l' f'gtl'].
have ff'lexx' : forall t : {nonneg R}, t%:num <= ratr (x%:num + x'%:num) ->
    ((f : Fd) * f') t = +oo%E.
  move=> t tlexx'; apply/le_anti; rewrite leey/=.
  apply: lb_ereal_inf => _ [[u v] /= uvt <-].
  have [?|vgtx'] := leP v%:num (ratr x'%:num); first by rewrite f'lex'// addrC.
  rewrite fnx// neq_lt -(ltrD2r v%:num) uvt.
  by rewrite (le_lt_trans tlexx')// !rmorphD/= ltrD2l.
have ff' : forall t : {nonneg R},
    ratr (x%:num + x'%:num) < t%:num ->
    t%:num < ratr (x%:num + l'%:num) ?<= if ~~l'inf ->
    ((f : Fd) * f') t = ((ratr r' * (t%:num - ratr (x%:num + x'%:num)))%:E
                         + er_map ratr (s' + y)%dE)%dE.
  move=> t xx'ltt tltxl'; apply/le_anti/andP; split.
    have Ptmx : 0 <= t%:num - ratr x%:num.
      by rewrite subr_ge0 (le_trans _ (ltW xx'ltt))// rmorphD/= lerDl.
    apply: ereal_inf_lbound; exists ((ratr x%:num)%:nng, NngNum Ptmx) => /=.
      by rewrite /= addrCA subrr addr0//.
    rewrite fx addrC er_map_ratr_add addrA f'x'l'/=.
    - by rewrite rmorphD/= opprD addrA.
    - by rewrite ltrBrDl -rmorphD.
    - by rewrite lteBSide/= lteifBlDl -rmorphD.
  apply: lb_ereal_inf => _ [[u v] /= uvt <-].
  have [ux|/eqP ux] := @eqP _ u%:num (ratr x%:num); last by rewrite fnx// leey.
  have -> : u = (ratr x%:num)%:nng by exact/val_inj.
  rewrite fx [leRHS]addrC er_map_ratr_add addrA f'x'l'/=.
  - by rewrite rmorphD/= opprD -uvt ux addrACA subrr add0r.
  - by rewrite -(ltrD2l u%:num) uvt ux -rmorphD.
  - by rewrite lteBSide -(lteifD2l _ u%:num) uvt ux -rmorphD/=.
have ff'gexl' : forall t : {nonneg R},
    ratr (x%:num + l'%:num) < t%:num ?<= if l'inf ->
    ((f : Fd) * f') t = +oo%E.
  move=> t xl'ltt; apply/le_anti; rewrite leey/=.
  apply: lb_ereal_inf => _ [[u v] /= uvt <-].
  have [ux|/eqP ux] := @eqP _ u%:num (ratr x%:num); last by rewrite fnx// leey.
  have: ratr l'%:num < v%:num ?<= if l'inf.
    by rewrite -(lteifD2l _ u%:num) uvt ux -rmorphD/=.
  by move/f'gtl' => ->; rewrite addrC.
rewrite /min_conv_point_affine /JS_segment.
case: leP => [|i]; first by rewrite /= lerD2l leNgt x'ltl'.
apply: JS_inf_left_correct => /= {i} [? ?|]; first exact/ff'lexx'/ltW.
apply/and4P; split.
- by rewrite /= ?(lerD xlel'')// (le_trans _ l'lel''')// ltW.
- apply/andP; split=> /=; first by rewrite /eq_point ff'lexx'.
  by apply/asboolP => t xx'ltt tltxl'; rewrite ff'// lteifS.
- case: l'inf ff' ff'gexl' f'x'l' f'gtl' => /= ff' ff'gexl' _ _.
    by rewrite /eq_point/= ff'gexl'.
  rewrite /eq_point/= ff'//= ?ltr_rat ?ltrD2l//.
  by rewrite !er_map_ratr_add/= rmorphM/= rmorphB.
- by apply/asboolP => t /= tgtxl' _; apply/ff'gexl'/lteifS.
Qed.

Lemma min_conv_affine_correct r s (x l : {nonneg rat}) linf
    r' s' (x' l' : {nonneg rat}) l'inf f f' (l'' l''' : {nonneg rat}) :
  l%:num <= l''%:num -> l'%:num <= l'''%:num ->
  f_affine f (r, s) x l linf -> f_affine f' (r', s') x' l' l'inf ->
  JS_of ((f : Fd) * f') (min_conv_affine r s x l linf r' s' x' l' l'inf)
    (l''%:num + l'''%:num)%:nng.
Proof.
suff: !!(forall f f' (x x' l l' l'' l''' : {nonneg rat}) r r' s s' linf l'inf,
    r <= r' ->
    l%:num <= l''%:num -> l'%:num <= l'''%:num ->
    f_affine f (r, s) x l linf -> f_affine f' (r', s') x' l' l'inf ->
    JS_of ((f : Fd) * f') (min_conv_affine r s x l linf r' s' x' l' l'inf)
      (l''%:num + l'''%:num)%:nng).
  move=> hyp llel'' l'lel''' aff aff'.
  have [rler'|/ltW rftr'] := leP r r'; first exact: hyp.
  rewrite mulrC /min_conv_affine/= min_conv_affine_subdefC.
  have -> : (l''%:num + l'''%:num)%:nng = (l'''%:num + l''%:num)%:nng.
    by apply/val_inj; rewrite /= addrC.
  exact: hyp.
move=> {}f {}f' {}x {}x' {}l {}l' {}l'' {}l''' {}r {}r' {}s {}s' {}linf {}l'inf.
move=> rler' llel'' l'lel''' [xltl flex /asboolP fxl fgtl].
move=> [x'ltl' f'lex' /asboolP f'x'l' f'gtl'].
have ff'lexx' : forall t, t%:num <= ratr (x%:num + x'%:num) ->
    ((f : Fd) * f') t = +oo%E.
  move=> t tlexx'; apply/le_anti; rewrite leey/=.
  apply: lb_ereal_inf => _ [[u v] /= uvt <-].
  have [ulex|ugtx] := leP u%:num (ratr x%:num); first by rewrite flex.
  rewrite f'lex' 1?addrC// -(lerD2l u%:num) uvt (le_trans tlexx')//.
  by rewrite rmorphD/= lerD2r ltW.
have ff'gell' : forall t,
    ratr (l%:num + l'%:num) < t%:num ?<= if linf || l'inf ->
    ((f : Fd) * f') t = +oo%E.
  move=> t tlexx'; apply/le_anti; rewrite leey/=.
  apply: lb_ereal_inf => _ [[u v] /= uvt <-].
  have [lltu|lgeu] := boolP (ratr l%:num < u%:num ?<= if linf).
    by rewrite fgtl.
  rewrite f'gtl' 1?addrC// -(lteifD2l _ u%:num) uvt.
  case: (linf) tlexx' lgeu => /=; rewrite -?ltNge -?leNgt rmorphD/=.
    move=> ll'let; rewrite -(ltrD2r (ratr l'%:num)) => ul'ltll'.
    by rewrite lteifS// (lt_le_trans _ ll'let).
  move=> ll'ltt; rewrite -(lerD2r (ratr l'%:num)) => ul'lell'.
  by rewrite -[l'inf]andTb (lteif_trans _ ll'ltt).
apply: JS_inf_left_correct => [t /=|].
  rewrite /min_conv_affine_subdef/= 2!leNgt xltl x'ltl'/=.
  case: (ltgtP r r') rler' => [rltr'|//|rr'] _; last first.
    by rewrite JS_head raddf0 lt0F.
  by rewrite /= => /ltW; apply: ff'lexx'.
rewrite /min_conv_affine_subdef/= 2!leNgt xltl x'ltl'/=.
have e'lee (z e : R) (egt0 : 0 < e) :
    ratr (r' - r) * (Num.min (e / Num.max (ratr (r' - r)) 1) z) <= e.
  case: ltgtP rler' => [rltr'|//|<-] _; last by rewrite subrr raddf0 mul0r ltW.
  rewrite -ler_pdivlMl ?ltr0q ?subr_gt0// mulrC.
  rewrite ge_min; apply/orP; left.
  rewrite ler_pM2r// lef_pV2 ?qualifE/= ?le_max ?lexx ?ltr0q ?subr_gt0//.
  by rewrite lt_max ltr01 orbT.
have ff'xx'x'l : forall t, ratr (x%:num + x'%:num) < t%:num ->
    t%:num <= ratr (x'%:num + l%:num) ->
    ((f : Fd) * f') t = (ratr r * (t%:num - ratr (x%:num + x'%:num)))%:dE
                        + er_map ratr (s + s')%dE.
  move=> t xx'ltt tlex'l; apply/le_anti/andP; split; last first.
    apply: lb_ereal_inf => _ [[u v] /= <- <-]; rewrite rmorphD/= opprD.
    rewrite addrACA mulrDr dEFinD er_map_ratr_add addrACA lee_dD//.
      have [ulex|ugtx] := leP u%:num (ratr x%:num); first by rewrite flex ?leey.
      have [lu|lu] := boolP (ratr l%:num < u%:num ?<= if linf).
        by rewrite fgtl ?leey.
      by rewrite fxl// lteBSide/= implybF comparable_lteifNE// real_comparable.
    have [vlex'|vgtx'] := leP v%:num (ratr x'%:num).
      by rewrite f'lex' ?leey.
    have [l'v|l'v] := boolP (ratr l'%:num < v%:num ?<= if l'inf).
      by rewrite f'gtl' ?leey.
    rewrite f'x'l'//=; last first.
      by rewrite lteBSide/= implybF comparable_lteifNE// real_comparable.
    by rewrite lee_dD2r// lee_fin ler_pM2r ?subr_gt0// ler_rat.
  apply/lee_daddgt0Pr => e egt0.
  pose e' := Num.min (e / Num.max (ratr (r' - r)) 1) (Num.min
    ((ratr (l'%:num - x'%:num)) / 2) ((t%:num - ratr (x%:num + x'%:num)) / 2)).
  pose v := ratr x'%:num + e'.
  pose u := t%:num - v.
  have e'gt0 : 0 < e'.
    rewrite !lt_min 2?divr_gt0//=.
    - by rewrite divr_gt0// subr_gt0.
    - by rewrite rmorphB/= subr_gt0 ltr_rat.
    - by rewrite lt_max ltr01 orbT.
  have xltu : ratr x%:num < u.
    rewrite /u ltrBrDr /v addrA -ltrBrDl [ltRHS]splitr.
    rewrite -rmorphD/= !gt_min; apply/orP; right; apply/orP; right.
    by rewrite ltrDr divr_gt0// subr_gt0.
  have ultl : u < ratr l%:num.
    rewrite /u ltrBlDl /v addrAC (le_lt_trans tlex'l)// rmorphD/=.
    by rewrite ltrDl.
  have x'ltv : ratr x'%:num < v by rewrite /v ltrDl.
  have vltl' : v < ratr l'%:num.
    rewrite /v -ltrBrDl -rmorphB/= [ltRHS]splitr.
    rewrite !gt_min; apply/orP; right; apply/orP; left.
    by rewrite ltrDl divr_gt0// rmorphB/= subr_gt0 ltr_rat.
  have uge0 : 0 <= u by exact: le_trans (ltW xltu).
  have vge0 : 0 <= v by exact: le_trans (ltW x'ltv).
  have: ((f : Fd) * f') t <= f (NngNum uge0) + f' (NngNum vge0).
    apply: ereal_inf_lbound; exists (NngNum uge0, NngNum vge0) => //.
    by rewrite /= subrK.
  move/le_trans; apply; rewrite fxl ?f'x'l'//=; last first.
  - by rewrite lteBSide/= lteifS.
  - by rewrite lteBSide/= lteifS.
  rewrite addrACA -er_map_ratr_add [leRHS]addrAC lee_dD2r//.
  rewrite -!dEFinD lee_fin /u /v [ratr x'%:num + _]addrC addrK.
  rewrite addrAC [e' + _]addrC opprD addrA mulrDr rmorphD/= opprD addrA.
  rewrite -[leLHS]addrA lerD2l mulrN -mulNr -mulrDl addrC -rmorphB/=.
  exact: e'lee.
have ff'x'lll' : forall t, ratr (x'%:num + l%:num) < t%:num ->
    t%:num < ratr (l%:num + l'%:num) ?<= if ~~ (linf || l'inf) ->
    ((f : Fd) * f') t = (ratr r' * (t%:num - ratr (x'%:num + l%:num)%E))%:dE
                        + er_map ratr ((r * (l%:num - x%:num))%:dE + s + s').
  move=> t x'lltt tltll'; apply/le_anti/andP; split; last first.
    apply: lb_ereal_inf => _ [[u v] /= <- <-]; rewrite rmorphD/= opprD.
    rewrite [- _ - _]addrC addrACA mulrDr dEFinD !er_map_ratr_add.
    rewrite !addrA [X in (X + _ + _)%dE]addrAC [X in (X + _)%dE]addrAC -addrA.
    apply: lee_dD; last first.
      have [vlex'|vgtx'] := leP v%:num (ratr x'%:num).
        by rewrite f'lex' ?leey.
      have [l'v|l'v] := boolP (ratr l'%:num < v%:num ?<= if l'inf).
        by rewrite f'gtl' ?leey.
      rewrite f'x'l'//=.
      by rewrite lteBSide/= implybF comparable_lteifNE// real_comparable.
    have [ulex|ugtx] := leP u%:num (ratr x%:num); first by rewrite flex ?leey.
    have [lu|lu] := boolP (ratr l%:num < u%:num ?<= if linf).
      by rewrite fgtl ?leey.
    rewrite fxl// ?lteBSide/= ?implybF ?comparable_lteifNE// ?real_comparable//.
    rewrite lee_dD2r// -dEFinD lee_fin.
    rewrite -[r'](subrK r) rmorphD/= mulrDl rmorphM/= -addrA -mulrDr.
    rewrite !rmorphB/= addrACA addrA addrK -lerBrDl lerDl.
    rewrite -mulrN opprB mulr_ge0// subr_ge0 ?ler_rat//.
    by move: lu; rewrite -comparable_lteifNE ?real_comparable// => /lteifW.
  apply/lee_daddgt0Pr => e egt0.
  pose e' := Num.min (e / Num.max (ratr (r' - r)) 1) (Num.min
    ((ratr (l%:num - x%:num)) / 2) ((ratr (l%:num + l'%:num) - t%:num) / 2)).
  pose u := ratr l%:num - e'.
  pose v := t%:num - u.
  have e'gt0 : 0 < e' ?<= if ~~ (linf || l'inf).
    rewrite !lteif_minr; apply/andP; split; [|apply/andP; split].
    - by rewrite lteifS// divr_gt0// lt_max ltr01 orbT.
    - by rewrite lteifS// divr_gt0// rmorphB/= subr_gt0 ltr_rat.
    - by rewrite lteif_pdivlMr// mul0r subr_lteif0r.
  have xltu : ratr x%:num < u.
    rewrite /u ltrBrDr -ltrBrDl [ltRHS]splitr.
    rewrite -rmorphB/= !gt_min; apply/orP; right; apply/orP; left.
    by rewrite ltrDr divr_gt0// rmorphB/= subr_gt0 ltr_rat.
  have ultl : u < ratr l%:num ?<= if ~~ linf.
    rewrite /u lteifBlDr.
    by case: (linf) e'gt0 => [|/lteifW]; rewrite /= ?ltrDl// lerDl.
  have x'ltv : ratr x'%:num < v.
    rewrite /v /u opprB addrA ltrBrDr -rmorphD/= (lt_le_trans x'lltt)//.
    by rewrite lerDl (lteifW e'gt0).
  have vltl' : v < ratr l'%:num ?<= if ~~ l'inf.
    rewrite /v /u opprB addrCA addrA lteifBlDl -lteifBrDr.
    rewrite -rmorphD/= !lteif_minl; apply/orP; right; apply/orP; right.
    case: (l'inf) tltll' => [|/lteifW]; rewrite ?orbT => /= tll'.
    - by rewrite [ltRHS]splitr ltrDl divr_gt0// subr_gt0.
    - by rewrite [leRHS]splitr lerDl divr_ge0// subr_ge0.
  have uge0 : 0 <= u by exact: le_trans (ltW xltu).
  have vge0 : 0 <= v by exact: le_trans (ltW x'ltv).
  have: ((f : Fd) * f') t <= f (NngNum uge0) + f' (NngNum vge0).
    apply: ereal_inf_lbound; exists (NngNum uge0, NngNum vge0) => //.
    by rewrite /= /v addrC subrK.
  move/le_trans; apply; rewrite fxl ?f'x'l'//=.
  rewrite addrACA -er_map_ratr_add [leRHS]addrAC -[(_ + s + s')%dE]addrA.
  rewrite [in leRHS]er_map_ratr_add addrA lee_dD2r//= -!dEFinD lee_fin.
  rewrite [leRHS]addrC rmorphM/= /u [_ - e' - _]addrAC -rmorphB/= mulrDr.
  rewrite -addrA lerD2l addrC /v 2!mulrDr [in leRHS]mulrDr -!addrA lerD2l.
  rewrite /u addrA -mulrDr opprB -addrA -opprD [e' + _]addrC -rmorphD/=.
  rewrite [l%:num + _]addrC mulrDr -addrA lerD2l.
  by rewrite mulrN -mulNr -mulrDl -rmorphB/= e'lee.
case: (ltgtP r r') rler' => [rltr'|//|rr'] _; last first.
  rewrite /JS_segment/=; case: leP => [|xx'ltll'].
    by rewrite leNgt (ltrD xltl x'ltl').
  apply: JS_inf_left_correct => [t /= tltxx'|]; first by rewrite ff'lexx'// ltW.
  have ff'xx'll' : forall t, ratr (x%:num + x'%:num) < t%:num ->
      t%:num < ratr (l%:num + l'%:num) ?<= if ~~ (linf || l'inf) ->
      ((f : Fd) * f') t = (ratr r * (t%:num - ratr (x%:num + x'%:num)%E))%:dE
                          + er_map ratr (s + s')%dE.
    move=> t xx'ltt tltll'.
    have [tlex'l|tgtx'l] := leP t%:num (ratr (x'%:num + l%:num)).
      by rewrite ff'xx'x'l.
    rewrite ff'x'lll'// -rr' -[(_ + s + s')%dE]addrA er_map_ratr_add/= addrA.
    rewrite -dEFinD rmorphM/= -mulrDr; congr +%R; congr _%:E; congr *%R.
    rewrite -addrA; congr +%R; rewrite rmorphB/= rmorphD/= opprD subrKA.
    by rewrite -opprD rmorphD/= addrC.
  apply/and4P; split=> /=.
  - rewrite (lerD llel'' l'lel''') lerD//.
    + by apply: le_trans llel''; apply: ltW.
    + by apply: le_trans l'lel'''; apply: ltW.
  - apply/andP; split; first by rewrite /eq_point/= ff'lexx'.
    by apply/asboolP => t /= xx'ltt tltll'; rewrite ff'xx'll'// lteifS.
  - have [inf|ninf] := boolP (linf || l'inf).
    + by rewrite /eq_point/= ff'gell'// inf/=.
    + rewrite /eq_point/= er_map_ratr_add/= rmorphM/= rmorphB/=.
      by rewrite ff'xx'll'// ?ltr_rat// ninf/=.
  - by apply/asboolP => t ll'ltt tlel''l'''; rewrite daddey ff'gell'// lteifS.
apply/and5P; split=> /=.
- rewrite (lerD llel'' l'lel'''); apply/and3P; split=> //.
  + apply: lerD; first by apply: le_trans llel''; apply: ltW.
    by apply: le_trans l'lel'''; apply: ltW.
  + by rewrite addrC lerD//; apply: le_trans l'lel'''; apply: ltW.
- apply/andP; split; first by rewrite /eq_point/= ff'lexx'.
  apply/asboolP => t /= xx'ltt tltx'l.
  by rewrite ff'xx'x'l// ltW.
- apply/andP; split.
  + rewrite /eq_point/= ff'xx'x'l/=.
    * rewrite !rmorphD/= opprD addrA [ratr x'%:num + _]addrC addrAC addrK.
      by rewrite !er_map_ratr_add/= addrA rmorphM/= rmorphB/=.
    * by rewrite !rmorphD/= addrC ltrD2l ltr_rat.
    * by rewrite !rmorphD/= lerD2l ler_rat.
  + by apply/asboolP => t /= x'lltt tltll'; rewrite ff'x'lll'// lteifS.
- have [inf|ninf] := boolP (linf || l'inf).
  + by rewrite /eq_point/= ff'gell'// inf/=.
  + rewrite /eq_point/= -addrA er_map_ratr_add/= ff'x'lll'/=.
    * rewrite !rmorphD/= opprD [ratr l%:num + _]addrC addrACA subrr addr0.
      rewrite !er_map_ratr_add/= !addrA -dEFinD [X in X%:dE]addrC.
      by rewrite !rmorphM/= !rmorphB/=.
    * by rewrite addrC !rmorphD/= ltrD2l ltr_rat.
    * by rewrite (negbTE ninf)/=.
- by apply/asboolP => t ll'ltt tlel''l'''; rewrite daddey ff'gell'// lteifS.
Qed.

Lemma min_conv_segment_correct y r s (x l : {nonneg rat}) linf
    y' r' s' (x' l' : {nonneg rat}) l'inf f f' (l'' : {nonneg rat}) :
  l%:num <= l''%:num -> l'%:num <= l''%:num ->
  f_segment f y (r, s) x l linf -> f_segment f' y' (r', s') x' l' l'inf ->
  JS_of ((f : Fd) * f')
    (min_conv_segment x y r s l linf x' y' r' s' l' l'inf l'') l''.
Proof.
move=> llel'' l'lel'' seg seg'; rewrite /min_conv_segment.
have l''le2l'' : l''%:num <= (l''%:num + l''%:num)%:nng%:num.
  by rewrite lerDl.
have xlel'' : x%:num <= l''%:num.
  by apply: le_trans llel''; move: seg => [/lteifW].
have x'lel'' : x'%:num <= l''%:num.
  by apply: le_trans l'lel''; move: seg' => [/lteifW].
have [llex|xltl] := leP.
- have pf := f_segment_point llex seg.
  have [l'lex'|x'ltl'] := leP.
  + have pf' := f_segment_point l'lex' seg'.
    exact/(JS_below_correct l''le2l'')/min_conv_point_correct.
  + have [[g h] [-> /= pg affh]] := f_segment_point_affine x'ltl' seg'.
    rewrite mulrDr; apply: JS_min_correct.
      exact/(JS_below_correct l''le2l'')/min_conv_point_correct.
    exact/(JS_below_correct l''le2l'')/min_conv_point_affine_correct.
- have [[g h] [-> /= pg affh]] := f_segment_point_affine xltl seg.
  have [l'lex'|x'ltl'] := leP.
  + have pf' := f_segment_point l'lex' seg'.
    rewrite mulrDl [(h : Fd) * f']mulrC; apply: JS_min_correct.
      exact/(JS_below_correct l''le2l'')/min_conv_point_correct.
    exact/(JS_below_correct l''le2l'')/min_conv_point_affine_correct.
  + have [[g' h'] [-> /= pg' affh']] := f_segment_point_affine x'ltl' seg'.
    rewrite mulrDl !mulrDr [(h : Fd) * g']mulrC; apply: JS_min_correct.
    * apply: JS_min_correct.
        exact/(JS_below_correct l''le2l'')/min_conv_point_correct.
      exact/(JS_below_correct l''le2l'')/min_conv_point_affine_correct.
    * apply: JS_min_correct.
        exact/(JS_below_correct l''le2l'')/min_conv_point_affine_correct.
      exact/(JS_below_correct l''le2l'')/min_conv_affine_correct.
Qed.

Lemma min_conv_segment_JS_correct x y r s (l : {nonneg rat}) linf
    (a : JS) f f' l'' :
  l%:num <= l''%:num -> f_segment f y (r, s) x l linf -> JS_of f' a l'' ->
  (forall t, ratr l''%:num < t%:num -> f' t = +oo%E) ->
  JS_of ((f : Fd) * f') (min_conv_segment_JS x y r s l linf a l'') l''.
Proof.
move=> llel'' + + f'gtl''.
case: a => -[[//|[x' [y' [r' s']] a] /= _ pa x'0]].
move=> fxl /andP[/=/andP[x'lel'' ala] JSa].
have {x'0} : forall t : {nonneg R}, t%:num < ratr x'%:num -> f' t = +oo%E.
  by move=> t; rewrite (eqP x'0) raddf0 lt0F.
move: a f' x' y' r' s' pa x'lel'' ala JSa f'gtl''.
elim=> [|[x'' [y'' [r'' s'']]] a IHa] f' x' y' r' s' + x'lel'' + + f'ltx'.
  move=> _ _ JSa f'gtl''.
  exact: min_conv_segment_correct.
move=> /=/andP[x'ltx'' pa] /andP[x''lel'' ala] /andP[seg JSa] f'gtl''.
rewrite /min_conv_segment_JS_rec_subdef -/min_conv_segment_JS_rec_subdef.
have [f'' [f''' [f'f''f''' f''x'x'' f'''ltx'' JSa' f'''gtl'']]] :
  exists f'' f''', [/\ f' = f'' \min f''', f_segment f'' y' (r', s') x' x'' true
    , (forall t : {nonneg R}, t%:num < ratr x''%:num -> f''' t = +oo%E)
    , JS_of_rec f''' (x'', (y'', (r'', s''))) a l''
    & (forall t : {nonneg R}, ratr l''%:num < t%:num -> f''' t = +oo%E)].
  exact: JS_of_first_segment.
rewrite f'f''f''' mulrDr F_dioid_addE JS_min_correct//.
  exact: min_conv_segment_correct.
exact: IHa.
Qed.

Lemma JS_min_conv_correct (a a' : JS) f f' (l : {nonneg rat}) :
  JS_of f a l -> JS_of f' a' l ->
  JS_of ((f : Fd) * f') (JS_min_conv a a' l) l.
Proof.
rewrite -JS_of_cutting_below => JSa.
rewrite -JS_of_cutting_below => JSa'.
rewrite -JS_of_cutting_below cutting_below_conv JS_of_cutting_below.
move: JSa JSa'.
set f'' := cutting_below f l false.
set f''' := cutting_below f' l false.
have: forall t, ratr l%:num < t%:num -> f'' t = +oo%E.
  by move=> t lltt; rewrite /f''/cutting_below/= leNgt lltt.
have: forall t, ratr l%:num < t%:num -> f''' t = +oo%E.
  by move=> t lltt; rewrite /f'''/cutting_below/= leNgt lltt.
move: f'' f''' => {}f {}f' f'gtl fgtl + JSa'.
case: a => -[[//|[x [y [r s]]] a] /= _ pa x0].
move=> /andP[/=/andP[xlel ala] JSa].
have {x0} : forall t : {nonneg R}, t%:num < ratr x%:num -> f t = +oo%E.
  by move=> t; rewrite (eqP x0) raddf0 lt0F.
move: a f x y r s pa xlel ala JSa fgtl.
elim=> [|[x' [y' [r' s']]] a IHa] f x y r s + xlel + + fgtl fltx.
  move=> _ _ JSa.
  rewrite /JS_min_conv_rec_subdef.
  exact: min_conv_segment_JS_correct.
move=> /=/andP[xltx' pa] /andP[x'lel ala] /andP[seg JSa].
have [f'' [f''' [f'f''f''' f''xx' f'''ltx' JSa'' f'''gtl]]] :
  exists f'' f''', [/\ f = f'' \min f''', f_segment f'' y (r, s) x x' true
    , (forall t : {nonneg R}, t%:num < ratr x'%:num -> f''' t = +oo%E)
    , JS_of_rec f''' (x', (y', (r', s'))) a l
    & (forall t : {nonneg R}, ratr l%:num < t%:num -> f''' t = +oo%E)].
  exact: JS_of_first_segment.
rewrite f'f''f''' mulrDl F_dioid_addE JS_min_correct//.
  exact: min_conv_segment_JS_correct.
exact: IHa.
Qed.

End PA.
Arguments JS_cat_correct {R} [f a b] l l'.
