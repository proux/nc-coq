From mathcomp Require Import ssreflect ssrbool ssrfun ssrnat seq path ssrnum.
From mathcomp Require Import fintype finfun bigop ssralg ssrint rat eqtype.
From mathcomp Require Import order interval.
From mathcomp Require Import boolp classical_sets signed reals ereal.
From NCCoq Require Import RminStruct mathcomp_complements analysis_complements.
Require Import NCCoq.deviations NCCoq.usual_functions.
Require Import ratdiv jump_sequences PA UPP.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope classical_set_scope.
Local Open Scope ring_scope.
Local Open Scope order_scope.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.

Section UPP_PA.

Context {R : realType}.

Local Notation F_UPP := (@F_UPP R).

Record F_UPP_PA := {
  F_UPP_PA_UPP :> F_UPP;
  F_UPP_PA_JS : JS;
  _ : JS_of F_UPP_PA_UPP F_UPP_PA_JS
            ((F_UPP_T F_UPP_PA_UPP)%:nngnum + (F_UPP_d F_UPP_PA_UPP)%:num)%:nng
}.
Arguments Build_F_UPP_PA : clear implicits.

Lemma F_UPP_PA_prop (f : F_UPP_PA) :
  JS_of f (F_UPP_PA_JS f) ((F_UPP_T f)%:nngnum + (F_UPP_d f)%:num)%:nng.
Proof. by case: f. Qed.

Program Definition F_UPP_PA_of_JS (a : JS) (T : {nonneg rat}) (d : {posnum rat}) (c : rat)
    (H : (last_JS a).1%:num <= T%:num + d%:num) : F_UPP_PA :=
  Build_F_UPP_PA (F_UPP_of_F (F_of_JS a) T d c) a _.
Next Obligation.
move=> a T d c ?.
rewrite (@JS_of_eq _ _ _ (F_of_JS a)); first exact: F_of_JS_correct.
{ exact: JS_sorted. }
by move=> t /andP[_]; rewrite /= /F_UPP_of_F_def rmorphD /= => ->.
Qed.

Lemma eq_point_UPP (f : F_UPP) (x : {nonneg rat}) (y : \bar rat) :
    F_UPP_T f < x -> eq_point f x y ->
  eq_point f (x%:nngnum + (F_UPP_d f)%:num)%:nng (y + (F_UPP_c f)%:E)%dE.
Proof.
move=> Hx.
rewrite /eq_point er_map_ratr_add => /eqP<-.
have -> : (ratr ((x%:num + (F_UPP_d f)%:num)%:nng)%:num)%:nng
  = ((ratr x%:num)%:nng%:num + ratr (F_UPP_d f)%:num)%:nng :> {nonneg R}.
{ by apply: val_inj; rewrite /= rmorphD. }
by rewrite F_UPP_prop // ltr_rat.
Qed.

Lemma eq_point_UPP_n (f : F_UPP) (x : {nonneg rat}) (y : \bar rat) (n : nat) :
    F_UPP_T f < x -> eq_point f x y ->
  eq_point
    f (x%:num + n%:R * (F_UPP_d f)%:num)%:nng
    (y + (n%:R * F_UPP_c f)%R%:E)%dE.
Proof.
move=> HT Hfx.
elim: n => [|n IH].
{ rewrite (_ : (y + _)%dE = y).
  { rewrite (_ : (_ + _)%:nng = x) //.
    by apply: val_inj; rewrite /= mul0r addr0. }
  case: y {Hfx} => [y|//|//].
  by rewrite mul0r dadde0. }
rewrite (_ : _ + _ = (y + (n%:R * F_UPP_c f)%R%:E) + (F_UPP_c f)%:E)%dE.
2:{ by rewrite -addn1 natrD mulrDl mul1r dEFinD daddeA. }
rewrite (_ : (x%:num + _)%:nng
             = (x%:num + n%:R * (F_UPP_d f)%:num + (F_UPP_d f)%:num)%:nng).
2:{ by apply: val_inj; rewrite /= -addn1 natrD mulrDl mul1r addrA. }
apply: eq_point_UPP.
{ by apply: lt_le_trans HT _; rewrite -num_le lerDl. }
by rewrite /eq_point -(eqP IH); apply/eqP; congr (f _); apply: val_inj.
Qed.

Lemma affine_on_UPP (f : F_UPP) r s (x : rat) b (y : {nonneg rat}) :
    (F_UPP_T f)%:num <= x -> affine_on f (r, s) x (BSide b (ratr y%:num)) ->
  affine_on
    f (r, (s + (F_UPP_c f)%:E)%dE)
    (x + (F_UPP_d f)%:num) (BSide b (ratr (y%:num + (F_UPP_d f)%:num))).
Proof.
move=> xgeT /asboolP affxy; apply/asboolP => t tgtxd tleyd /=.
have Pt' : 0 <= t%:num - ratr (F_UPP_d f)%:num.
  rewrite subr_ge0 (le_trans _ (ltW tgtxd))// rmorphD lerDr ler0q.
  exact: le_trans xgeT.
pose t' := NngNum Pt'.
have -> : t = (t'%:num + ratr (F_UPP_d f)%:num)%:nng.
  by apply: val_inj; rewrite /t'/= subrK.
have t'gtT : ratr (F_UPP_T f)%:num < t'%:num.
  by rewrite /= ltrBrDr (le_lt_trans _ tgtxd)// rmorphD lerD2r ler_rat.
rewrite F_UPP_prop// [x + _]addrC rmorphD addrKA er_map_ratr_add daddeA affxy//.
  by rewrite /= ltrBrDr -rmorphD.
by case: b affxy tleyd => _; rewrite !bnd_simp rmorphD/= ?ltrBlDr ?lerBlDr.
Qed.

Lemma affine_on_UPP_n (f : F_UPP) rs (x : rat) b (y : {nonneg rat}) n :
    (F_UPP_T f)%:num <= x -> affine_on f rs x (BSide b (ratr y%:num)) ->
  affine_on
    f (rs.1, (rs.2 + (n%:R * F_UPP_c f)%R%:E)%dE)
    (x + n%:R * (F_UPP_d f)%:num)
    (BSide b (ratr (y%:num + (n%:R * (F_UPP_d f)%:num)))).
Proof.
move=> xgeT affxy.
elim: n => [|n IHn]; first by rewrite !mul0r !addr0.
rewrite mulrSr ![(_ + 1) * _]mulrDl !mul1r dEFinD !addrA.
rewrite -[y%:num + _]/(!!(y%:num + n%:R * (F_UPP_d f)%:num)%:nng%:num).
by rewrite affine_on_UPP// (le_trans xgeT)// lerDl mulr_ge0.
Qed.

Definition shift_elem (xyrs : JS_elem) (d : {posnum rat}) c n : JS_elem :=
  let x := (xyrs.1%:nngnum + n%:R * d%:num)%:nng in
  let y := (xyrs.2.1 + (n%:R * c)%R%:E)%dE in
  let s := (xyrs.2.2.2 + (n%:R * c)%R%:E)%dE in
  (x, (y, (xyrs.2.2.1, s))).

Definition shift_period_subdef a d c n := [seq shift_elem x d c n | x <- a].

Program Definition shift_period (a : preJS) d c n :=
  Build_preJST (shift_period_subdef a d c n) _ _.
Next Obligation. by move=> -[[]]. Qed.
Next Obligation.
case=> [+ /= _ +] d c n; elim=> [//|[x [y [r s]]] a IHa] /=.
rewrite !(path_sortedE lt_fst_num_trans) => /andP[alla sa].
by rewrite IHa// all_map (sub_all _ alla)// => -[x' yrs']; rewrite /= ltrD2r.
Qed.

Lemma shift_period_head (a : preJS) (f : F_UPP) n :
  (head_JS (shift_period a (F_UPP_d f) (F_UPP_c f) n)).1
  = ((head_JS a).1%:num + n%:R * (F_UPP_d f)%:num)%:nng.
Proof. by case: a => -[]. Qed.

Lemma eq_segment_UPP_n (f : F_UPP) xyrs b (y : {nonneg rat}) n :
    (F_UPP_T f)%:num < xyrs.1%:num ->
    eq_segment f xyrs (BSide b (ratr y%:num)) ->
  eq_segment f (shift_elem xyrs (F_UPP_d f) (F_UPP_c f) n)
    (BSide b (ratr (y%:num + (n%:R * (F_UPP_d f)%:num))%:nng%:num)).
Proof.
move=> xgtT /andP[epf afff].
by rewrite /eq_segment eq_point_UPP_n//= affine_on_UPP_n// ltW.
Qed.

Lemma shift_period_spec (a : preJS) (f : F_UPP) l n l' :
    l'%:num = l%:num + n%:R * (F_UPP_d f)%:num ->
    (F_UPP_T f)%:num < (head_JS a).1%:num -> JS_of f a l ->
  JS_of f (shift_period a (F_UPP_d f) (F_UPP_c f) n) l'.
Proof.
case: a => [[//|xyrs a] /= _] + l'lnd.
have -> : l' = (l%:num + n%:R * (F_UPP_d f)%:num)%:nng by exact: val_inj.
rewrite (path_sortedE lt_fst_num_trans) => /andP[Hall Hs] HT /andP[Hxl H].
apply/andP; split.
{ have /=/andP[Hx Hl] := Hxl => {Hall Hs Hxl H}; rewrite lerD2r Hx /=.
  by elim: a Hl => [//|h a IHa] /= /andP[Hh Hl]; rewrite lerD2r Hh /= IHa. }
move=> {Hxl}.
elim: a xyrs HT Hs Hall H => [|xyrs' a IHa] xyrs HT Hs.
  by move=> _ /=; apply: eq_segment_UPP_n.
move=> /=/andP[xltx' alla] /andP[eqsf JSf].
rewrite [X in X && _]eq_segment_UPP_n//=.
rewrite IHa ?(lt_trans _ xltx') ?(path_sorted Hs)//.
by move: Hs; rewrite /= (path_sortedE lt_fst_num_trans) => /andP[].
Qed.
Arguments shift_period_spec [a f] l n l'.

Let Td (f : F_UPP) : {nonneg rat} := ((F_UPP_T f)%:num + (F_UPP_d f)%:num)%:nng.
Let Tdd (f : F_UPP) : {nonneg rat} := ((Td f)%:num + (F_UPP_d f)%:num)%:nng.

Definition further_period_subdef (f : F_UPP_PA) :=
  let: suf := JS_above (F_UPP_PA_JS f) (F_UPP_T f) in
  let: y := let: l := last_JS suf in (shift_JS_elem l (Td f)).2.1 in
  let: suf' := shift_period suf (F_UPP_d f) (F_UPP_c f) 1 in
  let: (_, (_, rs)) := head_JS suf' in
  (Td f, (y, rs)) :: behead suf'.

Program Definition further_period f :=
  Build_preJST (further_period_subdef f) _ _.
Next Obligation.
by rewrite /further_period_subdef => f; case: head_JS => _ [].
Qed.
Next Obligation.
rewrite /further_period_subdef => f; case: head_JS => _ [_ rs].
set s : preJS := shift_period _ _ _ _.
have: (head_JS s).1 = Td f.
  by apply: val_inj; rewrite shift_period_head JS_above_head /= mul1r.
case: s => -[//|[_ yrs] s] /= _ /[swap] ->.
rewrite !(path_sortedE lt_fst_num_trans) => /andP[+ -> /[!andbT]].
by apply: sub_all => -[].
Qed.

Lemma further_period_head f : (head_JS (further_period f)).1%:num = (Td f)%:num.
Proof.
rewrite /further_period/further_period_subdef/=.
by case: (head_JS (shift_period_subdef _ _ _ _)) => _ [].
Qed.

Lemma further_period_spec (f : F_UPP_PA) : JS_of f (further_period f) (Tdd f).
Proof.
rewrite /further_period/further_period_subdef; set suf := JS_above _ _.
have: JS_of f suf (Td f) by rewrite JS_above_correct ?lerDl// F_UPP_PA_prop.
have: (head_JS suf).1 = F_UPP_T f by exact: JS_above_head.
rewrite /preJST_seq; case: suf => -[//|[_ [y rs]] a] /= _ /[swap]-> pa.
move=> /andP[/=/andP[xleTd alla] JSa].
apply/andP; split=> /=; first (apply/andP; split); first by rewrite lerDl.
  by rewrite all_map (sub_all _ alla)// => xyrs'/=; rewrite mul1r lerD2r.
have last_le_Td : ((last (F_UPP_T f, (y, rs)) a).1)%:num <= (Td f)%:num.
  case: a alla {pa JSa} => [|xyrs' a] alla; first by rewrite lerDl.
  by rewrite -nth_last (all_nthP _ alla).
set e := shift_JS_elem (last (F_UPP_T f, (y, rs)) a) (Td f).
have epx : eq_point f (Td f) e.2.1; last rewrite {}/e in epx *.
  suff: eq_segment_cc f e (Td f).
    by move=> /andP[+ _]; rewrite shift_JS_elem_correct.
  by apply: eq_segment_ccWl; [rewrite last_le_Td/=|move/JS_of_recP: JSa => []].
case: a pa alla epx JSa {last_le_Td} => [|xyrs' a] pa alla /= epx JSa.
  apply/andP; split=> [//|/=]; move/andP: JSa => [_]/=; rewrite mul1r => ?.
  rewrite -[ratr _]/(!!(ratr ((Td f)%:num + (F_UPP_d f)%:num) : R)).
  exact: affine_on_UPP.
apply/andP; split; first (apply/andP; split=> [//|/=]).
  by move/andP: JSa => [/andP[_ +] _]; rewrite !mul1r; apply: affine_on_UPP.
have Tltx' : F_UPP_T f < (head_JS (xyrs' :: a)).1 by move/andP: pa => [].
have JSx'a : JS_of f (xyrs' :: a) (Td f).
  by apply/andP; split=> //; move/andP: JSa => [].
pose x'a := Build_preJST (xyrs' :: a) erefl (path_sorted pa).
pose l' := !!((Td f)%:num + 1 * (F_UPP_d f)%:num)%:nng.
have el' : l'%:num = (Td f)%:num + 1 * (F_UPP_d f)%:num by [].
have /=/andP[_] := @shift_period_spec x'a f (Td f) 1 l' el' Tltx' JSx'a.
by rewrite (_ : l' = Tdd f)//; apply: val_inj => /=; rewrite mul1r.
Qed.

Definition F_UPP_PA_JS_upto_subdef (f : F_UPP_PA) (l : {nonneg rat}) :=
  let fp := further_period f in
  let sp a n := shift_period a (F_UPP_d f) (F_UPP_c f) n in
  let fix aux (n n' : nat) :=
    match n with
    | 0 => sp fp n'
    | n.+1 => preJS_cat (sp fp n') (aux n n'.+1)
    end in
  let n := absz (ceilq ((l%:num - (Td f)%:num) / (F_UPP_d f)%:num)) in
  if n isn't n'.+1 then F_UPP_PA_JS f : preJS else
    JS_cat (F_UPP_PA_JS f) (aux n' 0%N).

Program Definition F_UPP_PA_JS_upto_subdef' (f : F_UPP_PA) (l : {nonneg rat}) :=
  JS_below (Build_JS (F_UPP_PA_JS_upto_subdef f l) _) l.
Next Obligation.
move=> f l; rewrite /F_UPP_PA_JS_upto_subdef; case: absz => [|n].
  by rewrite JS_head.
rewrite JS_cat_head JS_head/=; apply/eqP/min_idPl; elim: n 0%N => [|n IHn] n'.
  by rewrite shift_period_head/= further_period_head.
rewrite JS_cat_head shift_period_head/= further_period_head.
by rewrite le_min IHn andbT.
Qed.

Lemma F_UPP_PA_JS_upto_subdef'_spec (f : F_UPP_PA) l :
  JS_of f (F_UPP_PA_JS_upto_subdef' f l) l.
Proof.
pose n := `|ceilq ((l%:num - (Td f)%:num) / (F_UPP_d f)%:num)|%N.
pose l' := !!((Td f)%:num + n%:R * (F_UPP_d f)%:num)%:nng.
have ll' : l%:num <= l'%:num.
  have [lTd|lTd] := leP l%:num (Td f)%:num.
    by rewrite (le_trans lTd)// lerDl.
  rewrite /l' -lerBlDl/= -ler_pdivrMr// pmulrn gez0_abs ?le_ceilq//.
  rewrite -(ler_int rat) mulr0z (le_trans _ (le_ceilq _))//.
  by rewrite divr_ge0// subr_ge0 ltW.
apply: JS_below_correct ll' _.
rewrite /F_UPP_PA_JS_upto_subdef/= -/n {}/l'.
case: n => [|n].
  set Td' := _%:nng; have -> : Td' = Td f; last exact: F_UPP_PA_prop.
  by apply/val_inj; rewrite /= mul0r addr0.
set fi := fix aux n _ := match n with 0%N => _ | _ => _ end.
have headn'' n' n'' : (head_JS (fi n' n'')).1%:num
                      = (((Td f)%:num + n''%:R * (F_UPP_d f)%:num)%:nng)%:num.
  elim: {1 3}n' {1 3 4 5}n'' (erefl (n' + n'')%N) => [|m' IHm'] m''.
    by rewrite add0n => ->; rewrite shift_period_head/= further_period_head.
  rewrite JS_cat_head shift_period_head/= further_period_head.
  by rewrite addSn -addnS =>/IHm'->; rewrite min_l//= -natr1 mulrDl addrA lerDl.
apply: (JS_cat_correct (Td f)); [|exact: F_UPP_PA_prop|].
  by rewrite headn''/= mul0r addr0.
elim: {1 3}n 0%N (addn0 n) => [n''|n' IHn' n'' n'n''].
  rewrite add0n => ->; apply: (shift_period_spec (Tdd f)).
  - by rewrite /= -nat1r mulrDl mul1r addrA.
  - by rewrite further_period_head ltrDl.
  - exact: further_period_spec.
apply: (JS_cat_correct ((Tdd f)%:num + n''%:R * (F_UPP_d f)%:num)%:nng).
- by rewrite headn''/= -nat1r mulrDl mul1r addrA.
- apply: (shift_period_spec (Tdd f)) => //; last exact: further_period_spec.
  by rewrite further_period_head ltrDl.
- by rewrite IHn'// addnS -addSn.
Qed.

Definition is_ultimately_affine (f : F_UPP_PA) : bool :=
  let: (x, (_, (r, _))) := last_JS (F_UPP_PA_JS f) in
  (x <= F_UPP_T f) && ((F_UPP_d f)%:num * r == F_UPP_c f).

Lemma is_ultimately_affine_last f : is_ultimately_affine f ->
  (last_JS (F_UPP_PA_JS f)).1%:num <= (F_UPP_T f)%:num.
Proof.
rewrite /is_ultimately_affine.
by case: last_JS => x [_ [_ _]] /andP[+ _]/=; rewrite num_le.
Qed.

Lemma is_ultimately_affine_correct_subproof f :
  is_ultimately_affine f ->
  let: (x, (_, (r, s))) := last_JS (F_UPP_PA_JS f) in
  forall t : {nonneg R}, ratr (F_UPP_T f)%:nngnum < t%:nngnum ->
    (f t = (ratr r * (t%:num - ratr x%:num))%:E + er_map ratr s)%dE.
Proof.
rewrite /is_ultimately_affine.
have: eq_segment_cc f (last_JS (F_UPP_PA_JS f)) (Td f).
  by have /JS_ofP[_ [_]] := F_UPP_PA_prop f; apply.
case: last_JS => x [y [r s]] /andP[_ /asboolP/= fxTd] /andP[xT /eqP drc] t Tt.
have [k /andP[Hk Hk']] := F_UPP_min_aux (F_UPP_d f) Tt.
pose tmkd := !!(NngNum (le_trans (ge0 _) (ltW Hk))).
have -> : t = (tmkd%:num + ratr (k%:R * (F_UPP_d f)%:num))%:nng.
  by apply: val_inj; rewrite /= subrK.
rewrite (UPP_extension_prop _ (le_refl (F_UPP_T f)))//.
rewrite -drc [_ * r]mulrC mulrCA rmorphM/= [LHS]addrC.
rewrite [_ + ratr _]addrC -addrA mulrDr dEFinD -addrA.
by rewrite fxTd// (le_lt_trans _ Hk)// ler_rat.
Qed.

Lemma is_ultimately_affine_correct f l :
    is_ultimately_affine f -> (F_UPP_T f)%:num <= l%:num ->
  JS_of f (F_UPP_PA_JS f) l.
Proof.
move=> aff Tl; have /JS_ofP[alla [ia la]] := F_UPP_PA_prop f.
apply/JS_ofP; split; [|split=> [//|]].
  apply/(all_nthP default_JS) => i isz; apply: le_trans Tl.
  apply: le_trans (is_ultimately_affine_last aff); rewrite -nth_last.
  apply: (sorted_leq_nth _ _ _ (JS_sorted_le _)) => //.
  - by rewrite inE ltn_predL size_JS_gt0.
  - by rewrite -ltnS prednK// size_JS_gt0.
move/la => /andP[epf /asboolP afff]; apply/andP; split=> [//|].
apply/asboolP => t tlast tl.
have [tT|tT] := leP t%:num (ratr (F_UPP_T f)%:num).
  by rewrite afff// bnd_simp (le_trans tT)// ler_rat lerDl.
have := is_ultimately_affine_correct_subproof aff.
by case: last_JS => x [y [r s]]; apply.
Qed.

Definition F_UPP_PA_JS_upto (f : F_UPP_PA) (l : {nonneg rat}) :=
  if is_ultimately_affine f && ((F_UPP_T f)%:num <= l%:num) then F_UPP_PA_JS f
  else F_UPP_PA_JS_upto_subdef' f l.

Lemma F_UPP_PA_JS_upto_spec (f : F_UPP_PA) l : JS_of f (F_UPP_PA_JS_upto f l) l.
Proof.
rewrite /F_UPP_PA_JS_upto.
case: ifP => [/andP[]|_]; first exact: is_ultimately_affine_correct.
exact: F_UPP_PA_JS_upto_subdef'_spec.
Qed.

Program Definition F_UPP_PA_opp (f : F_UPP_PA) : F_UPP_PA :=
  Build_F_UPP_PA (F_UPP_opp f) (Build_JS (JS_opp (F_UPP_PA_JS f)) _) _.
Next Obligation. by move=> -[_ [[[//|h t]/= _ _] ?]] _. Qed.
Next Obligation. move=> f; exact/JS_opp_correct/F_UPP_PA_prop. Qed.

Lemma F_UPP_PA_opp_correct f : F_UPP_PA_opp f = (fun x => - (f x))%E :> F.
Proof. by []. Qed.

Program Definition F_UPP_PA_add_cst c (f : F_UPP_PA) : F_UPP_PA :=
  Build_F_UPP_PA
    (F_UPP_add_cst (er_map ratr c) f)
    (JS_add_cst c (F_UPP_PA_JS f))
    _.
Next Obligation. move=> c f; exact/JS_add_cst_correct/F_UPP_PA_prop. Qed.

Lemma F_UPP_PA_add_cst_correct c f :
  F_UPP_PA_add_cst c f = (f : F) + (fun=> er_map ratr c) :> F.
Proof. by []. Qed.

Program Definition F_UPP_PA_add (f f' : F_UPP_PA) : F_UPP_PA :=
  let l :=
      @NngNum _ ((Order.max (F_UPP_T f)%:num (F_UPP_T f')%:num)
                 + (lcm_pos_rat (F_UPP_d f) (F_UPP_d f'))%:num) _ in
  Build_F_UPP_PA
    (F_UPP_add f f')
    (JS_add (F_UPP_PA_JS_upto f l) (F_UPP_PA_JS_upto f' l))
    _.
Next Obligation. by move=> f f'; apply: addr_ge0. Qed.
Next Obligation.
move=> f f'; apply: JS_add_correct;
  (set x := NngNum _; set y := _%:nng; have -> : x = y by exact: val_inj);
  exact: F_UPP_PA_JS_upto_spec.
Qed.

Lemma F_UPP_PA_add_correct f f' : F_UPP_PA_add f f' = (f : F) + f' :> F.
Proof. by []. Qed.

Let T := @F_UPP_T R.
Let d := @F_UPP_d R.
Let c := @F_UPP_c R.
Let cd f := c f / (d f)%:num.

Lemma F_UPP_PA_min'_obligation_1 (f f' : F_UPP_PA) (Hcd_eq : cd f = cd f') :
  hypotheses_UPP_min f f' 0 0.
Proof. by left. Qed.

Lemma F_UPP_PA_min'_obligation_2 (f f' : F_UPP_PA) (Hcd_lt : cd f < cd f')
  M (HM : JS_max_val (JS_above (JS_add (F_UPP_PA_JS f) (JS_affine (- (cd f)) 0)) (T f)) ((T f)%:nngnum + (d f)%:num)%:nng = M%:E)
  m (Hm : JS_min_val (JS_above (JS_add (F_UPP_PA_JS f') (JS_affine (- (cd f')) 0)) (T f')) ((T f')%:nngnum + (d f')%:num)%:nng = m%:E) :
  hypotheses_UPP_min f f' M m.
Proof.
right; left.
pose ffmcd : F := (fun t => f t - (ratr (cd f) * t%:num)%:E)%dE.
pose ffmcd' : F := (fun t => f' t - (ratr (cd f') * t%:num)%:E)%dE.
move: HM Hm.
set JSfmcd := JS_add _ _.
set JSfmcd' := JS_add _ _.
move=> HM Hm.
have Hfmcd : JS_of ffmcd JSfmcd (Td f).
  apply: JS_add_correct; first exact: F_UPP_PA_prop.
  have -> : (fun t : {nonneg R} => (- (ratr (cd f) * t%:num))%:E)
            = (fun t => (ratr (- (cd f)) * t%:num + ratr 0)%:E).
    by apply/funext => t; apply: f_equal; rewrite rmorph0 addr0 rmorphN mulNr.
  exact: JS_affine_correct.
have Hfmcd' : JS_of ffmcd' JSfmcd' (Td f').
  apply: JS_add_correct; first exact: F_UPP_PA_prop.
  have -> : (fun t : {nonneg R} => (- (ratr (cd f') * t%:num))%:E)
            = (fun t => (ratr (- (cd f')) * t%:num + ratr 0)%:E).
    by apply/funext => t; apply: f_equal; rewrite rmorph0 addr0 rmorphN mulNr.
  exact: JS_affine_correct.
have Hafmcd : JS_of ffmcd (JS_above JSfmcd (T f)) (Td f).
  by apply: JS_above_correct => [|//]; rewrite lerDl.
have Hafmcd' : JS_of ffmcd' (JS_above JSfmcd' (T f')) (Td f').
  by apply: JS_above_correct => [|//]; rewrite lerDl.
split=> [//|]; split=> t /andP[Ht Ht'].
  rewrite dEFinD -lee_dsubl_addr//.
  apply: le_trans (JS_max_val_correct Hafmcd _) _; last by rewrite HM.
  by rewrite JS_above_head Ht.
rewrite dEFinD -lee_dsubr_addr//.
apply: le_trans (JS_min_val_correct Hafmcd' _); first by rewrite Hm.
by rewrite JS_above_head Ht.
Qed.

Lemma F_UPP_PA_min'_obligation_3 (f f' : F_UPP_PA) M m Hmin :
  let um := @F_UPP_min R f f' M m Hmin in
  let l := ((T um)%:num + (d um)%:num)%:nng in
  let ejs := F_UPP_PA_JS_upto f l in
  let ejs' := F_UPP_PA_JS_upto f' l in
  let jsm := JS_min ejs ejs' l%:num%:nng in
  is_true (JS_of um jsm ((T um)%:num + (d um)%:num)%:nng).
Proof.
move=> um l ejs ejs' jsm.
rewrite (_ : _%:nng = l%:num%:nng); [|by apply/val_inj => /=].
apply: JS_min_correct; (have -> : l%:num%:nng = l by apply: val_inj => /=);
  exact: F_UPP_PA_JS_upto_spec.
Qed.

Variant er_spec T (x : \bar T) :=
  | Efinite y of x = y%:E
  | Epinfty of x = +oo%E
  | Eminfty of x = -oo%E.

Lemma erP T' (x : \bar T') : er_spec x.
Proof.
case: x.
{ move=> y; exact: Efinite. }
{ exact: Epinfty. }
exact: Eminfty.
Qed.

Definition F_UPP_PA_min' (f f' : F_UPP_PA) : option F_UPP_PA :=
  let aux (f f' : F_UPP_PA) M m Hmin :=
    let um := @F_UPP_min R f f' M m Hmin in
    let l := ((T um)%:num + (d um)%:num)%:nng in
    let ejs := F_UPP_PA_JS_upto f l in
    let ejs' := F_UPP_PA_JS_upto f' l in
    let jsm := JS_min ejs ejs' l%:num%:nng in
    Some (Build_F_UPP_PA um jsm (F_UPP_PA_min'_obligation_3 Hmin)) in
  let js := F_UPP_PA_JS f in
  let js' := F_UPP_PA_JS f' in
  let Tdf := ((T f)%:nngnum + (d f)%:num)%:nng in
  let Tdf' := ((T f')%:nngnum + (d f')%:num)%:nng in
  match ltgtP (cd f) (cd f') with
  | Order.ComparelEq Hcd_eq =>
    aux f f' 0 0 (F_UPP_PA_min'_obligation_1 Hcd_eq)
  | Order.ComparelLt Hcd_lt =>
    match (erP (JS_max_val (JS_above (JS_add js (JS_affine (- (cd f)) 0)) (T f)) Tdf),
           erP (JS_min_val (JS_above (JS_add js' (JS_affine (- (cd f')) 0)) (T f')) Tdf')) with
    | (Efinite M HM, Efinite m Hm) =>
      aux f f' M m (F_UPP_PA_min'_obligation_2 Hcd_lt HM Hm)
    | _ => None
    end
  | Order.ComparelGt Hcd_gt =>
    match (erP (JS_max_val (JS_above (JS_add js' (JS_affine (- (cd f')) 0)) (T f')) Tdf'),
           erP (JS_min_val (JS_above (JS_add js (JS_affine (- (cd f)) 0)) (T f)) Tdf)) with
    | (Efinite M HM, Efinite m Hm) =>
      aux f' f M m (F_UPP_PA_min'_obligation_2 Hcd_gt HM Hm)
    | _ => None
    end
  end.

Lemma F_UPP_PA_min'_correct f f' f'' :
  F_UPP_PA_min' f f' = Some f'' -> f'' = f \min f' :> F.
Proof.
rewrite /F_UPP_PA_min'.
case: ltgtP => ?.
- case: erP => [M HM|//|//]; case: erP => [m Hm|//|//].
  by move=> /(f_equal (fun x => match x with Some x => x | None => f'' end)) <-.
- rewrite min_funC; case: erP => [M HM|//|//]; case: erP => [m Hm|//|//].
  by move=> /(f_equal (fun x => match x with Some x => x | None => f'' end)) <-.
- by move=> /(f_equal (fun x => match x with Some x => x | None => f'' end)) <-.
Qed.

Program Definition F_UPP_PA_change_d_c (f : F_UPP_PA) d c H : F_UPP_PA :=
  let a := F_UPP_PA_JS f in
  let yT := JS_eval a (T f) in
  let yTd := JS_eval a (Td f) in
  Build_F_UPP_PA
    (@UPP_change_d_c R f d c H)
    (JS_cat (JS_below a (T f)) (JS_cst_inf (T f) yT yTd))
    _.
Next Obligation.
move=> f d0 c0 H a yT yTd; apply: (JS_cat_correct (T f)) => //=.
  by rewrite (JS_below_correct _ (F_UPP_PA_prop f))// lerDl.
rewrite JS_cst_inf_correct ?lerDl ?(JS_eval_correct (F_UPP_PA_prop f)) ?lerDl//.
have <- : f (ratr (Td f)%:num)%:nng = er_map ratr yTd.
  by apply/eqP; apply: JS_eval_correct (F_UPP_PA_prop f) _.
move=> t /andP[Tt _].
have [k /andP[Hk Hk']] := F_UPP_min_aux (F_UPP_d f) Tt.
pose tmkd := !!(NngNum (le_trans (ge0 _) (ltW Hk))).
have -> : t = (tmkd%:num + ratr (k%:R * (F_UPP_d f)%:num))%:nng.
  by apply: val_inj; rewrite /= subrK.
rewrite (UPP_extension_prop _ (le_refl (F_UPP_T f)))//.
by case: H => H; rewrite !H ?Hk ?lexx ?andbT ?ltr_rat ?ltrDl.
Qed.

Definition is_infinite_on_period (f : F_UPP_PA) : bool :=
  let js := JS_above (F_UPP_PA_JS f) (T f) in
  (JS_min_val js (Td f) == +oo%E) || (JS_max_val js (Td f) == -oo%E).

Lemma is_infinite_on_period_correct f : is_infinite_on_period f ->
  ((forall t, ratr (F_UPP_T f)%:num < t%:num <= ratr (Td f)%:num ->
              f t = +oo%E)
   \/ (forall t, ratr (F_UPP_T f)%:num < t%:num <= ratr (Td f)%:num ->
                 f t = -oo%E)).
Proof.
move=> /orP[/eqP mi|/eqP ma]; [left|right]; move=> t tp; apply/eqP.
- rewrite -leye_eq -[leLHS]/(er_map ratr +oo) -mi JS_min_val_correct//.
    by rewrite JS_above_correct ?lerDl ?(F_UPP_PA_prop f).
  by rewrite JS_above_head.
- rewrite -leeNy_eq -[leRHS]/(er_map ratr -oo) -ma JS_max_val_correct//.
    by rewrite JS_above_correct ?lerDl ?(F_UPP_PA_prop f).
  by rewrite JS_above_head.
Qed.

Definition F_UPP_PA_preprocess_d_c (f f' : F_UPP_PA) : (F_UPP_PA * F_UPP_PA) :=
  match Sumbool.sumbool_of_bool (is_infinite_on_period f) with
  | left H =>
    (F_UPP_PA_change_d_c (d f') (c f') (is_infinite_on_period_correct H), f')
  | right _ =>
    match Sumbool.sumbool_of_bool (is_infinite_on_period f') with
    | left H =>
      (f, F_UPP_PA_change_d_c (d f) (c f) (is_infinite_on_period_correct H))
    | right _ => (f, f')
    end
  end.

Lemma F_UPP_PA_preprocess_d_c_correct f f' :
  (F_UPP_PA_preprocess_d_c f f').1 = f :> F.
Proof.
rewrite /F_UPP_PA_preprocess_d_c.
case: Sumbool.sumbool_of_bool => [//|H].
by case: Sumbool.sumbool_of_bool.
Qed.

Lemma F_UPP_PA_preprocess_d_c_correct' f f' :
  (F_UPP_PA_preprocess_d_c f f').2 = f' :> F.
Proof.
rewrite /F_UPP_PA_preprocess_d_c.
case: Sumbool.sumbool_of_bool => [//|H].
by case: Sumbool.sumbool_of_bool.
Qed.

Definition F_UPP_PA_min f f' :=
  let ff' := F_UPP_PA_preprocess_d_c f f' in
  F_UPP_PA_min' ff'.1 ff'.2.

Lemma F_UPP_PA_min_correct f f' f'' :
  F_UPP_PA_min f f' = Some f'' -> f'' = f \min f' :> F.
Proof.
move=> H.
rewrite -(F_UPP_PA_preprocess_d_c_correct f f').
rewrite -(F_UPP_PA_preprocess_d_c_correct' f f').
exact: F_UPP_PA_min'_correct.
Qed.

Program Definition F_UPP_PA_f1 (f : F_UPP_PA) d1 c1 : F_UPP_PA :=
  let a := F_UPP_PA_JS f in
  Build_F_UPP_PA
    (F_UPP_f1 f d1 c1)
    (JS_cat (JS_below a (T f)) (JS_cst_inf (T f) (JS_eval a (T f)) +oo%E))
    _.
Next Obligation.
move=> f d1 c1 a; apply: (JS_cat_correct (T f)) => //.
  rewrite -(@JS_of_eq _ _ f) ?JS_sorted// => [|t /andP[_ tT]].
    by apply: JS_below_correct (F_UPP_PA_prop f); rewrite lerDl.
  by rewrite /F_UPP_f1/=/UPP.f1 tT.
rewrite JS_cst_inf_correct//= ?lerDl// /UPP.f1; last first.
  by move=> t /andP[Tt _]; rewrite leNgt Tt.
by rewrite -(@eq_point_eq _ f) ?(JS_eval_correct (F_UPP_PA_prop f))?lerDl ?lexx.
Qed.

Program Definition F_UPP_PA_f2 (f : F_UPP_PA) : F_UPP_PA :=
  Build_F_UPP_PA
    (F_UPP_f2 f)
    (JS_inf_left_eq (JS_above (F_UPP_PA_JS f) (T f)))
    _.
Next Obligation.
move=> f; apply: (@JS_inf_left_eq_correct _ f) => [t|t|];
  do ?[by rewrite JS_above_head /F_UPP_f2/=/UPP.f2 ltNge => ->].
by rewrite JS_above_correct// ?lerDl// F_UPP_PA_prop.
Qed.

Program Definition F_UPP_PA_f1_f1' (f f' : F_UPP_PA) d1 c1 :=
  let umc := @F_UPP_conv_f1_f1' R f f' d1 c1 in
  let l := ((T f)%:num + (T f')%:num + d1%:num)%:nng in
  let ejs := F_UPP_PA_JS_upto (F_UPP_PA_f1 f d1 c1) l in
  let ejs' := F_UPP_PA_JS_upto (F_UPP_PA_f1 f' d1 c1) l in
  let jsmc := JS_min_conv ejs ejs' l in
  Build_F_UPP_PA umc jsmc _.
Next Obligation.
move=> f f' d1 c1 umc l /=; rewrite (_ : _%:nng = l); last by apply: val_inj.
by apply: JS_min_conv_correct; apply: F_UPP_PA_JS_upto_spec.
Qed.

Lemma F_UPP_PA_f1_f1'_correct f f' d1 c1 :
  F_UPP_PA_f1_f1' f f' d1 c1 = (UPP.f1 f : Fd) * UPP.f1 f' :> F.
Proof. by []. Qed.

Program Definition F_UPP_PA_f1_f2' (f f' : F_UPP_PA) :=
  let umc := @F_UPP_conv_f1_f2' R f f' in
  let l := ((T f)%:num + (T f')%:num + (d f')%:num)%:nng in
  let ejs := F_UPP_PA_JS_upto (F_UPP_PA_f1 f (d f') (c f')) l in
  let ejs' := F_UPP_PA_JS_upto (F_UPP_PA_f2 f') l in
  let jsmc := JS_min_conv ejs ejs' l in
  Build_F_UPP_PA umc jsmc _.
Next Obligation.
move=> f f' umc l /=; rewrite (_ : _%:nng = l); last by apply: val_inj.
by apply: JS_min_conv_correct; apply: F_UPP_PA_JS_upto_spec.
Qed.

Lemma F_UPP_PA_f1_f2'_correct f f' :
  F_UPP_PA_f1_f2' f f' = (UPP.f1 f : Fd) * UPP.f2 f' :> F.
Proof. by []. Qed.

Program Definition F_UPP_PA_f2_f2' (f f' : F_UPP_PA) :=
  let umc := @F_UPP_conv_f2_f2' R f f' in
  let lcm := lcm_pos_rat (d f) (d f') in
  let l := ((T f)%:num + (T f')%:num + lcm%:num + lcm%:num)%:nng in
  let ejs := F_UPP_PA_JS_upto (F_UPP_PA_f2 f) l in
  let ejs' := F_UPP_PA_JS_upto (F_UPP_PA_f2 f') l in
  let jsmc := JS_min_conv ejs ejs' l in
  Build_F_UPP_PA umc jsmc _.
Next Obligation.
move=> f f' umc lcm l /=; rewrite (_ : _%:nng = l); last by apply: val_inj.
by apply: JS_min_conv_correct; apply: F_UPP_PA_JS_upto_spec.
Qed.

Lemma F_UPP_PA_f2_f2'_correct f f' :
  F_UPP_PA_f2_f2' f f' = (UPP.f2 f : Fd) * UPP.f2 f' :> F.
Proof. by []. Qed.

Definition F_UPP_PA_conv' (f f' : F_UPP_PA) : option F_UPP_PA :=
  let f1f1' := F_UPP_PA_f1_f1' f f' (d f) (c f) in
  let f2f1' := F_UPP_PA_f1_f2' f' f in
  let f1f2' := F_UPP_PA_f1_f2' f f' in
  let f2f2' := F_UPP_PA_f2_f2' f f' in
  obind
    (fun r1 =>
       obind
         (fun r2 => F_UPP_PA_min r2 f2f2')
         (F_UPP_PA_min r1 f1f2'))
    (F_UPP_PA_min f1f1' f2f1').

Lemma F_UPP_PA_conv'_correct f f' f'' : F_UPP_PA_conv' f f' = Some f'' ->
  f'' = (f : Fd) * f' :> F.
Proof.
rewrite /F_UPP_PA_conv'.
case E1 : F_UPP_PA_min => [r1|//]/=.
case E2 : (F_UPP_PA_min r1) => [r2|//]/=.
case E3 : (F_UPP_PA_min r2) => [r3|//]/= [<-].
rewrite (F_UPP_PA_min_correct E3).
rewrite (F_UPP_PA_min_correct E2).
rewrite (F_UPP_PA_min_correct E1).
rewrite F_UPP_PA_f1_f1'_correct.
rewrite !F_UPP_PA_f1_f2'_correct [(UPP.f1 f' : Fd) * (UPP.f2 f)]mulrC.
rewrite F_UPP_PA_f2_f2'_correct.
by rewrite F_UPP_conv_aux.
Qed.

Definition F_UPP_PA_conv f f' :=
  let ff' := F_UPP_PA_preprocess_d_c f f' in
  F_UPP_PA_conv' ff'.1 ff'.2.

Lemma F_UPP_PA_conv_correct f f' f'' :
  F_UPP_PA_conv f f' = Some f'' -> f'' = (f : Fd) * f' :> F.
Proof.
move=> H.
rewrite -(F_UPP_PA_preprocess_d_c_correct f f').
rewrite -(F_UPP_PA_preprocess_d_c_correct' f f').
exact: F_UPP_PA_conv'_correct.
Qed.

Definition F_UPP_PA_eqb' (f f' : F_UPP_PA) : bool :=
  (cd f == cd f')
  && let l := ((Order.max (F_UPP_T f)%:num (F_UPP_T f')%:num)
               + (lcm_pos_rat (F_UPP_d f) (F_UPP_d f'))%:num)%:nng in
     JS_eqb (F_UPP_PA_JS_upto f l) (F_UPP_PA_JS_upto f' l) l.

Lemma F_UPP_PA_eqb'_correct f f' : F_UPP_PA_eqb' f f' -> f = f' :> F.
Proof.
move=> /andP[/eqP cdcd']; set l := _%:nng => /= JSe.
apply/funext/(UPP_equality cdcd'); rewrite -[ratr _]/(ratr l%:num).
by apply: JS_eqb_correct JSe; apply: F_UPP_PA_JS_upto_spec.
Qed.

Definition F_UPP_PA_eqb f f' :=
  let ff' := F_UPP_PA_preprocess_d_c f f' in
  F_UPP_PA_eqb' ff'.1 ff'.2.

Lemma F_UPP_PA_eqb_correct f f' : F_UPP_PA_eqb f f' -> f = f' :> F.
Proof.
move=> H.
rewrite -(F_UPP_PA_preprocess_d_c_correct f f').
rewrite -(F_UPP_PA_preprocess_d_c_correct' f f').
exact: F_UPP_PA_eqb'_correct.
Qed.

Definition F_UPP_PA_leb f f' :=
  match F_UPP_PA_min f f' with
  | None => false
  | Some f'' => F_UPP_PA_eqb f f''
  end.

Lemma F_UPP_PA_leb_correct f f' : F_UPP_PA_leb f f' -> (f <= f' :> F)%O.
Proof.
rewrite /F_UPP_PA_leb; case E: F_UPP_PA_min => [f''|//] /F_UPP_PA_eqb_correct.
by move: E => /F_UPP_PA_min_correct -> ->; rewrite min_funE leIr.
Qed.

Program Definition F_UPP_PA_0 (d' : {posnum rat}) :=
  @F_UPP_PA_of_JS JS_0 0%:nng d' 0 _.
Next Obligation. by move=> d' /=. Qed.

Lemma F_UPP_PA_0_correct d' : F_UPP_PA_0 d' = \0 :> F.
Proof.
apply/funext => t; rewrite /F_UPP_PA_0 /=.
rewrite /F_UPP_of_F_def /= add0r mulr0 rmorph0 subr0.
case: ltP => Ht; rewrite /F_of_JS /=.
  by case: leP => _; rewrite rmorph0 ?mul0r !addr0.
by case: leP => _; rewrite rmorph0 ?mul0r ?dadde0.
Qed.

Program Definition F_UPP_PA_delta (b : rat) d c : F_UPP_PA :=
  Build_F_UPP_PA (F_UPP_delta b d c) (JS_delta b) _.
Next Obligation.
move=> b d0 c0.
rewrite JS_delta_correct//= (le_trans _ (eqbRL (lerDl _ _) _))//.
by rewrite val_insubd/=; case: (leP 0) => // /ltW.
Qed.

Lemma F_UPP_PA_delta_correct b d0 c0 :
  F_UPP_PA_delta b d0 c0 = delta (ratr b)%:E :> F.
Proof. by []. Qed.

Definition F_UPP_PA_non_decr_closure (f : F_UPP_PA) : option F_UPP_PA :=
  omap F_UPP_PA_opp (F_UPP_PA_conv (F_UPP_PA_opp f) (F_UPP_PA_0 (F_UPP_d f))).

Lemma F_UPP_PA_non_decr_closure_correct f f' :
  F_UPP_PA_non_decr_closure f = Some f' -> f' = non_decr_closure f :> F.
Proof.
rewrite /F_UPP_PA_non_decr_closure.
case E: F_UPP_PA_conv => [ | //] => -[<-]; move: E => /F_UPP_PA_conv_correct E.
rewrite !(F_UPP_PA_opp_correct, E) F_UPP_PA_0_correct.
by rewrite non_decr_closure_F_min_conv_F_0.
Qed.

Definition F_UPP_PA_non_neg (f : F_UPP_PA) : bool :=
  F_UPP_PA_leb (F_UPP_PA_0 (F_UPP_d f)) f.

Lemma F_UPP_PA_non_neg_correct f : F_UPP_PA_non_neg f -> (f : F) \is a nonnegF.
Proof.
move=> /F_UPP_PA_leb_correct/lefP fge0 /[!inE] x.
by apply: le_trans (fge0 x); rewrite F_UPP_PA_0_correct.
Qed.

Definition F_UPP_PA_non_decr (f : F_UPP_PA) : bool :=
  oapp (F_UPP_PA_leb ^~ f) false (F_UPP_PA_non_decr_closure f).

Lemma F_UPP_PA_non_decr_correct (f : F_UPP_PA) (fnneg : (f : F) \is a nonnegF) :
  F_UPP_PA_non_decr f -> Build_Fplus f fnneg \is a nondecrFplus.
Proof.
rewrite /F_UPP_PA_non_decr.
case Endc : F_UPP_PA_non_decr_closure => [ndc|//] /F_UPP_PA_leb_correct.
rewrite (F_UPP_PA_non_decr_closure_correct Endc) => /non_decr_closureP fnd.
by rewrite inE => x y /fnd.
Qed.

Definition F_UPP_PA_hDev_bounded (f f' : F_UPP_PA) (d : \bar rat) :=
  match d with +oo%E => true | -oo%E => false | d%:E =>
    [&& (0 <= d), F_UPP_PA_non_neg f, F_UPP_PA_non_decr f &
      (* f * delta d <= f' *)
      let deltad := F_UPP_PA_delta d (F_UPP_d f) (F_UPP_c f) in
      oapp (F_UPP_PA_leb ^~ f') false (F_UPP_PA_conv f deltad)]
  end.

Lemma F_UPP_PA_hDev_bounded_correct f f' b :
  F_UPP_PA_hDev_bounded f f' b -> (hDev f f')%:num <= er_map ratr b.
Proof.
case: b => [b /and4P[bge0 + + fdclef']|_|//]; last by rewrite leey.
move=> /F_UPP_PA_non_neg_correct fnn /(F_UPP_PA_non_decr_correct fnn) fnd.
rewrite -[f : F]/(Build_Fup _ fnd : F) /er_map.
rewrite -[(ratr b)%:E]/(ratr (NngNum bge0)%:num)%:E%:sgn%:num.
apply: hDev_suff; move: fdclef' => /=.
case E : F_UPP_PA_conv => [fcd|//] /F_UPP_PA_leb_correct.
by rewrite (F_UPP_PA_conv_correct E).
Qed.

Definition F_UPP_PA_never_pinfty (f : F_UPP_PA) : bool :=
  let a := F_UPP_PA_JS f in
  (JS_eval a 0%:nng != +oo%E) && (JS_max_val a (Td f) != +oo%E).

Lemma F_UPP_PA_never_pinfty_correct f :
  F_UPP_PA_never_pinfty f -> forall t, f t != +oo%E.
Proof.
move=> /andP[]; set ev := JS_eval _ _; set ma := JS_max_val _ _ => e0 mTd t.
have [tgt0||t0] := ltgtP 0 t%:num; [|by rewrite lt0F|]; last first.
  apply: contraNN e0 => /eqP ft; have Tdge0 : 0%:nng%:num <= (Td f)%:num.
    by rewrite /=.
  have := JS_eval_correct (F_UPP_PA_prop f) Tdge0 => /eqP /=.
  rewrite (_ : _%:nng = t) ?ft; last by apply: val_inj; rewrite /= rmorph0.
  by rewrite /ev; case: JS_eval.
have [Tt|Tt] := leP t%:num (ratr (T f)%:num).
  have ftma : f t <= er_map ratr ma.
    rewrite JS_max_val_correct ?F_UPP_PA_prop//= JS_head rmorph0 tgt0/=.
    by rewrite (le_trans Tt)// ler_rat lerDl.
  rewrite -ltey (le_lt_trans ftma)//.
  by case: (ma) mTd => [ma'|//|//]; rewrite ltry.
have [k /andP[Hk Hk']] := F_UPP_min_aux (F_UPP_d f) Tt.
pose tmkd := !!(NngNum (le_trans (ge0 _) (ltW Hk))).
have -> : t = (tmkd%:num + ratr (k%:R * (F_UPP_d f)%:num))%:nng.
  by apply: val_inj; rewrite /= subrK.
rewrite (UPP_extension_prop _ (le_refl (F_UPP_T f)))//.
have ftma : (f tmkd + (ratr (k%:R * c f))%:E
             <= er_map ratr ma + (ratr (k%:R * c f))%:E)%dE.
  rewrite lee_dD2r ?JS_max_val_correct ?F_UPP_PA_prop//=.
  by rewrite JS_head rmorph0 Hk' (le_lt_trans _ Hk).
rewrite -ltey (le_lt_trans ftma)//.
by case: (ma) mTd => [ma'|//|//]; rewrite -dEFinD ltry.
Qed.

Definition F_UPP_PA_never_ninfty (f : F_UPP_PA) : bool :=
  F_UPP_PA_never_pinfty (F_UPP_PA_opp f).

Lemma F_UPP_PA_never_ninfty_correct f :
  F_UPP_PA_never_ninfty f -> forall t, f t != -oo%E.
Proof.
move=> /F_UPP_PA_never_pinfty_correct + t => /(_ t) ny.
rewrite -eqe_opp/=; apply: contraNN ny => /eqP<-.
by rewrite F_UPP_PA_opp_correct.
Qed.

Definition F_UPP_PA_vDev_bounded (f f' : F_UPP_PA) (b : \bar rat) :=
  [&& F_UPP_PA_never_ninfty f, F_UPP_PA_never_pinfty f' &
   F_UPP_PA_leb f (F_UPP_PA_add_cst b f')].

Lemma F_UPP_PA_vDev_bounded_correct f f' b :
  F_UPP_PA_vDev_bounded f f' b -> (vDev f f' <= er_map ratr b)%E.
Proof.
move/and3P => [nny npy leff'b]; apply: vDev_suff.
- exact: F_UPP_PA_never_ninfty_correct.
- exact: F_UPP_PA_never_pinfty_correct.
- by rewrite (le_trans (F_UPP_PA_leb_correct leff'b)) ?F_UPP_PA_add_cst_correct.
Qed.

End UPP_PA.
