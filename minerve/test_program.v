From mathcomp Require Import all_ssreflect.
Require Import program.

Local Open Scope ereal_scope.
Local Open Scope ring_scope.
Local Open Scope order_scope.
Local Open Scope dioid_scope.

Section Test.

Context {R : realType}.

Notation F := (@F R).

Section Test1.

(* x |-> 2x *)
Let f : F := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 2;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (2, EFin 0)))] |}%bigQ.

(* x |-> min(0, 2(x-3)) *)
Let g : F := F_of_sequpp {|
  sequpp_T := 3;
  sequpp_d := 1;
  sequpp_c := 2;
  sequpp_js :=
    [:: (0, (EFin 0, (0, EFin 0)));
       (2, (EFin 0, (2, EFin 0)))] |}%bigQ.

Let delta1 : F := F_of_sequpp {|
  sequpp_T := 2;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js :=
    [:: (0, (EFin 0, (0, EFin 0)));
       (1, (EFin 0, (0, +oo)))] |}%bigQ.

Let p :=
  [:: Assign "f" (Cst f);
     Assign "g" (Cst g);
     Exists "h";
     AssertDeConvLe (Var "g") (Cst delta1) (Var "h");
     AssertHDevBounded (Var "f") (Var "h") (bigQ2rat 2%bigQ)].

Goal sem_program p.
Proof.
apply: (fill_ex_correct [::
  (* we use this particular function (typically computed by Pegase) for h *)
  F_of_sequpp {|
    sequpp_T := 1;
    sequpp_d := 1;
    sequpp_c := 2;
    sequpp_js :=
      [:: (0, (EFin 0, (0, EFin 0)));
       (1, (EFin 0, (2, EFin 0)))] |}%bigQ]) => /=.
by do ![split=> [/=|]; [nccoq|]].
Qed.

End Test1.

End Test.
