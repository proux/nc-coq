From HB Require Import structures.
From mathcomp Require Import ssreflect ssrbool ssrfun fintype ssrnat bigop.
From mathcomp Require Import seq finfun path eqtype tuple choice order.
From mathcomp Require Import ssralg ssrnum ssrint rat intdiv archimedean.
From mathcomp Require Import mathcomp_extra boolp classical_sets signed reals.
From mathcomp Require Import ereal.
From NCCoq Require Import analysis_complements RminStruct.
Require Import NCCoq.usual_functions.
Require Import ratdiv.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope classical_set_scope.
Local Open Scope ring_scope.
Local Open Scope order_scope.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.

Section UPP.

Context {R : realType}.

Local Notation F := (@F R).

Record F_UPP := {
  F_UPP_val :> F;
  F_UPP_T : {nonneg rat};
  F_UPP_d : {posnum rat};
  F_UPP_c : rat;
  _ : forall t : {nonneg R}, ratr F_UPP_T%:num < t%:num ->
        F_UPP_val (t%:num + ratr F_UPP_d%:num)%:nng
        = F_UPP_val t + (ratr F_UPP_c)%:E
}.
Arguments Build_F_UPP : clear implicits.

Lemma F_UPP_prop (f : F_UPP) :
  forall t : {nonneg R}, ratr (F_UPP_T f)%:num < t%:num ->
    f (t%:nngnum + ratr (F_UPP_d f)%:num)%R%:nng = f t + (ratr (F_UPP_c f))%:E.
Proof. by case: f. Qed.

Definition F_UPP_of_F_def (f : F) (T d c : rat) : F :=
  fun (t : {nonneg R}) =>
    if t%:num <= ratr (T + d) then f t else
      let n := (Num.ceil ((t%:num - ratr T) / ratr d) - 1)%:~R in
      let t' := insubd 0%:nng (t%:num - ratr (n * d)) in
      f t' + (ratr (n * c))%:E.

Program Definition F_UPP_of_F (f : F) (T : {nonneg rat}) (d : {posnum rat}) (c : rat) :=
  Build_F_UPP (F_UPP_of_F_def f T%:nngnum d%:num c) T d c _.
Next Obligation.
move=> f T d c t Tltt.
rewrite /F_UPP_of_F_def rmorphD /= lerD2r leNgt Tltt /= !rmorphM /= !ratr_int.
case: leP => Ht.
{ rewrite (eqP (eqbLR (ceilP _ 2) _)) ?mul1r; last first.
  { rewrite -[1]/(1%:~R) -rmorphB/= ltr_pdivlMr ?ltr0q// mul1r.
    rewrite ltrBrDl ltrD2r Tltt/= ler_pdivrMr ?ler0q//.
    by rewrite mulrDl mul1r addrAC lerD2r lerBlDl. }
  apply/f_equal2 => //; apply/f_equal.
  by rewrite addrK /insubd insubT //= => H'; apply/val_inj. }
have -> : ((t%:num + ratr d%:num) - ratr T%:num) / ratr d%:num
            = (t%:num - ratr T%:num) / ratr d%:num + 1%:~R.
{ by rewrite addrAC mulrDl mulfV. }
rewrite ceilDz addrK rmorphB/= !mulrBl !mul1r dEFinB opprB -addrA.
by rewrite -!daddeA -EFinN -dEFinD [_ + ratr c]addrC subrr addr0.
Qed.

Lemma UPP_extension_prop (f : F_UPP) (k : nat) (T' : {nonneg rat}) :
  F_UPP_T f <= T' ->
  forall t : {nonneg R}, ratr T'%:num < t%:num ->
  f (t%:num + ratr (k%:R * (F_UPP_d f)%:num))%:nng
  = f t + (ratr (k%:R * F_UPP_c f))%:E.
Proof.
move=> HTT' t Ht.
elim: k => [| k IHk].
{ rewrite (_ : (t%:nngnum + _)%:nng = t); last first.
  { by apply/val_inj; rewrite /= mul0r raddf0 addr0. }
  by rewrite mul0r raddf0 addr0. }
rewrite -addn1 [in RHS]natrD mulrDl mul1r.
rewrite [in RHS]rmorphD/= dEFinD.
rewrite -[(?[a]%:E + ?[b]%:E)%dE]/(?[a]%:E%dE + ?[b]%:E%dE).
rewrite addrA -IHk -F_UPP_prop.
{ apply/f_equal/val_inj.
  by rewrite /= natrD mulrDl mul1r rmorphD /= addrA. }
move: HTT'; rewrite -num_le -(ler_rat R) /= => HTT'; apply: (le_lt_trans HTT').
by apply: lt_le_trans Ht _; rewrite lerDl rmorphM/= mulr_ge0.
Qed.

Program Definition UPP_extension
        (f : F_UPP) (k : nat) (T' : {nonneg rat}) (HTT' : F_UPP_T f <= T') : F_UPP :=
  Build_F_UPP
    f
    T'
    (k.+1%:R * (F_UPP_d f)%:num)%:pos
    (k.+1%:R * F_UPP_c f)
    _.
Next Obligation.
move=> f k T' TleT' t T'let; rewrite -(UPP_extension_prop _ TleT') //.
by apply/f_equal/val_inj.
Qed.

Program Definition F_UPP_opp (f : F_UPP) : F_UPP :=
  Build_F_UPP (\- f)%E (F_UPP_T f) (F_UPP_d f) (- F_UPP_c f) _.
Next Obligation.
by move=> f t Tlet; rewrite /= F_UPP_prop// doppeD 1?adde_defC// -EFinN rmorphN.
Qed.

Program Definition F_UPP_add_cst c (f : F_UPP) : F_UPP :=
  Build_F_UPP ((f : F) + (fun=> c)) (F_UPP_T f) (F_UPP_d f) (F_UPP_c f) _.
Next Obligation.
by move=> c f t Tlet; rewrite /GRing.add/= F_UPP_prop// addrAC.
Qed.

Program Definition F_UPP_add (f f' : F_UPP) : F_UPP :=
  Build_F_UPP
    ((f : F) + f')
    (Order.max (F_UPP_T f)%:num (F_UPP_T f')%:num)%:nng
    (lcm_pos_rat (F_UPP_d f) (F_UPP_d f'))
    ((lcm_pos_rat (F_UPP_d f) (F_UPP_d f'))%:num
     * (F_UPP_c f / (F_UPP_d f)%:num + F_UPP_c f' / (F_UPP_d f')%:num))
    _.
Next Obligation.
move=> f f' t.
set tilde_T := (Num.max _ _)%:nng => Ht.
set tilde_d := lcm_pos_rat _ _.
rewrite /GRing.add/=.
move: (dvdq_lcml (F_UPP_d f) (F_UPP_d f')) => /= [k Hk].
move: (dvdq_lcmr (F_UPP_d f) (F_UPP_d f')) => /= [k' Hk'].
have {1}-> : (t%:nngnum + ratr tilde_d%:num)%:nng
             = (t%:nngnum + ratr (k%:R * (F_UPP_d f)%:num))%:nng.
{ by apply/val_inj; rewrite /= /tilde_d Hk. }
have {1}-> : (t%:nngnum + ratr tilde_d%:num)%:nng
             = (t%:nngnum + ratr (k'%:R * (F_UPP_d f')%:num))%:nng.
{ by apply/val_inj; rewrite /= /tilde_d Hk'. }
rewrite (@UPP_extension_prop f k tilde_T) => //; last first.
{ by rewrite -num_le /= le_max lexx. }
rewrite (@UPP_extension_prop f' k' tilde_T) => //; last first.
{ by rewrite -num_le le_max lexx orbC. }
rewrite addrACA -dEFinD mulrDr {1}Hk Hk'.
rewrite mulrAC -mulrA divrK; [|exact: neq0].
rewrite mulrAC -mulrA divrK; [|exact: neq0].
by rewrite rmorphD /= [f t + _]addrC.
Qed.

Lemma F_UPP_min_aux (t : R) (T : {nonneg rat}) (d : {posnum rat}) :
  ratr T%:nngnum < t ->
  exists k : nat,
    ratr T%:nngnum < t - ratr (k%:R * d%:num) <= ratr (T%:nngnum + d%:num).
Proof.
move=> Ht.
pose zk := Num.ceil ((t - ratr T%:nngnum) / ratr d%:num) - 1.
exists (absz zk).
rewrite rmorphD rmorphM /= ltrBrDr lerBlDr -addrA.
rewrite -ltrBrDl -lerBlDl.
rewrite -{2}(mul1r (ratr d%:num)) -mulrDl (addrC 1).
rewrite -ltr_pdivlMr ?ltr0q// -ler_pdivrMr ?ltr0q//.
rewrite -[?[a]%:R]/((Posz ?[a])%:~R) gez0_abs; last first.
{ rewrite subr_ge0 -gtz0_ge1 -ceil_gt_int.
  by apply: divr_gt0; rewrite // subr_gt0. }
rewrite ratr_int rmorphB/= subrK /Num.ceil.
rewrite rmorphN/= -opprD ltrNl lerNr andbC -RfloorE.
apply: mem_rg1_Rfloor.
Qed.

Let T := F_UPP_T.
Let d := F_UPP_d.
Let c := F_UPP_c.
Let cd f := c f / (d f)%:num.

Definition hypotheses_eq_UPP_min f f' := cd f = cd f'.

Definition hypotheses_lt_UPP_min f f' M m :=
  cd f < cd f'
  /\ (forall t : {nonneg R},
        ratr (T f)%:num < t%:num <= ratr ((T f)%:num + (d f)%:num) ->
        f t <= (ratr M + ratr (cd f) * t%:num)%R%:E)
  /\ (forall t : {nonneg R},
        ratr (T f')%:num < t%:num <= ratr ((T f')%:num + (d f')%:num) ->
        (ratr m + ratr (cd f') * t%:num)%R%:E <= f' t).

Definition hypotheses_UPP_min f f' M m :=
  hypotheses_eq_UPP_min f f'
  \/ hypotheses_lt_UPP_min f f' M m \/ hypotheses_lt_UPP_min f' f M m.

Definition tilde_T_min f f' M m :=
  (Order.max
     (Order.max (T f)%:nngnum (T f')%:nngnum)
     (if cd f == cd f' then 0%:nng
      else if cd f < cd f' then
        ((insubd 0%:nng (M - m))%:nngnum / (insubd 0%:nng (cd f' - cd f))%:nngnum)%:nng
      else (* cd f' < cd f *)
        ((insubd 0%:nng (M - m))%:nngnum / (insubd 0%:nng (cd f - cd f'))%:nngnum)%:nng)%:nngnum)%:nng.

Definition tilde_d_min f f' :=
  if cd f == cd f' then lcm_pos_rat (d f) (d f')
  else if cd f < cd f' then d f else d f'.

Definition tilde_c_min f f' :=
  if cd f == cd f' then (lcm_pos_rat (d f) (d f'))%:num * cd f
  else if cd f < cd f' then c f else c f'.

Lemma tilde_T_minC f f' M m : tilde_T_min f f' M m = tilde_T_min f' f M m.
Proof.
rewrite /tilde_T_min eq_sym.
apply/val_inj => /=; congr Num.max; first by rewrite maxC.
by case: eqP => [//|] /eqP Hcd; rewrite lt_def Hcd /= leNgt; case: ltP.
Qed.

Lemma tilde_d_minC : commutative tilde_d_min.
Proof.
move=> f f'; rewrite /tilde_d_min lcm_pos_ratC eq_sym.
by case: eqP => [//|] /eqP Hcd; rewrite lt_def Hcd /= leNgt; case: ltP.
Qed.

Lemma tilde_c_minC : commutative tilde_c_min.
Proof.
move=> f f'; rewrite /tilde_c_min lcm_pos_ratC eq_sym.
by case: eqP => [-> //|] /eqP Hcd; rewrite lt_def Hcd /= leNgt; case: ltP.
Qed.

Program Definition F_UPP_min (f f' : F_UPP) (M m : rat) :
    hypotheses_UPP_min f f' M m -> F_UPP :=
  fun _ =>
    Build_F_UPP
      (f \min f')
      (tilde_T_min f f' M m)
      (tilde_d_min f f')
      (tilde_c_min f f') _.
Next Obligation.
suff : !!(forall f f' M m,
    (cd f = cd f' \/ hypotheses_lt_UPP_min f f' M m) ->
    forall t : {nonneg R}, ratr (tilde_T_min f f' M m)%:nngnum < t%:nngnum ->
               (f \min f') (t%:nngnum + ratr (tilde_d_min f f')%:num)%:nng
               = ((f \min f') t + (ratr (tilde_c_min f f'))%:E)%dE).
{ move=> H f f' M m [|[]] H' t Ht.
  { by apply: (H f f' M m) => [|//]; left. }
  { by apply: (H f f' M m) => [|//]; right. }
  rewrite tilde_d_minC tilde_c_minC -F_dioid_addE addrC.
  by apply: (H f' f M m); [right|rewrite tilde_T_minC]. }
move=> f f' M m [/eqP|] Hmin t.
{ rewrite /tilde_T_min /tilde_d_min /tilde_c_min Hmin /=.
  rewrite maxEge [0 <= _]ge0 => Ht.
  rewrite -!Rbar_dioidE mulrDl !Rbar_dioidE; congr mine.
  { have [k Hk] := dvdq_lcml (d f) (d f').
    rewrite (_ : (_ + _)%:nng = (t%:nngnum + ratr (k%:R * (d f)%:num))%:nng).
    2:{ by apply/val_inj; rewrite /= -Hk. }
    rewrite (@UPP_extension_prop _ _ (Order.max (T f)%:nngnum (T f')%:nngnum)%:nng) => [| |//].
    2:{ by rewrite -num_le /= le_max lexx. }
    rewrite Hk -mulrA /cd (mulrC (c f)) (mulrA (d f)%:num) mulfV ?neq0//.
    by rewrite mul1r rmorphM /= ratr_nat. }
  have [k' Hk'] := dvdq_lcmr (d f) (d f').
  rewrite (_ : (_ + _)%:nng = (t%:nngnum + ratr (k'%:R * (d f')%:num))%:nng).
  2:{ by apply/val_inj; rewrite /= -Hk'. }
  rewrite (@UPP_extension_prop _ _ (Order.max (T f)%:nngnum (T f')%:nngnum)%:nng) => [| |//].
  2:{ by rewrite -num_le /= le_max orbC lexx. }
  move: Hmin => /eqP Hmin.
  rewrite Hk' Hmin.
  rewrite -mulrA /cd (mulrC (c f')) (mulrA (d f')%:num) mulfV ?neq0//.
  by rewrite mul1r rmorphM /= ratr_nat. }
move=> H't.
move: Hmin (H't) => -[Hcd [HM Hm]].
have Hcd' : cd f == cd f' = false.
{ by apply/negbTE; move: Hcd; rewrite eq_sym lt_def => /andP []. }
rewrite /tilde_T_min /tilde_d_min /tilde_c_min Hcd' Hcd => Ht.
have {}HM : forall t : {nonneg R}, ratr (T f)%:nngnum < t%:nngnum ->
                       (f t <= (ratr M + ratr (cd f) * t%:nngnum)%R%:E)%dE.
{ move=> t' Ht'.
  have PTf : 0 <= ratr (T f)%:nngnum :> R by rewrite ler0q.
  have [k /andP[Hk Hk']] := F_UPP_min_aux (d f) Ht'.
  pose t'mkdf := NngNum (ltW (le_lt_trans PTf Hk)).
  have -> : t' = (t'mkdf%:nngnum + ratr (k%:R * (d f)%:num))%:nng.
  { by apply: val_inj; rewrite /= subrK. }
  rewrite (UPP_extension_prop _ (le_refl (T f))) //.
  rewrite mulrDr /= addrA dEFinD.
  rewrite (_ : ratr (cd f) * ratr (k%:R * _) = ratr (k%:R * F_UPP_c f)).
  2:{ rewrite /cd -rmorphM /= mulrCA divrK //; exact: neq0. }
  by apply/lee_dD2r/HM; rewrite Hk Hk'. }
have {}Hm : forall t : {nonneg R}, ratr (T f')%:nngnum < t%:nngnum ->
                           ((ratr m + ratr (cd f') * t%:nngnum)%R%:E <= f' t)%dE.
{ move=> t' Ht'.
  have PTf' : 0 <= ratr (T f')%:num :> R by rewrite ler0q.
  have [k /andP[Hk Hk']] := F_UPP_min_aux (d f') Ht'.
  pose t'mkdf' := NngNum (ltW (le_lt_trans PTf' Hk)).
  have -> : t' = (t'mkdf'%:nngnum + ratr (k%:R * (d f')%:num))%:nng.
  { by apply: val_inj; rewrite /= subrK. }
  rewrite (UPP_extension_prop _ (le_refl (T f'))) //.
  rewrite mulrDr /= addrA dEFinD.
  rewrite (_ : ratr (cd f') * ratr (k%:R * _) = ratr (k%:R * F_UPP_c f')).
  2:{ rewrite /cd -rmorphM /= mulrCA divrK //; exact: neq0. }
  by apply/lee_dD2r/Hm; rewrite Hk Hk'. }
have HMm : forall t : {nonneg R}, ratr (tilde_T_min f f' M m)%:nngnum < t%:nngnum ->
  ratr M + ratr (cd f) * t%:nngnum <= ratr m + ratr (cd f') * t%:nngnum.
{ move=> t'.
  rewrite /tilde_T_min Hcd' Hcd => Ht'.
  case: (leP M m) => HmM.
  { apply: lerD; [by rewrite ler_rat|].
    by apply: ler_wpM2r => //; rewrite ler_rat ltW. }
  have HmM' : (insubd 0%:nng (M - m))%:nngnum = M - m.
  { by rewrite insubdK // /in_mem /= subr_ge0 ltW. }
  have Hcdf' : (insubd (0)%:nng (cd f' - cd f))%:nngnum = cd f' - cd f.
  { by rewrite insubdK // /in_mem /= subr_ge0 ltW. }
  rewrite -lerBlDl addrAC -lerBrDr.
  rewrite -[in X in _ <= X]mulNr -mulrDl -!rmorphB /=.
  rewrite mulrC -ler_pdivrMr; [|by rewrite ltr0q subr_gt0].
  rewrite -rmorphV -?rmorphM /=; [|by apply: lt0r_neq0; rewrite subr_gt0].
  apply/ltW; apply: le_lt_trans _ Ht'; rewrite ler_rat.
  by rewrite -{1}HmM' -{1}Hcdf' le_max lexx orbT. }
have HT_tilde_T : T f <= tilde_T_min f f' M m.
{ by rewrite -num_le /tilde_T_min !le_max lexx. }
have HT'_tilde_T : T f' <= tilde_T_min f f' M m.
{ by rewrite -num_le /tilde_T_min !le_max lexx orbT. }
have Hf_le_f' : forall t : {nonneg R}, ratr (tilde_T_min f f' M m)%:nngnum < t%:nngnum -> (f t <= f' t)%E.
{ move=> t' Ht'.
  refine (le_trans (HM _ _) _).
  { by apply: le_lt_trans _ Ht'; rewrite ler_rat num_le. }
  refine (le_trans _ (Hm _ _)).
  { rewrite leEereal /=; exact: HMm. }
  by apply: le_lt_trans _ Ht'; rewrite ler_rat num_le. }
rewrite /= !minEle !Hf_le_f' //.
{ apply: F_UPP_prop.
  by apply: le_lt_trans _ Ht; rewrite ler_rat !le_max lexx. }
by apply: (lt_le_trans H't); rewrite lerDl /=.
Qed.

Local Definition f1 (f : F_UPP) : F :=
  fun t => if t%:nngnum <= ratr (T f)%:nngnum then f t else +oo%E.
Local Definition f2 (f : F_UPP) : F :=
  fun t => if ratr (T f)%:nngnum < t%:nngnum then f t else +oo%E.

Program Definition F_UPP_f1 (f : F_UPP) d1 c1 : F_UPP :=
  Build_F_UPP (f1 f) (T f) d1 c1 _.
Next Obligation.
move=> f d1 c1 t Tt; rewrite /f1 [in RHS]leNgt Tt/= daddye.
by rewrite ifN// -ltNge (lt_trans Tt)// ltrDl.
Qed.

Program Definition F_UPP_f2 (f : F_UPP) : F_UPP :=
  Build_F_UPP (f2 f) (F_UPP_T f) (F_UPP_d f) (F_UPP_c f) _.
Next Obligation.
by move=> f t Tt; rewrite /f2 Tt ifT/= ?(lt_trans Tt) ?ltrDl// F_UPP_prop.
Qed.

Program Definition F_UPP_conv_f1_f1' (f f' : F_UPP) d11 c11 : F_UPP :=
  Build_F_UPP
    ((f1 f : Fd) * f1 f')
    ((T f)%:nngnum + (T f')%:nngnum)%:nng
    d11
    c11
    _.
Next Obligation.
move=> f f' d11 c11 t Tltt.
suff : forall t : {nonneg R}, ratr ((T f)%:nngnum + (T f')%:nngnum) < t%:nngnum ->
                  ((f1 f : Fd) * f1 f') t = +oo%E.
{ move=> H'; rewrite !H' //.
  by apply: (lt_le_trans Tltt); rewrite num_le -num_le /= lerDl. }
move=> t' Ht'.
apply: le_anti; rewrite leey /=.
apply: lb_ereal_inf => _ [[u v] /= Huv] <-.
move: Ht'; rewrite rmorphD /= -Huv /f1.
case: (leP u%:num) => [Hu | //].
case: (leP v%:num) => [Hv |]; [|by rewrite daddeC].
move=> {}Huv; exfalso; move: Huv; apply/negP; rewrite -leNgt.
exact: lerD.
Qed.

Program Definition F_UPP_conv_f1_f2' (f f' : F_UPP) : F_UPP :=
  Build_F_UPP
    ((f1 f : Fd) * f2 f')
    ((T f)%:nngnum + (T f')%:nngnum)%:nng
    (d f')
    (c f')
    _.
Next Obligation.
move=> f f' t Tltt.
have Hut : forall u : {nonneg R}, u%:nngnum <= ratr (T f)%:nngnum -> u < t.
{ move=> u /= Hu; rewrite -num_lt /=; apply: le_lt_trans Tltt.
  by apply: le_trans Hu _; rewrite rmorphD /= lerDl. }
have Hutd' : forall u : {nonneg R}, u%:nngnum <= ratr (T f)%:nngnum ->
                        u%:nngnum < t%:nngnum + ratr (d f')%:num.
{ move=> u /= Hu; move: (Hut _ Hu); rewrite -num_lt /= => H'.
  by apply: (lt_le_trans H'); rewrite lerDl. }
rewrite F_dioid_mulE !alt_def_F_min_conv.
transitivity (ereal_inf
  [set (f1 f u + f2 f'
     (insubd 0%:nng (((t%:num + ratr (d f')%:num)%:nng)%:num - u%:num))%R)%dE
  | u in [set u | u <= (ratr (T f)%:nngnum)%:nng :> {nonneg R}]%classic]).
{ have -> : [set u | u <= (t%:num + ratr (d f')%:num)%:nng]
            = [set u | u <= (ratr (T f)%:num)%:nng]
                `|` [set u | (ratr (T f)%:num)%:nng < u <= (t%:num + ratr (d f')%:num)%:nng].
  { rewrite -[RHS]set_orb; apply/f_equal/funext => u.
    rewrite inE /= -/(u%:num <= ratr (T f)%:num) -num_le -num_lt /=.
    by case: ltP => //= /Hutd'/ltW->. }
  rewrite image_setU ereal_infU min_l// [leRHS](_ : _ = +oo%E) ?leey//.
  apply: le_anti; rewrite leey/=.
  apply: lb_ereal_inf => ? [u /= /andP[Hu _] <-].
  by rewrite /f1 ifN -?ltNge -?num_lt. }
transitivity ((ereal_inf
  ([set f1 f u + f2 f' (insubd (0)%:nng ((t)%:num - (u)%:num))
  | u in [set u0 | (u0%:num <= ratr (T f)%:num)%R]]) : \bar^d R) + (ratr (c f'))%:E).
2:{ apply: f_equal2 => //.
  have -> : [set u | u <= t]
            = [set u | u <= (ratr (T f)%:num)%:nng]
                `|` [set u | (ratr (T f)%:num)%:nng < u <= t].
  { rewrite -[RHS]set_orb; apply/f_equal/funext => u.
    rewrite inE /= -/(u%:num <= ratr (T f)%:num) -num_le -num_lt /=.
    by case: ltP => //= /Hut; rewrite -num_lt => /ltW->. }
  rewrite image_setU ereal_infU min_l// [leRHS](_ : _ = +oo%E) ?leey//.
  apply: le_anti; rewrite leey/=.
  apply: lb_ereal_inf => ? [u /= /andP[Hu _] <-].
  by rewrite /f1 ifN -?ltNge -?num_lt. }
have Hf2 : forall u : {nonneg R}, u%:nngnum <= ratr (T f)%:nngnum ->
  (f2 f' (insubd 0%:nng (t%:nngnum + ratr (d f')%:num - u%:nngnum))%R
   = f2 f' (insubd 0%:nng (t%:nngnum - u%:nngnum))%R + (ratr (c f'))%:E)%dE.
{ move=> u Hu; rewrite /f2.
  rewrite ifT.
  2:{ rewrite insubdK; [|rewrite /in_mem /= subr_ge0; exact/ltW/Hutd'].
    rewrite ltrBrDl; apply: (@lt_le_trans _ _ t%:nngnum).
    { by apply: le_lt_trans Tltt; rewrite rmorphD /= lerD2r. }
    by rewrite lerDl. }
  rewrite ifT.
  2:{ rewrite insubdK; [|rewrite /in_mem /= subr_ge0; exact/ltW/Hut].
    rewrite ltrBrDl; apply: le_lt_trans Tltt.
    by rewrite rmorphD /= lerD2r. }
  rewrite -[(f' ?[t] + _)%dE]/(f' ?[t] + (ratr (c f'))%:E) -F_UPP_prop.
  2:{ rewrite insubdK; [|rewrite /in_mem /= subr_ge0; exact/ltW/Hut].
    rewrite ltrBrDl; apply: le_lt_trans Tltt.
    by rewrite rmorphD /= lerD2r. }
  apply/f_equal/val_inj => /=.
  rewrite insubdK; [|rewrite /in_mem /= subr_ge0; exact/ltW/Hutd'].
  rewrite insubdK; [|rewrite /in_mem /= subr_ge0; exact/ltW/Hut].
  by rewrite addrAC. }
rewrite daddeC -!Rbar_dioidE complete_dioid.set_mulDl !Rbar_dioidE.
apply: f_equal; rewrite predeqP => x; split.
{ move=> -[u Hu <-].
  exists (f1 f u + f2 f' (insubd 0%:nng (t%:nngnum - u%:nngnum))%R)%dE.
  { by exists u. }
  by rewrite (Hf2 _ Hu) /GRing.mul/= daddeC daddeA. }
move=> [_ [u Hu <-] <-]; exists u => //.
by rewrite (Hf2 _ Hu) daddeA daddeC.
Qed.

Program Definition F_UPP_conv_f2_f2' (f f' : F_UPP) : F_UPP :=
  Build_F_UPP
    ((f2 f : Fd) * f2 f')
    ((T f)%:nngnum + (T f')%:nngnum + (lcm_pos_rat (d f) (d f'))%:num)%:nng
    (lcm_pos_rat (d f) (d f'))
    ((lcm_pos_rat (d f) (d f'))%:num * Order.min (cd f) (cd f'))
    _.
Next Obligation.
move=> f f' t; rewrite 2!rmorphD /= => H.
have Hconv : forall t : {nonneg R},
  ((f2 f : Fd) * f2 f') t
  = ereal_inf [set (f2 f u + f2 f' (insubd 0%:nng (t%:nngnum - u%:nngnum))%R)%dE
              | u in [set u | ratr (T f)%:nngnum < u%:nngnum < t%:nngnum - ratr (T f')%:nngnum]].
{ move=> t'; rewrite F_dioid_mulE alt_def_F_min_conv.
  rewrite (Rbar_inf_split [set u | ratr (T f)%:num < u%:num < t'%:num - ratr (T f')%:num]).
  rewrite minC (_ : ereal_inf _ = +oo%E).
  2:{ apply: le_anti; rewrite leey /=.
    apply: lb_ereal_inf => _ [y [/= Hyt' /negP +] <-].
    rewrite negb_and => /orP[/negbTE Hy' |].
    { by rewrite [f2 f]/f2 Hy'. }
    rewrite -leNgt lerBlDl -lerBlDr [f2 f']/f2 insubdK.
    2:{ by rewrite /in_mem /= subr_ge0 num_le. }
    by rewrite leNgt => /negbTE->; rewrite daddeC. }
  rewrite minEge leey.
  apply: f_equal; rewrite predeqP => x; split.
  { move=> [u [/= Hu' Hu] <-]; by exists u. }
  move=> [u /= Hu <-]; exists u; split=> //=.
  move: Hu => /andP[_]; rewrite ltrBrDr -num_le /= => /ltW.
  by apply: le_trans; rewrite lerDl. }
have Hconv' : forall t : {nonneg R},
  ((f2 f : Fd) * f2 f') t
  = ereal_inf [set (f2 f (insubd 0%:nng (t%:nngnum - v%:nngnum))%R + f2 f' v)%dE
              | v in [set v | ratr (T f')%:nngnum < v%:nngnum < t%:nngnum - ratr (T f)%:nngnum]].
{ move=> t'; rewrite mulrC F_dioid_mulE alt_def_F_min_conv.
  rewrite (Rbar_inf_split [set v | ratr (T f')%:num < v%:num < t'%:num - ratr (T f)%:num]).
  rewrite minC (_ : ereal_inf _ = +oo%E).
  2:{ apply: le_anti; rewrite leey /=.
    apply: lb_ereal_inf => _ [y [/= Hyt' /negP +] <-].
    rewrite negb_and => /orP[/negbTE Hy' |].
    { by rewrite [f2 f']/f2 Hy'. }
    rewrite -leNgt lerBlDl -lerBlDr [f2 f]/f2 insubdK.
    2:{ by rewrite /in_mem /= subr_ge0 num_le. }
    by rewrite leNgt => /negbTE->; rewrite daddeC. }
  rewrite minEge leey.
  apply: f_equal; rewrite predeqP => x; split.
  { by move=> [v [/= Hv']] ? ?; exists v => //; rewrite daddeC. }
  move=> [v /= Hv ?]; exists v => //.
  { split=> //=; rewrite -num_le /=; move: Hv => /andP[_].
    by rewrite ltrBrDr => /ltW; apply: le_trans; rewrite lerDl. }
  by rewrite daddeC. }
rewrite Hconv.
rewrite (@Rbar_inf_split_range _
           (ratr (T f)%:nngnum + ratr (lcm_pos_rat (d f) (d f'))%:num)
           (t%:nngnum - ratr (T f')%:nngnum)); last first.
{ by rewrite -addrA lerD2l lerBrDl subrr. }
{ by rewrite ltrBrDl addrCA addrA. }
{ by rewrite lerDl. }
set inf1 := ereal_inf _.
set inf2 := ereal_inf _.
have -> : inf2
          = ereal_inf [set (f2 f (insubd 0%:nng (t%:nngnum + ratr (lcm_pos_rat (d f) (d f'))%:num - v%:nngnum))%R + f2 f' v)%dE
                      | v in [set v | ratr (T f')%:nngnum < v%:nngnum < t%:nngnum - ratr (T f)%:nngnum]].
{ apply: f_equal; rewrite predeqP => x; split;
    move=> [u /= /andP[Hu Hu'] <-];
    exists (insubd 0%:nng (t%:nngnum + ratr (lcm_pos_rat (d f) (d f'))%:num - u%:nngnum)).
  { rewrite insubdK.
    2:{ rewrite /in_mem /=.
      by rewrite subr_ge0; apply: (le_trans (ltW Hu')); rewrite gerDl oppr_le0. }
    apply/andP; split.
    { rewrite ltrBrDl -ltrBrDr; exact: (lt_le_trans Hu'). }
    by rewrite ltrBrDr -!addrA -ltrBrDl subrr addrA addrAC subr_lt0 addrC. }
  { rewrite insubdK.
    { apply: f_equal2 => //; apply/f_equal/val_inj; rewrite insubdK ?subKr //.
      by rewrite /in_mem /=. }
    rewrite /in_mem /= subr_ge0; apply: (le_trans (ltW Hu')).
    by rewrite gerDl oppr_le0. }
  { rewrite insubdK.
    { rewrite ltrD2l ltrNl opprK Hu andbT.
      by rewrite -addrAC ltrD2r ltrBrDl -ltrBrDr. }
    rewrite /in_mem /= subr_ge0; apply: (le_trans (ltW Hu')); rewrite lerD2l.
    by apply: (@le_trans _ _ 0); [rewrite oppr_le0|]. }
  apply: f_equal2 => //; apply: f_equal; rewrite insubdK.
  { by apply/val_inj; rewrite insubdK; [|rewrite /in_mem /=]; rewrite subKr. }
  rewrite /in_mem /= subr_ge0; apply: (le_trans (ltW Hu')); rewrite lerD2l.
  by apply: (@le_trans _ _ 0); [rewrite oppr_le0|]. }
rewrite minr_pMr => //.
rewrite minr_rat (_ : (Order.min ?[a] ?[b])%:E = Order.min ?a%:E ?b%:E).
2:{ by rewrite !minEle leEereal /=; case: leP. }
rewrite minC [RHS]addrC -![in RHS]Rbar_dioidE [RHS]mulrDl ![in RHS]Rbar_dioidE.
apply: f_equal2.
{ rewrite Hconv'.
  rewrite -![in RHS]Rbar_dioidE complete_dioid.set_mulDl ![in RHS]Rbar_dioidE.
  apply: f_equal.
  rewrite image_comp /comp; apply: eq_imagel => t' /andP[Ht' H't'].
  have P1 : t%:nngnum + ratr (lcm_pos_rat (d f) (d f'))%:num - t'%:nngnum \in >= 0.
  { rewrite /in_mem /= subr_ge0.
    apply: (le_trans (ltW H't')); rewrite lerD2l.
    by apply: (@le_trans _ _ 0); [rewrite oppr_le0|]. }
  have P2 : t%:nngnum - t'%:nngnum \in >= 0.
  { rewrite /in_mem /= subr_ge0; apply: (le_trans (ltW H't')).
    by rewrite gerDl oppr_le0. }
  rewrite Rbar_dioid_mulE daddeA; apply: f_equal2 => //.
  rewrite /f2 ifT.
  2:{ rewrite insubdK // ltrBrDr -ltrBrDl addrAC.
    by apply: (lt_le_trans H't'); rewrite lerDl. }
  rewrite ifT.
  2:{ by rewrite insubdK // ltrBrDr -ltrBrDl. }
  have [k Hk] := dvdq_lcml (d f) (d f').
  rewrite [in LHS](_ : insubd _ _
    = ((insubd 0%:nng (t%:nngnum - t'%:nngnum))%:nngnum
       + ratr (k%:R * (d f)%:num))%:nng).
  2:{ by apply/val_inj; rewrite /= !insubdK // addrAC -Hk. }
  rewrite (UPP_extension_prop _ (le_refl (T f))).
  2:{ by rewrite insubdK // ltrBrDl -ltrBrDr. }
  by rewrite Hk daddeC /cd mulrAC mulrA divrK //; apply: lt0r_neq0. }
rewrite Hconv -!Rbar_dioidE complete_dioid.set_mulDl !Rbar_dioidE.
apply: f_equal.
rewrite image_comp /comp; apply: eq_imagel => t' /andP[Ht' H't'].
have P1 : ((t%:nngnum + ratr (lcm_pos_rat (d f) (d f'))%:num)%:nng)%:nngnum
          - t'%:nngnum \in >= 0.
{ rewrite /in_mem /= subr_ge0.
  apply: (le_trans (ltW H't')); rewrite lerD2l.
  by apply: (@le_trans _ _ 0); [rewrite oppr_le0|]. }
have P2 : t%:nngnum - t'%:nngnum \in >= 0.
{ rewrite /in_mem /= subr_ge0; apply: (le_trans (ltW H't')).
  by rewrite gerDl oppr_le0. }
rewrite Rbar_dioid_mulE [in RHS]daddeC -daddeA; apply: f_equal2 => //.
rewrite /f2 ifT.
2: { rewrite insubdK // ltrBrDr -ltrBrDl.
  by apply: (lt_le_trans H't'); rewrite addrAC lerDl. }
rewrite ifT.
2:{ by rewrite insubdK // ltrBrDr -ltrBrDl. }
have [k Hk] := dvdq_lcmr (d f) (d f').
rewrite (_ : insubd _ _
    = ((insubd 0%:nng (t%:nngnum - t'%:nngnum))%:nngnum
       + ratr (k%:R * (d f')%:num))%:nng).
2:{ by apply/val_inj; rewrite /= !insubdK // addrAC -Hk. }
rewrite (UPP_extension_prop _ (le_refl (T f'))).
2:{ by rewrite insubdK // ltrBrDl -ltrBrDr. }
by rewrite Hk /cd mulrAC mulrA divrK //; apply: lt0r_neq0.
Qed.

Lemma F_UPP_conv_aux (f f' : F_UPP) :
  (f : Fd) * f'
  = (f1 f : Fd) * f1 f' \min (f2 f : Fd) * f1 f'
    \min (f1 f : Fd) * f2 f' \min (f2 f : Fd) * f2 f'.
Proof.
have -> : f = (f1 f) \min (f2 f) :> F.
{ apply/funext => t; rewrite /f1 /f2 /=.
  by case: leP; [rewrite minEle|rewrite minEge]; rewrite leey. }
have -> : f' = (f1 f') \min (f2 f') :> F.
{ apply/funext => t; rewrite /f1 /f2 /=.
  by case: leP; [rewrite minEle|rewrite minEge]; rewrite leey. }
by rewrite -!F_dioidE mulrDl !mulrDr !addrA -addrA addrACA addrA.
Qed.

Program Definition UPP_change_d_c (f : F_UPP) (d : {posnum rat}) (c : rat)
  (H : (forall t : {nonneg R}, ratr (F_UPP_T f)%:num < t%:num
                   <= ratr ((F_UPP_T f)%:num + (F_UPP_d f)%:num) ->
                   f t = +oo%E)
       \/ (forall t : {nonneg R}, ratr (F_UPP_T f)%:num < t%:num
                      <= ratr ((F_UPP_T f)%:num + (F_UPP_d f)%:num) ->
                      f t = -oo%E)) : F_UPP :=
  Build_F_UPP
    f
    (F_UPP_T f)
    d
    c
    _.
Next Obligation.
move=> f d0 c0 H t Tlet.
have PTf : 0 <= ratr (T f)%:num :> R; [by []|].
have [k /andP[Hk H'k]] := F_UPP_min_aux (F_UPP_d f) Tlet.
pose tmkdf := NngNum (le_trans PTf (ltW Hk)).
rewrite [in RHS](_ : t = (tmkdf%:nngnum + ratr (k%:R * (d f)%:num))%:nng).
2:{ by apply: val_inj; rewrite /= subrK. }
rewrite [in RHS](UPP_extension_prop _ (le_refl (T f))) //.
rewrite -daddeA.
have H1 : ratr (F_UPP_T f)%:nngnum < t%:nngnum + ratr d0%:num.
{ by apply: (lt_le_trans Tlet); rewrite lerDl. }
have [k' /andP[Hk' H'k']] := F_UPP_min_aux (F_UPP_d f) H1.
pose tmk'df := NngNum (le_trans PTf (ltW Hk')).
rewrite (_ : (t%:nngnum + ratr d0%:num)%:nng
             = (tmk'df%:nngnum + ratr (k'%:R * (d f)%:num))%:nng).
2:{ by apply: val_inj; rewrite /= subrK. }
rewrite (UPP_extension_prop _ (le_refl (T f))) //.
by case: H => H; rewrite !H //= ?Hk // Hk'.
Qed.

Lemma UPP_same_d_c_equality (f f' : F_UPP) :
  F_UPP_d f = F_UPP_d f' -> F_UPP_c f = F_UPP_c f' ->
  let l := Order.max (F_UPP_T f)%:num (F_UPP_T f')%:num + (F_UPP_d f)%:num in
  (forall t : {nonneg R}, t%:num <= ratr l -> f t = f' t) ->
  forall t, f t = f' t.
Proof.
pose theta := !!((Num.max (F_UPP_T f)%:num (F_UPP_T f')%:num)%:nng).
move=> Hd Hc l Htheta t.
have [Ht|Ht] := leP t%:nngnum (ratr theta%:num).
{ apply: Htheta; rewrite /= rmorphD /=.
  by apply: le_trans Ht _; rewrite lerDl. }
have [k /andP[Hk Hk']] := F_UPP_min_aux (F_UPP_d f) Ht.
have Htmkd : (insubd 0%:nng (t%:num - ratr (k%:R * (F_UPP_d f)%:num)))%:num
             = t%:num - ratr (k%:R * (F_UPP_d f)%:num).
{ by rewrite /= insubdK => //; apply: le_trans (ltW Hk). }
have -> : t = ((insubd 0%:nng (t%:num - ratr (k%:R * (F_UPP_d f)%:num)))%:num
               + ratr (k%:R * (F_UPP_d f)%:num))%:nng.
{ by apply/val_inj; rewrite /= Htmkd subrK. }
rewrite (@UPP_extension_prop _ _ theta); last first.
{ by rewrite Htmkd. }
{ by rewrite -num_le le_max lexx. }
rewrite Htheta; last first.
{ by rewrite Htmkd. }
rewrite Hc.
rewrite -(@UPP_extension_prop _ _ theta); last first.
{ by rewrite Htmkd. }
{ by rewrite -num_le le_max lexx orbT. }
by rewrite Hd.
Qed.

Program Definition F_UPP_delta (b : rat) d c :=
  Build_F_UPP (delta (ratr b)%:E) (insubd 0%:nng b) d c _.
Next Obligation.
move=> b d0 c0 t /= bt; have {}bt : ratr b < t%:num.
  by rewrite (le_lt_trans _ bt)// ler_rat val_insubd/=; case: (leP 0) => ///ltW.
by rewrite !ifN// -ltNge lte_fin// (lt_le_trans bt)//lerDl.
Qed.

Lemma UPP_equality (f f' : F_UPP) :
  F_UPP_c f / (F_UPP_d f)%:num = F_UPP_c f' / (F_UPP_d f')%:num ->
  let l := Order.max (F_UPP_T f)%:num (F_UPP_T f')%:num
           + (lcm_pos_rat (F_UPP_d f) (F_UPP_d f'))%:num in
  (forall t : {nonneg R}, t%:num <= ratr l -> f t = f' t) ->
  forall t, f t = f' t.
Proof.
pose theta := !!((Num.max (F_UPP_T f)%:num (F_UPP_T f')%:num)%:nng).
move=> Hcd l Hff' t.
move: (dvdq_lcml (F_UPP_d f) (F_UPP_d f')) => [] [|k Hk].
{ by move=> + /ltac:(exfalso); apply/eqP; rewrite mul0r. }
move: (dvdq_lcmr (F_UPP_d f) (F_UPP_d f')) => [] [|k' Hk'].
{ by move=> + /ltac:(exfalso); apply/eqP; rewrite mul0r. }
have Htheta : (F_UPP_T f)%:nngnum <= theta%:num.
{ by rewrite le_max lexx. }
have Htheta' : (F_UPP_T f')%:nngnum <= theta%:num.
{ by rewrite le_max lexx orbT. }
rewrite -[F_UPP_val f]/(F_UPP_val (UPP_extension k Htheta)).
rewrite -[F_UPP_val f']/(F_UPP_val (UPP_extension k' Htheta')).
apply UPP_same_d_c_equality.
{ by apply/val_inj; rewrite /= -Hk -Hk'. }
{ rewrite /=.
  have Hd : (F_UPP_d f)%:num \is a GRing.unit.
  { by rewrite /mem /in_mem /=. }
  have Hd' : (F_UPP_d f')%:num \is a GRing.unit.
  { by rewrite /mem /in_mem /=. }
  move: (Hd) => /mulrI; apply.
  rewrite mulrA (mulrC (F_UPP_d f)%:num) -Hk Hk'.
  rewrite (mulrC k'.+1%:R) -mulrA mulrC.
  move: (Hd'); rewrite -unitrV => /mulIr; apply.
  rewrite -!mulrA mulrV // mulr1.
  rewrite !mulrA -mulrA -Hcd.
  by rewrite mulrA [RHS]mulrC !mulrA mulVr // mul1r. }
move=> t'.
rewrite /= maxxx -Hk.
move: Hff'; apply.
Qed.

End UPP.
