From mathcomp Require Import ssreflect ssrbool ssrfun ssrnat ssrint ssrnum.
From mathcomp Require Import bigop ssralg rat order eqtype div ssrint intdiv.
From mathcomp Require Import fintype finfun.
From mathcomp Require Import signed.
From NCCoq Require Import RminStruct.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope ring_scope.

Definition lcm_rat (d d' : rat) : rat :=
  fracq (lcmz (numq d * (lcmz (denq d) (denq d') %/ denq d)%Z)
              (numq d' * (lcmz (denq d) (denq d') %/ denq d')%Z),
         lcmz (denq d) (denq d')).

Lemma lcm_pos_rat_aux1 (a : int) (b c : nat) :
  a%:Q * (lcmz b.+1 c.+1 %/ b.+1)%Z%:Q = a%:Q / b.+1%:R * (lcmz b.+1 c.+1)%:Q.
Proof.
rewrite -GRing.mulrA; apply f_equal2 => [//|].
have H_b_unit : (b.+1%:~R : rat) \is a GRing.unit.
{ by rewrite /= /in_mem /mem /= intq_eq0. }
move: (GRing.mulIr H_b_unit); apply.
rewrite -intrM divzK ?dvdz_lcml //.
by rewrite GRing.mulrC GRing.mulrA GRing.mulrV // GRing.mul1r.
Qed.

Lemma lcm_pos_rat_aux2 (d d' : rat) :
  (numq d)%:Q * (lcmz (denq d) (denq d') %/ denq d)%Z%:Q
  = d * (lcmz (denq d) (denq d'))%:Q.
Proof.
by case: ratP => a b Hab; case: ratP => a' b' Hab'; rewrite lcm_pos_rat_aux1.
Qed.

Program Definition lcm_pos_rat_def (d d' : {posnum rat}) : {posnum rat} :=
  @PosNum _ (lcm_rat d%:num d'%:num) _.
Next Obligation.
move=> d d'; rewrite /lcm_rat fracqE /=.
apply Num.Theory.mulr_gt0.
{ rewrite -[0]/(0%:~R) ltr_int.
  rewrite Order.POrderTheory.lt_def lcmz_ge0 andbC /=.
  rewrite lcmz_neq0; apply/andP; split.
  { apply/eqP => /(f_equal (fun x => x%:Q)).
    rewrite intrM lcm_pos_rat_aux2.
    apply/eqP/GRing.mulf_neq0 => //.
    by rewrite intr_eq0 lcmz_neq0 !denq_neq0. }
  apply/eqP => /(f_equal (fun x => x%:Q)).
  rewrite intrM lcmzC lcm_pos_rat_aux2.
  apply/eqP/GRing.mulf_neq0 => //.
  by rewrite intr_eq0 lcmz_neq0 !denq_neq0. }
rewrite Num.Theory.invr_gt0.
rewrite -[0]/(0%:~R) ltr_int.
rewrite Order.POrderTheory.lt_def lcmz_ge0 andbC /=.
by rewrite lcmz_neq0 !denq_neq0.
Qed.
Fact lcm_pos_rat_key : unit. Proof. by []. Qed.
Definition lcm_pos_rat := locked_with lcm_pos_rat_key lcm_pos_rat_def.
Canonical lcm_pos_rat_unlockable := [unlockable fun lcm_pos_rat].

Lemma lcm_pos_ratC  : commutative lcm_pos_rat.
Proof.
move=> d d'; apply/val_inj.
by rewrite unlock /= /lcm_rat /= lcmzC (lcmzC (denq d%:num)).
Qed.

Lemma dvdq_lcml (d d' : {posnum rat}) :
  exists k : nat, (lcm_pos_rat d d')%:num = k%:R * d%:num.
Proof.
rewrite unlock /= /lcm_rat /=.
set x := _ * _.
set y := _ * _.
move: (dvdn_lcml (absz x) (absz y)) => /dvdnP [k Hk]; rewrite {1}/lcmz Hk.
exists k.
have Hx : 0 <= x.
{ apply Num.Theory.mulr_ge0.
  { by rewrite numq_ge0. }
  by rewrite lez_divRL // GRing.mul0r lcmz_ge0. }
rewrite PoszM gez0_abs // fracqE /= /x 2!intrM lcm_pos_rat_aux2.
have H_lcmdd'_unit : (intr (lcmz (denq d%:num) (denq d'%:num)) : rat) \is a GRing.unit.
{ by rewrite /in_mem /mem /= intq_eq0 lcmz_neq0 /= !denq_neq0. }
by rewrite -!GRing.mulrA GRing.mulrV // GRing.mulr1.
Qed.

Lemma dvdq_lcmr (d d' : {posnum rat}) :
  exists k : nat, (lcm_pos_rat d d')%:num = k%:R * d'%:num.
Proof. by rewrite lcm_pos_ratC; apply dvdq_lcml. Qed.

Lemma dvdq_lcm (d d' : {posnum rat}) (m : rat) (k k' : nat) :
  m = k%:R * d%:num -> m = k'%:R * d'%:num ->
  exists k'' : nat, m = k''%:R * (lcm_pos_rat d d')%:num.
Proof.
move=> Hd Hd'.
rewrite unlock /= /lcm_rat /=.
set ldd' := lcmz (denq _) _.
set x := _ * _.
set y := _ * _.
have Hx : m * ldd'%:Q = (k%:Z * x)%:Q.
{ by rewrite Hd -GRing.mulrA -lcm_pos_rat_aux2 !intrM. }
have Hy : m * ldd'%:Q = (k'%:Z * y)%:Q.
{ by rewrite Hd' -GRing.mulrA /ldd' lcmzC -lcm_pos_rat_aux2 lcmzC !intrM. }
have H_ldd'_unit : (intr ldd' : rat) \is a GRing.unit.
{ by rewrite /in_mem /mem /= intq_eq0 lcmz_neq0 /= !denq_neq0. }
have Px : 0 <= x.
{ rewrite -(@ler_int rat) /x intrM lcm_pos_rat_aux2.
  exact: Num.Theory.mulr_ge0. }
have Py : 0 <= y.
{ rewrite -(@ler_int rat) /y intrM /ldd' lcmzC lcm_pos_rat_aux2.
  exact: Num.Theory.mulr_ge0. }
have : (lcmn (absz x) (absz y) %| k * absz x)%nat.
{ rewrite dvdn_lcm dvdn_mull //=.
  apply/dvdnP; exists k'.
  suff: intr (k%:Z * absz x) = intr (k'%:Z * absz y) :> rat.
  { by move=> /eqP; rewrite eqr_int -!PoszM => /eqP []. }
  by rewrite !gez0_abs // -Hx -Hy. }
move=> /dvdnP [k'' Hk''].
exists k''.
rewrite fracqE /= GRing.mulrA.
apply (GRing.mulIr H_ldd'_unit).
rewrite -!GRing.mulrA GRing.mulVr // GRing.mulr1.
by rewrite -[k''%:R]/(k''%:~R) -intrM -PoszM -Hk'' PoszM gez0_abs.
Qed.

Lemma dvdq_ge_lcm (d d' m : {posnum rat}) (k k' : nat) :
  m%:num = k%:R * d%:num -> m%:num = k'%:R * d'%:num ->
  (lcm_pos_rat d d')%:num <= m%:num.
Proof.
move=> Hd Hd'.
move: (dvdq_lcm Hd Hd') => [] [/eqP|k'' ->]; first by rewrite GRing.mul0r eq0F.
rewrite -addn1 GRing.natrD GRing.mulrDl GRing.mul1r.
by rewrite Num.Theory.lerDr; apply Num.Theory.mulr_ge0.
Qed.

Lemma lcm_pos_ratA : associative lcm_pos_rat.
Proof.
move=> a b c.
apply/Order.POrderTheory.le_anti/andP; split.
{ move: (dvdq_lcml (lcm_pos_rat a b) c) => [k].
  set m := lcm_pos_rat (lcm_pos_rat a b) c => Hab.
  move: (dvdq_lcml a b) (Hab) => [k' ->].
  rewrite GRing.mulrA -GRing.natrM => Ha.
  move: (dvdq_lcmr a b) (Hab) => [k'' ->].
  rewrite GRing.mulrA -GRing.natrM => Hb.
  move: (dvdq_lcmr (lcm_pos_rat a b) c) => [k'''].
  rewrite -/m => Hc.
  move: (dvdq_lcm Hb Hc) => [k''''].
  move: Ha; apply dvdq_ge_lcm. }
move: (dvdq_lcmr a (lcm_pos_rat b c)) => [k].
set m := lcm_pos_rat a (lcm_pos_rat b c) => Hbc.
move: (dvdq_lcml b c) (Hbc) => [k' ->].
rewrite GRing.mulrA -GRing.natrM => Hb.
move: (dvdq_lcmr b c) (Hbc) => [k'' ->].
rewrite GRing.mulrA -GRing.natrM => Hc.
move: (dvdq_lcml a (lcm_pos_rat b c)) => [k'''].
rewrite -/m => Ha.
move: (dvdq_lcm Ha Hb) Hc => [k''''].
apply dvdq_ge_lcm.
Qed.

Lemma lcm_pos_rat_xx : idempotent_op lcm_pos_rat.
Proof.
move=> x; apply/val_inj => /=.
rewrite unlock /=/ lcm_rat /lcmz /= !Order.NatDvd.lcmnn.
rewrite (@gez0_abs (denq x%:num)) ?denq_ge0 //.
rewrite divzz denq_eq0 /= GRing.mulr1.
move: (!!(gt0 x)); rewrite -numq_gt0.
case: ratP => n d _ Hn; rewrite fracqE /=.
by rewrite gez0_abs // Order.POrderTheory.ltW.
Qed.
