From HB Require Import structures.
From mathcomp Require Import ssreflect ssrbool ssrfun order eqtype order seq.
From mathcomp Require Import choice bigop ssrint intdiv ssralg ssrnum rat.
From mathcomp Require Import archimedean.
From mathcomp Require Import mathcomp_extra constructive_ereal reals.
From mathcomp Require Import boolp classical_sets ereal signed.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import Order.Theory GRing.Theory Num.Theory.

Lemma er_map_ratr_opp (R : numFieldType) x :
  er_map ratr (- x)%E = (- er_map ratr x)%E :> \bar R.
Proof. by case: x => [x||] //=; rewrite raddfN. Qed.

Lemma er_map_ratr_max (R : realFieldType) x y :
  er_map ratr (Order.max x y)
  = Order.max (er_map ratr x) (er_map ratr y) :> \bar R.
Proof.
by move: x y => [x||] [y||] //=; rewrite ?maxey ?maxNye -?EFin_max/= ?maxr_rat.
Qed.

Lemma er_map_ratr_min (R : realFieldType) x y :
  er_map ratr (Order.min x y)
  = Order.min (er_map ratr x) (er_map ratr y) :> \bar R.
Proof.
by move: x y => [x||] [y||] //=; rewrite ?miney ?minNye -?EFin_min/= ?minr_rat.
Qed.

Lemma dEFinD {R : numDomainType} (r r' : R) : (r + r')%:E = (r%:E + r'%:E)%dE.
Proof. by []. Qed.

Lemma lee_daddgt0Pr (R' : realFieldType) (x y : \bar^d R') :
  reflect (forall e : R', (0 < e)%R -> (x <= y + e%:E)%E) (x <= y)%E.
Proof.
apply/(iffP idP).
  case: x y => [x||] [y||] //; last by move=> _ ? _; apply: leNye.
  by move=> xley e ?; rewrite (le_trans xley)// -dEFinD lee_fin lerDl ltW.
case: x y => [x||] [y||] // xleye.
- by rewrite lee_fin; apply/ler_addgt0Pr/xleye.
- exact: leey.
- exact: (le_trans (xleye 1%R _)).
- exact: xleye.
- exact: xleye.
- exact: leNye.
Qed.

Lemma ereal_infU {R : realType} (A B : set \bar R) :
  ereal_inf (A `|` B) = mine (ereal_inf A) (ereal_inf B).
Proof.
apply/le_anti/andP; split; case: (leP (ereal_inf A)) => infAB.
- by apply: lb_ereal_inf => x Ax; apply: ereal_inf_lbound; left.
- by apply: lb_ereal_inf => x Bx; apply: ereal_inf_lbound; right.
- apply: lb_ereal_inf => x [Ax|Bx]; first exact: ereal_inf_lbound.
  by apply: le_trans infAB _; apply: ereal_inf_lbound.
- apply: lb_ereal_inf => x [Ax|Bx]; last exact: ereal_inf_lbound.
  by apply: le_trans (ltW infAB) _; apply: ereal_inf_lbound.
Qed.

Local Open Scope ring_scope.

Lemma daddNye {R : realType} (x : \bar R) : (x != +oo -> -oo + x = -oo)%dE.
Proof. by case: x. Qed.
Lemma daddey {R : realType} (x : \bar R) : (x + +oo = +oo)%dE.
Proof. by case: x. Qed.

Lemma min_funE d T (T' : orderType d) (f g : T -> T') : (f \min g = f `&` g)%O.
Proof. by apply/funext => x /=; rewrite -meetEtotal. Qed.

Lemma max_funE d T (T' : orderType d) (f g : T -> T') : (f \max g = f `|` g)%O.
Proof. by apply/funext => x /=; rewrite -joinEtotal. Qed.

Lemma min_funA {d T} {T' : orderType d} : @associative (T -> T') Order.min_fun.
Proof. move=> f g h; apply/funext => x; exact: minA. Qed.

Lemma min_funC {d T} {T' : orderType d} : @commutative (T -> T') _ Order.min_fun.
Proof. move=> f g; apply/funext => x; exact: minC. Qed.

Lemma minff {d T} {T' : porderType d} : @idempotent_op (T -> T') Order.min_fun.
Proof. move=> f; apply/funext => x; exact: minxx. Qed.

Lemma addfA {U} {V : nmodType} : @associative (U -> V) GRing.add_fun.
Proof. by move=> f g h; apply/funext => x; rewrite /= addrA. Qed.

Lemma addfC {U} {V : nmodType} : @commutative (U -> V) _ GRing.add_fun.
Proof. by move=> f g; apply/funext => x; rewrite /= addrC. Qed.

Lemma add0f {U} {V : nmodType} : @left_id (U -> V) _ \0 GRing.add_fun.
Proof. by move=> f; apply/funext => x; rewrite /= add0r. Qed.

Lemma addf0 {U} {V : nmodType} : @right_id (U -> V) _ \0 GRing.add_fun.
Proof. by move=> f; rewrite addfC add0f. Qed.

HB.instance Definition _ U (V : nmodType) :=
  Monoid.isComLaw.Build (U -> V) \0 GRing.add_fun addfA addfC add0f.

HB.instance Definition _ U (V : nmodType) :=
  GRing.isNmodule.Build (U -> V) addfA addfC add0f.

Lemma addNf {U} {V : zmodType} :
  @left_inverse (U -> V) _ _ \0 GRing.opp_fun GRing.add_fun.
Proof. by move=> f; apply/funext => x; rewrite /= addNr. Qed.

HB.instance Definition _ U (V : zmodType) :=
  GRing.Nmodule_isZmodule.Build (U -> V) addNf.

Lemma mulfA {U} {V : semiRingType} : @associative (U -> V) GRing.mul_fun.
Proof. by move=> f g h; apply/funext => x; rewrite /= mulrA. Qed.

Lemma mul1f {U} {V : semiRingType} :
  @left_id (U -> V) _ (fun=> 1) GRing.mul_fun.
Proof. by move=> f; apply/funext => x; rewrite /= mul1r. Qed.

Lemma mulf1 {U} {V : semiRingType} :
  @right_id (U -> V) _ (fun=> 1) GRing.mul_fun.
Proof. by move=> f; apply/funext => x; rewrite /= mulr1. Qed.

Lemma mulfDl {U} {V : semiRingType} :
  @left_distributive (U -> V) _ GRing.mul_fun GRing.add_fun.
Proof. by move=> f g h; apply/funext => x; rewrite /= mulrDl. Qed.

Lemma mulfDr {U} {V : semiRingType} :
  @right_distributive (U -> V) _ GRing.mul_fun GRing.add_fun.
Proof. by move=> f g h; apply/funext => x; rewrite /= mulrDr. Qed.

Lemma mul0f {U} {V : semiRingType} : @left_zero (U -> V) _ \0 GRing.mul_fun.
Proof. by move=> f; apply/funext => x; rewrite /= mul0r. Qed.

Lemma mulf0 {U} {V : semiRingType} : @right_zero (U -> V) _ \0 GRing.mul_fun.
Proof. by move=> f; apply/funext => x; rewrite /= mulr0. Qed.

Lemma onef_neq0 {U : pointedType} {V : semiRingType} :
  (fun=> 1) != \0 :> (U -> V).
Proof. by apply/eqP => /funeqP/(_ point); apply/eqP; rewrite oner_neq0. Qed.

HB.instance Definition _ U (V : semiRingType) :=
  Monoid.isLaw.Build (U -> V) (fun=> 1) GRing.mul_fun mulfA mul1f mulf1.

HB.instance Definition _ U (V : semiRingType) :=
  Monoid.isMulLaw.Build (U -> V) \0 GRing.mul_fun mul0f mulf0.

HB.instance Definition _ U (V : semiRingType) :=
  Monoid.isAddLaw.Build (U -> V) GRing.mul_fun GRing.add_fun mulfDl mulfDr.

HB.instance Definition _ (U : pointedType) (V : semiRingType) :=
  GRing.Nmodule_isSemiRing.Build (U -> V)
    mulfA mul1f mulf1 mulfDl mulfDr mul0f mulf0 onef_neq0.

Lemma mulfC {U} {V : comSemiRingType} : @commutative (U -> V) _ GRing.mul_fun.
Proof. by move=> f g; apply/funext => x; rewrite /= mulrC. Qed.

HB.instance Definition _ U (V : comSemiRingType) :=
  SemiGroup.isCommutativeLaw.Build (U -> V) GRing.mul_fun mulfC.

HB.instance Definition _ (U : pointedType) (V : comSemiRingType) :=
  GRing.SemiRing_hasCommutativeMul.Build (U -> V) mulfC.

HB.instance Definition _ (U : pointedType) (V : comRingType) :=
  GRing.ComSemiRing.on (U -> V).

HB.instance Definition _ {R : realType} := Choice.on {nonneg R}.
HB.instance Definition _ {R : realType} := isPointed.Build {nonneg R} 0%:nng.

Lemma sumfE U (V : nmodType) I (r : seq I) P (F : I -> U -> V) x :
  (\sum_(i <- r | P i) F i) x = \sum_(i <- r | P i) F i x.
Proof. by apply: (big_ind2 (fun f y => f x = y)) => // f1 _ f2 _ <- <-. Qed.

Section Ceil.

Context {R : realType}.

Lemma ceilDz (x : R) (n : int) : Num.ceil (x + n%:~R) = Num.ceil x + n%:~R.
Proof.
apply/le_anti/andP; split.
  by rewrite -ceil_le_int rmorphD/= intz lerD2r le_ceil.
rewrite -lerBrDr -ceil_le_int rmorphD/= intz !rmorphN/=.
by rewrite -opprD -lerNr -lerBrDr opprD ge_floor.
Qed.

Lemma gt_pred_ceil (x : R) : (Num.ceil x)%:~R - 1 < x.
Proof.
by rewrite /Num.ceil rmorphN/= -opprD ltrNl intrD1 lt_succ_floor ?num_real.
Qed.

Lemma ceilP (x : R) (n : int) : (n%:~R - 1 < x <= n%:~R) = (Num.ceil x == n).
Proof.
apply/idP/idP; last by move=> /eqP<-; rewrite le_ceil gt_pred_ceil.
move=> /andP[nltx xleSn]; apply/eqP/le_anti.
by rewrite -ceil_le_int xleSn -ltzD1 -ltrBlDr -ceil_gt_int rmorphB.
Qed.

End Ceil.
