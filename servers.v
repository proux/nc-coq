(******************************************************************************)
(* Definitions of servers.                                                    *)
(*                                                                            *)
(* A server is a relation between input flows (often called A for arrival)    *)
(* and output flows (often called D for departure).                           *)
(*                                                                            *)
(*         server == the type of servers, i.e., relations between pairs       *)
(*                   of flows (A, D) with D <= A and such that for any A,     *)
(*                   there is at least one D                                  *)
(* partial_server == server such that for some input, there may               *)
(*                   not be any output                                        *)
(*      nserver n == a server crossed by n flows numbered from 0 to n-1       *)
(*      n.-server == notation for nserver n (n is a natural number)           *)
(* aggregate_server S == from nserver S, build the server with a single flow  *)
(*                   that is the sum of all n flows crossing S                *)
(* residual_server S i == the i-th residual server of nserver S               *)
(* residual_server_constr S i alpha == the i-th residual server of nserver S  *)
(*                   whose inputs are constrained by alpha (this is only      *)
(*                   a partial server)                                        *)
(* server_concat S1 S2 == the server made of S1 followed by S2                *)
(*       S1 '; S2 == notation fr server_concat S1 S2 (in scope Server_scope)  *)
(*                                                                            *)
(******************************************************************************)

From mathcomp Require Import ssreflect ssrfun ssrbool eqtype order ssrnat.
From mathcomp Require Import choice seq fintype finfun bigop ssralg ssrnum rat.
From mathcomp Require Import boolp classical_sets signed reals ereal.
From mathcomp.dioid Require Import dioid complete_lattice complete_dioid.
Require Import RminStruct cumulative_curves arrival_curves.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope ring_scope.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.

Declare Scope Server_scope.
Delimit Scope Server_scope with S.

Section Servers.

Context {R : realType}.

(** ** Server *)

(** Servers are relations between flow_cc which
  - are left total;
  - verifies D <= A. *)

Record partial_server := {
  server_f :> @flow_cc R -> flow_cc -> Prop;
  _ : forall A D, server_f A D -> (D <= A :> F)%O
}.
Arguments Build_partial_server : clear implicits.

Record server := {
    server_t :> partial_server;
    _ : forall A, exists D, server_f server_t A D
}.
Arguments Build_server : clear implicits.

Bind Scope Server_scope with partial_server.
Bind Scope Server_scope with server.

Lemma server_le (S : partial_server) A D : S A D -> (D <= A :> F)%O.
Proof. case: S => S /= HS SAD; exact: HS. Qed.

Lemma server_left_total (S : server) : forall A : flow_cc, exists D, S A D.
Proof. by case: S => S HS A; move: (HS A) => -[D HD]; exists D. Qed.

(** *** n.-server *)
Record nserver (n : nat) := {
  nserver_f :> (@flow_cc R)^n -> flow_cc^n -> Prop;
  _ : forall A, exists D, nserver_f A D;
  _ : forall A D, nserver_f A D -> forall i, (D i <= A i :> F)%O
}.

Local Notation "n .-server" := (nserver n)
  (at level 2, format "n .-server") : type_scope.

Lemma nserver_le n (S : n.-server) A D : S A D -> forall i, (D i <= A i :> F)%O.
Proof. case: S => S /= _ HS SAD; exact: HS. Qed.

Lemma nserver_left_total n (S : n.-server) (A : flow_cc^n) : exists D, S A D.
Proof. by case: S => S /= HS _; move: (HS A) => -[D HD]; exists D. Qed.

(** Aggregate server (n.+1 to avoid the weird 0-server) *)
Program Definition aggregate_server_cond n (S : n.+1.-server)
        (P : pred 'I_n.+1) : partial_server :=
  Build_partial_server
    (fun As Ds =>
       exists A D, S A D /\ As = \sum_(i | P i) A i
                   /\ Ds = \sum_(i | P i) D i)
    _.
Next Obligation.
move=> n S P _ _ [As [Ds [SAsDs [-> ->]]]]; apply/lefP => t.
rewrite !flow_cc_sumE.
apply: lee_dsum => i Pi.
move: t; exact/lefP/(nserver_le SAsDs).
Qed.

(** Aggregate server (n.+1 to avoid the weird 0-server) *)
Program Definition aggregate_server n (S : n.+1.-server) :=
  Build_server (aggregate_server_cond S predT) _.
Next Obligation.
move=> n S A.
move: (nserver_left_total S [ffun i => if i == ord0 then A else 0]).
move=> -[D HD].
exists (\sum_i D i).
eexists; eexists; split; [exact HD|]; split=> [|//].
rewrite big_ord_recl /= ffunE eqxx.
under eq_bigr => i _ do rewrite ffunE /=.
by rewrite big1_eq addr0.
Qed.

Lemma aggregate_server_prop_cond n (S : (n.+1).-server) A D (HAD : S A D)
  (P : pred 'I_n.+1) :
  aggregate_server_cond S P (\sum_(i | P i) A i) (\sum_(i | P i) D i).
Proof. by exists A, D. Qed.

Lemma aggregate_server_prop n (S : (n.+1).-server) A D (HAD : S A D) :
  aggregate_server S (\sum_i A i) (\sum_i D i).
Proof. exact: aggregate_server_prop_cond. Qed.

(** Residual server *)
Program Definition residual_server n (S : n.-server) i :=
  Build_server
    (Build_partial_server
       (fun Ai Di => exists A D, nserver_f S A D /\ Ai = A i /\ Di = D i)
       _)
    _.
Next Obligation.
move=> n S i _ _ [As [Ds [SAsDs [-> ->]]]]; exact: (nserver_le SAsDs).
Qed.
Next Obligation.
move=> n S i A.
move: (nserver_left_total S [ffun j => if j == i then A else 0]).
move=> -[D HD].
exists (D i); eexists; exists D; split; [exact: HD|]; split=> [|//].
by rewrite ffunE eqxx.
Qed.

Program Definition residual_server_constr (n : nat) (S : n.-server) i
           (alpha : F^n) : partial_server :=
  Build_partial_server (fun Ai Di =>
    exists A D, S A D /\ Ai = A i /\ Di = D i
           /\ forall j, max_arrival (A j) (alpha j)) _.
Next Obligation.
move=> n S i alpha _ _ [As [Ds [SAsDs [-> [-> _]]]]]; exact: (nserver_le SAsDs).
Qed.

Program Definition server_concat (S1 S2 : partial_server) :=
  Build_partial_server (fun A C => exists B, S1 A B /\ S2 B C) _.
Next Obligation.
move=> S1 S2 A D [B [S1AB S2BD]].
exact: (le_trans (server_le S2BD) (server_le S1AB)).
Qed.

End Servers.
Arguments Build_partial_server {R} _ _.

Notation "n .-server" := (nserver n)
  (at level 2, format "n .-server") : type_scope.

Notation "S1 '';' S2" := (server_concat S1 S2) (at level 100) : Server_scope.
