(* Copyright : Lucien Rakotomalala, Pierre Roux and Marc Boyer. *)

(******************************************************************************)
(*  This files contains the definition of a backlogged period                 *)
(*      and the start of a backlogged period                                  *)
(*                                                                            *)
(* backlog_itv A F s t == definition of a backlogged period for A and F       *)
(*                        between s and t (definiton 5.4 from DNC2018).       *)
(*                                                                            *)
(*    start A D t == defintion of the start of the backlogged period between  *)
(*                   A and D before t (definiton 5.4 from DNC2018).           *)
(******************************************************************************)

From mathcomp Require Import ssreflect ssrfun ssrbool eqtype order ssrnat.
From mathcomp Require Import choice seq fintype finfun bigop ssralg ssrnum rat interval.
From mathcomp Require Import boolp classical_sets signed reals ereal.
From mathcomp.dioid Require Import dioid complete_lattice complete_dioid.
Require Import RminStruct cumulative_curves servers.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope classical_set_scope.
Local Open Scope ring_scope.
Local Open Scope order_scope.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.

Section BacklogItv.

Context {R : realType}.

Definition backlog_itv (A D : F) (I : interval {nonneg R}) :=
  forall x, x \in I -> (D x < A x)%E.

Lemma start_proof (A D : F) (t : {nonneg R}) :
  { y : {nonneg R} | y%:nngnum%:E
    = Order.max
        0%:E
        (ereal_sup [set u%:nngnum%:E | u in [set u | u <= t /\ D u = A u]])}.
Proof.
set s := [set u | u <= t /\ D u = A u].
case (@eqP _ s set0) => [->| /eqP/set0P/cid[y sy]].
{ exists 0%:nng.
  rewrite (_ : image _ _ = set0) ?ereal_sup0 ?maxEge ?leNye //.
  by rewrite -subset0 => ? [? []]. }
case E: (ereal_sup _) => [l | |].
{ have Pl : 0 <= l.
  { apply: le_trans (ge0 y) _.
    suff: (y%:nngnum%:E <= l%:E)%E; [by rewrite leEereal|].
    by rewrite -E; apply: ereal_sup_ubound; exists y. }
  by exists (NngNum Pl); rewrite maxEle leEereal /= Pl. }
{ exfalso; move: (le_refl (+oo%E : \bar R)); apply/negP; rewrite -ltNge -{1}E.
  move: (ltry t%:nngnum); apply: le_lt_trans.
  by apply: ub_ereal_sup => _ [x [sx _] <-]; rewrite leEereal /= num_le. }
have Hy : [set (u)%:nngnum%:E | u in s] (y)%:nngnum%:E; [by exists y|].
by exfalso; move: E => /ereal_sup_ninfty /(_ y%:nngnum%:E Hy).
Qed.

Definition start (A D : F) (t : {nonneg R}) : {nonneg R} :=
  proj1_sig (start_proof A D t).

Lemma start_non_decr (A D : flow_cc) : {homo (start A D) : x y / x <= y }.
Proof.
move=> x y Hxy.
rewrite -num_le /= -lee_fin.
rewrite /start; case: start_proof => _ ->; case: start_proof => _ ->.
case (@eqP _ [set u | u <= x /\ D u = A u] set0) => [->| /eqP/set0P/cid[z sz]].
{ rewrite (_ : image _ _ = set0) -?subset0 => [| ? [? []] //].
  by rewrite ereal_sup0 maxEge leNye le_max lexx. }
rewrite maxEle ifT.
2:{ apply: (@le_trans _ _ z%:nngnum%:E); [by rewrite leEereal /=|].
  by apply: ereal_sup_ubound; exists z. }
rewrite maxEle ifT.
2:{ apply: (@le_trans _ _ z%:nngnum%:E); [by rewrite leEereal /=|].
  apply: ereal_sup_ubound; exists z => //.
  by move: sz => -[sz sz']; split=> //; move: Hxy; apply: le_trans. }
apply: ub_ereal_sup => _ [z' [Hz' H'z'] <-].
by apply: ereal_sup_ubound; exists z' => //; split=> //; move: Hxy; apply: le_trans.
Qed.

Lemma start_eq (A D : flow_cc) t : A (start A D t) = D (start A D t).
Proof.
set s := start _ _ _.
case (@eqP _ s 0%:nng) => [-> | /eqP Hs]; [by rewrite !flow_cc0|].
have Ps : 0 < s%:nngnum; [by rewrite lt_def; apply/andP; split|].
case: (@eqP _ (A s) (D s)) => [//| /eqP HAD]; exfalso.
move: (le_refl s); apply/negP; rewrite -ltNge.
move: (HAD) (le_total (A s) (D s)).
rewrite !le_eqVlt eq_sym => /negbTE -> /= /orP[] {}HAD.
{ have Peps : 0 < (D%:fcc s)%:nngnum - (A%:fcc s)%:nngnum.
  { by rewrite subr_gt0 -lte_fin !fin_flow_ccE. }
  pose eps := PosNum Peps.
  have [eta Heta] := flow_cc_left_cont D s eps.
  have Py : 0 <= Order.max 0 (s%:nngnum - eta%:num); [by rewrite le_max lexx|].
  pose y := NngNum Py.
  apply: (@le_lt_trans _ _ y).
  2:{ by rewrite -num_lt /y /= gt_max Ps /= ltrBDr ltrDl. }
  rewrite -num_le /val -lee_fin.
  rewrite /s /start; case: start_proof => _ ->.
  rewrite ge_max; apply/andP; split=> //.
  apply: ub_ereal_sup => _ [z [Hz H'z] <-].
  have Hzs : z%:nngnum <= s%:nngnum.
  { rewrite -lee_fin /s /start; case: start_proof => _ ->.
    by rewrite le_max; apply/orP; right; apply: ereal_sup_ubound; exists z. }
  rewrite lee_fin leNgt; apply/negP => Hyz.
  suff: (A z < D z)%E; [by rewrite H'z; apply/negP; rewrite -leNgt|].
  apply: (@le_lt_trans _ _ (A s)).
  { apply/ndFP => //; exact: Fup_ge_non_decr_closure. }
  have H''z : s%:nngnum - eta%:num <= z%:nngnum <= s%:nngnum.
  { rewrite Hzs andbT; move: Hyz => /ltW; apply: le_trans.
    by rewrite /y /= le_max lexx orbT. }
  move: (Heta _ H''z) => /andP[+ _].
  by rewrite -fin_flow_ccE /eps/= -dEFinB subKr fin_flow_ccE. }
have Peps : 0 < (A%:fcc s)%:nngnum - (D%:fcc s)%:nngnum.
{ by rewrite subr_gt0 -lte_fin !fin_flow_ccE. }
pose eps := PosNum Peps.
have [eta Heta] := flow_cc_left_cont A s eps.
have Py : 0 <= Order.max 0 (s%:nngnum - eta%:num); [by rewrite le_max lexx|].
pose y := NngNum Py.
apply: (@le_lt_trans _ _ y).
2:{ by rewrite -num_lt /y /= gt_max Ps /= ltrBDr ltrDl. }
rewrite -num_le /val -lee_fin.
rewrite /s /start; case: start_proof => _ ->.
rewrite ge_max; apply/andP; split=> //.
apply: ub_ereal_sup => _ [z [Hz H'z] <-].
have Hzs : z%:nngnum <= s%:nngnum.
{ rewrite -lee_fin /s /start; case: start_proof => _ ->.
  by rewrite le_max; apply/orP; right; apply: ereal_sup_ubound; exists z. }
rewrite lee_fin leNgt; apply/negP => Hyz.
suff: (D z < A z)%E; [by rewrite H'z; apply/negP; rewrite -leNgt|].
apply: (@le_lt_trans _ _ (D s)).
{ apply/ndFP => //; exact: Fup_ge_non_decr_closure. }
have H''z : s%:nngnum - eta%:num <= z%:nngnum <= s%:nngnum.
{ rewrite Hzs andbT; move: Hyz => /ltW; apply: le_trans.
  by rewrite /y /= le_max lexx orbT. }
move: (Heta _ H''z) => /andP[+ _].
by rewrite -fin_flow_ccE /eps/= -dEFinB subKr fin_flow_ccE.
Qed.

Lemma start_le (A D : F) (t : {nonneg R}) : (start A D t)%:nngnum <= t%:nngnum.
Proof.
rewrite /= -lee_fin /start; case: start_proof => _ ->.
rewrite ge_max lee_fin ge0 /=.
apply: ub_ereal_sup => _ [y [Hy _] <-].
by rewrite lee_fin num_le.
Qed.

Lemma start_t_is_a_backlog_itv (S : partial_server) (A D : flow_cc) t :
  S A D -> backlog_itv A D `]start A D t, t].
Proof.
move=> HAD u uI.
rewrite lt_def; move: (server_le HAD) => /lefP /(_ u) ->; rewrite andbT.
apply/eqP => H.
have: start A D t < u by rewrite (itvP uI).
apply/negP; rewrite -leNgt.
rewrite -num_le /= -lee_fin /start; case: start_proof => _ ->.
rewrite le_max; apply/orP; right.
by apply: ereal_sup_ubound; exists u; rewrite //= (itvP uI) H.
Qed.

Lemma backlog_itv_sub I I' A D : (I' <= I)%O ->
  backlog_itv A D I -> backlog_itv A D I'.
Proof. by move=> /subitvP I'subI blI + /I'subI. Qed.

Lemma backlog_itv_condW n (S : n.-server) A D (HS : S A D) I
      (P P' : pred 'I_n) :
  (forall i, P i -> P' i) ->
  backlog_itv (\sum_(i | P i) A i) (\sum_(i | P i) D i) I
  -> backlog_itv (\sum_(i | P' i) A i) (\sum_(i | P' i) D i) I.
Proof.
move=> HPP' blP t tinI.
rewrite !flow_cc_sumE.
under eq_bigr => i _ do rewrite -fin_flow_ccE.
under [X in (_ < X)%E]eq_bigr => i _ do rewrite -fin_flow_ccE.
rewrite !dsumEFin.
rewrite (bigID P) [in X in (_ < X)%E](bigID P)/= !dEFinD.
under eq_bigl => i do (rewrite (_ : _ && _ = P i);
  [|case_eq (P i); rewrite andbC => //=; apply HPP']).
under [in X in (_ < X + _)%dE]eq_bigl => i do (rewrite (_ : _ && _ = P i);
  [|case_eq (P i); rewrite andbC => //=; apply HPP']).
apply: lte_le_dD => //.
{ rewrite -!dsumEFin.
  under eq_bigr => i _ do rewrite fin_flow_ccE.
  under [X in (_ < X)%E]eq_bigr => i _ do rewrite fin_flow_ccE.
  rewrite -!flow_cc_sumE.
  exact: blP. }
rewrite -!dsumEFin; apply: lee_dsum => i _; rewrite !fin_flow_ccE.
by move: (nserver_le HS i) => /lefP.
Qed.

Lemma backlog_itv_pred1 n (S : n.-server) A D (HS : S A D) I
    (P : pred 'I_n) i :
  P i ->
  backlog_itv (A i) (D i) I ->
  backlog_itv (\sum_(i | P i) A i) (\sum_(i | P i) D i) I.
Proof.
move=> Pi.
have -> : A i = (\sum_(j | j == i) A j) by rewrite big_pred1_eq.
have -> : D i = (\sum_(j | j == i) D j) by rewrite big_pred1_eq.
by apply: (backlog_itv_condW HS) => ? /eqP->.
Qed.

End BacklogItv.
