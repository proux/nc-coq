From mathcomp Require Import ssreflect ssrbool ssrfun order.
From mathcomp Require Import ssrint intdiv ssralg ssrnum rat.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import Order.Theory.

(* rat *)

Definition ceilq x :=
  if (0 <= numq x)%R then ((numq x + denq x - 1) %/ (denq x))%Z
  else (- (- numq x %/ denq x)%Z)%R.

Import GRing.Theory Num.Theory.

Lemma le_ceilq x : (x <= (ceilq x)%:Q)%R.
Proof.
rewrite /ceilq.
case: (ratP x) => n d _ {x}.
rewrite ler_pdivrMr ?ltr0z //.
rewrite -intrM ler_int.
case: (leP 0%R) => Hn; last first.
  by rewrite mulNr lerNr lez_floor.
rewrite addrAC.
rewrite -ltzD1 -ltrBlDr.
rewrite divzDr // divzz /=.
exact: ltz_ceil.
Qed.
