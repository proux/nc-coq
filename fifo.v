(******************************************************************************)
(* First in First Out server.                                                 *)
(*                                                                            *)
(* FIFO_service_policy S <-> S is a FIFO server                               *)
(*                                                                            *)
(* The theorem FIFO_delay gives a bound to the delay of each flow of a FIFO   *)
(* server assuming a bound on the service of the aggregate server.            *)
(*                                                                            *)
(******************************************************************************)

From mathcomp Require Import ssreflect ssrfun ssrbool eqtype order ssrnat.
From mathcomp Require Import choice seq fintype finfun bigop ssralg ssrnum rat.
From mathcomp Require Import boolp classical_sets signed reals ereal.
From mathcomp.dioid Require Import dioid complete_lattice complete_dioid.
Require Import analysis_complements RminStruct cumulative_curves arrival_curves.
Require Import servers deviations services NCCoq.usual_functions.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope classical_set_scope.
Local Open Scope ring_scope.
Local Open Scope order_scope.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.

Section Fifo.

Context {R : realType}.

(* def 44 p183 *)
(* Final : def 7.7 p164 *)
Definition FIFO_service_policy n (S : n.+1.-server ) :=
  forall A D, S A D -> forall i j, forall t u : {nonneg R},
    ((A i) u < (D i) t -> (A j) u <= (D j) t)%E.

Lemma FIFO_one_server (S : 1.-server) : FIFO_service_policy S.
Proof. move=> A D HAD i j t u; rewrite !ord1; exact: ltW. Qed.

(* Lem 26 p184 *)
(* Final : Lem 7.2 p 165 *)
Lemma FIFO_aggregate_server n (S : n.+1.-server) :
  FIFO_service_policy S ->
  forall A D, S A D -> forall t u : {nonneg R},
    (\sum_i A i) u <= (\sum_i D i) t <-> forall i, (A i) u <= (D i) t.
Proof.
move=> fifo_server A D hAD t u; split; last first.
{ move=> Hi.
  elim/big_ind2: _ => // x1 x2 y1 y2 Hx Hy.
  by rewrite /GRing.add/=; apply: lee_dD. }
move=> HAD.
suff: all (fun '(ai, di) => ai u <= di t)
          [seq ((A i)%:fcc, (D i)%:fcc) | i <- enum 'I_n.+1].
{ rewrite all_map => /allP /= Gbool i.
  rewrite -!fin_flow_ccE.
  exact/Gbool/mem_enum. }
move: HAD; apply: contraTT.
rewrite -has_predC has_map => /hasP [i _ /=]; rewrite -ltNge => HDA.
rewrite -ltNge.
rewrite !flow_cc_sumE.
rewrite (bigD1 i) //= [in X in (_ < X)%E](bigD1 i) //=.
rewrite -fin_flow_ccE.
under eq_bigr => ? _ do rewrite -fin_flow_ccE.
rewrite dsumEFin.
apply: lte_le_dD => //; [by rewrite -fin_flow_ccE|].
rewrite -dsumEFin.
under [leRHS]eq_bigr => ? _ do rewrite -fin_flow_ccE.
apply: lee_dsum => j _.
rewrite !fin_flow_ccE.
rewrite leNgt; apply/negP => H.
move: HDA; apply/negP; rewrite -leNgt.
rewrite -num_le -lee_fin !fin_flow_ccE.
move: H; exact: fifo_server.
Qed.

Theorem FIFO_delay n (S : n.+1.-server) (S_FIFO : FIFO_service_policy S)
        (alpha : Fup^n.+1) (beta : F0up) i :
  min_service (aggregate_server S) beta ->
  min_service (residual_server_constr S i [ffun i => alpha i : F])
              (delta (hDev (\sum_j alpha j) beta)%:num).
Proof.
have Hbeta0 := F0up0 beta.
move=> Hbeta _ _ [A [D [Had [-> [-> Halpha]]]]].
set del := hDev _ _; move: (erefl del); rewrite {2}/del; clearbody del.
move=> /(f_equal Signed.r).
case: del%:num => {del} [d' | |] Hd'; last first.
{ exfalso; move: (le_refl (-oo%E : \bar R)); apply/negP; rewrite -ltNge {2}Hd'.
  exact: (@lt_le_trans _ _ 0%E). }
{ apply/lefP => t.
  rewrite delta_infty F_dioid_mulE Fup_min_conv_0_r.
  by rewrite flow_cc0. }
have Pd' : 0 <= d' by rewrite -lee_fin Hd' ge0.
pose d'' := NngNum Pd'.
apply/lefP => t.
rewrite F_dioidE -[d'%:E]/(d''%:num%:E) -Fup_dioid_mulE delta_prop_conv.
apply: (proj1 (FIFO_aggregate_server S_FIFO Had _ _)).
rewrite -delta_prop_conv.
rewrite -[d''%:nngnum]/(d') Hd'.
(* have -> : \sum_j alpha j = \big[Fup_plus/Fup_0]_j alpha j. *)
(* { by apply/funext => t'; rewrite F_sumE Fup_sumE. } *)
apply/lefP/(@Service_for_a_jitter _ (\sum_i A i) (aggregate_server S)) => //.
{ have -> : \sum_j alpha j = \sum_j [ffun i => alpha i : F] j :> F.
  { apply/funext => t'.
    by rewrite sumfE Fup_sumE; apply: eq_bigr => ? _; rewrite ffunE. }
  apply: max_arrival_sum_flow_cc => j.
  by move: (Halpha j); rewrite ffunE. }
by exists A; exists D.
Qed.

End Fifo.
