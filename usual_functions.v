(* Copyright : Lucien Rakotomalala, Pierre Roux and Marc Boyer. *)
(**************************************************************************)
(* This files describes some usual functions for network                  *)
(* calculus. They are supported by some properties we plan to use in      *)
(* actual computation of case study.                                      *)
(*                                                                        *)
(*     delta d == the pure delay function (delay d)                       *)
(* gamma_h r b == the token bucket function (rate r, initial bucket b)    *)
(*                                                                        *)
(**************************************************************************)

From mathcomp Require Import ssreflect ssrfun ssrbool eqtype order ssrnat.
From mathcomp Require Import choice seq fintype finfun bigop tuple ssralg ssrnum rat.
From mathcomp Require Import boolp classical_sets signed reals ereal.
From mathcomp.dioid Require Import dioid complete_lattice complete_dioid.
Require Import RminStruct.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope classical_set_scope.
Local Open Scope ring_scope.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.

Section UsualFunctions.

Context {R : realType}.

(** *** Pure delay **)
(* Def 18 p 60 *)
(* Final Def 3.1 p 38 *)
Program Definition delta (d : \bar R) : Fup :=
  Build_Fup
    (Build_Fplus (fun t => if (t%:nngnum%:E <= d)%E then 0%:E else +oo%E) _) _.
Next Obligation.
by move=> d; apply/lefP => t; case: (leP _ d); rewrite ?leey.
Qed.
Next Obligation.
move=> d; apply/asboolP/non_decr_closureP/lefP => t /=.
apply: ub_ereal_sup => _ [x /= Hxt <-].
case: (leP _ d) => Hxd.
{ case: (leP _ d) => [// | _]; exact: leey. }
rewrite ifF //; apply/negbTE; rewrite -ltNge.
by apply: (lt_le_trans Hxd); rewrite lee_fin num_le.
Qed.
Arguments delta d : simpl nomatch.

Lemma delta_infty : delta +oo%E = 0.
Proof. by apply/val_inj/val_inj/funext => t /=; rewrite leey. Qed.

(* Prop 12 p.62 *)
(* Final Prop 3.2 p40 *)
Lemma delta_prop_conv (f : Fup) (d : {nonneg R}) t :
  ((f : Fupd) * (delta d%:nngnum%:E)) t
  = f (insubd 0%:nng (t%:nngnum - d%:nngnum)).
Proof.
case: (leP 0 (t%:nngnum - d%:nngnum)) => S_t_d.
{ have S_t_d' : d <= t; [by rewrite -num_le -subr_ge0|].
  apply/le_anti/andP; split.
  { rewrite Fup_dioidE; apply: ereal_inf_lbound.
    exists ((insubd 0%:nng (t%:nngnum - d%:nngnum)), d).
    { by rewrite /= insubdK // subrK. }
    by rewrite /= lexx dadde0. }
  rewrite Fup_dioidE; apply: lb_ereal_inf => _ [[u v] /= Huv <-].
  case: (leP v%:nngnum%:E) => Hdv; [|by rewrite daddeC /= leey].
  rewrite dadde0; apply/ndFP.
  by rewrite -num_le insubdK //= -Huv lerBlDr lerD2l. }
have -> : insubd 0%:nng (t%:nngnum -d%:nngnum) = 0%:nng.
{ apply/val_inj; rewrite /= /insubd insubF //.
  by apply/negbTE; rewrite -ltNge. }
apply/le_anti/andP; split.
{ apply: (@le_trans _ _ (f 0%:nng + delta d%:num%:E t)).
  { rewrite Fup_dioidE; apply: ereal_inf_lbound.
    by exists (0%:nng, t) => //=; rewrite add0r. }
  by rewrite /delta/= ifT ?addr0// lee_fin -subr_le0 ltW. }
rewrite Fup_dioidE; apply: lb_ereal_inf => _ [[u v] /= Huv <-].
rewrite ifT ?dadde0; first by apply/ndFP; rewrite -num_le /=.
rewrite lee_fin -(lerD2l u%:nngnum) Huv.
move: S_t_d; rewrite subr_lt0 => /ltW S_t_d; apply: (le_trans S_t_d).
by rewrite lerDr.
Qed.

Lemma delta_le d d' : (d' <= d)%E -> (delta d <= delta d' :> F)%O.
Proof.
move=> d'led; apply/lefP => t /=.
have [tled'|tgtd'] := leP _ d'; last by rewrite leey.
by rewrite ifT //; apply: le_trans d'led.
Qed.

(** *** Token bucket *)
Program Definition gamma_h (r b : {nonneg R}) :=
  Build_Fup
    (Build_Fplus (fun t => ((r%:nngnum * t%:nngnum) + b%:nngnum)%:E) _) _.
Next Obligation. by move=> r b; apply/lefP => t; rewrite lee_fin. Qed.
Next Obligation.
move=> r b; apply/asboolP => x y Hxy /=.
by rewrite lee_fin lerD2r; apply: ler_pM.
Qed.

End UsualFunctions.

Arguments delta : simpl nomatch.
