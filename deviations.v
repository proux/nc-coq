(******************************************************************************)
(* Horizontal and vertical deviantions.                                       *)
(*                                                                            *)
(* These notions are used in network calculus to define delay and backlog     *)
(* between two data flows.                                                    *)
(*                                                                            *)
(*      delay A D hAD == the delay between data flows A and D                 *)
(*                       where hAD is a proof that A and D are respectively   *)
(*                       arrival and departure of a server S                  *)
(*    backlog A D hAD == the backlog between data flows A and D               *)
(*                       where hAD is a proof that A and D are respectively   *)
(*                       arrival and departure of a server S                  *)
(*           hDev f g == horizontal deviation between functions f and g       *)
(*           vDev f g == vertical deviation between functions f and g         *)
(*                                                                            *)
(******************************************************************************)

From mathcomp Require Import ssreflect ssrfun ssrbool eqtype order ssrnat.
From mathcomp Require Import choice seq fintype finfun bigop ssralg ssrnum rat.
From mathcomp Require Import boolp classical_sets signed reals ereal.
From mathcomp.dioid Require Import dioid complete_lattice complete_dioid.
Require Import analysis_complements RminStruct cumulative_curves servers.
Require Import NCCoq.usual_functions.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope classical_set_scope.
Local Open Scope ring_scope.
Local Open Scope order_scope.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.

Section Deviations.

Context {R : realType}.

(* Def 5 p 32 *)
(* Final Def 1.5 p10 *)
(** *** Horizontal and vertical deviations **)
Definition hDev_at (f g : F) (t : {nonneg R}) : {nonneg \bar R} :=
  (ereal_inf [set d%:num | d in [set d%:num%:E%:sgn | d in [set d
             | f t <= g (t%:num + d%:nngnum)%:nng]]])%:sgn.

Definition vDev_at (f g : F) (t : {nonneg R}) : \bar R := ((f t) - (g t))%dE.

(** Worst Case hDev and vDev **)
Program Definition hDev (f g : F) : {nonneg \bar R} :=
  @Signed.mk _ _ 0%E ?=0 >=0
    (ereal_sup [set (hDev_at f g t)%:num | t in setT]) _.
Next Obligation.
move=> f g; apply: (@le_trans _ _ (hDev_at f g 0%:nng)%:num) => //.
by apply: ereal_sup_ubound; exists 0%:nng.
Qed.

Definition vDev (f g : F) : \bar R := ereal_sup [set vDev_at f g t | t in setT].

(* Def 3 p30 *)
(* Final Def 1.3 p8 *)
(** *** Delay and Backlog  **)
Definition delay_at (A D : F) (t : {nonneg R}) : {nonneg \bar R} :=
  hDev_at A D t.

Definition backlog_at (A D : flow_cc) t : {nonneg R} :=
  insubd 0%:nng ((A%:fcc t)%:nngnum - (D%:fcc t)%:nngnum).

Lemma backlog_atE A D t (S : partial_server) :
  S A D -> (backlog_at A D t)%:nngnum = (A%:fcc t)%:nngnum - (D%:fcc t)%:nngnum.
Proof.
move=> SAD.
rewrite insubdK // /in_mem /= subr_ge0.
rewrite -[?[a] <= ?[b]]/(?[a]%:E <= ?[b]%:E)%dE !fin_flow_ccE.
move: t; exact/lefP/(server_le SAD).
Qed.

(* Def 4 p 31 *)
(* Final Def 1.4 p9 *)
(** Worst Case - backlog - delay **)
Definition delay (A D : F) : {nonneg \bar R} := hDev A D.

Program Definition backlog (A D : flow_cc) : {nonneg \bar R} :=
  @Signed.mk _ _ 0%E ?=0 >=0
    (ereal_sup [set (backlog_at A D t)%:nngnum%:E | t in setT]) _.
Next Obligation.
move=> A D; apply: (@le_trans _ _ (backlog_at A D 0%:nng)%:nngnum%:E).
{ by rewrite leEereal /=. }
by apply: ereal_sup_ubound; exists 0%:nng.
Qed.

(* Lem 19 p136*)
(* Final Lem 5.1 p116 *)
Lemma sup_horizontal_deviations (f g : Fup) :
  forall t,
    (hDev_at f g t)%:num
    = Order.max
        0%dE
        (ereal_sup [set d%:num%:E | d in [set d
                   | g (t%:num + d%:nngnum)%:nng < f t]]).
Proof.
move=> t.
have [f' Hf'] : exists f' : Fup, forall x, f' x = g (t%:nngnum + x%:nngnum)%:nng.
{ by exists (Fup_shift_l g t) => x /=; apply/f_equal/val_inj; rewrite /= addrC. }
have -> : hDev_at f g t = pseudo_inv_inf_nnR f' (f t)%:sgn.
{ rewrite /hDev_at /pseudo_inv_inf_nnR /=.
  apply/val_inj => /=; apply/f_equal; rewrite predeqP => x.
  split=> -[_ [y /= Hy <-] <-] {x}.
  { by exists y%:num%:E%:sgn => //; exists y => //; rewrite Hf'. }
  by exists y%:num%:E%:sgn => //; exists y => //; rewrite -Hf'. }
rewrite alt_def_pseudo_inv_inf_nnR.
apply/f_equal/f_equal; rewrite predeqP => x; split=> -[y /= Hy <-].
{ by exists y => //; rewrite -Hf'. }
by exists y => //; rewrite Hf'.
Qed.

(* Proposition 41 p 136 *)
(* Final Proposition 5.12 p 115 *)
Proposition vDev_monotonicity (f f' g g' : Fplus) :
  f' <= f :> F -> g <= g' :> F -> vDev f' g' <= vDev f g.
Proof.
move=> Hf Hg.
apply: ub_ereal_sup => _ [x _ <-].
rewrite /vDev_at.
apply: (@le_trans _ _ (f x - g x)%dE); [|by apply: ereal_sup_ubound; exists x].
move: Hg => /lefP /(_ x).
move: Hf => /lefP /(_ x).
case: (f' x) => [{}f' | |]; case: (f x) => [{}f | |] //;
  case: (g' x) => [{}g' | |]; case: (g x) => [{}g | |] //=;
  [|by move=> _ _; rewrite ?leey ?leNye..].
rewrite !leEereal /=; exact: lerB.
Qed.

(* Proposition 41 p 136 *)
(* Final Proposition 5.12 p 115 *)
Proposition hDev_monotonicity (f f' g g' : Fplus) :
  f' <= f :> F -> g <= g' :> F ->
  (hDev f' g')%:num <= (hDev f g)%:num.
Proof.
move=> Hf Hg.
apply: ub_ereal_sup => _ [t _ <-].
apply: (@le_trans _ _ (hDev_at f g t)%:num%E).
2:{ by apply: ereal_sup_ubound; exists t. }
apply: le_ereal_inf => _ [_ [d /= Hd <-] <-].
exists d%:num%:E%:sgn => //; exists d => //.
apply: (@le_trans _ _ (f t)); [by move: t {Hd}; apply/lefP|].
by apply: (le_trans Hd); move: Hg => /lefP.
Qed.

Lemma vDev_suff (A D : F) (b : \bar R) :
  (forall t, A t != -oo%E) -> (forall t, D t != +oo%E) ->
  A <= D + (fun=> b) -> vDev A D <= b.
Proof.
move=> HA HD /lefP H.
apply: ub_ereal_sup => _ [t _ <-].
move: HA HD H => /(_ t) HA /(_ t) HD /(_ t).
rewrite /GRing.add/= /GRing.add/= /vDev_at.
case: (A t) HA => [a | |//] _; case: (D t) HD => [d | // |] _;
  case: b => [b | |] //.
{ by rewrite !lee_fin -lerBlDl. }
move=> _; exact: leey.
Qed.

Lemma hDev_suff (A : Fup) (D : F) (d : {nonneg \bar R}) :
  (A : Fupd) * delta d%:num <= D :> F -> (hDev A D)%:num <= d%:num.
Proof.
case: (@nonnegeP _ d%:num) => // [_|d' H]; [by rewrite leey|].
rewrite leNgt; apply/negP => /ereal_sup_gt[_ [t _ <-]] H'.
case: (leP (A t) (D (t%:nngnum + d'%:nngnum)%:nng)).
{ move=> H''; move: H'; apply/negP; rewrite -leNgt.
  by apply: ereal_inf_lbound; exists d'%:num%:E%:sgn => //; exists d'. }
set t' := (_ + _)%:nng.
have -> : t = insubd 0%:nng (t'%:nngnum - d'%:nngnum).
{ by apply/val_inj; rewrite /= insubdK ?addrK // /in_mem /=. }
by rewrite -delta_prop_conv; apply/negP; rewrite -leNgt; apply/lefP.
Qed.

End Deviations.
