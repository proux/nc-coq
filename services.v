(******************************************************************************)
(* Definitions of services.                                                   *)
(*                                                                            *)
(* Minimal services are specifications of servers in network calculus.        *)
(*                                                                            *)
(*   min_service S beta <-> beta is a (min-plus) minimal service for server S *)
(* strict_min_service S beta <-> beta is a strict minimal service             *)
(*                          for server S                                      *)
(*   max_service S beta <-> beta is a (min-plus) maximal service for server S *)
(* shaper_server sima S <-> the server S is a shaper server                   *)
(*                          with shaping sigma                                *)
(*                                                                            *)
(******************************************************************************)

From mathcomp Require Import ssreflect ssrfun ssrbool eqtype order ssrnat.
From mathcomp Require Import choice seq fintype finfun bigop ssralg ssrnum rat interval.
From mathcomp Require Import boolp classical_sets signed reals ereal.
From mathcomp.dioid Require Import dioid complete_lattice complete_dioid.
Require Import RminStruct cumulative_curves arrival_curves servers deviations.
Require Import NCCoq.usual_functions backlog_itv.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope classical_set_scope.
Local Open Scope ring_scope.
Local Open Scope order_scope.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.

Section Services.

Context {R : realType}.

(** ** Service curves *)

(** Min-plus service *)
(* def p126 *)
(* Final : def 5.3 p106 *)
Definition min_service_for (S : flow_cc -> flow_cc -> Prop) (beta : @F R) A :=
  forall D, S A D -> (A : Fd) * beta <= D :> F.

Definition min_service S beta := forall A, min_service_for S beta A.

(** Strict service *)
(* def p128 *)
(* Final : def 5.5 p108 *)
Definition strict_min_service (S : flow_cc -> flow_cc -> Prop) (beta : F) :=
  forall A D, S A D ->
    forall u v : {nonneg R}, (u%:nngnum <= v%:nngnum)%R ->
      backlog_itv A D `]u, v] ->
      (beta (insubd 0%:nng (v%:nngnum - u%:nngnum))%R <= D v - D u)%dE.

(** Max service *)
(* def p132 *)
(* Final : def 5.6 p113 *)
Definition max_service (S : flow_cc -> flow_cc -> Prop) (beta : @F R) :=
  forall A D, S A D -> D <= (A : Fd) * beta :> F.

Lemma strict_min_service_non_decr_closure S beta :
  strict_min_service S beta -> strict_min_service S (non_decr_closure beta).
Proof.
move=> strict_service_beta A D SAD u v ulev bloguv.
apply: ub_ereal_sup => _ [x /= xlevmu <-].
have upxlev : (u%:nngnum + x%:nngnum <= v%:nngnum)%R.
{ move: xlevmu; rewrite -num_le /= insubdK ?lerBrDl//.
  by rewrite /in_mem /= subr_ge0. }
have -> : (x = insubd 0%:nng (u%:nngnum + x%:nngnum - u%:nngnum))%R.
{ apply/val_inj => /=.
  by rewrite /= insubdK [X in (X - _)%R]addrC addrK// /in_mem /=. }
rewrite -[u%:num +x%:num]/(!!((u%:num + x%:num)%:nng%:num)).
apply: le_trans (strict_service_beta _ _ SAD _ _ _ _) _.
{ by rewrite /= lerDl. }
{ by apply: backlog_itv_sub bloguv; rewrite subitvE lexx. }
apply: lee_dB => //; exact/ndFP.
Qed.

Lemma strict_min_service_max0 S beta :
  strict_min_service S beta ->
  strict_min_service S (fun t => Order.max 0%dE (beta t)).
Proof.
move=> strict_service_beta A D SAD u v ulev bloguv.
rewrite ge_max; apply/andP; split.
{ rewrite -!fin_flow_ccE lee_fin subr_ge0 -lee_fin !fin_flow_ccE; exact/ndFP. }
exact: strict_service_beta bloguv.
Qed.

(** Shaper *)
(* def p133 *)
(* Final : def 5.7 p113 *)
Definition shaper_server (sigma : @Fup R) (S : @flow_cc R -> flow_cc -> Prop) :=
  forall A D, S A D -> D <= (D : Fd) * sigma :> F.

Lemma Fup_zero_is_shaper_server S : shaper_server (0 : Fupd) S.
Proof. by move=> A D H; rewrite mulr0 -F_dioid_leE le0d. Qed.

Lemma Fup_one_is_shaper_server S : shaper_server (1 : Fupd) S.
Proof. by move=> A D H; rewrite mulr1. Qed.

Lemma Fup_zero_max_service (S : partial_server) : max_service S (0 : Fd).
Proof. by move=> A D _; rewrite mulr0 -F_dioid_leE le0d. Qed.

Lemma Fup_one_max_service (S : partial_server) : max_service S (1 : Fd).
Proof. by move=> A D SAD; rewrite mulr1; exact: (server_le SAD). Qed.

(* Th 10 p.135 *)
(* Final : Th 5.2 p.115 *)
(** *** Backlog and delay bounds**)
Theorem delay_bounds (S : partial_server) (alpha beta : Fup) A D :
  S A D -> max_arrival A alpha -> min_service S beta ->
  (delay A D)%:num <= (hDev alpha beta)%:num.
Proof.
move=> SAD Halpha Hbeta.
apply: (@le_trans _ _ (hDev A ((A : Fupd) * beta))%:num).
{ apply: hDev_monotonicity => //; exact: Hbeta. }
apply: ub_ereal_sup => _ [t _ <-].
rewrite sup_horizontal_deviations.
rewrite ge_max; apply/andP; split=> //.
apply: ub_ereal_sup => _ [d + <-].
rewrite [X in X -> _]/= F_dioid_mulE [X in X -> _]/= alt_def_F_min_conv.
rewrite (Rbar_inf_split [set u | (u <= t)%R]) gt_min => /orP[].
{ case E: ereal_inf => [l | |] Hl; last first.
  { exfalso; move: (le_refl (-oo%E : \bar R)); apply/negP; rewrite -ltNge -{2}E.
    apply: (@lt_le_trans _ _ 0%R%:E); [exact: ltNye|].
    by apply: lb_ereal_inf => _ [x _ <-]; apply: dadde_ge0. }
  { by move: Hl; rewrite ltNge leey. }
  suff: exists t, !!(beta (t%:nngnum + d%:num)%:nng%R < alpha t).
  { move=> [t' Ht'].
    apply: (@le_trans _ _ (hDev_at alpha beta t')%:num).
    { rewrite sup_horizontal_deviations maxEle ifT.
      { by apply: ereal_sup_ubound; exists d. }
      apply: ereal_sup_ubound; exists 0%:nng%R => //.
      move: Ht'; apply/le_lt_trans/ndFP.
      by rewrite -num_le /= addr0 lerDl. }
    by apply: ereal_sup_ubound; exists t'. }
  have Peps : (0 < (A%:fcc t)%:nngnum - l)%R.
  { by rewrite subr_gt0 -lte_fin fin_flow_ccE. }
  pose eps := PosNum Peps.
  have: l%:E \is a fin_num by []; rewrite -E => FE.
  have [_ [u [_ /= Hu] <-] Hy] := !!(lb_ereal_inf_adherent (gt0 eps) FE).
  move: Hy; rewrite E /eps [X in X -> _]/= -!EFinD addrCA subrr addr0 => Hy.
  have Pt' : (0 <= t%:nngnum - u%:nngnum)%R.
  { by rewrite subr_ge0 num_le. }
  pose t' := NngNum Pt'.
  exists t'.
  apply: (@lt_le_trans _ _ (A t - A u)%dE).
  { rewrite -!fin_flow_ccE lte_dBrDr// daddeC fin_flow_ccE.
    move: Hy; apply: le_lt_trans; rewrite le_eqVlt; apply/orP; left.
    apply/eqP/f_equal/f_equal/val_inj; rewrite /= insubdK addrAC //.
    exact: addr_ge0. }
  have -> // : t = (u%:nngnum + t'%:nngnum)%:nng%R.
  by apply/val_inj; rewrite /= addrC subrK. }
move=> H; exfalso; move: H; apply/negP; rewrite -leNgt.
apply: lb_ereal_inf => _ [u [/= Hu /negP +] <-]; rewrite -ltNge => Hu'.
apply: (@le_trans _ _(A u)); [exact/ndFP'|exact: lee_dDl].
Qed.

Theorem backlog_bounds (S : partial_server) (alpha beta : Fup) A D :
  S A D -> max_arrival A alpha -> min_service S beta ->
  (backlog A D)%:num <= vDev alpha beta.
Proof.
move=> HAD Halpha Hbeta.
have -> : (backlog A D)%:num = vDev A D.
{ rewrite /backlog /vDev /=; apply: f_equal; rewrite predeqP => x; split.
  { move=> -[y _ <-]; exists y => //.
    rewrite insubdK /vDev_at /= ?dEFinD ?EFinN ?fin_flow_ccE //.
    rewrite /in_mem /= subr_ge0 -lee_fin !fin_flow_ccE.
    exact/lefP/(server_le HAD). }
  move=> -[y _ <-]; exists y => //.
  rewrite insubdK /vDev_at /= ?dEFinD ?EFinN ?fin_flow_ccE //.
  rewrite /in_mem /= subr_ge0 -lee_fin !fin_flow_ccE.
  exact/lefP/(server_le HAD). }
apply: (@le_trans _ _ (vDev A ((A : Fupd) * beta))).
{ apply: vDev_monotonicity => //; exact: Hbeta. }
apply: ub_ereal_sup => _ [t _ <-].
rewrite /vDev_at /= daddeC -fin_flow_ccE -lee_dsubr_addr//.
rewrite leeNl doppeD 1?adde_defC// -EFinN opprK.
apply: lb_ereal_inf => _ [[u v] Huv <-]; rewrite /fst /snd.
rewrite -lee_dsubr_addr// leeNl doppeD 1?adde_defC// -EFinN opprK daddeC.
apply: (@le_trans _ _ (A t - A u - beta v)%dE).
{ rewrite (daddeC (A u)) -!fin_flow_ccE doppeD 1?adde_defC//.
  by rewrite (daddeC (- _)%E) daddeA. }
apply: (@le_trans _ _ (alpha v - beta v)%dE).
2:{ by apply: ereal_sup_ubound; exists v. }
apply: lee_dD => //.
have -> : t = (u%:nngnum + v%:nngnum)%:nng%R.
{ by apply/val_inj; rewrite /= Huv. }
exact: Halpha.
Qed.

(* Th 16 p 163 *)
(* Final Th 6.2 p 143 *)
(** *** Server as a Jitter *)
Theorem Server_as_a_jitter_min (A D : flow_cc) (S : partial_server) (dm : {nonneg R}) :
  S A D ->
  dm%:num%:E <= ereal_inf [set (delay_at A D t)%:num | t in setT] ->
  D <= (A : Fupd) * (delta dm%:num%:E) :> F.
Proof.
move=> HAD Hdm.
apply/lefP => t.
rewrite delta_prop_conv.
case: (ltP t dm) => S_t_dm.
{ have -> : (insubd 0%:nng (t%:nngnum - dm%:nngnum) = 0%:nng)%R.
  { apply/val_inj; rewrite /insubd insubF //.
    by apply/negbTE; rewrite -ltNge subr_lt0 num_lt. }
  rewrite leNgt; apply/negP => HDA.
  move: S_t_dm; apply/negP; rewrite -leNgt.
  rewrite -num_le /= -lee_fin; apply: (le_trans Hdm).
  apply: (@le_trans _ _ (delay_at A D 0%:nng)%:num).
  { by apply: ereal_inf_lbound; exists 0%:nng%R. }
  apply: ereal_inf_lbound.
  exists t%:num%:E%:sgn => //; exists t => //=; apply/ltW.
  set t' := ((_ + _)%:nng).
  by have -> : t' = t by apply/val_inj; rewrite /= add0r. }
case: (leP t 0%:nng%R) => St.
{ have -> : t = 0%:nng%R; [by apply/le_anti; rewrite St /= -num_le /=|].
  by rewrite flow_cc0. }
rewrite -left_cont_closure_flow_cc /left_cont_closure ge_max.
apply/andP; split=> //; apply: ub_ereal_sup => _ [t' /= Ht' <-].
set t'' := (insubd 0%:nng (t%:nngnum - dm%:nngnum))%R.
case: (leP t' t'') => H_t'_t''.
{ apply: (@le_trans _ _ (D t'')); [exact/ndFP|exact/lefP/(server_le HAD)]. }
have S_d : (0 <= t'%:nngnum - t''%:nngnum)%R; [by rewrite subr_ge0 ltW|].
set d := NngNum S_d.
have H_d_dm : d < dm.
{ rewrite -num_lt /= insubdK.
  { by rewrite opprD opprK addrA -ltrBrDr subrr subr_lt0. }
  by rewrite /in_mem /= subr_ge0. }
have -> : t' = (t''%:nngnum + d%:nngnum)%:nng%R.
{ by apply/val_inj; rewrite /= addrCA subrr addr0. }
rewrite leNgt; apply/negP => H_A_D.
move : H_d_dm; apply/negP; rewrite -leNgt -num_le /val -lee_fin.
apply: (le_trans Hdm).
apply: (@le_trans _ _ (delay_at A D t'')%:num).
{ by apply: ereal_inf_lbound; exists t''. }
apply: ereal_inf_lbound.
exists d%:num%:E%:sgn => //; exists d => //; exact/ltW.
Qed.

(* Th 16 p 163 *)
(* Final Th 6.2 p 143 *)
(** *** Server as a Jitter *)
Theorem Server_as_a_jitter_max (A D : flow_cc) (S : partial_server)
    (dM : {nonneg \bar R}) : S A D -> (delay A D)%:num <= dM%:num ->
  (A : Fupd) * delta dM%:num <= D :> F.
Proof.
move: dM => [dM'].
move: dM' => [dM' | |] SdM hS; last first.
{ by exfalso; move: SdM; apply/negP; rewrite -ltNge ltNye. }
{ move=> _; apply/lefP => t.
  apply: (@le_trans _ _ 0%dE) => //=.
  apply: ereal_inf_lbound; exists (0%:nng, t); [by rewrite /= add0r|].
  by rewrite /= leey dadde0 flow_cc0. }
pose dM := NngNum SdM.
rewrite -[dM']/(dM%:nngnum%R) => HdM.
apply/lefP => t.
rewrite delta_prop_conv.
case: (leP (t%:nngnum - dM%:nngnum) 0) => [S_t_dM |].
{ have -> : (insubd 0%:nng (t%:nngnum - dM%:nngnum) = 0%:nng)%R.
  { apply/val_inj => /=.
    move: S_t_dM; rewrite le_eqVlt => /orP[/eqP | ] StdM.
    { by rewrite insubdK // StdM /in_mem /=. }
    by rewrite /insubd insubF //; apply/negbTE; rewrite -ltNge. }
  by rewrite flow_cc0. }
rewrite subr_gt0 => S_t_dM.
set eps := insubd _ _.
rewrite -left_cont_closure_flow_cc /left_cont_closure ge_max.
apply/andP; split=> //; apply: ub_ereal_sup => _ [t'' /= Ht'' <-].
rewrite leNgt; apply/negP => H_Dt.
have S_d : (0 <= t%:nngnum - t''%:nngnum)%R.
{ rewrite subr_ge0 num_le; apply/ltW/(lt_le_trans Ht'').
  rewrite -num_le insubdK /= /in_mem /= ?lerBlDr ?lerDl //.
  by rewrite subr_ge0 ltW. }
pose d := NngNum S_d.
move: Ht''; apply/negP; rewrite -leNgt /eps /=.
rewrite -num_le /= insubdK; [|by rewrite /in_mem /= subr_ge0 ltW].
rewrite lerBlDr -lerBlDl -lee_fin.
move: HdM; apply: le_trans.
apply: (@le_trans _ _ (delay_at A D t'')%:num).
2:{ by apply: ereal_sup_ubound; exists t''. }
apply: lb_ereal_inf => _ [_ [d' /= Hd't <-] <-].
rewrite leNgt; apply/negP => Hdd'.
move: H_Dt; apply/negP; rewrite -leNgt.
apply: (le_trans Hd't).
by apply/ndFP; rewrite -num_le /= -lerBrDl ltW.
Qed.

Corollary Service_for_a_jitter (A : flow_cc) (S : partial_server)
          (alpha beta : Fup) :
  max_arrival A alpha -> min_service S beta ->
  min_service_for S (delta (hDev alpha beta)%:num) A.
Proof.
move => Halpha Hbeta D SAD.
apply: (Server_as_a_jitter_max SAD).
exact: (delay_bounds SAD).
Qed.

Theorem server_concat_conv (S1 S2 : partial_server) (beta1 beta2 : Fup) :
  min_service S1 beta1 -> min_service S2 beta2 ->
  min_service (S1 '; S2)%S ((beta1 : Fupd) * beta2).
Proof.
move=> H1 H2 A D [B [HS1 HS2]].
move: (H1 _ _ HS1) (H2 _ _ HS2).
rewrite !Fup_dioidE -!F_dioid_leE => {}H1 {}H2.
apply: le_trans H2 _.
rewrite mulrA /=.
exact: led_mul2r.
Qed.

End Services.
