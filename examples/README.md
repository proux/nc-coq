This directory contains examples of proofs using network calculus.

Subdirectories
--------------

* `case_study` a simple case study with five flows crossing six servers
