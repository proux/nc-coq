(* Copyright : Lucien Rakotomalala, Pierre Roux and Marc Boyer. *)

(**************************************************************************)
(* This files contains the complete formal proof of the study case        *)
(* presented in the paper "Formal Verification of real Time Network".     *)
(* The network is described in the figure "caseStudy.png" in this         *)
(* folder.                                                                *)
(*                                                                        *)
(* As described in the figure, each flow of data is represented by a      *)
(* Variable "fi_Sj" where i is the number of the flow and j the number of *)
(* server from which the flow exits                                       *)
(*                                                                        *)
(* The variables with capital letters are representing the constraints    *)
(* on each flow : each flow has an arrival curves with respect the        *)
(* definition 5 in the paper. It is the same notation i and j as the      *)
(* flow variables                                                         *)
(**************************************************************************)

(** * Case Study *)
From mathcomp Require Import ssreflect ssrfun ssrbool eqtype order ssrnat.
From mathcomp Require Import choice seq fintype finfun bigop ssralg ssrnum tuple.
From mathcomp Require Import boolp classical_sets signed reals ereal.
From mathcomp.dioid Require Import dioid complete_lattice complete_dioid.
Require Import RminStruct cumulative_curves arrival_curves servers deviations.
Require Import services arrival_curves NCCoq.usual_functions propagation fifo.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope ring_scope.

Import Order.Theory GRing.Theory Num.Theory.

Section CaseStudy.

Context {R : realType}.

(** ** Modelisation *)

(** Entrance flows *)
Variables F1 F2 F3 F4 F5 : @flow_cc R.

(** Arrival for flow F1, F2, F3, F4 and F5 *)
Variables alpha1 alpha2 alpha3 alpha4 alpha5 : @Fup R.

(** Each arrival is a constraint of a entrance flow*)
Hypothesis Halpha1 : max_arrival F1 alpha1.
Hypothesis Halpha2 : max_arrival F2 alpha2.
Hypothesis Halpha3 : max_arrival F3 alpha3.
Hypothesis Halpha4 : max_arrival F4 alpha4.
Hypothesis Halpha5 : max_arrival F5 alpha5.

(** Servers *)
Variables (S1 : @nserver R 3) (S2_1 : @nserver R 1) (S2_2 : @nserver R 1)
          (S2_3 : @nserver R 1) (S3 : @nserver R 2) (S4 : @nserver R 2)
          (S5_1 : @nserver R 3) (S5_2 : @nserver R 2).

(** FIFO hypothesis for S1, S3, S4, S5_1 and S5_2 *)
Hypothesis (H_S1 : FIFO_service_policy S1).
Hypothesis (H_S3 : FIFO_service_policy S3).
Hypothesis (H_S4 : FIFO_service_policy S4).
Hypothesis (H_S5_1 : FIFO_service_policy S5_1).
Hypothesis (H_S5_2 : FIFO_service_policy S5_2).

(** Service for each server *)
(* Variable beta_S1 beta_S2_1 beta_S2_2 beta_S2_3 *)
(*          beta_S3 beta_S4 beta_S5 beta_S6 : F0up. *)

Variable beta1 beta2 beta3 beta4 beta5 : @F0up R.

(** Each service is a constraint for a server *)
Hypothesis Hbeta_S1 : min_service (aggregate_server S1) beta1.
Hypothesis Hbeta_S2_1 : min_service (aggregate_server S2_1) beta2.
Hypothesis Hbeta_S2_2 : min_service (aggregate_server S2_2) beta2.
Hypothesis Hbeta_S2_3 : min_service (aggregate_server S2_3) beta2.
Hypothesis Hbeta_S3 : min_service (aggregate_server S3) beta3.
Hypothesis Hbeta_S4 : min_service (aggregate_server S4) beta4.
Hypothesis Hbeta_S5 : min_service (aggregate_server S5_1) beta5.
Hypothesis Hbeta_S6 : min_service (aggregate_server S5_2) beta5.

(** ** Topology model *)
(** Server 1 ***************************************************)
Variables (F1_S1 F2_S1 F3_S1 : @flow_cc R).

Hypothesis H_server_S1 :
  S1
    (finfun_of_tuple [tuple F1; F2; F3])
    (finfun_of_tuple [tuple F1_S1; F2_S1; F3_S1]).

(** Server 2 - 1 ***************************************************)
Variables F1_S2 : @flow_cc R.

Hypothesis H_server_S2_1 : S2_1 [ffun=> F1_S1] [ffun=> F1_S2].

(** Server 2 - 2 ***************************************************)
Variables F2_S2 : @flow_cc R.

Hypothesis H_server_S2_2 : S2_2 [ffun=> F2_S1] [ffun=> F2_S2].

(** Server 2 - 3 ***************************************************)
Variables F3_S2 : @flow_cc R.

Hypothesis H_server_S2_3 : S2_3 [ffun=> F3_S1] [ffun=> F3_S2].

(** Server 3 ***************************************************)
Variables F1_S3 F4_S3 : @flow_cc R.

Hypothesis H_server_S3 :
  S3
    (finfun_of_tuple [tuple F1_S2; F4])
    (finfun_of_tuple [tuple F1_S3; F4_S3]).

(** Server 4 ***************************************************)
Variables F3_S4 F5_S4 : @flow_cc R.

Hypothesis H_server_S4 :
  S4
    (finfun_of_tuple [tuple F3_S2; F5])
    (finfun_of_tuple [tuple F3_S4; F5_S4]).

(** Server 5 -1 ************************************************)
Variables F1_S5 F2_S5 F3_S5 : @flow_cc R.

Hypothesis H_server_S5_1 :
  S5_1
    (finfun_of_tuple [tuple F1_S3; F2_S2; F3_S4])
    (finfun_of_tuple [tuple F1_S5; F2_S5; F3_S5]).

(** Server 5 -2 ************************************************)
Variables F4_S5 F5_S5 : @flow_cc R.

Hypothesis H_server_S5_2 :
  S5_2
    (finfun_of_tuple [tuple F4_S3; F5_S4])
    (finfun_of_tuple [tuple F4_S5; F5_S5]).

(* begin hide *)
Ltac unfold_tnth :=
  rewrite /tnth ![nth _ _ _]/=.

Section switch_1.

Definition beta_S1 : Fup :=
  delta (hDev (alpha1 + alpha2 + alpha3) beta1)%:num.

Definition alpha_S1 : Fup^3 := finfun_of_tuple [tuple alpha1; alpha2; alpha3].

Lemma min_residual_service_S1 i :
  min_service
    (residual_server_constr S1 i [ffun i => alpha_S1 i : F]) beta_S1.
Proof.
rewrite /beta_S1.
have -> : (alpha1 + alpha2 + alpha3 = \sum_j alpha_S1 j).
{ by rewrite !big_ord_recr big_ord0 /= add0r !ffunE. }
exact: FIFO_delay.
Qed.

Definition alpha1_S1 : Fup := ((alpha1 : Fupd) / beta_S1)%D + delta 0.
Definition alpha2_S1 : Fup := ((alpha2 : Fupd) / beta_S1)%D + delta 0.
Definition alpha3_S1 : Fup := ((alpha3 : Fupd) / beta_S1)%D + delta 0.

Let alpha_S1_out : Fup^3 := finfun_of_tuple [tuple alpha1_S1; alpha2_S1; alpha3_S1].

Lemma alpha_S1_correct i :
  max_arrival (finfun_of_tuple [tuple F1; F2; F3] i) (alpha_S1 i).
Proof.
case: i => -[| [| [|//]]] ?; rewrite !ffunE.
- exact: Halpha1.
- exact: Halpha2.
- exact: Halpha3.
Qed.

Lemma maximal_departure_S1 i :
  max_arrival
    (finfun_of_tuple [tuple F1_S1; F2_S1; F3_S1] i)
    (alpha_S1_out i).
Proof.
case: i => -[| [| [|//]]] Hi; rewrite !ffunE; unfold_tnth;
( rewrite Fup_dioidE; apply: max_arrival_F_min; [|apply: max_arrival_delta_0];
  apply: output_arrival_curve_mp_Fup (@min_residual_service_S1 (Ordinal Hi)) _ => [|//];
  eexists; eexists; split; [exact: H_server_S1|];
  rewrite 2!ffunE; split=> [//|]; split=> [//|j];
  rewrite ffunE; exact: alpha_S1_correct).
Qed.

End switch_1.

Section switch_2_1.

Lemma Halpha1_S2 : max_arrival F1_S1 alpha1_S1.
Proof.
move: (maximal_departure_S1 (inord 0)).
by rewrite !ffunE /inord /insubd insubT.
Qed.

Definition beta_S2_1 : Fup := delta (hDev alpha1_S1 beta2)%:num.

Definition alpha1_S2 : Fup := ((alpha1_S1 : Fupd) / beta_S2_1)%D + delta 0.

Lemma min_residual_service_S2_1 i :
  min_service
    (residual_server_constr S2_1 i [ffun=> alpha1_S1 : F]) beta_S2_1.
Proof.
rewrite /beta_S2_1.
have {2}-> : (alpha1_S1 = \sum_(j < 1) [ffun=> alpha1_S1] j :> F).
{ by rewrite !big_ord_recr big_ord0 [RHS]/= add0r !ffunE. }
have {1}-> : [ffun=> alpha1_S1 : F] = [ffun i : 'I_1 => [ffun=> alpha1_S1] i : F].
{ by apply/ffunP => j; rewrite !ffunE. }
by apply: FIFO_delay; [exact: FIFO_one_server|].
Qed.

Lemma maximal_departure_S2_1 : max_arrival F1_S2 alpha1_S2.
Proof.
rewrite Fup_dioid_addE.
apply: max_arrival_F_min; [|apply: max_arrival_delta_0];
apply: output_arrival_curve_mp_Fup;
  [|exact: (@min_residual_service_S2_1 ord0)|exact: Halpha1_S2].
eexists; eexists; split; [exact: H_server_S2_1|].
rewrite !ffunE; split=> //; split=> // j.
rewrite !ffunE; exact: Halpha1_S2.
Qed.

End switch_2_1.

Section switch_2_2.

Lemma Halpha2_S2 : max_arrival F2_S1 alpha2_S1.
Proof.
move: (maximal_departure_S1 (inord 1)).
by rewrite !ffunE /inord /insubd insubT.
Qed.

Definition beta_S2_2 : Fup := delta (hDev alpha2_S1 beta2)%:num.

Definition alpha2_S2 : Fup := ((alpha2_S1 : Fupd) / beta_S2_2)%D + delta 0.

Lemma min_residual_service_S2_2 i :
  min_service
    (residual_server_constr S2_2 i [ffun=> alpha2_S1 : F]) beta_S2_2.
Proof.
rewrite /beta_S2_2.
have {2}-> : (alpha2_S1 = \sum_(j < 1) [ffun=> alpha2_S1] j :> F).
{ by rewrite !big_ord_recr big_ord0 [RHS]/= add0r !ffunE. }
have {1}-> : [ffun=> alpha2_S1 : F] = [ffun i : 'I_1 => [ffun=> alpha2_S1] i : F].
{ by apply/ffunP => j; rewrite !ffunE. }
by apply: FIFO_delay; [exact: FIFO_one_server|].
Qed.

Lemma maximal_departure_S2_2 : max_arrival F2_S2 alpha2_S2.
Proof.
rewrite Fup_dioid_addE.
apply: max_arrival_F_min; [|apply: max_arrival_delta_0];
apply: output_arrival_curve_mp_Fup;
  [|exact: (@min_residual_service_S2_2 ord0)|exact: Halpha2_S2].
eexists; eexists; split; [exact: H_server_S2_2|].
rewrite !ffunE; split=> //; split=> // j.
rewrite !ffunE; exact: Halpha2_S2.
Qed.

End switch_2_2.

Section switch_2_3.

Definition beta_S2_3 : Fup := delta (hDev alpha3_S1 beta2)%:num.

Definition alpha3_S2 : Fup := ((alpha3_S1 : Fupd) / beta_S2_3)%D + delta 0.

Lemma Halpha3_S2 : max_arrival F3_S1 alpha3_S1.
Proof.
move: (maximal_departure_S1 (inord 2)).
by rewrite !ffunE /inord /insubd insubT.
Qed.

Lemma min_residual_service_S2_3 i :
  min_service
    (residual_server_constr S2_3 i [ffun=> alpha3_S1 : F]) beta_S2_3.
Proof.
rewrite /beta_S2_3.
have {2}-> : (alpha3_S1 = \sum_(j < 1) [ffun=> alpha3_S1] j :> F).
{ by rewrite !big_ord_recr big_ord0 [RHS]/= add0r !ffunE. }
have {1}-> : [ffun=> alpha3_S1 : F] = [ffun i : 'I_1 => [ffun=> alpha3_S1] i : F].
{ by apply/ffunP => j; rewrite !ffunE. }
by apply: FIFO_delay; [exact: FIFO_one_server|].
Qed.

Lemma maximal_departure_S2_3 : max_arrival F3_S2 alpha3_S2.
Proof.
rewrite Fup_dioid_addE.
apply: max_arrival_F_min; [|apply: max_arrival_delta_0];
apply: output_arrival_curve_mp_Fup;
  [|exact: (@min_residual_service_S2_3 ord0)|exact: Halpha3_S2].
eexists; eexists; split; [exact: H_server_S2_3|].
rewrite !ffunE; split=> //; split=> // j.
rewrite !ffunE; exact: Halpha3_S2.
Qed.

End switch_2_3.

Section switch_3.

Definition alpha_S3 : Fup^2 := finfun_of_tuple [tuple alpha1_S2; alpha4].

Definition beta_S3 : Fup := delta (hDev (alpha1_S2 + alpha4) beta3)%:num.

Definition alpha1_S3 : Fup := ((alpha1_S2 : Fupd) / beta_S3)%D + delta 0.
Definition alpha4_S3 : Fup := ((alpha4 : Fupd) / beta_S3)%D + delta 0.

Lemma min_residual_service_S3 i :
  min_service
    (residual_server_constr S3 i [ffun i => alpha_S3 i : F]) beta_S3.
Proof.
rewrite /beta_S3.
have -> : (alpha1_S2 + alpha4 = \sum_j alpha_S3 j).
{ by rewrite !big_ord_recr big_ord0 [RHS]/= add0r !ffunE. }
exact: FIFO_delay.
Qed.

Lemma alpha_S3_correct i :
  max_arrival (finfun_of_tuple [tuple F1_S2; F4] i) (alpha_S3 i).
Proof.
case: i => -[| [|//]] ?; rewrite !ffunE.
- exact: maximal_departure_S2_1.
- exact: Halpha4.
Qed.

Let alpha_S3_out := finfun_of_tuple [tuple alpha1_S3; alpha4_S3].

Lemma maximal_departure_S3 i :
  max_arrival
    (finfun_of_tuple [tuple F1_S3; F4_S3] i) (alpha_S3_out i).
Proof.
case: i => -[| [|//]] Hi; rewrite !ffunE; unfold_tnth;
( rewrite Fup_dioid_addE;
  apply: max_arrival_F_min; [|apply: max_arrival_delta_0];
  apply: output_arrival_curve_mp_Fup;
  [|exact: (@min_residual_service_S3 (Ordinal Hi))
   |(exact: maximal_departure_S2_1 || by [])];
  eexists; eexists; split; [exact: H_server_S3|];
  rewrite !ffunE; split=> //; split=> // j;
  rewrite ffunE; exact: alpha_S3_correct).
Qed.

End switch_3.

Section switch_4.

Definition alpha_S4 : Fup^2 := finfun_of_tuple [tuple alpha3_S2; alpha5].

Definition beta_S4 : Fup := delta (hDev (alpha3_S2 + alpha5) beta4)%:num.

Definition alpha3_S4 : Fup := ((alpha3_S2 : Fupd) / beta_S4)%D + delta 0.
Definition alpha5_S4 : Fup := ((alpha5 : Fupd) / beta_S4)%D + delta 0.

Lemma min_residual_service_S4 i :
  min_service
    (residual_server_constr S4 i [ffun i => alpha_S4 i : F]) beta_S4.
Proof.
rewrite /beta_S4.
have -> : (alpha3_S2 + alpha5 = \sum_j alpha_S4 j).
{ by rewrite !big_ord_recr big_ord0 [RHS]/= add0r !ffunE. }
exact: FIFO_delay.
Qed.

Lemma alpha_S4_correct i :
  max_arrival (finfun_of_tuple [tuple F3_S2; F5] i) (alpha_S4 i).
Proof.
case: i => -[| [|//]] ?; rewrite !ffunE.
- exact: maximal_departure_S2_3.
- exact: Halpha5.
Qed.

Let alpha_S4_out := finfun_of_tuple [tuple alpha3_S4; alpha5_S4].

Lemma maximal_departure_S4 i :
  max_arrival
    (finfun_of_tuple [tuple F3_S4; F5_S4] i) (alpha_S4_out i).
Proof.
case: i => -[| [|//]] Hi; rewrite !ffunE; unfold_tnth;
( rewrite Fup_dioid_addE;
  apply: max_arrival_F_min; [|apply: max_arrival_delta_0];
  apply: output_arrival_curve_mp_Fup;
    [|exact: (@min_residual_service_S4 (Ordinal Hi))
     |(exact: maximal_departure_S2_3 || by [])];
   eexists; eexists; split; [exact: H_server_S4|];
   rewrite !ffunE; split=> //; split=> // j;
   rewrite ffunE; exact: alpha_S4_correct).
Qed.

End switch_4.

Section switch_5_1.

Lemma Halpha1_S5 : max_arrival F1_S3 alpha1_S3.
Proof.
move: (maximal_departure_S3 (inord 0)).
by rewrite !ffunE /inord /insubd insubT.
Qed.

Lemma Halpha3_S5 : max_arrival F3_S4 alpha3_S4.
Proof.
move: (maximal_departure_S4 (inord 0)).
by rewrite !ffunE /inord /insubd insubT.
Qed.

Definition alpha_S5 : Fup^3 := finfun_of_tuple [tuple alpha1_S3; alpha2_S2; alpha3_S4].

Definition beta_S5_1 : Fup :=
  delta (hDev ((alpha1_S3 + alpha2_S2) + alpha3_S4) beta5)%:num.

Lemma min_residual_service_S5_1 i :
  min_service
    (residual_server_constr S5_1 i [ffun i => alpha_S5 i : F]) beta_S5_1.
Proof.
rewrite /beta_S5_1.
have -> : (alpha1_S3 + alpha2_S2 + alpha3_S4 = \sum_j alpha_S5 j).
{ by rewrite !big_ord_recr big_ord0 [RHS]/= add0r !ffunE. }
exact: FIFO_delay.
Qed.

Lemma alpha_S5_correct i :
  max_arrival
    (finfun_of_tuple [tuple F1_S3; F2_S2; F3_S4] i) (alpha_S5 i).
Proof.
case: i => -[| [| [|//]]] ?; rewrite !ffunE.
- exact: Halpha1_S5.
- exact: maximal_departure_S2_2.
- exact: Halpha3_S5.
Qed.

Definition alpha1_S5 : Fup := ((alpha1_S3 : Fupd) / beta_S5_1)%D + delta 0.
Definition alpha2_S5 : Fup := ((alpha2_S2 : Fupd) / beta_S5_1)%D + delta 0.
Definition alpha3_S5 : Fup := ((alpha3_S4 : Fupd) / beta_S5_1)%D + delta 0.

Let alpha_S5_out := finfun_of_tuple [tuple alpha1_S5; alpha2_S5; alpha3_S5].

Lemma maximal_departure_S5 i :
  max_arrival
    (finfun_of_tuple [tuple F1_S5; F2_S5; F3_S5] i)
    (alpha_S5_out i).
Proof.
case: i => -[| [| [|//]]] Hi; rewrite !ffunE; unfold_tnth;
( rewrite Fup_dioid_addE;
  apply: max_arrival_F_min; [|apply: max_arrival_delta_0];
  apply: output_arrival_curve_mp_Fup;
    [|exact: (@min_residual_service_S5_1 (Ordinal Hi))|]);
    [|exact: Halpha1_S5| |exact: maximal_departure_S2_2| | exact: Halpha3_S5];
    (eexists; eexists; split; [exact: H_server_S5_1|];
     rewrite !ffunE; split=> //; split=> // j;
     rewrite ffunE; exact: alpha_S5_correct).
Qed.

End switch_5_1.

Section switch_5_2.

Lemma Halpha4_S5 : max_arrival F4_S3 alpha4_S3.
Proof.
move: (maximal_departure_S3 (inord 1)).
by rewrite !ffunE /inord /insubd insubT.
Qed.

Lemma Halpha5_S5 : max_arrival F5_S4 alpha5_S4.
Proof.
move: (maximal_departure_S4 (inord 1)).
by rewrite !ffunE /inord /insubd insubT.
Qed.

Definition alpha_S5_2 : Fup^2 := finfun_of_tuple [tuple alpha4_S3; alpha5_S4].

Definition beta_S5_2 : Fup :=
  delta (hDev (alpha4_S3 + alpha5_S4) beta5)%:num.

Lemma min_residual_service_S5_2 i :
  min_service
    (residual_server_constr S5_2 i [ffun i => alpha_S5_2 i : F]) beta_S5_2.
Proof.
rewrite /beta_S5_2.
have -> : (alpha4_S3 + alpha5_S4 = \sum_j alpha_S5_2 j).
{ by rewrite !big_ord_recr big_ord0 [RHS]/= add0r !ffunE. }
exact: FIFO_delay.
Qed.

Lemma alpha_S5_2_correct i :
  max_arrival (tnth [tuple F4_S3; F5_S4] i) (alpha_S5_2 i).
Proof.
case: i => -[| [|//]] ?; rewrite !ffunE.
- exact: Halpha4_S5.
- exact: Halpha5_S5.
Qed.

Definition alpha4_S5 : Fup := ((alpha4_S3 : Fupd) / beta_S5_2)%D + delta 0.
Definition alpha5_S5 : Fup := ((alpha5_S4 : Fupd) / beta_S5_2)%D + delta 0.

Let alpha_S5_2_out := finfun_of_tuple [tuple alpha4_S5 ; alpha5_S5].

Lemma maximal_departure_S5_2 i :
  max_arrival
    (finfun_of_tuple [tuple F4_S5; F5_S5] i)
    (alpha_S5_2_out i).
Proof.
case: i => -[| [|//]] Hi; rewrite !ffunE; unfold_tnth;
( rewrite Fup_dioid_addE;
  apply: max_arrival_F_min; [|apply: max_arrival_delta_0];
  apply: output_arrival_curve_mp_Fup;
  [|exact: (@min_residual_service_S5_2 (Ordinal Hi))|]);
  [|exact: Halpha4_S5| | exact: Halpha5_S5];
  (eexists; eexists; split; [exact: H_server_S5_2|];
   rewrite !ffunE; split=> //; split=> // j;
   rewrite 2!ffunE; exact: alpha_S5_2_correct).
Qed.

End switch_5_2.

(* end hide *)
(** * End to end delay **************************************)
(* fi servers are servers crossed by fi                  *)
(* fi_ End To end delay are represented by a service     *)
(* Then proof of these servers have a min plus service   *)
(*  fi_endToend                                          *)
Definition f1_servers :=
  (residual_server_constr S1 (inord 0) [ffun i => alpha_S1 i : F] ';
   residual_server_constr S2_1 (inord 0) [ffun _ => alpha1_S1 : F] ';
   residual_server_constr S3 (inord 0) [ffun i => alpha_S3 i : F] ';
   residual_server_constr S5_1 (inord 0) [ffun i => alpha_S5 i : F])%S.

Lemma f1_servers_crossed : (f1_servers F1 F1_S5).
Proof.
exists F1_S3; split.
{ exists F1_S2; split.
  { exists F1_S1; split.
    { rewrite /inord /insubd insubT.
      eexists; eexists; split; [exact: H_server_S1|];
      rewrite !ffunE; split=> //; split=> // j;
      rewrite ffunE; exact: alpha_S1_correct. }
    rewrite /inord /insubd insubT.
    eexists; eexists; split; [exact: H_server_S2_1|];
    rewrite !ffunE; split=> //; split=> // j;
    rewrite !ffunE; exact: Halpha1_S2. }
  rewrite /inord /insubd insubT.
  eexists; eexists; split; [exact: H_server_S3|];
  rewrite !ffunE; split=> //; split=> // j;
  rewrite ffunE; exact: alpha_S3_correct. }
rewrite /inord /insubd insubT.
eexists; eexists; split; [exact: H_server_S5_1|];
rewrite !ffunE; split=> //; split=> // j;
rewrite ffunE; exact: alpha_S5_correct.
Qed.

Definition beta1_E2E : Fup :=
  (beta_S1 : Fupd) * beta_S2_1 * beta_S3 * beta_S5_1.

Lemma f1_E2E_delay : ((delay F1 F1_S5)%:num <= (hDev alpha1 beta1_E2E)%:num)%E.
Proof.
apply (delay_bounds f1_servers_crossed); [apply Halpha1|].
apply server_concat_conv;
[apply server_concat_conv;
 [apply server_concat_conv;
  [apply min_residual_service_S1|apply min_residual_service_S2_1]
 |apply min_residual_service_S3]|apply min_residual_service_S5_1].
Qed.

Definition f2_servers :=
  (residual_server_constr S1 (inord 1) [ffun i => alpha_S1 i : F] ';
   residual_server_constr S2_2(inord 0) [ffun => alpha2_S1 : F ] ';
   residual_server_constr S5_1 (inord 1) [ffun i => alpha_S5 i : F] )%S.

Lemma f2_servers_crossed : (f2_servers F2 F2_S5).
Proof.
exists F2_S2; split.
{ exists F2_S1; split.
  { rewrite /inord /insubd insubT.
    eexists; eexists; split; [exact: H_server_S1|];
    rewrite !ffunE; split=> //; split=> // j;
    rewrite ffunE; exact: alpha_S1_correct. }
  rewrite /inord /insubd insubT.
  eexists; eexists; split; [exact: H_server_S2_2|];
  rewrite !ffunE; split=> //; split=> // j;
  rewrite !ffunE; exact: Halpha2_S2. }
rewrite /inord /insubd insubT.
eexists; eexists; split; [exact: H_server_S5_1|];
rewrite !ffunE; split=> //; split=> // j;
rewrite ffunE; exact: alpha_S5_correct.
Qed.

Definition beta2_E2E : Fup := (beta_S1 : Fupd) * beta_S2_2 * beta_S5_1.

Lemma f2_E2E_delay : ((delay F2 F2_S5)%:num <= (hDev alpha2 beta2_E2E)%:num)%E.
Proof.
apply (delay_bounds f2_servers_crossed); [apply Halpha2|].
apply server_concat_conv;
[apply server_concat_conv;
 [apply min_residual_service_S1|apply min_residual_service_S2_2]
 |apply min_residual_service_S5_1].
Qed.

Definition f3_servers :=
  ((residual_server_constr S1 (inord 2) [ffun i => alpha_S1 i : F]) ';
   (residual_server_constr S2_3 (inord 0) [ffun _ => alpha3_S1 : F]) ';
   (residual_server_constr S4 (inord 0) [ffun i => alpha_S4 i : F]) ';
   (residual_server_constr S5_1 (inord 2) [ffun i => alpha_S5 i : F]))%S.

Lemma f3_servers_crossed : (f3_servers F3 F3_S5).
Proof.
exists F3_S4; split.
{ exists F3_S2; split.
  { exists F3_S1; split.
    { rewrite /inord /insubd insubT.
      eexists; eexists; split; [exact: H_server_S1|];
      rewrite !ffunE; split=> //; split=> // j;
      rewrite ffunE; exact: alpha_S1_correct. }
    rewrite /inord /insubd insubT.
    eexists; eexists; split; [exact: H_server_S2_3|];
    rewrite !ffunE; split=> //; split=> // j;
    rewrite !ffunE; exact: Halpha3_S2. }
  rewrite /inord /insubd insubT.
  eexists; eexists; split; [exact: H_server_S4|];
  rewrite !ffunE; split=> //; split=> // j;
  rewrite ffunE; exact: alpha_S4_correct. }
rewrite /inord /insubd insubT.
eexists; eexists; split; [exact: H_server_S5_1|];
rewrite !ffunE; split=> //; split=> // j;
rewrite ffunE; exact: alpha_S5_correct.
Qed.

Definition beta3_E2E : Fup := (beta_S1 : Fupd) * beta_S2_3 * beta_S4 * beta_S5_1.

Lemma f3_E2E_delay : ((delay F3 F3_S5)%:num <= (hDev alpha3 beta3_E2E)%:num)%E.
Proof.
apply (delay_bounds f3_servers_crossed); [apply Halpha3|].
apply server_concat_conv;
[apply server_concat_conv;
 [apply server_concat_conv;
  [apply min_residual_service_S1|apply min_residual_service_S2_3]
 |apply min_residual_service_S4]|apply min_residual_service_S5_1].
Qed.

Definition f4_servers :=
  ((residual_server_constr S3 (inord 1) [ffun i => alpha_S3 i : F]) ';
   (residual_server_constr S5_2 (inord 0) [ffun i => alpha_S5_2 i : F]))%S.

Lemma f4_servers_crossed : (f4_servers F4 F4_S5).
Proof.
exists F4_S3; split.
{ rewrite /inord /insubd insubT.
  eexists; eexists; split; [exact: H_server_S3|];
  rewrite !ffunE; split=> //; split=> // j;
  rewrite ffunE; exact: alpha_S3_correct. }
rewrite /inord /insubd insubT.
eexists; eexists; split; [exact: H_server_S5_2|];
rewrite !ffunE; split=> //; split=> // j;
rewrite 2!ffunE; exact: alpha_S5_2_correct.
Qed.

Definition beta_4_E2E : Fup := (beta_S3 : Fupd) * beta_S5_2.

Lemma f4_E2E_delay : ((delay F4 F4_S5)%:num <= (hDev alpha4 beta_4_E2E)%:num)%E.
Proof.
apply (delay_bounds f4_servers_crossed); [apply Halpha4|].
apply server_concat_conv;
[apply min_residual_service_S3|apply min_residual_service_S5_2].
Qed.

Definition f5_servers :=
  ((residual_server_constr S4 (inord 1) [ffun i => alpha_S4 i : F]) ';
   (residual_server_constr S5_2 (inord 1) [ffun i => alpha_S5_2 i : F]))%S.

Lemma f5_servers_crossed : (f5_servers F5 F5_S5).
Proof.
exists F5_S4; split.
{ rewrite /inord /insubd insubT.
  eexists; eexists; split; [exact: H_server_S4|];
  rewrite !ffunE; split=> //; split=> // j;
  rewrite ffunE; exact: alpha_S4_correct. }
rewrite /inord /insubd insubT.
eexists; eexists; split; [exact: H_server_S5_2|];
rewrite !ffunE; split=> //; split=> // j;
rewrite 2!ffunE; exact: alpha_S5_2_correct.
Qed.

Definition beta_5_E2E : Fup := (beta_S4 : Fupd) * beta_S5_2.

Lemma f5_E2E_delay : ((delay F5 F5_S5)%:num <= (hDev alpha5 beta_5_E2E)%:num)%E.
Proof.
apply (delay_bounds f5_servers_crossed); [apply Halpha5|].
apply server_concat_conv;
[apply min_residual_service_S4|apply min_residual_service_S5_2].
Qed.

End CaseStudy.
