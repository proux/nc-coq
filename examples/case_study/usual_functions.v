(* Copyright : Lucien Rakotomalala, Pierre Roux and Marc Boyer. *)

(**************************************************************************)
(* This files contains the complete formal proof of the study case        *)
(* presented in the paper "Formal Verification of real Time Network".     *)
(* The network is described in the figure "caseStudy.png" in this         *)
(* folder.                                                                *)
(*                                                                        *)
(* As described in the figure, each flow of data is represented by a      *)
(* Variable "fi_Sj" where i is the number of the flow and j the number of *)
(* server from which the flow exits                                       *)
(*                                                                        *)
(* The variables with capital letters are representing the constraints    *)
(* on each flow : each flow has an arrival curves with respect the        *)
(* definition 5 in the paper. It is the same notation i and j as the      *)
(* flow variables                                                         *)
(**************************************************************************)

(** * Case Study *)
From mathcomp Require Import all_ssreflect ssralg ssrnum rat ereal signed.
From NCCoq Require Import cumulative_curves arrival_curves services servers.
From NCCoq Require Import fifo arbitrary_flows.
From minerve Require Import tactic.

Local Open Scope ereal_scope.
Local Open Scope ring_scope.
Local Open Scope order_scope.
Local Open Scope dioid_scope.

Import Order.Theory.

Section UsualFunctions.

Context {R : realType}.

Local Notation flow_cc := (@flow_cc R).
Local Notation "n '.-server'" := (@nserver R n) : type_scope.

Definition all_finite : _ -> @seqjs bigQ :=
  map (fun xyrs => (xyrs.1, (xyrs.2.1%:E, (xyrs.2.2.1, xyrs.2.2.2%:E)))).

Section CaseStudy.

(** ** Modelisation *)
(** Entrance flows *)
Variables F1 F2 F3 F4 F5 : flow_cc.

(* delta 0 *)
Definition d0 : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (0%bigQ%:E, (0, +oo%E))) ] |}%bigQ.

Lemma d0_def : d0 = usual_functions.delta 0.
Proof.
(* maybe a bit painful but there should be a genric lemma *)
(* then this should be automated somehow in MinErVe by adding a typeclass
   instance so that the nccoq automatic tactic known about delta *)
Admitted.

(** Arrival for flow F1, F2, F3, F4 and F5 *)
(* Arrival functions *)
Definition arrival_function : @F R := F_of_sequpp {|
  sequpp_T := 2;  (* initial segment *)
  sequpp_d := 1;  (* period *)
  sequpp_c := 20_000;  (* increment for each period *)
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    all_finite
      [:: (0, (0, (20_000, 8_000)))] |}%bigQ.
(* rate of 20Mbit/s *)
(* burst of 1000 bytes = 8000 bits *)

Lemma arrival_function_nonneg : arrival_function \is a nonnegF.
Proof.
(* probably relatively easy using replacing 0 <= arrival_function
   with F_of_sequpp <UPP_encoding_of_0> <= arrival_function
   then completing the proof automatically with nccoq *)
Admitted.

Definition Fplus_arrival_function : Fplus :=
  Build_Fplus arrival_function arrival_function_nonneg.

Lemma arrival_function_nondecr : Fplus_arrival_function \is a nondecrFplus.
Proof. apply/boolp.asboolP/non_decr_closureP; nccoq. Qed.

Definition Fup_arrival_function : Fup :=
  Build_Fup Fplus_arrival_function arrival_function_nondecr.

Definition alpha1 := Fup_arrival_function.
Definition alpha2 := Fup_arrival_function.
Definition alpha3 := Fup_arrival_function.
Definition alpha4 := Fup_arrival_function.
Definition alpha5 := Fup_arrival_function.

(** Each arrival is a constraint of a entrance flow*)
Hypothesis Halpha1 : max_arrival F1 alpha1.
Hypothesis Halpha2 : max_arrival F2 alpha2.
Hypothesis Halpha3 : max_arrival F3 alpha3.
Hypothesis Halpha4 : max_arrival F4 alpha4.
Hypothesis Halpha5 : max_arrival F5 alpha5.

(** Servers *)
Variables (S1 : 3.-server) (S2_1 : 1.-server) (S2_2 : 1.-server)
          (S2_3 : 1.-server) (S3 : 2.-server) (S4 : 2.-server)
          (S5 : 3.-server) (S6 : 2.-server).

(** FIFO hypothesis for S1, S3, S4, S5 and S6 *)
Hypothesis (H_S1 : FIFO_service_policy S1).
Hypothesis (H_S3 : FIFO_service_policy S3).
Hypothesis (H_S4 : FIFO_service_policy S4).
Hypothesis (H_S5 : FIFO_service_policy S5).
Hypothesis (H_S6 : FIFO_service_policy S6).

(** Service for each server *)
(* Services function *)
Definition capacity_function : @F R := F_of_sequpp {|
  sequpp_T := 0;  (* no initial segment *)
  sequpp_d := 1;  (* period *)
  sequpp_c := 100_000;  (* increment for each period *)
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    all_finite
      [:: (0, (0, (100_000, 0)))] |}%bigQ.
(* rate of 100Mbit/s *)
(* linear function (no affine part) *)

Lemma capacity_function_nonneg : capacity_function \is a nonnegF.
Proof.
(* same as arrival_function_nonneg above *)
Admitted.

Definition Fplus_capacity_function : Fplus :=
  Build_Fplus capacity_function capacity_function_nonneg.

Lemma capacity_function_nondecr : Fplus_capacity_function \is a nondecrFplus.
Proof. apply/boolp.asboolP/non_decr_closureP; nccoq. Qed.

Definition Fup_capacity_function : Fup :=
  Build_Fup Fplus_capacity_function capacity_function_nondecr.

Lemma capacity_function_0 : Fup_capacity_function 0%:nng%R == 0%:E.
Proof.
(* maybe a bit tedious but sounds feasible *)
Admitted.

Definition F0up_capacity_function := mk_F0up capacity_function_0.

Definition beta_S1 := F0up_capacity_function.
Definition beta_S2_1 := F0up_capacity_function.
Definition beta_S2_2 := F0up_capacity_function.
Definition beta_S2_3 := F0up_capacity_function.
Definition beta_S3 := F0up_capacity_function.
Definition beta_S4 := F0up_capacity_function.
Definition beta_S5 := F0up_capacity_function.
Definition beta_S6 := F0up_capacity_function.

(** Each service is a constraint for a server *)
Hypothesis Hbeta_S1 : min_service (aggregate_server S1) beta_S1.
Hypothesis Hbeta_S2_1 : min_service (aggregate_server S2_1) beta_S2_1.
Hypothesis Hbeta_S2_2 : min_service (aggregate_server S2_2) beta_S2_2.
Hypothesis Hbeta_S2_3 : min_service (aggregate_server S2_3) beta_S2_3.
Hypothesis Hbeta_S3 : min_service (aggregate_server S3) beta_S3.
Hypothesis Hbeta_S4 : min_service (aggregate_server S4) beta_S4.
Hypothesis Hbeta_S5 : min_service (aggregate_server S5) beta_S5.
Hypothesis Hbeta_S6 : min_service (aggregate_server S6) beta_S6.

(** ** Topology model *)
(** Server 1 ***************************************************)
Variables (F1_S1 F2_S1 F3_S1 : flow_cc).

Hypothesis H_server_S1 :
  S1
    (finfun_of_tuple [tuple F1; F2; F3])
    (finfun_of_tuple [tuple F1_S1; F2_S1; F3_S1]).

(** Server 2 - 1 ***************************************************)
Variables F1_S2 : flow_cc.

Hypothesis H_server_S2_1 : S2_1 [ffun=> F1_S1] [ffun=> F1_S2].

(** Server 2 - 2 ***************************************************)
Variables F2_S2 : flow_cc.

Hypothesis H_server_S2_2 : S2_2 [ffun=> F2_S1] [ffun=> F2_S2 ].

(** Server 2 - 3 ***************************************************)
Variables F3_S2 : flow_cc.

Hypothesis H_server_S2_3 : S2_3 [ffun=> F3_S1] [ffun=> F3_S2].

(** Server 3 ***************************************************)
Variables F1_S3 F4_S3 : flow_cc.

Hypothesis H_server_S3 :
  S3
    (finfun_of_tuple [tuple F1_S2; F4])
    (finfun_of_tuple [tuple F1_S3; F4_S3]).

(** Server 4 ***************************************************)
Variables F3_S4 F5_S4 : flow_cc.

Hypothesis H_server_S4 :
  S4
    (finfun_of_tuple [tuple F3_S2; F5])
    (finfun_of_tuple [tuple F3_S4; F5_S4]).

(** Server 5 ***************************************************)
Variables F1_S5 F2_S5 F3_S5 : flow_cc.

Hypothesis H_server_S5 :
  S5
    (finfun_of_tuple [tuple F1_S3; F2_S2; F3_S4])
    (finfun_of_tuple [tuple F1_S5; F2_S5; F3_S5]).

(** Server 6 ***************************************************)
Variables F4_S6 F5_S6 : flow_cc.

Hypothesis H_server_S6 :
  S6
    (finfun_of_tuple [tuple F4_S3; F5_S4])
    (finfun_of_tuple [tuple F4_S6; F5_S6]).

(** * End to end delay **************************************)

(***********************************************)
(*                  Serveur S1                 *)
(***********************************************)

Definition alpha_S1 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 60_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    all_finite
      [:: (0, (0, (60_000, 24_000)))] |}%bigQ.

(* alpha_S1:= alpha1 + alpha2 + alpha3 *)
Lemma alpha_S1_def : alpha_S1 = (alpha1 : F) + alpha2 + alpha3.
Proof. nccoq. Qed.

(* beta_S1_delay:= hdev(alpha_S1, beta1) *)
Lemma dev_alpha_S1_beta_S1 : hDev_bounded alpha_S1 beta_S1 (6/25).
Proof. nccoq. Qed.

(* beta_S1:= delay( beta_S1_delay ) *)
Definition beta_S1' : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (0%bigQ%:E, (0, 0%bigQ%:E)));
       (6/25, (0%bigQ%:E, (0, +oo%E)))
    ] |}%bigQ.

Lemma beta_S1'_def :
  beta_S1' = usual_functions.delta (ratr (bigQ2rat (6/25)))%:E.
Proof.
(* see above comment on delta *)
Admitted.

(* alpha1_S1':= alpha1 / beta_S1 *)
Definition alpha1_S1' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 12_800, (20_000, EFin 12_800))) ] |}%bigQ.

Lemma alpha1_S1'_ge : (alpha1 : Fd) / beta_S1' <= alpha1_S1' :> F.
Proof. nccoq. Qed.

(* alpha1_S1:= alpha1_S1 /\ d0 *)
Definition alpha1_S1 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 12_800))) ] |}%bigQ.

Lemma alpha1_S1_def : alpha1_S1' \min d0 = alpha1_S1.
Proof. nccoq. Qed.

(* Pareil pour alpha2 et alpha3 *)
Definition alpha2_S1 := alpha1_S1.
Definition alpha3_S1 := alpha1_S1.

(***********************************************)
(*                  Serveur S2 1               *)
(***********************************************)

(* beta_S2_1_delay:= hdev( alpha1_S1, beta2 ) *)
Lemma dev_alpha1_S1_beta_S2_1 : hDev_bounded alpha1_S1 beta_S2_1 (16/125)%bigQ.
Proof. nccoq. Qed.

(* beta_S2_1:= delay( beta_S2_1_delay ) *)
Definition beta_S2_1' : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (0%bigQ%:E, (0, 0%bigQ%:E)));
       (16/125, (0%bigQ%:E, (0, +oo%E)))
    ] |}%bigQ.

Lemma beta_S2_1'_def :
  beta_S2_1' = usual_functions.delta (ratr (bigQ2rat (16/125)))%:E.
Proof.
(* see above comment on delta *)
Admitted.

(* alpha1_S2' := ( alpha1_S1 / beta_S2_1 ) *)
Definition alpha1_S2' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 15_360, (20_000, EFin 15_360))) ] |}%bigQ.

Lemma alpha1_S2'_ge : (alpha1_S1 : Fd) / beta_S2_1 <= alpha1_S2' :> F.
Proof. nccoq. Qed.

(* alpha1_S2 := alpha1_S2' /\ d0 *)
Definition alpha1_S2 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0%bigQ, (20_000, EFin 15_360))) ] |}%bigQ.

Lemma alpha1_S2_def : alpha1_S2' \min d0 = alpha1_S2.
Proof. nccoq. Qed.

(***********************************************)
(*                  Serveur S2 2               *)
(***********************************************)

(* beta_S2_2_delay:= hdev( alpha2_S1, beta2 ) *)
Lemma dev_alpha2_S1_beta_S2_2 : hDev_bounded alpha2_S1 beta_S2_2 (16/125)%bigQ.
Proof. nccoq. Qed.

(* beta_S2_2:= delay( beta_S2_2_delay ) *)
Definition beta_S2_2' : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (0%bigQ%:E, (0, 0%bigQ%:E)));
       (16/125, (0%bigQ%:E, (0, +oo%E)))
    ] |}%bigQ.

Lemma beta_S2_2'_def :
  beta_S2_2' = usual_functions.delta (ratr (bigQ2rat (16/125)))%:E.
Proof.
(* see above comment on delta *)
Admitted.

(* alpha2_S2' := ( alpha1_S1 / beta_S2_1 ) *)
Definition alpha2_S2' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 15_360, (20_000, EFin 15_360))) ] |}%bigQ.

Lemma alpha2_S2'_ge : (alpha2_S1 : Fd) / beta_S2_2 <= alpha2_S2' :> F.
Proof. nccoq. Qed.

(* alpha2_S2 := alpha2_S2' /\ d0 *)
Definition alpha2_S2 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 15_360))) ] |}%bigQ.

Lemma alpha2_S2_def : alpha2_S2' \min d0 = alpha2_S2.
Proof. nccoq. Qed.

(***********************************************)
(*                  Serveur S2 3               *)
(***********************************************)

(* beta_S2_3_delay:= hdev( alpha1_S1, beta2 ) *)
Lemma dev_alpha1_S1_beta_S2_3 : hDev_bounded alpha1_S1 beta_S2_3 (16/125)%bigQ.
Proof. nccoq. Qed.

(* beta_S2_3:= delay( beta_S2_3_delay ) *)
Definition beta_S2_3' : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (0%bigQ%:E, (0, 0%bigQ%:E)));
       (16/125, (0%bigQ%:E, (0, +oo%E)))
    ] |}%bigQ.

Lemma beta_S2_3'_def :
  beta_S2_3' = usual_functions.delta (ratr (bigQ2rat (16/125)))%:E.
Proof.
(* see above comment on delta *)
Admitted.

(* alpha1_S2' := ( alpha1_S1 / beta_S2_3 ) *)
Definition alpha3_S2' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 15_360, (20_000, EFin 15_360))) ] |}%bigQ.

Lemma alpha3_S2'_ge : (alpha3_S1 : Fd) / beta_S2_3 <= alpha3_S2' :> F.
Proof. nccoq. Qed.

(* alpha1_S2 := alpha1_S2' /\ d0 *)
Definition alpha3_S2 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 15_360))) ] |}%bigQ.

Lemma alpha3_S2_def : alpha3_S2' \min d0 = alpha3_S2.
Proof. nccoq. Qed.

(***********************************************)
(*                  Serveur S3                 *)
(***********************************************)

(* alpha_S3 := alpha1_S2 + alpha4 *)
Definition alpha_S3 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 40_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (40_000, EFin 23_360)))] |}%bigQ.

(* beta_S3_delay:= hdev( alpha_S3 , beta2 ) *)
Lemma dev_alpha_S3_beta_S3 : hDev_bounded alpha_S3 beta_S3 (146/625)%bigQ.
Proof. nccoq. Qed.

(* beta_S3 := delay( beta_S3_delay ) *)
Definition beta_S3' : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (0%bigQ%:E, (0, 0%bigQ%:E)));
       (146/625, (0%bigQ%:E, (0, +oo%E)))
    ] |}%bigQ.

Lemma beta_S3'_def :
  beta_S3' = usual_functions.delta (ratr (bigQ2rat (146/625)))%:E.
Proof.
(* see above comment on delta *)
Admitted.

(* alpha1_S3':= (alpha1_S2 / beta_S3 ) *)
Definition alpha1_S3' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 20_032, (20_000, EFin 20_032))) ] |}%bigQ.

Lemma alpha1_S3'_ge : (alpha1_S2 : Fd) / beta_S3 <= alpha1_S3' :> F.
Proof. nccoq. Qed.

(* alpha4_S3':= (alpha4    / beta_S3 ) *)
Definition alpha4_S3' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 12_672, (20_000, EFin 12_672))) ] |}%bigQ.

Lemma alpha4_S3'_ge : (alpha4 : Fd) / beta_S3' <= alpha4_S3' :> F.
Proof. nccoq. Qed.

(* alpha1_S3 := alpha1_S3' /\ d0 *)
Definition alpha1_S3 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 20_032))) ] |}%bigQ.

Lemma alpha1_S3_def : alpha1_S3' \min d0 = alpha1_S3.
Proof. nccoq. Qed.

(* alpha4_S3 := alpha4_S3' /\ d0 *)
Definition alpha4_S3 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 12_672))) ] |}%bigQ.

Lemma alpha4_S3_def : alpha4_S3' \min d0 = alpha4_S3.
Proof. nccoq. Qed.

(***********************************************)
(*                  Serveur S4                 *)
(***********************************************)

(* alpha_S4 := alpha3_S2 + alpha5 *)
Definition alpha_S4 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 40_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (40_000, EFin 23_360)))] |}%bigQ.

(* beta_S4_delay:= hdev( alpha_S4 , beta4 ) *)
Lemma dev_alpha_S4_beta_S4 : hDev_bounded alpha_S4 beta_S4 (146/625)%bigQ.
Proof. nccoq. Qed.

(* beta_S4 := delay( beta_S4_delay ) *)
Definition beta_S4' : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (0%bigQ%:E, (0, 0%bigQ%:E)));
       (146/625, (0%bigQ%:E, (0, +oo%E)))
    ] |}%bigQ.

Lemma beta_S4'_def :
  beta_S4' = usual_functions.delta (ratr (bigQ2rat (146/625)))%:E.
Proof.
(* see above comment on delta *)
Admitted.

(* alpha1_S4':= (alpha3_S2 / beta_S4 ) *)
Definition alpha3_S4' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 20_032, (20_000, EFin 20_032))) ] |}%bigQ.

Lemma alpha3_S4'_ge : (alpha3_S2 : Fd) / beta_S4 <= alpha3_S4' :> F.
Proof. nccoq. Qed.

(* alpha5_S4':= (alpha5    / beta_S4 ) *)
Definition alpha5_S4' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 12_672, (20_000, EFin 12_672))) ] |}%bigQ.

Lemma alpha5_S4'_ge : (alpha5 : Fd) / beta_S4' <= alpha5_S4' :> F.
Proof. nccoq. Qed.

(* alpha3_S4:= alpha3_S4 /\ d0 *)
Definition alpha3_S4 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 20_032))) ] |}%bigQ.

Lemma alpha3_S4_def : alpha3_S4' \min d0 = alpha3_S4.
Proof. nccoq. Qed.

(* alpha5_S4:=  alpha5_S4 /\ d0 *)
Definition alpha5_S4 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 12_672))) ] |}%bigQ.

Lemma alpha5_S4_def : alpha5_S4' \min d0 = alpha5_S4.
Proof. nccoq. Qed.

(***********************************************)
(*                  Serveur S5_1               *)
(***********************************************)

(* alpha_S5_1:= alpha1_S3 + alpha2_S2 + alpha3_S4 *)
Definition alpha_S5_1 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 60_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (60_000, EFin 55424)))] |}%bigQ.

(* beta_S5_1_delay:= hdev( alpha_S5_1 , beta5 ) *)
Lemma dev_alpha_S5_1_beta_S5 : hDev_bounded alpha_S5_1 beta_S5 (1732/3125)%bigQ.
Proof. nccoq. Qed.

(* beta_S5_1 := delay( beta_S5_1_delay ) *)
Definition beta_S5_1 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (0%bigQ%:E, (0, 0%bigQ%:E)));
       (1732/3125, (0%bigQ%:E, (0, +oo%E)))
    ] |}%bigQ.

Lemma beta_S5_1_def :
  beta_S5_1 = usual_functions.delta (ratr (bigQ2rat (1732/3125)))%:E.
Proof.
(* see above comment on delta *)
Admitted.

(***********************************************)
(*                  Serveur S5_2               *)
(***********************************************)

(* alpha_S5_2:= alpha4_S3 + alpha5_S4 *)
Definition alpha_S5_2 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 40_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (40_000, EFin 25344)))] |}%bigQ.

(* beta_S5_2_delay:= hdev( alpha_S5_2 , beta5 ) *)
Lemma dev_alpha_S5_2_beta_S5 : hDev_bounded alpha_S5_2 beta_S5 (792/3125)%bigQ.
Proof. nccoq. Qed.

(* beta_S5_2 := delay( beta_S5_2_delay ) *)
Definition beta_S5_2 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (0%bigQ%:E, (0, 0%bigQ%:E)));
       (792/3125, (0%bigQ%:E, (0, +oo%E)))
    ] |}%bigQ.

Lemma beta_S5_2_def :
  beta_S5_1 = usual_functions.delta (ratr (bigQ2rat (792/3125)))%:E.
Proof.
(* see above comment on delta *)
Admitted.

(******************************************************)
(*               End to end delay                     *)
(******************************************************)

(* f1 *)

(* beta1_E2E:= ( beta_S1 * beta_S2_1 * beta_S3 * beta_S5_1 *)
Definition beta1_E2E : @F R := F_of_sequpp {|
  sequpp_T := 2;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (0%bigQ%:E, (0, 0%bigQ%:E)));
       (3612/3125, (0%bigQ%:E, (0, +oo%E)))
    ] |}%bigQ.

Lemma beta1_E2E_def :
  beta1_E2E = ((beta_S1' : Fd) * beta_S2_1' * beta_S3' * beta_S5_1).
Proof. nccoq. Qed.

(* f1_E2E_delay:= hdev(alpha1, f1_E2E_service) *)
Lemma dev_alpha1_beta1_E2E : hDev_bounded alpha1 beta1_E2E (3612/3125)%bigQ.
Proof. nccoq. Qed.

Lemma beta1_E2E_nonneg : beta1_E2E \is a nonnegF.
Proof.
(* see other _nonneg above *)
Admitted.

Lemma alpha1_2_3_nonneg : (alpha1 : F) + alpha2 + alpha3 \is a nonnegF.
Proof.
(* see other _nonneg above *)
Admitted.

Lemma alpha_S1_nonneg : alpha_S1 \is a nonnegF.
Proof.
(* see other _nonneg above *)
Admitted.

Lemma f1_E2E_delay_f :
  ((delay F1 F1_S5)%:num <= (ratr (bigQ2rat (3612/3125)))%:E)%E.
Proof.
apply: le_trans
  (f1_E2E_delay Halpha1 Halpha2 Halpha3 Halpha4 Halpha5 H_S1 H_S3 H_S4 H_S5
     Hbeta_S1 Hbeta_S2_1 Hbeta_S2_2 Hbeta_S2_3 Hbeta_S3 Hbeta_S4 Hbeta_S5
     H_server_S1 H_server_S2_1 H_server_S2_2 H_server_S2_3 H_server_S3
     H_server_S4 H_server_S5) _ => //.
move: dev_alpha1_beta1_E2E; rewrite unlock; apply: le_trans.
rewrite -[beta1_E2E]/(Build_Fplus _ beta1_E2E_nonneg : F).
apply: hDev_monotonicity => //.
rewrite [X in X <= _]/= beta1_E2E_def.
rewrite /arbitrary_flows.beta1_E2E !Fup_dioid_mulE.
rewrite -!F_dioidE.
apply: dioid.led_mul; [apply: dioid.led_mul|]; [apply: dioid.led_mul| |].
- rewrite beta_S1'_def; apply: usual_functions.delta_le.
  move: dev_alpha_S1_beta_S1; rewrite unlock; apply: le_trans.
  rewrite -[(_ + alpha3) : F]/(Build_Fplus _ alpha1_2_3_nonneg : F).
  rewrite -[alpha_S1]/(Build_Fplus _ alpha_S1_nonneg : F).
  by apply: hDev_monotonicity => //; rewrite /Fplus_val/= alpha_S1_def.
- admit.  (* probably similar to above *)
- admit.  (* probably similar to above *)
- admit.  (* probably similar to above *)
Admitted.

End CaseStudy.

End UsualFunctions.
