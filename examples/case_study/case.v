From mathcomp Require Import all_ssreflect.
From minerve Require Import tactic.

Local Open Scope ereal_scope.
Local Open Scope ring_scope.
Local Open Scope order_scope.
Local Open Scope dioid_scope.

Section Case.

Variable R : realType.

Definition d0 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (0, +oo%E))) ] |}%bigQ.

(* Contrainte *)

Definition alpha1 : @F R := F_of_sequpp {|
  sequpp_T := 2;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 8_000)))] |}%bigQ.

Definition alpha2 := alpha1.
Definition alpha3 := alpha1.
Definition alpha4 := alpha1.
Definition alpha5 := alpha1.

Definition beta1 : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 100_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (100_000, EFin 0)))] |}%bigQ.

Definition beta2 := beta1.
Definition beta3 := beta1.
Definition beta4 := beta1.
Definition beta5 := beta1.

(***********************************************)
(*                  Serveur S1                 *)
(***********************************************)

Definition alpha_S1 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 60_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (60_000, EFin 24_000)))] |}%bigQ.

(* alpha_S1:= alpha1 + alpha2 + alpha3 *)
Goal alpha_S1 = alpha1 + alpha2 + alpha3.
Proof. nccoq. Qed.

(* beta_S1_delay:= hdev(alpha_S1, beta1) *)
Goal hDev_bounded alpha_S1 beta1 (6/25).
Proof. nccoq. Qed.

(* beta_S1:= delay( beta_S1_delay ) *)
Definition beta_S1 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (0, EFin 0)));
       (6/25, (EFin 0, (0, +oo%E)))
    ] |}%bigQ.

(* alpha1_S1':= alpha1 / beta_S1 *)
Definition alpha1_S1' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 12_800, (20_000, EFin 12_800))) ] |}%bigQ.

Goal (alpha1 : Fd) / beta_S1 <= alpha1_S1' :> F.
Proof. nccoq. Qed.

(* alpha1_S1:= alpha1_S1 /\ d0 *)
Definition alpha1_S1 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 12_800))) ] |}%bigQ.

Goal alpha1_S1' \min d0 = alpha1_S1.
Proof. nccoq. Qed.

(* Pareil pour alpha2 et alpha3 *)
Definition alpha2_S1 := alpha1_S1.
Definition alpha3_S1 := alpha1_S1.

(***********************************************)
(*                  Serveur S2 1               *)
(***********************************************)

(* beta_S2_1_delay:= hdev( alpha1_S1, beta2 ) *)
Goal hDev_bounded alpha1_S1 beta2 (16/125)%bigQ.
Proof. nccoq. Qed.

(* beta_S2_1:= delay( beta_S2_1_delay ) *)
Definition beta_S2_1 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (0, EFin 0)));
       (16/125, (EFin 0, (0, +oo%E)))
    ] |}%bigQ.

(* alpha1_S2' := ( alpha1_S1 / beta_S2_1 ) *)
Definition alpha1_S2' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 15_360, (20_000, EFin 15_360))) ] |}%bigQ.

Goal (alpha1_S1 : Fd) / beta_S2_1 <= alpha1_S2' :> F.
Proof. nccoq. Qed.

(* alpha1_S2 := alpha1_S2' /\ d0 *)
Definition alpha1_S2 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 15_360))) ] |}%bigQ.

Goal alpha1_S2' \min d0 = alpha1_S2.
Proof. nccoq. Qed.

(***********************************************)
(*                  Serveur S2 2               *)
(***********************************************)

(* beta_S2_2_delay:= hdev( alpha2_S1, beta2 ) *)
Goal hDev_bounded alpha2_S1 beta2 (16/125)%bigQ.
Proof. nccoq. Qed.

(* beta_S2_2:= delay( beta_S2_2_delay ) *)
Definition beta_S2_2 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (0, EFin 0)));
       (16/125, (EFin 0, (0, +oo%E)))
    ] |}%bigQ.

(* alpha2_S2' := ( alpha1_S1 / beta_S2_1 ) *)
Definition alpha2_S2' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 15_360, (20_000, EFin 15_360))) ] |}%bigQ.

Goal (alpha2_S1 : Fd) / beta_S2_2 <= alpha2_S2' :> F.
Proof. nccoq. Qed.

(* alpha2_S2 := alpha2_S2' /\ d0 *)
Definition alpha2_S2 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 15_360))) ] |}%bigQ.

Goal alpha2_S2' \min d0 = alpha2_S2.
Proof. nccoq. Qed.

(***********************************************)
(*                  Serveur S2 3               *)
(***********************************************)

(* beta_S2_3_delay:= hdev( alpha1_S1, beta2 ) *)
Goal hDev_bounded alpha1_S1 beta2 (16/125)%bigQ.
Proof. nccoq. Qed.

(* beta_S2_3:= delay( beta_S2_3_delay ) *)
Definition beta_S2_3 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (0, EFin 0)));
       (16/125, (EFin 0, (0, +oo%E)))
    ] |}%bigQ.

(* alpha1_S2' := ( alpha1_S1 / beta_S2_3 ) *)
Definition alpha3_S2' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 15_360, (20_000, EFin 15_360))) ] |}%bigQ.

Goal (alpha3_S1 : Fd) / beta_S2_3 <= alpha3_S2' :> F.
Proof. nccoq. Qed.

(* alpha1_S2 := alpha1_S2' /\ d0 *)
Definition alpha3_S2 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 15_360))) ] |}%bigQ.

Goal alpha3_S2' \min d0 = alpha3_S2.
Proof. nccoq. Qed.

(***********************************************)
(*                  Serveur S3                 *)
(***********************************************)

(* alpha_S3 := alpha1_S2 + alpha4 *)
Definition alpha_S3 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 40_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (40_000, EFin 23_360)))] |}%bigQ.

(* beta_S3_delay:= hdev( alpha_S3 , beta2 ) *)
Goal hDev_bounded alpha_S3 beta2 (146/625)%bigQ.
Proof. nccoq. Qed.

(* beta_S3 := delay( beta_S3_delay ) *)
Definition beta_S3 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (0, EFin 0)));
       (146/625, (EFin 0, (0, +oo%E)))
    ] |}%bigQ.

(* alpha1_S3':= (alpha1_S2 / beta_S3 ) *)
Definition alpha1_S3' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 20_032, (20_000, EFin 20_032))) ] |}%bigQ.

Goal (alpha1_S2 : Fd) / beta_S3 <= alpha1_S3' :> F.
Proof. nccoq. Qed.

(* alpha4_S3':= (alpha4    / beta_S3 ) *)
Definition alpha4_S3' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 12_672, (20_000, EFin 12_672))) ] |}%bigQ.

Goal (alpha4 : Fd) / beta_S3 <= alpha4_S3' :> F.
Proof. nccoq. Qed.

(* alpha1_S3 := alpha1_S3'	 /\ d0 *)
Definition alpha1_S3 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 20_032))) ] |}%bigQ.

Goal alpha1_S3' \min d0 = alpha1_S3.
Proof. nccoq. Qed.

(* alpha4_S3 := alpha4_S3'	 /\ d0 *)
Definition alpha4_S3 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 12_672))) ] |}%bigQ.

Goal alpha4_S3' \min d0 = alpha4_S3.
Proof. nccoq. Qed.

(***********************************************)
(*                  Serveur S4                 *)
(***********************************************)

(* alpha_S4 := alpha3_S2 + alpha5 *)
Definition alpha_S4 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 40_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (40_000, EFin 23_360)))] |}%bigQ.

(* beta_S4_delay:= hdev( alpha_S4 , beta4 ) *)
Goal hDev_bounded alpha_S4 beta4 (146/625)%bigQ.
Proof. nccoq. Qed.

(* beta_S4 := delay( beta_S4_delay ) *)
Definition beta_S4 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (0, EFin 0)));
       (146/625, (EFin 0, (0, +oo%E)))
    ] |}%bigQ.

(* alpha1_S4':= (alpha3_S2 / beta_S4 ) *)
Definition alpha3_S4' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 20_032, (20_000, EFin 20_032))) ] |}%bigQ.

Goal (alpha3_S2 : Fd) / beta_S4 <= alpha3_S4' :> F.
Proof. nccoq. Qed.

(* alpha5_S4':= (alpha5    / beta_S4 ) *)
Definition alpha5_S4' : @F R := F_of_sequpp {|
  sequpp_T := 0;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 12_672, (20_000, EFin 12_672))) ] |}%bigQ.

Goal (alpha5 : Fd) / beta_S4 <= alpha5_S4' :> F.
Proof. nccoq. Qed.

(* alpha3_S4:= alpha3_S4 /\ d0 *)
Definition alpha3_S4 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 20_032))) ] |}%bigQ.

Goal alpha3_S4' \min d0 = alpha3_S4.
Proof. nccoq. Qed.

(* alpha5_S4:=  alpha5_S4 /\ d0 *)
Definition alpha5_S4 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 20_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (20_000, EFin 12_672))) ] |}%bigQ.

Goal alpha5_S4' \min d0 = alpha5_S4.
Proof. nccoq. Qed.

(***********************************************)
(*                  Serveur S5_1               *)
(***********************************************)

(* alpha_S5_1:= alpha1_S3 + alpha2_S2 + alpha3_S4 *)
Definition alpha_S5_1 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 60_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (60_000, EFin 55424)))] |}%bigQ.

(* beta_S5_1_delay:= hdev( alpha_S5_1 , beta5 ) *)
Goal hDev_bounded alpha_S5_1 beta5 (1732/3125)%bigQ.
Proof. nccoq. Qed.

(* beta_S5_1 := delay( beta_S5_1_delay ) *)
Definition beta_S5_1 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (0, EFin 0)));
       (1732/3125, (EFin 0, (0, +oo%E)))
    ] |}%bigQ.

(***********************************************)
(*                  Serveur S5_2               *)
(***********************************************)

(* alpha_S5_2:= alpha4_S3 + alpha5_S4 *)
Definition alpha_S5_2 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 40_000;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (40_000, EFin 25344)))] |}%bigQ.

(* beta_S5_2_delay:= hdev( alpha_S5_2 , beta5 ) *)
Goal hDev_bounded alpha_S5_2 beta5 (792/3125)%bigQ.
Proof. nccoq. Qed.

(* beta_S5_2 := delay( beta_S5_2_delay ) *)
Definition beta_S5_2 : @F R := F_of_sequpp {|
  sequpp_T := 1;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (0, EFin 0)));
       (792/3125, (EFin 0, (0, +oo%E)))
    ] |}%bigQ.


(******************************************************)
(*               End to end delay                     *)
(******************************************************)

(* f1 *)

(* beta1_E2E:= ( beta_S1 * beta_S2_1 * beta_S3 * beta_S5_1 *)
Definition beta1_E2E : @F R := F_of_sequpp {|
  sequpp_T := 2;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := (* list of (x, (y, (rho, sigma))) *)
    [:: (0, (EFin 0, (0, EFin 0)));
       (3612/3125, (EFin 0, (0, +oo%E)))
    ] |}%bigQ.

Goal beta1_E2E = ((beta_S1 : Fd) * beta_S2_1 * beta_S3 * beta_S5_1).
Proof. nccoq. Qed.

(* f1_E2E_delay:= hdev(alpha1, f1_E2E_service) *)
Goal hDev_bounded alpha1 beta1_E2E (3612/3125)%bigQ.
Proof.
nccoq.
Qed.

(* f2 *)

(* beta2_E2E:= (beta_S1 * beta_S2_2 * beta_S5_1 *)
Definition beta2_E2E : @F R := F_of_sequpp {|
  sequpp_T := 2;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := [:: (0, (EFin 0, (0, EFin 0)));
                  (2882/3125, (EFin 0, (0, +oo%E))) ] |}%bigQ.

Goal beta2_E2E = ((beta_S1 : Fd) * beta_S2_2 * beta_S5_1).
Proof. nccoq. Qed.

(* f2_E2E_delay:= hdev(alpha2, f2_E2E_service) *)
Goal hDev_bounded alpha2 beta2_E2E (2882/3125)%bigQ.
Proof.
nccoq.
Qed.

(* f3 *)

(* beta3_E2E:= ( beta_S1 * beta_S2_3 * beta_S4 * beta_S5_1 *)
Definition beta3_E2E : @F R := F_of_sequpp {|
  sequpp_T := 2;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := [:: (0, (EFin 0, (0, EFin 0)));
                  (3612/3125, (EFin 0, (0, +oo%E))) ] |}%bigQ.

Goal beta3_E2E = ((beta_S1 : Fd) * beta_S2_3 * beta_S4 * beta_S5_1).
Proof. nccoq. Qed.

(* f3_E2E_delay:= hdev(alpha3, f3_E2E_service) *)
Goal hDev_bounded alpha3 beta3_E2E (3612/3125)%bigQ.
Proof.
nccoq.
Qed.

(* f4 *)

(* beta4_E2E:= ( beta_S3 * beta_S5_2 *)
Definition beta4_E2E : @F R := F_of_sequpp {|
  sequpp_T := 2;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := [:: (0, (EFin 0, (0, EFin 0)));
                  (1522/3125, (EFin 0, (0, +oo%E))) ] |}%bigQ.

Goal beta4_E2E = ((beta_S3 : Fd) * beta_S5_2).
Proof. nccoq. Qed.

(* f4_E2E_delay:= hdev(alpha4, f4_E2E_service) *)
Goal hDev_bounded alpha4 beta4_E2E (1522/3125)%bigQ.
Proof.
nccoq.
Qed.

(* f5 *)

(* beta5_E2E:= ( beta_S4 * beta_S5_2 *)
Definition beta5_E2E : @F R := F_of_sequpp {|
  sequpp_T := 2;
  sequpp_d := 1;
  sequpp_c := 0;
  sequpp_js := [:: (0, (EFin 0, (0, EFin 0)));
                  (1522/3125, (EFin 0, (0, +oo%E))) ] |}%bigQ.

Goal beta5_E2E = ((beta_S4 : Fd) * beta_S5_2).
Proof. nccoq. Qed.

(* f5_E2E_delay:= hdev(alpha4, f4_E2E_service) *)
Goal hDev_bounded alpha5 beta5_E2E (1522/3125)%bigQ.
Proof.
nccoq.
Qed.

End Case.
