This directory contains a simple case study with five flows crossing
six servers.

Files
-----

* `topology.png` an image of the topology of the case study
* `CaseStudy.nc` a first (non formalized) analysis
* `arbitrary_flows.v` proofs for arbitrary input bounds and services
* `usual_functions.v` proofs for input bounds and services given as
  specific linear functions, with given numerical values
