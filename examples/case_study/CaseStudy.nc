######################################################################
##    Case study of article: encoding into network calculus (using
##  RTaW minplus interpreter format).
##
##    Run in http://realtimeatwork.com/minplus-playground#

## Sizes: bits
## Time unit: ms

d0:= delay(0)
TenPow5:= 100000

###################################
## Input data flows
##    Frame/Burst size: 1000 bytes = 8000 bits
##    Throughput: 20Mb/s = 20Kb/ms

alpha1:= bucket(20000, 8000)
alpha2:= alpha1
alpha3:= alpha1
alpha4:= alpha1
alpha5:= alpha1

##################################
## Servers Rate
##    Rate : 100Mb/s

beta1:= affine(TenPow5,0)
beta2:= beta1
beta3:= beta1
beta4:= beta1
beta5:= beta1

###################################
## First server : S1
##    Throughput: 100Mb/s = 100Kb/ms
##    Input: f1, f2, f3

alpha_S1:= alpha1 + alpha2
alpha_S1:= alpha_S1 + alpha3
  
beta_S1_delay:= hdev(alpha_S1, beta1)

beta_S1:= delay( beta_S1_delay )

alpha1_S1':= alpha1 / beta_S1
alpha2_S1':= alpha2 / beta_S1
alpha3_S1':= alpha3 / beta_S1

alpha1_S1:= alpha1_S1' /\ d0
alpha2_S1:= alpha2_S1' /\ d0
alpha3_S1:= alpha3_S1' /\ d0

###################################
## Second server : S2 1

beta_S2_1_delay:= hdev( alpha1_S1, beta2 )

beta_S2_1:= delay( beta_S2_1_delay )

alpha1_S2' := ( alpha1_S1 / beta_S2_1 )

alpha1_S2 := alpha1_S2' /\ d0

###################################
## Second server : S2_2

beta_S2_2_delay:= hdev( alpha2_S1, beta2 )

beta_S2_2:= delay( beta_S2_2_delay )

alpha2_S2':= ( alpha2_S1 / beta_S2_2 )

alpha2_S2 := alpha2_S2' /\ d0

###################################
## Second server : S2 3

beta_S2_3_delay:= hdev( alpha3_S1, beta2 )

beta_S2_3:= delay( beta_S2_3_delay )

alpha3_S2':= ( alpha3_S1 / beta_S2_3 )

alpha3_S2 := alpha3_S2' /\ d0

###################################
## Third server: S3
##    Throughput: 100Mb/s = 100Kb/ms
##    Input: f1, f4

alpha_S3:= alpha1_S2 + alpha4

beta_S3_delay:= hdev(alpha_S3, beta3)

beta_S3 := delay( beta_S3_delay )

alpha1_S3':= (alpha1_S2 / beta_S3 )
alpha4_S3':= (alpha4    / beta_S3 )

alpha1_S3 := alpha1_S3'	 /\ d0
alpha4_S3 := alpha4_S3'	 /\ d0

###################################
## Fourth server: S4
##    Throughput: 100Mb/s = 100Kb/ms
##    Input: f3, f5

alpha_S4 := alpha3_S2 + alpha5

beta_S4_delay := hdev(alpha_S4, beta4)

beta_S4 := delay( beta_S4_delay )

alpha3_S4':= ( alpha3_S2 / beta_S4 )
alpha5_S4':= ( alpha5    / beta_S4 )

alpha3_S4:= alpha3_S4' /\ d0
alpha5_S4:=  alpha5_S4' /\ d0

###################################
## Fifth server/ first port: S5_1
##    Throughput: 100Mb/s = 100Kb/ms
##    Input: f1,f2,f3

alpha_S5_1:= alpha1_S3 + alpha2_S2
alpha_S5_1:= alpha_S5_1 + alpha3_S4

beta_S5_1_delay := hdev(alpha_S5_1, beta5)

beta_S5_1 := delay( beta_S5_1_delay )

###################################
## Fifth server/ second port: S5_2
##    Throughput: 100Mb/s = 100Kb/ms
##    Input: f4,f5

alpha_S5_2:= alpha4_S3 + alpha5_S4

beta_S5_2_delay:= hdev(alpha_S5_2, beta5)

beta_S5_2 := delay( beta_S5_2_delay )

###################################
####   End-to-end delays ##########

###################################
### f1

beta1_E2E:= ( beta_S1 * beta_S2_1 ) * beta_S3
beta1_E2E:= f1_E2E_service * beta_S5_1

f1_E2E_delay:= hdev(alpha1, f1_E2E_service)
f1_E2E_delay

###################################
### f2

f2_E2E_service:=  beta_S1 * beta_S5_1
f2_E2E_service:= f2_E2E_service * beta_S2_2

f2_E2E_delay:= hdev(alpha2, f2_E2E_service)
f2_E2E_delay

###################################
### f3

f3_E2E_service:= ( beta_S1 * beta_S4 ) * beta_S5_1
f3_E2E_service:= f3_E2E_service * beta_S2_3

f3_E2E_delay:= hdev(alpha3, f3_E2E_service)
f3_E2E_delay

###################################
### f4

f4_E2E_service:=  beta_S3 * beta_S5_2
f4_E2E_service

f4_E2E_delay:= hdev(alpha4, f4_E2E_service)
f4_E2E_delay

###################################
### f5

f5_E2E_service:=  beta_S4 * beta_S5_2

f5_E2E_delay:= hdev(alpha5, f5_E2E_service)
f5_E2E_delay
