(* Copyright : Lucien Rakotomalala, Pierre Roux and Marc Boyer. *)

From HB Require Import structures.
From mathcomp Require Import ssreflect ssrfun ssrbool eqtype order ssrnat.
From mathcomp Require Import choice seq bigop fintype ssralg ssrnum rat.
From mathcomp Require Import boolp classical_sets reals ereal signed.
From mathcomp.dioid Require Import dioid complete_lattice complete_dioid.
Require Import analysis_complements.

(******************************************************************************)
(* Dioid instances used in network calculus.                                  *)
(*                                                                            *)
(* * number dioids                                                            *)
(* \bar R and {nonnege R} are equipped with a complete lattice canonical      *)
(* structure, first law is min, second law is addition, zero element is +oo   *)
(* and unit element is 0.                                                     *)
(*                                                                            *)
(* * functions dioids                                                         *)
(*         F == functions R+ -> \bar R                                        *)
(*     Fplus == functions f in F such that for all x, 0 <= f x                *)
(*       Fup == functions f in Fplus that are non decreasing                  *)
(*      F0up == functions f in Fup such that f 0 = 0                          *)
(* The first three types are equipped with a complete dioid canonical         *)
(* structure, first law is pointwise minimum, second law is min-plus          *)
(* convolution, zero element is the constant function +oo and unit element is *)
(* the function equal to +oo everywhere except in 0 where it is 0.            *)
(* The last type is *not* a dioid.                                            *)
(*                                                                            *)
(* * pointwise addition of functions                                          *)
(* Moreover, F is equipped with a pointwise addition F_plus with neutral      *)
(* element F_0, accessible with following notations in scope F_scope (%F):    *)
(*     f + g == pointwise addition of functions f and g                       *)
(*     f / g == deconvolution in dioid F                                      *)
(*        -f == pointwise opposite of f                                       *)
(*     f - g == pointwise subtraction of f and g                              *)
(*      \sum == notation for sum with bigop                                   *)
(*                                                                            *)
(* * other operators                                                          *)
(* Fup_shift_l f d == the function in Fup equal to (f : Fup) shifted by d     *)
(*                    (i.e., t |-> f (t + d))                                 *)
(* pseudo_inv_inf_nnR f == y |-> inf{ x | y <= f x }                          *)
(******************************************************************************)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope classical_set_scope.
Local Open Scope ring_scope.
Local Open Scope order_scope.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.
(* remove below line when requireing mathcomp >= 2.4.0 *)
Local Notation le_val := Order.le_val.

(* a few things about rationals *)

Section RatrProperties.

(* Assume a realType (could be instantiated by the Reals from standard
   library thanks to mathcomp.analysis.Rstruct) *)
Variable R : realType.

Lemma ratr_snum_subproof (xnz : KnownSign.nullity) (xr : KnownSign.reality)
    (x : {compare 0%Q & xnz & xr}) :
  Signed.spec (0 : R) xnz xr (ratr x%:num).
Proof.
apply/andP; split; first by move: xnz x => [x /= | //]; rewrite fmorph_eq0.
move: xr x => [[[]|] x /= | //]; rewrite ?ler0q ?lerq0 //.
- by rewrite fmorph_eq0; apply/eqP/eq0.
- exact: cmp0.
Qed.

Canonical ratr_snum (xnz : KnownSign.nullity) (xr : KnownSign.reality)
    (x : {compare 0%Q & xnz & xr}) :=
  Signed.mk (ratr_snum_subproof x).

Lemma er_map_ratr_add x y :
  er_map ratr (x + y)%dE = (er_map ratr x + er_map ratr y)%dE :> \bar R.
Proof. by move: x y => [x| |] [y| |] //=; rewrite rmorphD. Qed.

End RatrProperties.

(* (Rbar, Order.min, adde) is a comCompleteDioidType *)

Module BarRmin.
Section BarRmin.

Variable R : realType.

Lemma ereal_inf_is_lub : @set_f_is_lub _ (\bar R)^d (@ereal_inf R).
Proof.
move=> B; split=> [ | y HB]; [exact: ereal_inf_lbound | exact: lb_ereal_inf].
Qed.

Lemma set_mulDl (a : \bar R) B :
  (a + ereal_inf B)%dE = ereal_inf [set (a + x)%dE | x in B].
Proof.
apply/le_anti/andP; split.
  by apply: lb_ereal_inf => _ [x Hx <-]; exact/lee_dD2l/ereal_inf_lbound.
case: a => [a||].
- rewrite daddeC -lee_dsubl_addr//; apply: lb_ereal_inf => x Bx.
  by rewrite lee_dsubl_addr// daddeC; apply: ereal_inf_lbound; exists x.
- by case: (ereal_inf B) => [l||] /=; rewrite leey.
- case: (@eqP _ (B `\` [set +oo%E])%classic set0) => [ | /[dup]];
    rewrite setD_eq0 -{1}ereal_inf_pinfty; [by move=> -> /=; exact: leey|].
  move=> /eqP/daddNye->.
  move=> /asboolPn/existsp_asboolPn [x] /asboolPn /imply_asboolPn[Bx /eqP Hx].
  by apply: ereal_inf_lbound; exists x => [//|]; rewrite daddNye.
Qed.

End BarRmin.
End BarRmin.

Section BarRminDioid.

Variable R : realType.

Definition barRmin := (\bar R)^d.

HB.instance Definition _ := Order.POrder.on barRmin.
HB.instance Definition _ := POrder_isComDioid.Build _ barRmin
  (@joinA _ _) (@joinC _ _) minye minxx daddeA daddeC dadd0e dadde_minl
  (fun _ => erefl) erefl (fun a b : \bar R => esym (eq_minr a b)).

HB.instance Definition _ := POrder_isJoinCompleteLattice.Build _ barRmin
  (@BarRmin.ereal_inf_is_lub R).

HB.instance Definition _ := isComCompleteDioid.Build _ barRmin
  (@BarRmin.set_mulDl R).

Lemma Rbar_dioid_addE (x y : barRmin) : x + y = Order.min (x : \bar R) y.
Proof. by []. Qed.

Lemma Rbar_dioid_mulE (x y : barRmin) : x * y = (x + y)%dE.
Proof. by []. Qed.

Lemma Rbar_dioid_set_addE (B : set barRmin) : set_join B = ereal_inf B.
Proof. by []. Qed.

Definition Rbar_dioidE :=
  (Rbar_dioid_addE, Rbar_dioid_mulE, Rbar_dioid_set_addE).

(* ({nonnege R}, Order.min, adde) is a comCompleteDioidType *)

Definition nnbarRmin := {nonneg \bar R}.

Definition nnbarRmin_pred (r : barRmin) := Signed.spec 0%E ?=0 >=0 r.

Lemma nnbarRmin_semiring_closed : semiring_closed nnbarRmin_pred.
Proof.
split; split=> [|x y /andP[_ /=le0x] /andP[_ /=le0y]]; apply/andP; split=> //=.
- by rewrite leEereal/=.
- by rewrite lexI le0x.
- exact: dadde_ge0.
Qed.

HB.instance Definition _ := GRing.isSemiringClosed.Build barRmin nnbarRmin_pred
  nnbarRmin_semiring_closed.

Lemma nnbarRmin_set_join_closed : set_join_closed nnbarRmin_pred.
Proof. move=> S HS; exact/lb_ereal_inf/HS. Qed.

HB.instance Definition _ :=
  isJoinCompleteLatticeClosed.Build _ barRmin nnbarRmin_pred
    nnbarRmin_set_join_closed.

HB.instance Definition _ : SubChoice barRmin nnbarRmin_pred nnbarRmin :=
  SubChoice.on nnbarRmin.
HB.instance Definition _ :=
  [SubChoice_isJoinSubComCompleteDioid of nnbarRmin by <:
     with Order.dual_display ereal_display].

End BarRminDioid.

(* (F, F_min, F_min_conv) is a comCompleteDioidType *)

Section Fmin.

Context {R : realType}.

Definition F := {nonneg R} -> \bar^d R.

Definition Fd := F^d.

Definition F_zero : Fd := fun=> +oo%E.

Definition F_min_conv (f g : Fd) : Fd := fun t =>
  ereal_inf
    [set (f uv.1 + g uv.2)%dE
    | uv in [set uv | uv.1%:num + uv.2%:num = t%:num]].

Definition F_one : Fd := fun t => if t%:num == 0 then 0%:E else +oo%E.

Definition F_inf (S : set Fd) : Fd := fun t => ereal_inf [set f t | f in S].

End Fmin.

Module Fmin.
Section Fmin.

Context {R : realType}.

Lemma min0f : left_id (@F_zero R) Order.min_fun.
Proof. by move=> f; apply/funext => x; apply: meet1x. Qed.

Lemma mulA : associative (@F_min_conv R).
Proof.
move=> f g h; apply/funext => t.
have -> : F_min_conv f (F_min_conv g h) t
          = ereal_inf [set (f uvw.1.1 + g uvw.1.2 + h uvw.2)%dE
                      | uvw in [set uvw
                               | uvw.1.1%:nngnum + uvw.1.2%:nngnum + uvw.2%:nngnum = t%:nngnum]].
{ apply/le_anti/andP; split.
  { apply: lb_ereal_inf => _ [[[u v] w] /= Huvw] <- /=.
    rewrite -daddeA.
    apply (@le_trans
             _ _ (f u + F_min_conv g h ((v%:num + w%:num)%:nng%R))%dE).
    { rewrite -!Rbar_dioidE set_mulDl.
      apply: ereal_inf_lbound.
      exists (u, (v%:nngnum + w%:nngnum)%:nng); [by rewrite /= addrA|].
      by rewrite -!Rbar_dioidE set_mulDl. }
    by apply/lee_dD2l/ereal_inf_lbound; exists (v, w). }
  apply: lb_ereal_inf => _ [[u v] /= Huv <-].
  rewrite -!Rbar_dioidE set_mulDl !Rbar_dioidE.
  apply: le_ereal_inf => _ [_ [[u' v'] /= Hu'v'] <-] <-.
  exists (u, u', v').
  { by rewrite /= -Huv -Hu'v' addrA. }
  by rewrite -daddeA. }
apply/le_anti/andP; split.
{ apply: lb_ereal_inf => _ [[u v] /= Huv] <-.
  rewrite -!Rbar_dioidE set_mulDr.
  apply: le_ereal_inf => _ [_ [[u' v'] /= Hu'v'] <-] <-.
  exists (u', v', v) => [| //].
  by rewrite /= -Huv -Hu'v' -addrA. }
apply: lb_ereal_inf => _ [[[u v] w] /= Huvw] <-.
apply: (@le_trans _ _ (F_min_conv f g (u%:nngnum + v%:nngnum)%:nng%R + h w)%dE).
{ by apply: ereal_inf_lbound; exists ((u%:nngnum + v%:nngnum)%:nng, w). }
by apply/lee_dD2r/ereal_inf_lbound; exists (u, v).
Qed.

Lemma mulC : commutative (@F_min_conv R).
Proof.
move=> f g; apply/funext => t.
by rewrite /F_min_conv; apply: f_equal; rewrite predeqP => z; split;
  (move=> -[[u v] /= Huv <-]; exists (v, u); [rewrite addrC|rewrite daddeC]).
Qed.

Lemma mul1f : left_id (@F_one R) F_min_conv.
Proof.
move=> f; apply/funext => t.
apply/le_anti/andP; split.
{ apply: ereal_inf_lbound; exists (0%:nng, t); [by rewrite /= add0r|].
  by rewrite /F_one /= eqxx dadd0e. }
apply: lb_ereal_inf => _ [[u v] /= Huv] <-.
rewrite /F_one; case: eqP => Hu; [|exact: leey].
have -> : t = v; [by apply/val_inj; rewrite /= -Huv Hu /= add0r|].
by rewrite dadd0e.
Qed.

Lemma mulDl : left_distributive (@F_min_conv R) Order.join.
Proof.
move=> f g h; apply/funext => t; apply/le_anti/andP; split.
{ rewrite le_min; apply/andP; split.
  { apply: lb_ereal_inf => _ [[u v] /= Huv <-].
    apply: (@le_trans _ _ (Order.min (f u) (g u) + h v)%dE).
    { by apply: ereal_inf_lbound; exists (u, v). }
    by apply: lee_dD2r; rewrite ge_min lexx. }
  apply: lb_ereal_inf => _ [[u v] /= Huv <-].
  apply: (@le_trans _ _ (Order.min (f u) (g u) + h v)%dE).
  { by apply: ereal_inf_lbound; exists (u, v). }
  by apply: lee_dD2r; rewrite ge_min lexx orbC. }
apply: lb_ereal_inf => _ [[u v] /= Huv <-].
rewrite /Order.meet/= -!Rbar_dioidE mulrDl !Rbar_dioidE.
by rewrite le_min !ge_min; apply/andP; split; apply/orP; [left|right];
  apply: ereal_inf_lbound; exists (u, v).
Qed.

Lemma mul0f : left_zero (@F_zero R) F_min_conv.
Proof.
move=> f; apply/funext => t; apply/le_anti; rewrite leey/=.
by apply: lb_ereal_inf => _ [[u v] /= Huv <-].
Qed.

Lemma one_neq_zero : F_one != F_zero :> @Fd R.
Proof.
have: F_one 0%:nng%R != F_zero 0%:nng%R :> \bar R by rewrite /F_one /= eqxx.
by apply: contra_neq => ->.
Qed.

Lemma le_def (f f' : @Fd R) : (f <= f')%O = (f \min f' == f').
Proof. exact: leEjoin. Qed.

Lemma inf_is_lub : set_f_is_lub (@F_inf R).
Proof.
split=> [f Sf | f ubSf]; apply/lefP => t.
- by apply: ereal_inf_lbound; exists f.
- by apply: lb_ereal_inf => _ [g Sg <-]; move: (ubSf _ Sg) => /lefP.
Qed.

Lemma set_mulDl f S :
  F_min_conv f (F_inf S) = F_inf [set F_min_conv f g | g in S] :> @Fd R.
Proof.
apply/funext => t; apply/le_anti/andP; split.
{ apply: lb_ereal_inf => _ [_ [h Sh <-] <-].
  apply: lb_ereal_inf => _ [[u v] /= Huv <-].
  apply: (@le_trans _ _ (f u + F_inf S v)%dE).
  { by apply: ereal_inf_lbound; exists (u, v). }
  by apply/lee_dD2l/ereal_inf_lbound; exists h. }
apply: lb_ereal_inf => _ [[u v] /= Huv <-].
rewrite /F_inf -!Rbar_dioidE set_mulDl !Rbar_dioidE.
apply: lb_ereal_inf => _ [_ [f' Hf' <-] <-].
apply: (@le_trans _ _ (F_min_conv f f' t)).
{ by apply: ereal_inf_lbound; exists (F_min_conv f f'); [exists f'|]. }
by apply: ereal_inf_lbound; exists (u, v).
Qed.

End Fmin.
End Fmin.

Section FMin.

Context {R : realType}.

HB.instance Definition _ := Order.POrder.on (@Fd R).
HB.instance Definition _ := POrder_isComDioid.Build _ (@Fd R)
  min_funA min_funC Fmin.min0f minff
  Fmin.mulA Fmin.mulC Fmin.mul1f Fmin.mulDl Fmin.mul0f
  Fmin.one_neq_zero Fmin.le_def.

HB.instance Definition _ := POrder_isJoinCompleteLattice.Build _ Fd
  Fmin.inf_is_lub.

HB.instance Definition _ := isComCompleteDioid.Build _ Fd Fmin.set_mulDl.

Lemma F_dioid_addE (f g : Fd) : f + g = f \min g.
Proof. by []. Qed.

Lemma F_dioid_mulE (x y : Fd) : x * y = F_min_conv x y.
Proof. by []. Qed.

Lemma F_dioid_set_addE (B : set Fd) : set_join B = F_inf B.
Proof. by []. Qed.

Lemma F_dioid_oneE : 1%R = F_one :> Fd.
Proof. by []. Qed.

Lemma F_dioid_zeroE : 0 = F_zero :> Fd.
Proof. by []. Qed.

Lemma F_dioid_leE (f g : Fd) : (f <=^d g) = (g <= f :> F)%O.
Proof. by []. Qed.

Definition F_dioidE :=
  (F_dioid_addE, F_dioid_mulE, F_dioid_set_addE, F_dioid_oneE, F_dioid_zeroE,
   F_dioid_leE).

Lemma alt_def_F_min_conv (f g : F) (t : {nonneg R}) :
  F_min_conv f g t
  = ereal_inf [set (f u + g (insubd 0%:nng (t%:num - u%:num))%R)%dE
              | u in [set u | u <= t]].
Proof.
rewrite /F_min_conv; apply: f_equal; rewrite predeqP => x; split.
{ move=> -[[u v] /= Huv] <-; exists u.
  { by rewrite -num_le -Huv lerDl. }
  apply/f_equal/f_equal/val_inj.
  by rewrite /insubd insubT /= => [| _]; rewrite -Huv addrC addKr. }
move=> -[u /= Hu <-].
exists (u, insubd 0%:nng%R (t%:nngnum - u%:nngnum)) => [| //].
rewrite /= /insubd /= insubT /= => [| _].
{ by rewrite subr_ge0 num_le. }
by apply/eqP; rewrite addrA subr_eq addrC.
Qed.

Lemma F_min_conv0 (f g : @F R) :
  F_min_conv f g 0%:nng%R = (f 0%:nng%R + g 0%:nng%R)%dE.
Proof.
apply/le_anti/andP; split.
{ apply: ereal_inf_lbound.
  by exists (0%:nng, 0%:nng)%R; rewrite //= addr0. }
apply: lb_ereal_inf => _ [[u v] /= Huv <-].
have->: u = 0%:nng%R.
{ apply/val_inj/le_anti/andP; split => //=.
  by rewrite -(gerDr v%:nngnum) Huv. }
have->: v = 0%:nng%R.
{ apply/val_inj/le_anti/andP; split => //=.
  by rewrite -(gerDl u%:nngnum) Huv. }
exact: lexx.
Qed.

(* (F_plus, F_min, F_min_conv) is a comCompleteDioidType *)

Definition nonnegF_pred := fun f => \0 <= f :> @F R.
Arguments nonnegF_pred _ /.
Definition nonnegF := [qualify a f | nonnegF_pred f].

Record Fplus := {
  Fplus_val :> F;
  _ : Fplus_val \is a nonnegF
}.
Identity Coercion id_F_Funclass : F >-> Funclass.
Arguments Build_Fplus : clear implicits.

Lemma Fplus_ge0 (f : Fplus) t : (0%:E <= f t)%E.
Proof. by case: f => ? ?; exact/lefP. Qed.

Lemma Fplus_ge0_subproof (f : Fplus) t : Signed.spec 0%:E ?=0 >=0 (f t).
Proof. by apply/andP; split=> [//|]; exact: Fplus_ge0. Qed.

Canonical Fplus_nngenum (f : Fplus) t : {nonneg \bar R} :=
  Signed.mk (Fplus_ge0_subproof f t) : {nonneg \bar R}.

HB.instance Definition _ := [isSub for Fplus_val].
HB.instance Definition _ := [Choice of Fplus by <:].

Definition nonnegFd_pred : pred Fd := nonnegF_pred.

Lemma Fplus_semiring_closed : semiring_closed nonnegFd_pred.
Proof.
split; split.
{ apply/lefP => t; exact: leey. }
{ move=> f f' /lefP Hf /lefP Hf'; apply/lefP => t.
  by rewrite le_min Hf Hf'. }
{ apply/lefP => t; rewrite /GRing.one/= /F_one.
  case: eqP => [// | _]; exact: leey. }
move=> f f' /lefP Hf /lefP Hf'; apply/lefP => t.
apply: lb_ereal_inf => _ [? ? <-]; apply: dadde_ge0; [exact: Hf|exact: Hf'].
Qed.

HB.instance Definition _ := GRing.isSemiringClosed.Build Fd nonnegFd_pred
    Fplus_semiring_closed.

Lemma Fplus_set_join_closed : @set_join_closed _ Fd nonnegFd_pred.
Proof.
move=> S HS; apply/lefP => t.
apply: lb_ereal_inf => _ [f Sf <-].
move: t; exact/lefP/HS.
Qed.

HB.instance Definition _ := isJoinCompleteLatticeClosed.Build _ Fd nonnegFd_pred
  Fplus_set_join_closed.

Definition Fplusd := Fplus.

HB.instance Definition _ : SubChoice Fd nonnegFd_pred Fplusd :=
  SubChoice.on Fplusd.
HB.instance Definition _ :=
  [SubChoice_isJoinSubComCompleteDioid of Fplusd by <:
     with Order.dual_display ereal_display].

Definition Fplus_min (f g : Fplus) : Fplus := (f : Fplusd) `|^d` g.
Definition Fplus_max (f g : Fplus) : Fplus := (f : Fplusd) `&^d` g.

Lemma Fplus_dioid_addE (x y : Fplus) : (x : Fplusd) + y = x \min y :> F.
Proof. by []. Qed.

Lemma Fplus_dioid_mulE (x y : Fplus) : (x : Fplusd) * y = F_min_conv x y :> F.
Proof. by []. Qed.

Lemma Fplus_dioid_set_addE (B : set Fplusd) :
  set_join B = F_inf (val @` B) :> F.
Proof. by []. Qed.

Lemma Fplus_dioid_oneE : (1 : Fplusd) = F_one :> F.
Proof. by []. Qed.

Lemma Fplus_dioid_zeroE : (0 : Fplusd) = F_zero :> F.
Proof. by []. Qed.

Lemma Fplus_dioid_leE (x y : Fplus) : (x <=^d y :> Fplusd) = (y <= x :> F).
Proof. by []. Qed.

Definition Fplus_dioidE :=
  (Fplus_dioid_addE, Fplus_dioid_mulE, Fplus_dioid_set_addE,
   Fplus_dioid_oneE, Fplus_dioid_zeroE, Fplus_dioid_leE).

Lemma Fplus_divE (f g : Fplus) :
  ((f : Fplusd) / g)%D
  = (fun t => Order.max 0%dE
       (ereal_sup [set (if g u == +oo then -oo
                        else f (t%:nngnum + u%:nngnum)%:nng%R - g u)%dE
                  | u in setT])) :> F.
Proof.
apply/le_anti/andP; split; last first.
{ apply/lefP => t.
  rewrite ge_max; apply/andP; split=> //.
  apply: ub_ereal_sup => _ [u _ <-].
  apply: lb_ereal_inf => _ [_ /= [f0 /= + <-] <-].
  rewrite !Fplus_dioidE => /lefP Hf0.
  case: eqP => Hgu; [exact: leey|].
  apply: (le_trans (lee_dD (Hf0 _) (le_refl _))).
  case E: (g u) => [r | |].
  { rewrite lee_dsubl_addr// -E.
    by apply: ereal_inf_lbound; exists (t, u). }
  { exfalso; exact: Hgu. }
  exfalso; move: (le_refl (-oo%E : \bar R)); apply/negP; rewrite -ltNge.
  rewrite -{2}E; apply: (@lt_le_trans _ _ 0%dE) => //; exact: lte_ninfty. }
rewrite -F_dioidE.
apply: set_join_ub.
set f' := fun _ => Order.max _ _.
have Hf' : f' \is a nonnegF.
{ by apply/lefP => t; rewrite le_max lexx. }
exists (Build_Fplus _ Hf') => //=.
apply/asboolP => t /=.
apply: lb_ereal_inf => _ [[u v] /= Huv <-].
case E: (g v) => [r | |].
{ rewrite -lee_dsubl_addr// /f' le_max.
  case: leP => //= H.
  apply: ereal_sup_ubound; exists v => //.
  by rewrite E /=; apply: f_equal2 => //; apply/f_equal/val_inj. }
{ by rewrite daddeC /= leey. }
exfalso; move: (le_refl (-oo%E : \bar R)); apply/negP; rewrite -ltNge.
rewrite -{2}E; apply: (@lt_le_trans _ _ 0%dE) => //; exact: lte_ninfty.
Qed.

Lemma Fplus_addr_closed : addr_closed nonnegF_pred.
Proof.
split; first by rewrite inE => x.
move=> f g /[!inE] + + x => /(_ x) + /(_ x); exact: dadde_ge0.
Qed.

HB.instance Definition _ := GRing.isAddClosed.Build F nonnegF_pred
  Fplus_addr_closed.

HB.instance Definition _ := [SubChoice_isSubNmodule of Fplus by <:].

Lemma Fplus_sumE I (r : seq I) P (F : I -> Fplus) x :
  (\sum_(i <- r | P i) F i) x = (\sum_(i <- r | P i) F i x)%dE.
Proof.
by apply: (big_ind2 (fun (f : Fplus) y => f x = y)) => // f1 _ f2 _ <- <-.
Qed.

(* (Fup, F_min, F_min_conv) is a comCompleteDioidType *)

Definition non_decr_closure (f : @F R) : F :=
  fun t => ereal_sup [set f u | u in [set u | u <= t]].

Lemma non_decr_closureP (f : F) :
  reflect {homo f : x y / x <= y } (non_decr_closure f <= f).
Proof.
apply (iffP idP) => H.
{ move: H => /lefP H x y Hxy.
  by move: (H y); apply/le_trans/ereal_sup_ubound; exists x. }
apply/lefP => y; apply: ub_ereal_sup => _ [x /= Hxy <-]; exact: H.
Qed.

Lemma non_decr_closureP' (f : F) :
  reflect {homo f : x y / x < y >-> x <= y } (non_decr_closure f <= f).
Proof.
apply (iffP idP) => H.
{ move: H => /lefP H x y Hxy.
  move: (H y); apply/le_trans/ereal_sup_ubound; exists x => //; exact: ltW. }
apply/lefP => y; apply: ub_ereal_sup => _ [x /= Hxy <-].
move: Hxy; rewrite le_eqVlt => /orP[/eqP -> // | H']; exact: H.
Qed.

Lemma non_decr_closure_non_decr f :
  {homo (non_decr_closure f) : x y / x <= y }.
Proof.
move=> x y Hxy.
apply: ub_ereal_sup => _ [z /= Hz <-].
apply: ereal_sup_ubound; exists z => [/= | //].
move: Hxy; exact: le_trans.
Qed.

Lemma le_non_decr_closure f : f <= non_decr_closure f.
Proof. by apply/lefP => t; apply: ereal_sup_ubound; exists t => /=. Qed.

Lemma non_decr_closure_F_min_conv_F_0 (f : F) :
  non_decr_closure f = (\- (((\- f)%E : Fd) * \0)%R)%E.
Proof.
apply/funext => t; apply/le_anti/andP; split.
{ apply: ub_ereal_sup => _ [u /= Hu <-].
  rewrite leeNr; apply: ereal_inf_lbound.
  exists (u, insubd 0%:nng (t%:nngnum - u%:nngnum)).
  { by rewrite /= insubdK /in_mem /= ?subr_ge0 ?le_val // addrC subrK. }
  by rewrite dadde0. }
rewrite leeNl; apply: lb_ereal_inf => _ [[u v] /= Huv <-].
rewrite leeNl; apply: ereal_sup_ubound; exists u.
{ by rewrite /= -num_le -Huv lerDl. }
by rewrite dadde0 oppeK.
Qed.

Definition nondecrFplus_pred :=
  fun f : Fplus => `[< forall x y, x <= y -> f x <= f y >].
Arguments nondecrFplus_pred _ /.
Definition nondecrFplus := [qualify a f | nondecrFplus_pred f].

Record Fup := {
  Fup_val :> Fplus;
  _ : Fup_val \is a nondecrFplus
}.
Arguments Build_Fup : clear implicits.

Lemma Fup_ge_non_decr_closure (f : Fup) : non_decr_closure f <= f.
Proof. by case: f => f /=/[!inE]/non_decr_closureP. Qed.

Lemma non_decr_closure_Fup (f : Fup) : non_decr_closure f = f.
Proof.
by apply/le_anti; rewrite Fup_ge_non_decr_closure le_non_decr_closure.
Qed.

Lemma ndFP (f : Fup) : {homo f : x y / x <= y >-> (x <= y)%E }.
Proof. exact/non_decr_closureP/Fup_ge_non_decr_closure. Qed.

Lemma ndFP' (f : Fup) : {homo f : x y / x < y >-> (x <= y)%E }.
Proof. exact/non_decr_closureP'/Fup_ge_non_decr_closure. Qed.

HB.instance Definition _ := [isSub for Fup_val].
HB.instance Definition _ := [Choice of Fup by <:].

Definition nondecrFplusd_pred : pred Fplusd := nondecrFplus_pred.

Lemma Fup_semiring_closed : semiring_closed nondecrFplusd_pred.
Proof.
split; split.
{ apply/asboolP/non_decr_closureP/lefP => t; exact: leey. }
{ move=> f f' /asboolP/non_decr_closureP Hf /asboolP/non_decr_closureP Hf'.
  apply/asboolP/non_decr_closureP/lefP => t.
  apply: ub_ereal_sup => _ [y /= Hyt <-]; rewrite le_min !ge_min.
  apply/andP.
  split; apply/orP; [left|right]; move: y t Hyt; exact: non_decr_closureP. }
{ apply/asboolP/non_decr_closureP/lefP => t.
  rewrite /GRing.one/= /GRing.one/= /F_one; case: eqP => Ht; [|exact: leey].
  apply: ub_ereal_sup => _ [y /= Hyt <-].
  rewrite ifT //; apply/eqP/le_anti.
  by rewrite ge0 andbT; move: Hyt; rewrite -num_le Ht. }
move=> f f' /asboolP Hf /asboolP Hf'.
apply/asboolP => x y Hxy.
case: (leP y%:nngnum 0) => [| Hy].
{ rewrite le_eqVlt lt0F orbF num_eq0 => /eqP Hy.
  by have -> : x = y; [apply/le_anti; rewrite Hxy /= Hy -num_le /=|]. }
rewrite Fplus_dioidE.
apply: lb_ereal_inf => _ [[u v] /= Huv <-].
set u' := (u%:nngnum * (x%:nngnum / y%:nngnum))%:nng.
set v' := (v%:nngnum * (x%:nngnum / y%:nngnum))%:nng.
apply: (@le_trans _ _ (f u' + f' v')%dE).
{ apply: ereal_inf_lbound; exists (u', v') => [/= | //].
  rewrite -mulrDl Huv mulrC divfK //.
  by move: Hy; rewrite lt_def => /andP[]. }
by apply: lee_dD; [apply: Hf|apply: Hf'];
  apply: ler_piMr => //=; rewrite ler_pdivrMr // mul1r.
Qed.

HB.instance Definition _ :=
  GRing.isSemiringClosed.Build Fplusd nondecrFplusd_pred Fup_semiring_closed.

Lemma Fup_set_join_closed : set_join_closed nondecrFplusd_pred.
Proof.
move=> S HS; apply/asboolP => x y Hxy.
apply: lb_ereal_inf => _ [f Sf <-].
apply: (@le_trans _ _ (f x)).
{ by apply: ereal_inf_lbound; exists f. }
move: x y Hxy; apply/asboolP.
move: Sf => -[f' Sf' <-]; exact: HS.
Qed.

HB.instance Definition _ :=
  isJoinCompleteLatticeClosed.Build _ Fplusd nondecrFplusd_pred
    Fup_set_join_closed.

Definition Fupd := Fup.

HB.instance Definition _ : SubChoice Fplusd nondecrFplusd_pred Fupd :=
  SubChoice.on Fupd.
HB.instance Definition _ :=
  [SubChoice_isJoinSubComCompleteDioid of Fupd by <:
     with Order.dual_display ereal_display].

Definition Fup_min (f g : Fup) : Fup := (f : Fupd) `|^d` g.
Definition Fup_max (f g : Fup) : Fup := (f : Fupd) `&^d` g.

Lemma Fup_dioid_addE (x y : Fup) : (x : Fupd) + y = x \min y :> F.
Proof. by []. Qed.

Lemma Fup_dioid_mulE (x y : Fup) : (x : Fupd) * y = F_min_conv x y :> F.
Proof. by []. Qed.

Lemma Fup_dioid_set_addE (B : set Fupd) :
  set_join B = F_inf (val @` (val @` B)) :> F.
Proof. by []. Qed.

Lemma Fup_dioid_oneE : (1 : Fupd) = F_one :> F.
Proof. by []. Qed.

Lemma Fup_dioid_zeroE : (0 : Fupd) = F_zero :> F.
Proof. by []. Qed.

Lemma Fup_dioid_leE (x y : Fup) : (x <=^d y :> Fupd) = (y <= x :> F).
Proof. by []. Qed.

Definition Fup_dioidE :=
  (Fup_dioid_addE, Fup_dioid_mulE, Fup_dioid_set_addE,
   Fup_dioid_oneE, Fup_dioid_zeroE, Fup_dioid_leE).

Lemma Fplus_div_non_decr (f g : Fup) : ((f : Fplusd) / g)%D \is a nondecrFplus.
Proof.
apply/asboolP => u t Hut.
rewrite Fplus_divE.
set s1 := ereal_sup _.
set s2 := ereal_sup _.
suff: (s1 <= s2)%E.
{ case: s1 => [s1 | |]; case: s2 => [s2 | |] //= H.
  { by rewrite ge_max !le_max lexx /= H orbT. }
  { by rewrite le_max leey orbT. }
  { by rewrite ge_max !le_max lexx leNye. }
  by rewrite ge_max !le_max lexx leNye. }
apply: ub_ereal_sup => _ [v /= _ <-].
apply: (@le_trans _ _
  (if g v == +oo then -oo else f (t%:nngnum + v%:nngnum)%:nng%R - g v)%dE).
2:{ by apply: ereal_sup_ubound; exists v. }
case: eqP => Hgv //.
apply: lee_dD => //.
by apply/ndFP; rewrite -num_le /= lerD2r.
Qed.

Lemma Fup_divE (f g : Fup) : ((f : Fupd) / g = (f : Fplusd) / g :> Fplus)%D.
Proof.
apply/(@le_anti _ Fplusd)/andP; split.
{ rewrite -mul_div_equiv mulrC.
  rewrite !Fplus_dioidE -!Fup_dioidE.
  by rewrite mulrC mul_div_equiv. }
have -> : ((f : Fplusd) / g)%D = Build_Fup _ (Fplus_div_non_decr f g) by [].
rewrite Fplus_dioidE -Fup_dioidE.
rewrite -mul_div_equiv mulrC.
rewrite !Fup_dioidE -!Fplus_dioidE.
by rewrite mulrC mul_div_equiv.
Qed.

Lemma Fup_addr_closed : addr_closed nondecrFplus_pred.
Proof.
split; first by rewrite inE => x.
move=> f g /[!inE] + + x => /(_ x) ndf /(_ x) ndg y xley.
by rewrite /GRing.add/=; apply: lee_dD; [exact: ndf|exact: ndg].
Qed.

HB.instance Definition _ := GRing.isAddClosed.Build Fplus nondecrFplus_pred
  Fup_addr_closed.

HB.instance Definition _ := [SubChoice_isSubNmodule of Fup by <:].

Lemma Fup_sumE I (r : seq I) P (F : I -> Fup) x :
  (\sum_(i <- r | P i) F i) x = (\sum_(i <- r | P i) F i x)%dE.
Proof.
by apply: (big_ind2 (fun (f : Fup) y => f x = y)) => // f1 _ f2 _ <- <-.
Qed.

(** F0up are functions f in Fup such that f 0 = 0 *)
Record F0up := mk_F0up {
  F0up_val :> Fup;
  _ : F0up_val 0%:nng == 0%:E
}.

Lemma F0up0 (f : F0up) : f 0%:nng = 0%:E.
Proof. by case: f => f /= /eqP. Qed.

HB.instance Definition _ := [isSub for F0up_val].
HB.instance Definition _ := [Choice of F0up by <:].

(** *** Somes properties *)

Program Definition Fplus_shift_l (f : Fup) (d : {nonneg R}) :=
  Build_Fplus (fun t => f (t%:num + d%:num)%:nng) _.
Next Obligation. by move=> f d; apply/lefP => t /=. Qed.

Program Definition Fup_shift_l (f : Fup) (d : {nonneg R}) :=
  Build_Fup (Fplus_shift_l f d) _.
Next Obligation.
move=> f d; apply/asboolP => x y.
by rewrite /= -num_le -(lerD2r d%:num) => ?; apply/ndFP.
Qed.

(*definiton 20 p 68 *)
Definition pseudo_inv_inf_nnR (f : Fup) (y : {nonneg \bar R}) : {nonneg \bar R} :=
  (ereal_inf [set x%:num | x in
               [set x%:num%:E%:sgn | x in [set x | y%:num <= f x]]])%:sgn.

Lemma non_decr_recipr (f : Fup) (x y : {nonneg R}) : (f x < f y)%E -> x < y.
Proof.
move=> Hfxy; rewrite ltNge; apply/negP => Hxy.
move: Hfxy; apply/negP; rewrite -leNgt.
apply/ndFP => //; exact: Fup_ge_non_decr_closure.
Qed.

Lemma ex_el_between x y : (x < y -> {z : \bar R | x < z < y})%E.
Proof.
case: x => [x | |]; case: y => [y | |] // Hxy.
{ exists ((x + y) / 2)%:E; rewrite !ltEereal /= mulrDl; apply/andP; split.
  { by rewrite {1}(splitr x) ltrD2l ltr_pM2r. }
  by rewrite {2}(splitr y) ltrD2r ltr_pM2r. }
{ by exists (x + 1)%:E; rewrite ltey andbT ltEereal /= ltrDl. }
{ exists (y - 1)%:E; rewrite ltNye /=.
  by rewrite ltEereal /= ltrBlDr ltrDl. }
by exists 0%:E; rewrite ltNye ltey.
Qed.

(* TODO: notation %:nnge and %:pose *)

(* Réécriture du Lemma 8 : page 68 *)
Lemma alt_def_pseudo_inv_inf_nnR (f : Fup) (y : {nonneg \bar R}) :
  (pseudo_inv_inf_nnR f y)%:num
  = Order.max
      0%E (ereal_sup [set x%:num%:E | x in [set x | f x < y%:num]]).
Proof.
case: (leP y%:num%E (f 0%:nng)) => Hy.
{ rewrite (_ : ereal_sup _ = -oo%E).
  { rewrite maxEge leNye; apply/le_anti/andP; split.
    { apply: ereal_inf_lbound; exists (widen_signed 0%:E%:sgn) => //.
      by exists 0%:nng => //; apply: val_inj. }
    by apply: lb_ereal_inf => _ [_ /= [z Hz <-] <-] /=; rewrite leEereal /=. }
  rewrite ereal_sup_ninfty => _ /= [z Hz <-]; exfalso.
  move: Hz; apply/negP; rewrite -leNgt; apply: (le_trans Hy).
  by apply/ndFP; rewrite -num_le /=. }
set s := ereal_sup _.
have Ps : (0%:E <= s)%E; [by apply: ereal_sup_ubound; exists 0%:nng|].
rewrite maxEle Ps.
apply/le_anti/andP; split; last first.
{ apply: lb_ereal_inf => _ [_ [x /= Hx <-] <-].
  apply: ub_ereal_sup => _ [x' /= Hx' <-].
  rewrite leEereal /= num_le ltW //.
  by apply: (@non_decr_recipr f); move: Hx; apply: lt_le_trans. }
rewrite leNgt; apply/negP => Hlb.
have [m /andP[Hml Hmb]] := ex_el_between Hlb.
have [m' Hm'] : exists m' : {nonneg R}, m'%:nngnum%:E = m.
{ have Hm_pos : (0%:E <= m)%E.
  { by move: Hml => /ltW; apply: le_trans. }
  move: Hmb Hm_pos.
  case: m {Hml} => [m'' | | //] Hmb /= Hm_pos.
  { by exists (NngNum Hm_pos). }
  by exfalso; move: Hmb; apply/negP; rewrite -leNgt leey. }
case: (leP y%:num (f m')) => Hyfm'.
{ move: Hmb; apply/negP; rewrite -leNgt -Hm'.
  by apply: ereal_inf_lbound; exists m'%:num%:E%:sgn => //; exists m'. }
move: Hml; apply/negP; rewrite -leNgt.
by apply: ereal_sup_ubound; exists m'.
Qed.

Lemma Rbar_inf_split T' (B A : set T') (f : T' -> \bar R) :
  ereal_inf [set f x | x in A]
  = Order.min
      (ereal_inf [set f x | x in A `&` B]) (ereal_inf [set f x | x in A `\` B]).
Proof.
rewrite -!Rbar_dioidE -complete_dioid.set_addU.
by rewrite -image_setU setDE -setIUr setUCr setIT.
Qed.

Lemma Rbar_inf_split_range y y' x z (f : {nonneg R} -> \bar R) :
  x <= y -> y < y' -> y' <= z ->
  ereal_inf [set f u | u in [set u | x < u%:nngnum < z]]
  = Order.min
      (ereal_inf [set f u | u in [set u | x < u%:nngnum < y']])
      (ereal_inf [set f u | u in [set u | y < u%:nngnum < z]]).
Proof.
move=> Hxy Hyy' Hy'z.
rewrite (Rbar_inf_split [set u | u%:nngnum < y']).
rewrite (Rbar_inf_split [set u | u%:nngnum <= y]).
set i_xy := ereal_inf _.
set i_yy' := ereal_inf _.
set i_y'z := ereal_inf _.
rewrite -(minxx i_yy') minA -(minA _ _ i_y'z).
apply: f_equal2.
{ rewrite -Rbar_inf_split; apply: f_equal; rewrite predeqP => e; split.
  { by move=> [e' [/= /andP[He' _] H'e'] <-]; exists e' => //; rewrite He'. }
  move=> [e' /= /andP[He' H'e'] <-]; exists e' => //; split=> //=.
  by rewrite He' /=; apply: lt_le_trans Hy'z. }
rewrite (Rbar_inf_split [set u | u%:nngnum < y']); apply: f_equal2.
{ apply: f_equal; rewrite predeqP => e; split.
  { move=> [e' [[/= /andP[He' H'e'] He'y'] /negP He'y] <-].
    by exists e' => //; split=> //=; rewrite ?ltW// ltNge He'y. }
  move=> [e' [/= /andP[Hye' He'z] He'y'] <-]; exists e' => //.
  split; [split=> //=|by apply/negP; rewrite -ltNge].
  by rewrite He'z andbT; apply: le_lt_trans Hye'. }
apply: f_equal; rewrite predeqP => e; split.
{ move=> [e' [/= /andP[Hxe' He'z] He'y'] <-]; exists e' => //; split=> //=.
  by move: He'y' => /negP; rewrite He'z andbT -leNgt; apply: lt_le_trans. }
move=> [e' [/= /andP[Hye' He'z] He'y'] <-]; exists e' => //; split=> //=.
by rewrite He'z andbT; apply: le_lt_trans Hye'.
Qed.

Lemma Fup_min_conv_0_r (f : Fup) t : F_min_conv f \0 t = f 0%:nng.
Proof.
apply/le_anti/andP; split.
{ apply: ereal_inf_lbound; exists (0%:nng, t).
  { by rewrite /= add0r. }
  by rewrite dadde0. }
apply: lb_ereal_inf => _ [[u v] /= Huv <-]; rewrite dadde0.
by apply/ndFP; rewrite -num_le /=.
Qed.

End FMin.
Arguments Build_Fplus {R} _ _.
Arguments Build_Fup {R} _ _.

(* TODO: translate below properties to new definitions with Mathcomp Analysis

Fact Fplus_top : @top Fplus_completeDioid = mk_Fplus (fun=> 0%nnRbar).
Proof.
rewrite /top /=.
apply val_inj => /=.
apply boolp.functional_extensionality_dep=> x.
rewrite /= /set_add /=  /Fplus_Sub.
apply f_equal2 => // {x}; apply f_equal.
apply boolp.functional_extensionality_dep=> x.
apply val_inj; rewrite /= /set_add /= /F_glb /Rbar_glb.
case Rbar_ex_glb => [l [Hl Hl']] /=.
apply Rbar_le_antisym.
{ apply Hl.
  exists (fun=> 0%nnRbar); split=> [//|]; split=>[|//] .
  apply /leFP => t; apply Rbar_le_refl. }
by apply Hl' => _ [f [-> [Hf _]]]; move: Hf => /leFP.
Qed.

(* Lemma 2 *)
Lemma F_min_conv_le_l (f g : F) :
  (g 0%nnR <= 0)%Rbar -> (f * g)%F <=1 f.
Proof.
move=> H => t.
apply (Rbar_le_trans _ (Rbar_plus (f t) (g 0%nnR))).
{
  rewrite /F_min_conv /Rbar_glb.
  case Rbar_ex_glb => l [Hl _] /=; apply Hl.
  exists t, 0%nnR; split => //=; rewrite /Rbar_plus /=; apply f_equal.
  by rewrite Rplus_0_r.
}
rewrite -{2}(Rbar_plus_0_l (f t)). rewrite Rbar_plus_comm.
apply Rbar_plus_le_compat=> //=; apply Rbar_le_refl.
Qed.

Lemma F_min_conv_le_r (f g : F) :
  (f 0%nnR <= 0)%Rbar -> (f * g)%F <=1 g.
Proof.
by move=> H => t; rewrite F_min_conv_comm; apply F_min_conv_le_l.
Qed.

Lemma F_min_conv_le_F_min (f g : F) :
  (f 0%nnR = 0)%Rbar -> (g 0%nnR = 0)%Rbar -> (f * g)%F <=1 F_min f g.
Proof.
move => Hf Hg t.
apply (Rbar_le_trans _
            (Rbar_min (Rbar_plus (f 0%nnR) (g t) )(Rbar_plus (f t) (g 0%nnR)))).
{ apply Rbar_min_case.
  { rewrite Hf Rbar_plus_0_l; apply F_min_conv_le_r.
    rewrite Hf; apply Rbar_le_refl. }
  rewrite Hg Rbar_plus_comm Rbar_plus_0_l F_min_conv_comm.
  apply F_min_conv_le_r; rewrite Hg; apply Rbar_le_refl.
}
rewrite Hf Rbar_plus_0_l Hg Rbar_plus_comm Rbar_plus_0_l /F_min Rbar_min_comm.
apply Rbar_le_refl.
Qed.

Lemma Fup_min_conv_0_l (f : Fup) t : ((fun _ => 0%nnR) * f)%F t = f 0%nnR.
Proof. by rewrite FDioid.F_min_conv_comm; apply Fup_min_conv_0_r. Qed.

(* TODO: lemma about addition of constant (p. 42) *)

(* TODO: closed for F_glb? *)

Lemma F0_min_closed (f g : F0) :
  id_Fplus (add Fplus_semiRing (f : Fplus) (g : Fplus)) \is a start0Function.
Proof.
apply /eqP; case f => F' HF'; case g => G' HG'.
move: (eqP HF') => /= HF ; move: (eqP HG') => /= HG.
apply nnRbar_inj => /=; rewrite /Fplus_Sub /F_min.
by apply Rbar_min_case; rewrite /Fplus_val ;[rewrite HF | rewrite HG].
Qed.

Lemma F0_min_conv_closed (f g : F0) :
  id_Fplus (mul Fplus_semiRing (f : Fplus) (g : Fplus)) \is a start0Function.
apply /eqP; case f => F' HF'; case g => G' HG'.
move: (eqP HF') => /= HF ; move: (eqP HG') => /= HG.
apply nnRbar_inj => /=; rewrite /Fplus_Sub /F_min_conv.
rewrite /Rbar_glb; case Rbar_ex_glb => l [Hl Hl'] /=; apply Rbar_le_antisym.
{ apply Hl; exists 0%nnR, 0%nnR; split.
  - by rewrite /Rbar_plus /=; apply f_equal; rewrite Rplus_0_l.
  - by rewrite /Fplus_val /nnRbar_val HF HG /= Rbar_plus_0_l.
}
apply Hl' => y [u [v [_ ->]]]; rewrite -(Rbar_plus_0_l 0%Re).
apply Rbar_plus_le_compat; apply nnRbar_pos.
Qed.

Lemma F0up_min_closed (f g : F0up) :
  id_Fplus (Fup_val (add Fup_semiRing (f : Fup) (g : Fup))) \is a start0Function.
Proof.
apply /eqP; case f => F' HF'; case g => G' HG'.
move: (eqP HF') => /= HF ; move: (eqP HG') => /= HG.
apply nnRbar_inj => /=; rewrite /Fplus_Sub /F_min.
by apply Rbar_min_case; rewrite /Fplus_val ;[rewrite HF | rewrite HG].
Qed.

Lemma F0up_min_conv_closed (f g : F0up) :
  id_Fplus (Fup_val (mul Fup_semiRing (f : Fup) (g : Fup))) \is a start0Function.
apply /eqP; case f => F' HF'; case g => G' HG'.
move: (eqP HF') => /= HF ; move: (eqP HG') => /= HG.
apply nnRbar_inj => /=; rewrite /Fplus_Sub /F_min_conv.
rewrite /Rbar_glb; case Rbar_ex_glb => l [Hl Hl'] /=; apply Rbar_le_antisym.
{ apply Hl; exists 0%nnR, 0%nnR; split.
  - by rewrite /Rbar_plus /= ; apply f_equal; rewrite Rplus_0_l.
  - by rewrite /Fplus_val /nnRbar_val HF HG /= Rbar_plus_0_l.
}
apply Hl' => y [u [v [_ ->]]]; rewrite -(Rbar_plus_0_l 0%Re).
apply Rbar_plus_le_compat; apply nnRbar_pos.
Qed.

(* Some Properties on kleene and functions*)
Require Import dioid.kleene.

Lemma Fplus_prop_minfty (A : Fplus) : forall t, (Fplus_val A) t <> -oo%Rbar.
Proof.
move=> t.
apply /eqP; rewrite eq_sym; apply /eqP /Rbar_lt_not_eq.
by apply (Rbar_lt_le_trans _ 0); [|apply nnRbar_pos].
Qed.

(* Some Notation for min on F *)
Notation "\min_ ( i <- r | P ) F" :=
  (\big[F_min/F_zero]_(i <- r | P%B) F%F) : F_scope.
Notation "\min_ ( i <- r ) F" :=
  (\big[F_min/F_zero]_(i <- r) F%F) : F_scope.
Notation "\min_ ( m <= i < n | P ) F" :=
  (\big[F_min/F_zero]_(m <= i < n | P%B) F%F) : F_scope.
Notation "\min_ ( m <= i < n ) F" :=
  (\big[F_min/F_zero]_(m <= i < n) F%F) : F_scope.
Notation "\min_ ( i | P ) F" :=
  (\big[F_min/F_zero]_(i | P%B) F%F) : F_scope.
Notation "\min_ i F" :=
  (\big[F_min/F_zero]_i F%F) : F_scope.
Notation "\min_ ( i : t | P ) F" :=
  (\big[F_min/F_zero]_(i : t | P%B) F%F) (only parsing) : F_scope.
Notation "\min_ ( i : t ) F" :=
  (\big[F_min/F_zero]_(i : t) F%F) (only parsing) : F_scope.
Notation "\min_ ( i < n | P ) F" :=
  (\big[F_min/F_zero]_(i < n | P%B) F%F) : F_scope.
Notation "\min_ ( i < n ) F" :=
  (\big[F_min/F_zero]_(i < n) F%F) : F_scope.
Notation "\min_ ( i 'in' A | P ) F" :=
  (\big[F_min/F_zero]_(i in A | P%B) F%F) : F_scope.
Notation "\min_ ( i 'in' A ) F" :=
  (\big[F_min/F_zero]_(i in A) F%F) : F_scope.

From mathcomp Require Import fintype.

Lemma bigmin_F_Rbar (I : finType) F (P : pred I) t :
  (\min_(i | P i) F i)%F t = (\min_(i | P i) F i t)%Rbar.
Proof.
pose K := fun (x : nnR -> Rbar) (y : Rbar) => x t = y.
by apply (big_rec2 K) => // i y _ _ <-.
Qed.

Lemma bigmin_plus_distr_r n F (P : pred 'I_n) x :
  ((\min_(i | P i) F i) + x = \min_(i | P i) (F i + x))%Rbar.
Proof.
pose K := fun (y z : Rbar) => (y + x = z)%Rbar.
apply (big_rec2 K) => [|i y z Hi <-]; [by rewrite /K; case x|].
by rewrite /K Rbar_min_left_distributive.
Qed.

 *)
