(* Copyright : Lucien Rakotomalala, Pierre Roux and Marc Boyer. *)

(** * Theorem about SP server **)

(******************************************************************************)
(* This files contains the results about Static priority servers and          *)
(* complements on backlogged. The major result is the theorem about           *)
(* server and static priority policy from DNC2018 (Th. 7.6).                  *)
(*                                                                            *)
(* preemptive_SP n S prior == defines a static priority n.-server S with fixe *)
(*                            priority prior (def.7.8 from DNC2018).          *)
(******************************************************************************)

From mathcomp Require Import ssreflect ssrfun ssrbool eqtype order ssrnat.
From mathcomp Require Import choice seq fintype finfun bigop ssralg ssrnum rat interval.
From mathcomp Require Import boolp classical_sets signed reals ereal.
From mathcomp.dioid Require Import dioid complete_lattice complete_dioid.
Require Import mathcomp_complements RminStruct.
Require Import cumulative_curves arrival_curves servers services backlog_itv.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope classical_set_scope.
Local Open Scope ring_scope.
Local Open Scope order_scope.

Import Order.Theory GRing.Theory Num.Theory DualAddTheory.

Section StaticPriorities.

Context {R : realType}.

Lemma sum_flow_cc_equiv_cond n A D (S : @nserver R n) (P : pred 'I_n) :
  S A D ->
  forall t, (forall i, P i -> A i t = D i t) <->
            (\sum_(i | P i) A i) t = (\sum_(i | P i) D i) t.
Proof.
move => HS t; split.
{ move=> H; rewrite !flow_cc_sumE; exact: eq_bigr. }
move=> H i Pi.
apply/le_anti; move: (nserver_le HS i) => /lefP /(_ t) ->; rewrite andbT.
rewrite leNgt; apply/negP => HAD.
move: H => /eqP; apply/negP; rewrite neq_lt; apply/orP; right.
rewrite !flow_cc_sumE.
under [X in (X < _)%E]eq_bigr => ? _ do rewrite -fin_flow_ccE.
under [X in (_ < X)%E]eq_bigr => ? _ do rewrite -fin_flow_ccE.
rewrite (bigD1 i) //= [in X in (_ < X)%E](bigD1 i) //=.
apply: lte_le_dD; rewrite ?dsumEFin => //; [by rewrite !fin_flow_ccE|].
rewrite lee_fin.
apply: ler_sum => j _.
rewrite -lee_fin !fin_flow_ccE.
exact/lefP/(nserver_le HS j).
Qed.

Lemma sum_flow_cc_equiv_at_t n A D (S : @nserver R n) :
  S A D ->
  forall t, (forall i, A i t = D i t) <-> (\sum_i A i) t = (\sum_i D i) t.
Proof.
move => HS t; split.
{ move=> H; rewrite !flow_cc_sumE; exact: eq_bigr. }
move=> H i.
exact: (proj2 (sum_flow_cc_equiv_cond predT HS t)).
Qed.

Corollary sum_flow_cc_equiv n A D (S : @nserver R n) :
  S A D -> (forall i, A i = D i) <-> \sum_i A i = \sum_i D i.
Proof.
move=> HS; split=> H.
{ apply/val_inj/val_inj/val_inj/funext => t.
  rewrite -(sum_flow_cc_equiv_at_t HS) => i.
  by rewrite H. }
move=> i.
apply/val_inj/val_inj/val_inj/funext => t /=; move: i.
by rewrite (sum_flow_cc_equiv_at_t HS) H.
Qed.

(* Lemma 25 page 175 *)
Lemma backlog_itv_eqv_cond n (S : @nserver R (n.+1)) A D I
      (P : pred 'I_n.+1):
  S A D ->
  (forall u : {nonneg R}, u \in I -> exists i, P i /\ ((D i) u < (A i) u)%E)
  <-> backlog_itv (\sum_(i | P i) A i) (\sum_(i | P i) D i) I.
Proof.
move=> Hs; split.
{ move=> H u Hu.
  move: (H _ Hu) => [i [Hi HAD]].
  rewrite lt_def; apply/andP; split.
  { apply/negP => /eqP /(proj2 (sum_flow_cc_equiv_cond P Hs u)) /(_ _ Hi) /eqP.
    by apply/negP; move: HAD; rewrite lt_def => /andP[]. }
  rewrite !flow_cc_sumE; apply: lee_dsum => j _.
  exact/lefP/(nserver_le Hs). }
move=> H u Hu.
move: (H _ Hu); rewrite lt_def => /andP[H_u _].
rewrite not_existsP => Hf.
move: H_u => /eqP; apply.
rewrite -(sum_flow_cc_equiv_cond P Hs) => i Pi.
apply/le_anti; move: (nserver_le Hs i) => /lefP /(_ u) ->; rewrite andbT.
move: (Hf i) => /asboolPn.
rewrite asbool_and negb_and => /orP [] /asboolPn // /negP.
by rewrite -leNgt.
Qed.

(* Lemma 25 page 175 without condition *)
Lemma backlog_itv_eqv n (S : (n.+1).-server) A D I :
  S A D ->
  (forall u : {nonneg R}, u \in I -> exists i, ((D i) u < (A i) u)%E)
  <-> backlog_itv (\sum_i A i) (\sum_i D i) I.
Proof.
move=> HS; split.
{ move=> H.
  rewrite -(backlog_itv_eqv_cond _ _ HS) => u Hu.
  move: (H _ Hu) => [i HAD].
  by exists i. }
move=> H u Hu.
move: H; rewrite -(backlog_itv_eqv_cond _ _ HS) => /(_ _ Hu)[i [_ H]].
by exists i.
Qed.

Lemma backlog_itv_aggregate  n (S : @nserver R (n.+1)) A D I :
  S A D ->
  forall i, backlog_itv (A i) (D i) I ->
       backlog_itv (\sum_i A i) (\sum_i D i) I.
Proof.
move=> Hs i H.
rewrite -(backlog_itv_eqv I Hs) => u Hu.
by exists i; apply H.
Qed.

(* Definition 7.8 p169*)
Definition preemptive_SP n (S : n.+1.-server) (prior : 'I_n.+1 -> nat) :=
  forall A D, S A D ->
  forall i, forall s t : {nonneg R}, s%:nngnum <= t%:nngnum ->
    backlog_itv
      (\sum_(j | (prior j < prior i)%nat) A j)
      (\sum_(j | (prior j < prior i)%nat) D j)
      `[s, t] ->
    (D i) s = (D i) t.

(* Theorem 7.6 p170 *)
Theorem SP_residual_service_curve n (S : n.+1.-server) (beta : flow_cc) prior :
  injective prior ->
  preemptive_SP S prior ->
  strict_min_service (aggregate_server S) beta ->
  forall alpha : Fplus^n.+1,
  forall i,
    strict_min_service
      (residual_server_constr S i [ffun i => alpha i : F])
      (non_decr_closure
         (fun t => Order.max 0%E
            ((beta \- (\sum_(j | (prior j < prior i)%nat) alpha j)%R)%dE t))).
Proof.
move=> inj_prior SPS beta_aggr alpha i.
apply/strict_min_service_non_decr_closure/strict_min_service_max0.
move=> _ _ [A [D [SAD [-> [-> alpha_j]]]]] s t s_le_t st_BL_i.
have /ltac:(rewrite le_eqVlt)/orP[|s_lt_t] := s_le_t.
{ rewrite num_eq => /eqP->.
  rewrite subrr dsubee ?flow_cc_fin// dsubre_le0 ?flow_cc_fin//.
  have -> : insubd 0%:nng 0%R = 0%:nng :> {nonneg R}.
  { exact/val_inj/insubdK/lexx. }
  rewrite flow_cc0 Fplus_sumE; apply: dsume_ge0 => k _; exact: Fplus_ge0. }
pose A_hp_i := (\sum_(j | (prior j < prior i)%nat) A j).
pose D_hp_i := (\sum_(j | (prior j < prior i)%nat) D j).
set alpha_hp_i := (\sum_(j | (prior j < prior i)%nat) alpha j).
pose A_he_i := (\sum_(j | (prior j <= prior i)%nat) A j).
pose D_he_i := (\sum_(j | (prior j <= prior i)%nat) D j).
pose D_lp_i := (\sum_(j | ~~(prior j <= prior i)%nat) D j).
pose p := start A_hp_i D_hp_i s.
pose v := !!(p%:nngnum + (insubd 0%:nng (t%:nngnum - s%:nngnum))%:nngnum)%:nng.
have p_le_s : p <= s by exact: start_le.
have v_le_t : v <= t.
{ rewrite -num_le/= insubdK; [|by rewrite /in_mem/= subr_ge0 ltW].
  by rewrite addrCA gerDl subr_le0. }
have p_lt_v : p < v.
{ by rewrite -num_lt /= ltrDl insubdK /in_mem/= subr_gte0 ?ltW. }
have vmp_ge0 : 0 <= v%:nngnum - p%:nngnum by rewrite subr_ge0 ltW.
have ps_BL_hp_i : backlog_itv A_hp_i D_hp_i `]p, s].
{ exact: start_t_is_a_backlog_itv (aggregate_server_prop_cond SAD _). }
have Di_uv u : u \in `]p, v] ->
   (beta (insubd 0%:nng (v%:num - u%:num))%R
    <= D i v - D i u + alpha_hp_i (insubd 0%:nng (v%:num - p%:num))%R)%dE.
{ move=> u_in_pv.
  have ps_BL_he_i : backlog_itv A_he_i D_he_i `]p, s].
  { apply: (backlog_itv_condW SAD) ps_BL_hp_i => ?; exact: ltnW. }
  have st_BL_he_i : backlog_itv A_he_i D_he_i `]s, t].
  { exact: (backlog_itv_pred1 SAD (leqnn _)). }
  have pt_BL_he_i : backlog_itv A_he_i D_he_i `]p, t].
  { move=> t' t'_in_pt.
    have [t'_le_s|s_lt_t'] := leP t' s.
    - by apply: ps_BL_he_i; rewrite in_itv/= (itvP t'_in_pt).
    - by apply: st_BL_he_i; rewrite in_itv/= s_lt_t' (itvP t'_in_pt). }
  have uv_BL_he_i : backlog_itv A_he_i D_he_i `[u, v].
  { apply: backlog_itv_sub pt_BL_he_i.
    by rewrite subitvE /Order.le/= (itvP u_in_pv). }
  have uv_BL : backlog_itv (\sum_i A i) (\sum_i D i) `]u, v].
  { apply: (@backlog_itv_sub _ `]p, t]).
    - by rewrite subitvE /Order.le/= (itvP u_in_pv).
    - by apply: (backlog_itv_condW SAD) pt_BL_he_i. }
  have D_beta_le_D :
    (D_hp_i u + D i u + D_lp_i u + beta (insubd 0%:nng (v%:num - u%:num))%R
     <= D_hp_i v + D i v + D_lp_i v)%dE.
  { rewrite -!flow_cc_plusE.
    have -> : (D_hp_i + D i + D_lp_i = \sum_j D j).
    { rewrite (bigID (fun j => prior j <= prior i)%N)/= -/D_lp_i.
      rewrite (bigD1 i)//= [D i + _]addrC.
      by under eq_bigl => j do rewrite andbC -(inj_eq inj_prior) -ltn_neqAle. }
    rewrite daddeC -lee_dsubr_addr; [|by rewrite -fin_flow_cc_sumE].
    apply: beta_aggr uv_BL.
    - exact: aggregate_server_prop.
    - by rewrite num_le (itvP u_in_pv). }
  have {}D_beta_le_D :
    (D_hp_i u + D i u + beta (insubd 0%:nng (v%:num - u%:num))%R
     <= D_hp_i v + D i v)%dE.
  { rewrite -(@lee_dD2lE _ (D_lp_i v)); [|by rewrite -fin_flow_cc_sumE].
    rewrite !(daddeC (D_lp_i v)) daddeAC.
    have D_lp_i_uv : D_lp_i u = D_lp_i v.
    { rewrite !flow_cc_sumE.
      apply: eq_bigr => j.
      rewrite -ltnNge => i_lt_j.
      apply: (SPS _ _ SAD).
      - by rewrite num_le (itvP u_in_pv).
      - apply: (backlog_itv_condW SAD) uv_BL_he_i => k k_le_i.
        exact: leq_trans i_lt_j. }
    by rewrite -[in X in (X <= _)%E]D_lp_i_uv. }
  have {}D_beta_le_D :
    (D_hp_i u + D i u + beta (insubd 0%:nng (v%:num - u%:num))%R
     <= A_hp_i v + D i v)%dE.
  { apply: (le_trans D_beta_le_D).
    apply: lee_dD2r.
    rewrite !flow_cc_sumE.
    apply: lee_dsum => j _; exact/lefP/(nserver_le SAD). }
  have {}D_beta_le_D :
    (D i u - D i v + beta (insubd 0%:nng (v%:num - u%:num))%R
     <= A_hp_i v - A_hp_i p)%dE.
  { rewrite daddeAC -lee_dsubr_addr; [|by rewrite fin_numN -fin_flow_ccE].
    rewrite oppeK daddeAC -lee_dsubl_addr; [|by rewrite fin_numN -fin_flow_ccE].
    apply: le_trans D_beta_le_D.
    rewrite oppeK addrC -addrA.
    apply: lee_dD2r.
    rewrite start_eq.
    by apply: ndFP; rewrite (itvP u_in_pv). }
  rewrite daddeC -lee_dsubl_addr; [|by rewrite -!fin_flow_ccE -dEFinB].
  rewrite daddeC doppeD ?fin_num_adde_defr ?flow_cc_fin//.
  rewrite oppeK [(_ + D i u)%dE]daddeC.
  apply: (le_trans D_beta_le_D).
  set vmp := insubd 0%:nng (v%:nngnum - p%:nngnum).
  have -> : v = (p%:nngnum + vmp%:nngnum)%:nng.
  { by apply/val_inj; rewrite /= [in RHS]insubdK// addrAC subrr add0r. }
  rewrite -!fin_flow_cc_sumE -dEFinB -sumrN -big_split/= -dsumEFin Fplus_sumE.
  apply: lee_dsum => j _.
  rewrite dEFinB !fin_flow_ccE; move: (alpha_j j); rewrite ffunE; exact. }
have Di_vs :
  (beta (insubd 0%:nng (v%:num - p%:num))%R
   <= D i v - D i s + alpha_hp_i (insubd 0%:nng (v%:num - p%:num))%R)%dE.
{ pose m := !!(if p < s then insubd 0%:nng (v%:nngnum - s%:nngnum) else 0%:nng).
  have m_lt_vmp : m%:num < (insubd 0%:nng (v%:num - p%:num))%:num.
  { rewrite insubdK// /m; case: ifP => [p_lt_s|]; [|by rewrite subr_gt0].
    have [s_le_v|v_lt_s] := leP s%:nngnum v%:nngnum.
    - rewrite insubdK; [|by rewrite /in_mem/= subr_ge0].
      by rewrite ltrD2l ltrNl opprK.
    - rewrite /insubd insubF ?subr_gt0//.
      by apply/negbTE; rewrite -ltNge subr_lt0. }
  apply: (left_continuous_atP (flow_cc_left_cont _ _) m_lt_vmp) => y.
  rewrite -!num_lt insubdK// => /andP[m_lt_y y_lt_vmp].
  have vmy_ge0 : 0 <= v%:nngnum - y%:nngnum.
  { rewrite subr_ge0.
    by apply/ltW/(lt_le_trans y_lt_vmp); rewrite gerDl oppr_le0. }
  have p_lt_vmy : p < insubd 0%:nng (v%:nngnum - y%:nngnum).
  { by rewrite -num_lt insubdK// ltrBrDl -ltrBrDr. }
  have vmy_in_pv : insubd 0%:nng (v%:nngnum - y%:nngnum) \in `]p, v].
  { by rewrite in_itv/= p_lt_vmy/= -num_le insubdK// gerDl oppr_le0. }
  have -> : y = insubd 0%:nng (v%:nngnum
    - (insubd 0%:nng (v%:nngnum - y%:nngnum))%:nngnum).
  { by apply/val_inj; rewrite insubdK ?insubdK// ?subKr /in_mem/=. }
  apply: (le_trans (Di_uv _ vmy_in_pv)).
  apply/lee_dD2r/lee_dD2l; rewrite leeN2.
  have /ltac:(rewrite le_eqVlt)/orP[/eqP <-|p_lt_s] := p_le_s.
  { exact/ndFP/ltW. }
  have vmy_le_s : (insubd 0%:nng (v%:nngnum - y%:nngnum))%:nngnum <= s%:nngnum.
  { rewrite insubdK// lerBlDr -lerBlDl ltW//.
    apply: le_lt_trans m_lt_y; rewrite /m p_lt_s.
    have [s_le_v|v_lt_s] := leP s%:nngnum v%:nngnum.
    - by rewrite insubdK// /in_mem/= subr_ge0.
    - rewrite /insubd insubF; [by rewrite subr_le0 ltW|].
      by apply/negbTE; rewrite -ltNge subr_lt0. }
  have vmys_BL_hp_i : backlog_itv A_hp_i D_hp_i
    `[insubd 0%:nng (v%:nngnum - y%:nngnum), s].
  { apply:  backlog_itv_sub ps_BL_hp_i.
    by rewrite subitvE /Order.le/= p_lt_vmy/=. }
  by rewrite (SPS _ _ SAD _ _ _ vmy_le_s). }
have -> : insubd 0%:nng (t%:nngnum - s%:nngnum)
          = insubd 0%:nng (v%:nngnum - p%:nngnum).
{ apply/val_inj.
  rewrite [RHS]insubdK; [|by rewrite /in_mem/= subr_ge0 ltW].
  by rewrite addrAC subrr add0r/= insubdK; [|rewrite /in_mem/= subr_ge0 ltW]. }
rewrite lee_dsubel_addr ?flow_cc_fin//.
exact/(le_trans Di_vs)/lee_dD2r/lee_dD2r/ndFP.
Qed.

End StaticPriorities.
